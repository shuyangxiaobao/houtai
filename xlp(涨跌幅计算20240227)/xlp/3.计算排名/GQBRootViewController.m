 //
//  GQBRootViewController.m
//  xlp
//
//  Created by xiaobao on 2022/4/14.
//

#import "GQBRootViewController.h"
#import "StockModel.h""
@interface GQBRootViewController ()

@end

@implementation GQBRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
//    [self tiaojiandanName:@"南极电商" tiaojiandan_price:@"3.94" current_price:@"4.72"];
    [self demo1];
    [self chicang];
    [self tiaojiandan];
    [self demo3];
}


#pragma mark - 下跌75点候选
-(void)demo1{
    NSMutableArray *list = [NSMutableArray array];
    [list addObject:[self maxValue:@"24.16" current:4.67 name:@"南极电商⭐"]];
    [list addObject:[self maxValue:@"85.09" current:18.79 name:@"新经典⭐"]];
    [list addObject:[self maxValue:@"75.50" current:18.25 name:@"华兰生物⭐⭐_002007"]];
    [list addObject:[self maxValue:@"65.15" current:15.80 name:@"艾迪精密"]];
    [list addObject:[self maxValue:@"66.13" current:16.79 name:@"信维通信_创业板"]];
    [list addObject:[self maxValue:@"15.00" current:3.87 name:@"世纪华通"]];
    [list addObject:[self maxValue:@"41.97" current:10.87 name:@"中贝通信"]];
    [list addObject:[self maxValue:@"49.24" current:13.29 name:@"吉宏股份⭐"]];
    [list addObject:[self maxValue:@"32.43" current:8.78 name:@"双鹭药业⭐"]];
    [list addObject:[self maxValue:@"24.56" current:6.69 name:@"天下秀"]];
    [list addObject:[self maxValue:@"41.80" current:12.05 name:@"完美世界⭐⭐_002624"]];
    [list addObject:[self maxValue:@"35.02" current:10.15 name:@"中顺洁柔"]];
    [list addObject:[self maxValue:@"57.15" current:16.79 name:@"新宝股份⭐⭐_002705"]];
    [list addObject:[self maxValue:@"49.25" current:15.05 name:@"三一重工"]];
    [list addObject:[self maxValue:@"68.95" current:21.13 name:@"华正新材⭐"]];
    [list addObject:[self maxValue:@"48.69" current:14.92 name:@"拉卡拉⭐"]];
    [list addObject:[self maxValue:@"49.71" current:15.25 name:@"三七互娱⭐⭐_002555"]];
    [list addObject:[self maxValue:@"43.33" current:13.63 name:@"圣达生物⭐"]];
    [list addObject:[self maxValue:@"33.69" current:10.63 name:@"有友食品⭐"]];
    [list addObject:[self maxValue:@"44.38" current:14.03 name:@"新城市⭐"]];
    [list addObject:[self maxValue:@"29.17" current:9.24 name:@"福龙马"]];
    [list addObject:[self maxValue:@"33.09" current:10.49 name:@"煌上煌"]];
    [list addObject:[self maxValue:@"39.05" current:12.40 name:@"奥美医疗⭐"]];
    [list addObject:[self maxValue:@"30.78" current:9.83 name:@"深物业A⭐"]];
    [list addObject:[self maxValue:@"63.56" current:20.36 name:@"电魂网络⭐"]];
    [list addObject:[self maxValue:@"40.93" current:13.20 name:@"西麦食品⭐"]];
    [list addObject:[self maxValue:@"14.88" current:4.83 name:@"领益智造"]];
    [list addObject:[self maxValue:@"139.7" current:47.20 name:@"浙江鼎力⭐"]];
    [list addObject:[self maxValue:@"78.64" current:26.77 name:@"王府井⭐"]];
    [list addObject:[self maxValue:@"25.65" current:8.74 name:@"保龄宝⭐"]];
    [list addObject:[self maxValue:@"17.47" current:6.07 name:@"东华软件⭐"]];
    [list addObject:[self maxValue:@"43.98" current:15.31 name:@"九阳股份⭐"]];
    [list addObject:[self maxValue:@"34.31" current:11.96 name:@"亿帆医药"]];
    [list addObject:[self maxValue:@"45.20" current:15.92 name:@"姚记科技⭐"]];
    [list addObject:[self maxValue:@"30.94" current:10.93 name:@"航天信息"]];
    [list addObject:[self maxValue:@"33.69" current:11.99 name:@"桃李面包"]];
    [list addObject:[self maxValue:@"38.57" current:13.74 name:@"淳中科技⭐"]];
    [list addObject:[self maxValue:@"120.03" current:43.04 name:@"江山欧派⭐"]];
    [list addObject:[self maxValue:@"23.40" current:8.39 name:@"大亚圣象⭐"]];
    [list addObject:[self maxValue:@"38.24" current:14.15 name:@"宁水集团"]];
    [list addObject:[self maxValue:@"27.27" current:10.11 name:@"武商集团⭐"]];
    [list addObject:[self maxValue:@"108.98" current:40.97 name:@"信捷电气⭐"]];
    [list addObject:[self maxValue:@"13.33" current:5.01 name:@"红旗连锁⭐"]];
    [list addObject:[self maxValue:@"36.46" current:13.76 name:@"华阳国际⭐"]];
    [list addObject:[self maxValue:@"38.47" current:14.73 name:@"招商积余⭐"]];
    [list addObject:[self maxValue:@"196.1" current:65.65 name:@"深南电路⭐⭐"]];
    [list addObject:[self maxValue:@"164.1" current:63.70 name:@"小熊电器⭐⭐_002959"]];
    [list addObject:[self maxValue:@"61.43" current:24.09 name:@"双汇发展⭐"]];
    [list addObject:[self maxValue:@"35.40" current:14.22 name:@"生益科技⭐"]];
    [list addObject:[self maxValue:@"33.21" current:13.40 name:@"中国重汽"]];
    [list addObject:[self maxValue:@"76.25" current:31.07 name:@"山东药玻"]];
    [list addObject:[self maxValue:@"91.96" current:37.90 name:@"丸美股份⭐"]];
    [list addObject:[self maxValue:@"19.54" current:8.06 name:@"力合科创"]];
    [list addObject:[self maxValue:@"27.32" current:11.54 name:@"日丰股份"]];
    [list addObject:[self maxValue:@"49.37" current:21.49 name:@"星网锐捷⭐⭐"]];
    [list addObject:[self maxValue:@"28.10" current:12.51 name:@"沪电股份⭐"]];
    [list addObject:[self maxValue:@"35.22" current:16.12 name:@"网达软件"]];
    [list addObject:[self maxValue:@"70.96" current:36.77 name:@"老百姓"]];

    
    [list sortUsingComparator:^NSComparisonResult(StockModel *  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        double result = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
        if(result > 0){
            return NSOrderedDescending;
        } else if (result < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        int index = i + 1;
        StockModel *model = list[i];
        NSLog(@"序号: %02d %@",index,model);
    }
}

#pragma mark - 当前持仓
-(void)chicang{
    NSLog(@"");
    NSLog(@"当前持仓---------------start");
    NSMutableArray *list = [NSMutableArray array];
    [list addObject:[self maxValue:@"63.79" current:32.07 name:@"格力电器"]];
    [list addObject:[self maxValue:@"34.31" current:9.51 name:@"诚志股份"]];
    [list addObject:[self maxValue:@"75.5" current:19.06 name:@"华兰生物⭐⭐_002007"]];
    [list addObject:[self maxValue:@"24.16" current:4.78 name:@"南极电商"]];
    [list addObject:[self maxValue:@"25.65" current:9.04 name:@"保龄宝"]];
    [list addObject:[self maxValue:@"15.00" current:3.99 name:@"世纪华通"]];
    [list addObject:[self maxValue:@"43.58" current:4.96 name:@"中公教育"]];
    [list addObject:[self maxValue:@"57.15" current:17.51 name:@"新宝股份"]];
    [list addObject:[self maxValue:@"49.24" current:14.06 name:@"吉宏股份"]];
    [list addObject:[self maxValue:@"196.1" current:78.75 name:@"深南电路⭐⭐"]];
    [list addObject:[self maxValue:@"2.699" current:0.857 name:@"中国互联网164906"]];
    [list addObject:[self maxValue:@"0.907" current:0.474 name:@"医药ETF"]];
    [list addObject:[self maxValue:@"2.650" current:0.960 name:@"中概互联网ETF"]];
    [list addObject:[self maxValue:@"49.25" current:15.98 name:@"三一重工"]];
    [list addObject:[self maxValue:@"24.56" current:8.47 name:@"天下秀"]];
    [list addObject:[self maxValue:@"26.35" current:9.24 name:@"通化东宝"]];
    [list addObject:[self maxValue:@"85.09" current:19.16 name:@"新经典⭐"]];
    [list addObject:[self maxValue:@"386.53" current:52.19 name:@"汇顶科技"]];
    [list addObject:[self maxValue:@"67.80" current:21.96 name:@"朗博科技"]];
    
    [list sortUsingComparator:^NSComparisonResult(StockModel *  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        double result = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
        if(result > 0){
            return NSOrderedDescending;
        } else if (result < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        int index = i + 1;
        StockModel *model = list[i];
        NSLog(@"序号: %02d %@",index,model);
    }
    NSLog(@"当前持仓---------------end");
    NSLog(@"");
 
}

#pragma mark - 再跌多少触发条件单

-(void)tiaojiandan:(NSString *)name
     tiaojiandan:(NSString *)tiaojiandan_price
         current:(double)current_price{
    [self tiaojiandan:name tiaojiandan_price:tiaojiandan_price current_price:current_price trigger:false];
}


-(void)tiaojiandan{
    NSLog(@"");
    NSLog(@"再跌多少触发条件单========================================start");
    [self tiaojiandan:@"诚志股份" tiaojiandan:@"6.320" current:9.24];
    [self tiaojiandan:@"南极电商" tiaojiandan:@"3.600" current:4.71];
    [self tiaojiandan:@"保龄宝" tiaojiandan:@"7.77000" current:8.53];
    [self tiaojiandan:@"世纪华通" tiaojiandan:@"2.780" current:3.87];
    [self tiaojiandan:@"深南电路" tiaojiandan:@"53.79" current:78.67];
    [self tiaojiandan:@"天下秀" tiaojiandan:@"4.2700" current:6.34];
    [self tiaojiandan:@"华兰生物" tiaojiandan:@"16.88" current:18.41];
    [self tiaojiandan:@"完美世界" tiaojiandan:@"10.45" current:12.10];
    [self tiaojiandan:@"新宝股份" tiaojiandan:@"14.28" current:16.97];
    [self tiaojiandan:@"小熊电器" tiaojiandan:@"41.02" current:64.29];
    [self tiaojiandan:@"三七互娱" tiaojiandan:@"12.42" current:15.47];
    [self tiaojiandan:@"吉宏股份" tiaojiandan_price:@"11.60" current_price:14.54 trigger:YES];

    NSLog(@"再跌多少触发条件单========================================end");
    NSLog(@"");
}


#pragma mark - 历史数据
-(void)demo3{
    NSMutableArray *list = [NSMutableArray array];
    
    // 三七互娱 002555 (2018.10.12  2019.1.3)    1个涨停   8.32月100%     5%以下下跌： -9.12% 市盈率：11.78/43.25(最大)
    // （-9.12%   10.74%  ⭐️⭐️⭐️⭐️⭐️）
    [list addObject:[self maxValue:@"25.74" ninePer:@"11.92" fivePer:@"8.55" current:7.77 time:@"82" name:@"三七互娱"]];
    
//     中牧股份 600195 (2016.2.29 2018.10.29)    1个涨停   9.5月100%     5%以下下跌： -27.55% 市盈率：12.89/57.53(最大) （底部与大盘同步）
    [list addObject:[self maxValue:@"16.39" ninePer:@"9.64" fivePer:@"7.95" current:5.76 time:@"976" name:@"中牧股份"]];
    
    // 帝欧家居 002798 (2017.5.23 2019.1.3)     x个涨停   3月100%     5%以下下跌： -23.52%  市盈率：16.61/155.14(最大)
    [list addObject:[self maxValue:@"97.88" ninePer:@"63.12" fivePer:@"48.93" current:37.42 time:@"590" name:@"帝欧家居"]];
    
    // 星网锐捷 002396 (2015.9.16 2018.10.16)    x个涨停   11月100%     5%以下下跌： -30.69x%   市盈率：15.98/104.65(最大)
    [list addObject:[self maxValue:@"45.13" ninePer:@"25.34" fivePer:@"20.07" current:13.91 time:@"1128" name:@"星网锐捷"]];

    // 掌阅科技 603533 (2019.1.31  2019.8.15)    3个涨停   8月100%     5%以下下跌： -25.86x% 市盈率：40.32/233.01(最大)
    [list addObject:[self maxValue:@"73.40" ninePer:@"24.84" fivePer:@"17.21" current:12.76 time:@"197" name:@"掌阅科技"]];

    // 欧菲光   002456 (2019.1.16 2019.7.22)   x个涨停   6月100%     5%以下下跌： -30.10%   市盈率：-19.25/59.9(最大)
    [list addObject:[self maxValue:@"26.05" ninePer:@"15.77" fivePer:@"10.83" current:7.57 time:@"122" name:@"欧菲光"]];

    // 沪电股份 002463 (2016.2.29 2018.6.26)    x个涨停   4月100%     5%以下下跌: -24.40%  市盈率：28.34/-315(最大)
    [list addObject:[self maxValue:@"11.73" ninePer:@"4.89" fivePer:@"3.73" current:2.82 time:@"854" name:@"沪电股份"]];

    // 实丰文化 002862(2018.10.11 2019.1.31)  1.4月100%     5%以下下跌： -16.63x%   市盈率：37.74/77.88
    [list addObject:[self maxValue:@"42.94" ninePer:@"24.72" fivePer:@"20.44" current:17.04 time:@"42" name:@"实丰文化"]];


    // 游族网络 002174 (2018.10.12 2019.8.15)    x个涨停   6月天100%     5%以下下跌： -24.36% 市盈率：12.04/98.08
    [list addObject:[self maxValue:@"53.02" ninePer:@"25" fivePer:@"17.08" current:12.92 time:@"308" name:@"游族网络"]];
    
    // 新国脉 600640 (2016.3.4 2018.10.18) 2017.6.1    x个涨停   10月100%     5%以下下跌： -49.31%  市盈率：29.61/241.99 (第1轮)
    [list addObject:[self maxValue:@"38.5" ninePer:@"21.41" fivePer:@"15.94" current:8.08 time:@"959" name:@"新国脉"]];

    // 宁波飞翔 002048 (2018.10.18  2019.8.9)   x个涨停  5月100%  5%以下下跌： -15.55%  市盈率：7.87/16.77
    [list addObject:[self maxValue:@"23.83" ninePer:@"10.67" fivePer:@"6.56" current:5.54 time:@"296" name:@"宁波飞翔"]];

    // 双鹭药业 002038 (2016.2.29  2017.5.10)    x个涨停  12月100%  5%以下下跌： -17.93%  市盈率 37.4/47.48
    [list addObject:[self maxValue:@"32.49" ninePer:@"21.83" fivePer:@"17.9" current:14.69 time:@"438" name:@"双鹭药业"]];

    // 新易盛 300502 (2017.7.24 2018.8.6 )     x个涨停  16月100%  5%以下下跌： -60.99%  市盈率 37.79/119.97 -68.5%
    [list addObject:[self maxValue:@"28.35" ninePer:@"19.84" fivePer:@"15.20" current:5.93 time:@"377" name:@"新易盛"]];

    // 盘龙药业 002864 (2019.1.31 2021.2.5 )     x个涨停  13.8月100%  5%以下下跌： -27.38%  市盈率: 23.03/135.09 -82.95%
    [list addObject:[self maxValue:@"80.86" ninePer:@"32.53" fivePer:@"28.71" current:@"20.85" name:@"盘龙药业" beginTime:@"2019.1.31" endTime:@"2021.2.5"]];
    
    // 德赛电池 000049 (2018.10.12 2019.6.6)  7.5月100%  5%以下下跌： -16.83%  市盈率: 12.27/40.54 -69.73%
    [list addObject:[self maxValue:@"39.28" ninePer:@"23.34" fivePer:@"17.17" current:@"14.28" name:@"德赛电池" beginTime:@"2018.10.12" endTime:@"2019.6.6"]];

    // 伊力特 600197  9月100%  5%以下下跌： -17.25%  市盈率: 12.27/41.36  -70.33%
    [list addObject:[self maxValue:@"28.56" ninePer:@"21.28" fivePer:@"14.03" current:@"11.61" name:@"伊力特" beginTime:@"2018.10.29" endTime:@"2020.3.23"]];

    // 爱乐达 300696  4.3月100%  5%以下下跌： -19.12%  市盈率: 40.8/71.79  -43.17%
    [list addObject:[self maxValue:@"30.35" ninePer:@"16.87" fivePer:@"13.7" current:@"11.08" name:@"爱乐达" beginTime:@"2019.1.31" endTime:@"2019.12.30"]];

    // 天孚通信 25.84  4.3月100%  5%以下下跌： -39.37%  市盈率: 25/113  -77.88%
    [list addObject:[self maxValue:@"30.49" ninePer:@"16.46" fivePer:@"12.32" current:@"7.47" name:@"天孚通信" beginTime:@"2017.7.24" endTime:@"2018.2.7"]];
    
    NSMutableArray *list2 = [NSMutableArray array];
    [list2 addObjectsFromArray:list];
    
//    NSLogv(@"1234567890987654321234567890");
    NSMutableArray *resultList = [NSMutableArray array];
    
//    if (1) {
//        int resu =  [self demo2:list model:list[0]];
//        NSLog(@"%d",resu);
//    }
    
    if (1) {
        for (StockModel *model in list2) {
            BOOL print = NO;
            
            //            if (model == list2[0]){
            //                print = true;
            //            }
            
            int paiming = [self demo2:list model:model isPrint:print];
            [resultList addObject:[NSString stringWithFormat:@"%d",paiming]];
        }
        
        [resultList sortUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int result = [obj2 intValue] - [obj1 intValue];
            if (result > 0) {
                return NSOrderedAscending;
            } else if (result < 0){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        NSLog(@"resultList: %@",resultList);
    }
    
    
    NSLog(@"================");
    if (1) {
        for (StockModel *model in list2) {
           int pm = [self demo2:list model:model];
            [self checkBuyName:model.name resultList:resultList paiming:pm];
            NSLog(@"%@",model);
        }
    }
    
    
    if (YES) {
        for (int i = 0; i < 2; i++) {
            NSLog(@" ");
        }
        NSLog(@"条件单进行时========================================start");
        int n = 0;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [formatter dateFromString:@"2021-12-31"];
        NSDate *today = [NSDate new];
        long day = today.timeIntervalSinceReferenceDate;
        long old = date.timeIntervalSinceReferenceDate;
        n = (int) ((day - old) / (3600 * 24));
        NSLog(@"今天: %d",n);
        NSLog(@"市盈率正常--------start🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
        [self max:@"18.100" ninePer:@"10.980" fivePer:@"7.0900" current:5.57 time:358+n name:@"格力地产" list:list resultList:resultList];
        [self max:@"45.500" ninePer:@"32.810" fivePer:@"18.690" current:14.73 time:122+n name:@"姚记科技" list:list resultList:resultList];
        [self max:@"50.060" ninePer:@"29.870" fivePer:@"20.540" current:20.18 time:122+n name:@"三七互娱" list:list resultList:resultList];
        [self max:@"181.22" ninePer:@"72.660" fivePer:@"53.920" current:35.10 time:280+n name:@"西藏药业" list:list resultList:resultList];
        [self max:@"26.900" ninePer:@"18.240" fivePer:@"13.230" current:9.11 time:652+n name:@"通化东宝" list:list resultList:resultList];
        [self max:@"33.070" ninePer:@"17.940" fivePer:@"13.380" current:16.29 time:181+n name:@"亨通光电" list:list resultList:resultList];
        [self max:@"34.310" ninePer:@"18.980" fivePer:@"14.130" current:9.82 time:697+n name:@"诚志股份" list:list resultList:resultList];
        [self max:@"49.470" ninePer:@"32.500" fivePer:@"21.890" current:20.80 time:234+n name:@"星网锐捷" list:list resultList:resultList];
        [self max:@"26.420" ninePer:@"15.220" fivePer:@"10.590" current:8.63 time:155+n name:@"力合科创" list:list resultList:resultList];
        [self max:@"24.580" ninePer:@"15.310" fivePer:@"11.650" current:7.52 time:93+n name:@"天下秀" list:list resultList:resultList];
        [self max:@"25.710" ninePer:@"15.990" fivePer:@"13.260" current:11.30 time:328+n name:@"保龄宝" list:list resultList:resultList];
        [self max:@"41.800" ninePer:@"22.470" fivePer:@"13.430" current:15.00 time:122+n name:@"完美世界" list:list resultList:resultList];
        [self max:@"57.350" ninePer:@"29.270" fivePer:@"19.770" current:19.68 time:93+n name:@"新宝股份" list:list resultList:resultList];
        [self max:@"13.330" ninePer:@"5.8800" fivePer:@"4.7600" current:4.80 time:155+n name:@"红旗连锁" list:list resultList:resultList];
        [self max:@"89.760" ninePer:@"62.830" fivePer:@"38.300" current:30.30 time:582+n name:@"顶点软件" list:list resultList:resultList];
        [self max:@"82.700" ninePer:@"40.560" fivePer:@"31.280" current:36.50 time:100+n name:@"中炬高新" list:list resultList:resultList];
        [self max:@"64.390" ninePer:@"33.000" fivePer:@"24.170" current:20.37 time:122+n name:@"电魂网络" list:list resultList:resultList];
        [self max:@"64.750" ninePer:@"33.150" fivePer:@"23.850" current:26.33 time:233+n name:@"赛腾股份" list:list resultList:resultList];
        [self max:@"15.000" ninePer:@"8.6800" fivePer:@"6.9200" current:4.57 time:144+n name:@"世纪华通" list:list resultList:resultList];
        [self max:@"49.240" ninePer:@"25.740" fivePer:@"18.510" current:11.63 time:64+n name:@"吉宏股份" list:list resultList:resultList];
        [self max:@"31.100" ninePer:@"14.590" fivePer:@"11.950" current:10.38 time:332+n name:@"航天信息" list:list resultList:resultList];
        [self max:@"24.160" ninePer:@"12.400" fivePer:@"8.0600" current:4.85 time:238+n name:@"南极电商" list:list resultList:resultList];
        [self max:@"197.05" ninePer:@"123.74" fivePer:@"94.060" current:88.25 time:233+n name:@"深南电路" list:list resultList:resultList];
        [self max:@"60.120" ninePer:@"30.730" fivePer:@"23.760" current:32.63 time:155+n name:@"国恩股份" list:list resultList:resultList];
        NSLog(@"市盈率正常--------end🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
    }
    
    if (YES) {
        for (int i = 0; i < 2; i++) {
            NSLog(@" ");
        }
        NSLog(@"条件单进行时========================================start");
        int n = 0;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [formatter dateFromString:@"2021-12-31"];
        NSDate *today = [formatter dateFromString:@"2022-04-27"];
        long day = today.timeIntervalSinceReferenceDate;
        long old = date.timeIntervalSinceReferenceDate;
        n = (int) ((day - old) / (3600 * 24));
        NSLog(@"今天: 2022-04-27");
        NSLog(@"市盈率正常--------start🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
        [self max:@"18.100" ninePer:@"10.980" fivePer:@"7.0900" current:4.50 time:358+n name:@"格力地产" list:list resultList:resultList];
        [self max:@"45.500" ninePer:@"32.810" fivePer:@"18.690" current:13.16 time:122+n name:@"姚记科技" list:list resultList:resultList];
        [self max:@"50.060" ninePer:@"29.870" fivePer:@"20.540" current:19.33 time:122+n name:@"三七互娱" list:list resultList:resultList];
        [self max:@"181.22" ninePer:@"72.660" fivePer:@"53.920" current:29.55 time:280+n name:@"西藏药业" list:list resultList:resultList];
        [self max:@"26.900" ninePer:@"18.240" fivePer:@"13.230" current:8.9 time:652+n name:@"通化东宝" list:list resultList:resultList];
        [self max:@"33.070" ninePer:@"17.940" fivePer:@"13.380" current:9.23 time:181+n name:@"亨通光电" list:list resultList:resultList];
        [self max:@"34.310" ninePer:@"18.980" fivePer:@"14.130" current:8.8 time:697+n name:@"诚志股份" list:list resultList:resultList];
        [self max:@"49.470" ninePer:@"32.500" fivePer:@"21.890" current:19.60 time:234+n name:@"星网锐捷" list:list resultList:resultList];
        [self max:@"26.420" ninePer:@"15.220" fivePer:@"10.590" current:6.92 time:155+n name:@"力合科创" list:list resultList:resultList];
        [self max:@"24.580" ninePer:@"15.310" fivePer:@"11.650" current:6.29 time:93+n name:@"天下秀" list:list resultList:resultList];
        [self max:@"25.710" ninePer:@"15.990" fivePer:@"13.260" current:8.95 time:328+n name:@"保龄宝" list:list resultList:resultList];
        [self max:@"41.800" ninePer:@"22.470" fivePer:@"13.430" current:11.72 time:122+n name:@"完美世界" list:list resultList:resultList];
        [self max:@"57.350" ninePer:@"29.270" fivePer:@"19.770" current:13.85 time:93+n name:@"新宝股份" list:list resultList:resultList];
        [self max:@"13.330" ninePer:@"5.8800" fivePer:@"4.7600" current:4.54 time:155+n name:@"红旗连锁" list:list resultList:resultList];
        [self max:@"89.760" ninePer:@"62.830" fivePer:@"38.300" current:21.60 time:582+n name:@"顶点软件" list:list resultList:resultList];
        [self max:@"82.700" ninePer:@"40.560" fivePer:@"31.280" current:23.75 time:100+n name:@"中炬高新" list:list resultList:resultList];
        [self max:@"64.390" ninePer:@"33.000" fivePer:@"24.170" current:19.14 time:122+n name:@"电魂网络" list:list resultList:resultList];
        [self max:@"64.750" ninePer:@"33.150" fivePer:@"23.850" current:13.69 time:233+n name:@"赛腾股份" list:list resultList:resultList];
        [self max:@"15.000" ninePer:@"8.6800" fivePer:@"6.9200" current:4.39 time:144+n name:@"世纪华通" list:list resultList:resultList];
        [self max:@"49.240" ninePer:@"25.740" fivePer:@"18.510" current:9.60 time:64+n name:@"吉宏股份" list:list resultList:resultList];
        [self max:@"31.100" ninePer:@"14.590" fivePer:@"11.950" current:9.33 time:332+n name:@"航天信息" list:list resultList:resultList];
        [self max:@"24.160" ninePer:@"12.400" fivePer:@"8.0600" current:3.96 time:238+n name:@"南极电商" list:list resultList:resultList];
        [self max:@"197.05" ninePer:@"123.74" fivePer:@"94.060" current:84.95 time:233+n name:@"深南电路" list:list resultList:resultList];
        [self max:@"60.120" ninePer:@"30.730" fivePer:@"23.760" current:19.43 time:155+n name:@"国恩股份" list:list resultList:resultList];
        NSLog(@"市盈率正常--------end🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
    }
    
}


-(void)tiaojiandan:(NSString *)name
     tiaojiandan_price:(NSString *)tiaojiandan_price
     current_price:(double)current_price
           trigger:(BOOL)trigger{
    double result =  ((current_price - [tiaojiandan_price floatValue]) / current_price * 100);
    
    if(trigger){
        NSLog(@"%@ : %.2f%% 🍺🍺🍺🍺🍺已触发",name,result);
        return;
    }
    if (result < 0){
        NSLog(@"%@ : 触发后下跌 %.2f%%",name,fabs(result));
    } else  if (result < 10){
//            0-10
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐⭐⭐⭐",name,result);
    } else if (result < 20){
//            10 - 20
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐",name,result);
    } else {
//            > 20
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发",name,result);
    }
}



-(int)demo2:(NSMutableArray *)list
             model:(StockModel *)model{
    return [self demo2:list model:model isPrint:NO];
}

-(int)demo2:(NSMutableArray *)list
             model:(StockModel *)model
           isPrint:(BOOL)print{
    int a1 = 1;
    int a2 = 1;
    int a3 = 1;
    int a4 = 1;
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        return [obj2.currentDecline compare:obj1.currentDecline];
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if ([stockModel.currentDecline floatValue] == [model.currentDecline floatValue]) {
            a1 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %@ %%",i,stockModel.currentDecline);
        }
    }
    
    if (print) {
        NSLog(@"根据最大跌幅排序--------end" );
        NSLog(@"================================" );
        NSLog(@"根据90%盈利到二次探底最大跌幅排序--------start" );
    }
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        return [obj2.ninefall compare:obj1.ninefall];
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if ([stockModel.ninefall floatValue] == [model.ninefall floatValue]) {
            a2 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %@ %%",i,stockModel.ninefall);
        }
    }
    if (print) {
        NSLog(@"根据90%%盈利到二次探底最大跌幅排序--------end" );
        NSLog(@"================================" );
        NSLog(@"根据90%%盈利到二次探底时间--------start" );
    }
    
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        int result = obj1.ninefall_time - obj2.ninefall_time;
        if (result > 0) {
            return NSOrderedAscending;
        } else if (result < 0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if (stockModel.ninefall_time == model.ninefall_time) {
            a3 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %ld天",i,stockModel.ninefall_time);
        }
    }
    if (print) {
        NSLog(@"根据90%%盈利到二次探底时间--------end" );
        NSLog(@"================================" );
        NSLog(@"根据5%%盈利比例后继续下跌幅度排序----------start" );
    }
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        float result = [obj2.profitnumber floatValue] - [obj1.profitnumber floatValue];
        if (result > 0) {
            return NSOrderedAscending;
        } else if (result < 0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if ([stockModel.profitnumber floatValue] == [model.profitnumber floatValue]) {
            a4 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %@ %%",i,stockModel.profitnumber);
        }
    }
    
    if (print) {
         NSLog(@"根据5%%盈利比例后继续下跌幅度排序----------end" );
    }
    int sum = (a1*2  + a3*4  + a4*3);
    return sum;
}

-(void)checkBuyName:(NSString *)name
         resultList:(NSMutableArray *)resultList
            paiming:(int)paiming{
    for (int i = 0; i < resultList.count; i++) {
        if ([resultList[i] floatValue] >= paiming) {
            i++;
            if (i <= resultList.count / 2) {
                NSLog(@"%@  %d   %d%/%d  建议购买😄",name,paiming,i,resultList.count);
//                NSLog(@ name + "  " + paiming + "    " + i + "/" + resultList.size () + "    建议购买😄😄😄😄😄😄" );
            } else {
//                NSLog(@ name + "  " + paiming + "    " + i + "/" + resultList.size () + "    不买" );
                NSLog(@"%@  %d   %d%/%d  不买",name,paiming,i,resultList.count);

            }
            return;
        }
    }
    NSLog(@"%@  %d   %d%/%d不买",name,paiming,resultList.count,resultList.count);

}

-(StockModel *)maxValue:(NSString *)max_value
                       current:(double )current_value
                                name:(NSString *)name{
    return [self maxValue:max_value ninePer:@"0" fivePer:@"0" current:current_value time:@"0" name:name];
}




-(StockModel *)maxValue:(NSString *)max_value
                       ninePer:(NSString *)ninePer_value
                       fivePer:(NSString *)fivePer_value
                       current:(double)current_value
                       time:(NSString *)ninefall_time
                                name:(NSString *)name{
    double max_value1 = (current_value / [max_value doubleValue] - 1) * 100;
    double max_value2 = (current_value / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = (current_value / [fivePer_value doubleValue] - 1) * 100;
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:[ninefall_time intValue]  profitnumber:[NSString stringWithFormat:@"%.1f",max_value3] name:name];
}

-(StockModel *)createModle_max_value:(NSString *)max_value
                       ninePer_value:(NSString *)ninePer_value
                       fivePer_value:(NSString *)fivePer_value
                       current_value:(NSString *)current_value
                       ninefall_time:(NSInteger)ninefall_time{
    double max_value1 = ([current_value doubleValue] / [max_value doubleValue] - 1) * 100;
    double max_value2 = ([current_value doubleValue] / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = ([current_value doubleValue] / [fivePer_value doubleValue] - 1) * 100;
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:ninefall_time profitnumber:[NSString stringWithFormat:@"%.1f",max_value3]];
}


-(StockModel *)maxValue:(NSString *)max_value
                       ninePer:(NSString *)ninePer_value
                       fivePer:(NSString *)fivePer_value
                       current:(NSString *)current_value
                                name:(NSString *)name
                                beginTime:(NSString *)beginTime
                                endTime:(NSString *)endTime {
    double max_value1 = ([current_value doubleValue] / [max_value doubleValue] - 1) * 100;
    double max_value2 = ([current_value doubleValue] / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = ([current_value doubleValue] / [fivePer_value doubleValue] - 1) * 100;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if ([beginTime containsString:@"."]){
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    NSDate *beginDate = [formatter dateFromString:beginTime];
    NSDate *endDate = [formatter dateFromString:endTime];
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long n = (int) ((endDay - beginDay) / (3600 * 24));
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:n profitnumber:[NSString stringWithFormat:@"%.1f",max_value3] name:name];
}


-(void)max:(NSString *)max_value
             ninePer:(NSString *)ninePer_value
             fivePer:(NSString *)fivePer_value
             current:(double)current_value
             time:(NSInteger)ninefall_time
                      name:(NSString *)name
                      list:(NSMutableArray *)list
                resultList:(NSMutableArray *)resultList{
    StockModel *targetmodel = [self maxValue:max_value ninePer:ninePer_value fivePer:fivePer_value current:current_value time:[NSString stringWithFormat:@"%ld",ninefall_time] name:name];
    [list addObject:targetmodel];
    int paiming = [self demo2:list model:targetmodel];
    if ([targetmodel.profitnumber floatValue] < -30) {
        [self checkBuyName:name resultList:resultList paiming:paiming];
        NSLog(@"%@",targetmodel);
    }
    [list removeObject:targetmodel];
}









@end
