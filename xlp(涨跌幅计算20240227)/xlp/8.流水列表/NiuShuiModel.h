//
//  NiuShuiModel.h
//  xlp
//
//  Created by xiaobao on 2023/10/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NiuShuiModel : NSObject

@property (nonatomic,copy) NSString *str1;
@property (nonatomic,copy) NSString *str2;
@property (nonatomic,copy) NSString *str3;
@property (nonatomic,copy) NSString *str4;
@property (nonatomic,copy) NSString *str5;


-(instancetype)initWithStr1:(NSString *)str1
                       str2:(NSString *)str2
                       str3:(NSString *)str3
                       str4:(NSString *)str4
                       str5:(NSString *)str5;

@end

NS_ASSUME_NONNULL_END
