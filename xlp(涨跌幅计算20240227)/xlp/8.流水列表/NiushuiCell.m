//
//  NiushuiCell.m
//  xlp
//
//  Created by xiaobao on 2023/10/18.
//

#import "NiushuiCell.h"

@interface NiushuiCell ()
@property (nonatomic,strong) UILabel *label1;
@property (nonatomic,strong) UILabel *label2;
@property (nonatomic,strong) UILabel *label3;

@property (nonatomic,strong) UILabel *label4;
@property (nonatomic,strong) UILabel *label5;


@end


@implementation NiushuiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.contentView.backgroundColor = [UIColor colorWithHexString:@"#FEFEFE"];
    
    
    
    self.label1 = [UILabel createLabelWith:@"网上支付-广发基金..." font:QDSystemFont(15.0) textColor:@"#333333"];
    [self.contentView addSubview:self.label1];
    [self.label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(14);
        make.width.mas_equalTo(155);
        make.height.mas_equalTo(20);
    }];
    
    self.label2 = [UILabel createLabelWith:@"支付宝" font:QDSystemFont(14.0) textColor:@"#7f8994"];
    [self.contentView addSubview:self.label2];
    [self.label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(42);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(18);
    }];
    
    self.label3 = [UILabel createLabelWith:@"2023-12-7 15:20:48" font:[UIFont systemFontOfSize:14.0 weight:UIFontWeightLight] textColor:@"#7f8994"];
    [self.contentView addSubview:self.label3];
    [self.label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(64);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(20);
    }];
    
//    UIKIT_EXTERN const UIFontWeight UIFontWeightUltraLight API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightThin API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightLight API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightRegular API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightMedium API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightSemibold API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightBold API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightHeavy API_AVAILABLE(ios(8.2));
//    UIKIT_EXTERN const UIFontWeight UIFontWeightBlack API_AVAILABLE(ios(8.2));
    
    
    self.label4 = [UILabel createLabelWith:@"+12,710.20" font:[UIFont boldSystemFontOfSize:14.0] textColor:@"#F95659"];
    self.label4.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.label4];
    [self.label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(14);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
    }];
    
    
    self.label5 = [UILabel createLabelWith:@"余额:12,649.81" font:[UIFont systemFontOfSize:14.0] textColor:@"#38414E"];
    self.label5.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.label5];
    [self.label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(35);
        make.width.mas_equalTo(113);
        make.height.mas_equalTo(20);
    }];

    
    UIView *line = [[UIView alloc] initWithFrame:CGRectZero];
    line.backgroundColor = [UIColor colorWithHexString:@"EDEDED"];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.centerX.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];

   
}


-(void)refreshUI:(NiuShuiModel *)model{
    self.label1.text = model.str1;
    self.label2.text = model.str2;
    self.label3.text = model.str3;
    self.label4.text = model.str4;
    self.label5.text = model.str5;

    if([model.str4 doubleValue] > 0){
        self.label4.textColor = [UIColor colorWithHexString:@"#F95659"];
    } else {
        self.label4.textColor = [UIColor colorWithHexString:@"#333333"];
    }
    
    
}

@end
