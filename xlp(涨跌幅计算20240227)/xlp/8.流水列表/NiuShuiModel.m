//
//  NiuShuiModel.m
//  xlp
//
//  Created by xiaobao on 2023/10/18.
//

#import "NiuShuiModel.h"

@implementation NiuShuiModel

-(instancetype)initWithStr1:(NSString *)str1
                       str2:(NSString *)str2
                       str3:(NSString *)str3
                       str4:(NSString *)str4
                       str5:(NSString *)str5{
    self = [super init];
    if(self){
        _str1 = str1;
        _str2 = str2;
        _str3 = str3;
        _str4 = str4;
        _str5 = str5;
    }
    return self;
}

@end
