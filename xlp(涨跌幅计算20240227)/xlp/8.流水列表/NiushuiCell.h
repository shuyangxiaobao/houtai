//
//  NiushuiCell.h
//  xlp
//
//  Created by xiaobao on 2023/10/18.
//

#import <UIKit/UIKit.h>
#import "NiuShuiModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NiushuiCell : UITableViewCell

-(void)refreshUI:(NiuShuiModel *)model;

@end

NS_ASSUME_NONNULL_END
