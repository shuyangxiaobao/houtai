//
//  NiuShuiViewController.m
//  xlp
//
//  Created by xiaobao on 2023/10/18.
//

#import "NiuShuiViewController.h"
#import "NiushuiCell.h"
#import "NiuShuiModel.h"


@interface NiuShuiViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *datas;

@property(nonatomic,assign) BOOL ishigh;
@end

@implementation NiuShuiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableview];
    [self initDatas];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
}
-(void)initDatas{
    self.datas = [NSMutableArray array];
    if(!self.ishigh){
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2024-1-16 8:19:27" str4:@"-12,710.00" str5:@"余额:50.01"]];
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2024-1-15 15:20:48" str4:@"+12,710.00" str5:@"余额:12,760.01"]];
        
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-12-16 21:33:42" str4:@"-12,600.00" str5:@"余额:49.81"]];
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-12-15 15:05:50" str4:@"+12,561.10" str5:@"余额:12,649.81"]];
        
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-11-16 12:45:31" str4:@"-12,300.00" str5:@"余额:88.71"]];
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-11-15 13:15:46" str4:@"+12,387.30" str5:@"余额:12,388.71"]];
    } else {
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2024-1-16 8:19:27" str4:@"-15,710.00" str5:@"余额:50.01"]];
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2024-1-15 15:20:48" str4:@"+15,710.00" str5:@"余额:15,760.01"]];
        
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-12-16 21:33:42" str4:@"-15,600.00" str5:@"余额:49.81"]];
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-12-15 15:05:50" str4:@"+15,561.10" str5:@"余额:15,649.81"]];
        
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-11-16 12:45:31" str4:@"-15,300.00" str5:@"余额:88.71"]];
        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-11-15 13:15:46" str4:@"+15,387.30" str5:@"余额:15,388.71"]];
    }
    
   
    
    
    
    [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"存款利息-应付个人..." str2:@"批处理" str3:@"2023-10-11 19:31:23" str4:@"+0.01" str5:@"余额:1.41"]];

    [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-9-01 12:55:36" str4:@"-1,230.00" str5:@"余额:1.40"]];
    
    [self.tableView reloadData];
    
}

//-(void)initDatas{
//    self.datas = [NSMutableArray array];
//    if(!self.ishigh){
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-10-17 7:19:27" str4:@"-12,710.00" str5:@"余额:50.01"]];
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-10-13 15:20:48" str4:@"+12,710.00" str5:@"余额:12,760.01"]];
//        
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-09-16 21:33:42" str4:@"-12,600.00" str5:@"余额:49.81"]];
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-09-15 15:05:50" str4:@"+12,561.10" str5:@"余额:12,649.81"]];
//        
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-08-16 12:45:31" str4:@"-12,300.00" str5:@"余额:88.71"]];
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-08-15 13:15:46" str4:@"+12,387.30" str5:@"余额:12,388.71"]];
//    } else {
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-10-17 7:19:27" str4:@"-15,710.00" str5:@"余额:50.01"]];
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-10-13 15:20:48" str4:@"+15,710.00" str5:@"余额:15,760.01"]];
//        
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-09-16 21:33:42" str4:@"-15,600.00" str5:@"余额:49.81"]];
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-09-15 15:05:50" str4:@"+15,561.10" str5:@"余额:15,649.81"]];
//        
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-08-16 12:45:31" str4:@"-15,300.00" str5:@"余额:88.71"]];
//        [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"代发工资-上海嘉扬..." str2:@"企业网银" str3:@"2023-08-15 13:15:46" str4:@"+15,387.30" str5:@"余额:15,388.71"]];
//    }
//    
//   
//    
//    
//    
//    [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"存款利息-应付个人..." str2:@"批处理" str3:@"2023-08-11 19:31:23" str4:@"+0.01" str5:@"余额:1.41"]];
//
//    [self.datas addObject:[[NiuShuiModel alloc] initWithStr1:@"网上支付-广发基金..." str2:@"支付宝" str3:@"2023-08-01 12:55:36" str4:@"-1,230.00" str5:@"余额:1.40"]];
//
//    
//    [self.tableView reloadData];
//    
//}
-(void)initTableview{
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top"]];
    imageView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(168);
    }];
    
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor colorWithHexString:@"FEFEFE" alpha:1.0];
    _tableView.delegate = self;
    _tableView.dataSource =self;
    _tableView.scrollEnabled = YES;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.userInteractionEnabled = YES;
    _tableView.layer.masksToBounds = YES;
    _tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[NiushuiCell class] forCellReuseIdentifier:@"NiushuiCell"];


    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(168);
        make.left.equalTo(@(0));
        make.right.equalTo(@(0));
        make.bottom.equalTo(@(0));
    }];
    
    
  
    [self.tableView reloadData];
 
}


#pragma mark - UITableViewDelegate and UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 91;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NiushuiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NiushuiCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell refreshUI:self.datas[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.ishigh = !self.ishigh;
    [self initDatas];
    
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
