//
//  Settingmanager.h
//  xlp
//
//  Created by xiaobao on 2021/10/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GQBSettingmanager : NSObject
@property (nonatomic,strong) NSMutableArray *datas;

@property (nonatomic,strong) NSMutableArray *instrumentIDDatas; //!< 股票代码

@property (nonatomic,strong) NSMutableArray *satisArr;

@property (nonatomic,strong) NSMutableDictionary *positionInfoDic;//!< 持仓股票行情

@property(nonatomic,assign) BOOL minestPrice; //!< 最低价
/**初始化单例*/
+ (GQBSettingmanager *)shareInstance;

#pragma mark - 根据股票id 返回带市场标识的id
-(NSString *)returnMarketID:(NSString *)instrumentID;

@end

NS_ASSUME_NONNULL_END
