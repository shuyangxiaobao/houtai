//
//  JieTuViewController.m
//  xlp
//
//  Created by xiaobao on 2023/10/18.
//

#import "JieTuViewController.h"

@interface JieTuViewController ()

@end

@implementation JieTuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createUI];
}

-(void)createUI{
    self.view.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jietu"]];
    imageView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.mas_equalTo(0);
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
