//
//  PositionModel.m
//  xlp
//
//  Created by xiaobao on 2023/7/21.
//

#import "PositionModel.h"

@implementation PositionModel

-(instancetype)initWithName:(NSString *)name buyPrice:(CGFloat)buyPrice buyTime:(NSString *)buyTime sellPrice:(CGFloat)sellPrice sellTime:(NSString *)sellTime{
    self = [super init];
    if(self){
        _name = name;
        _buyPrice = buyPrice;
        _buyTime = buyTime;
        _sellPrice = sellPrice;
        _sellTime = sellTime;
    }
    return self;
}


@end
