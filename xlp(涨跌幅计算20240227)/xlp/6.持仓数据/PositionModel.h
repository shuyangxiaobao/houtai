//
//  PositionModel.h
//  xlp
//
//  Created by xiaobao on 2023/7/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/// 持仓信息
@interface PositionModel : NSObject
@property(nonatomic,copy)NSString * name;  //!< <#name#>
@property(nonatomic,assign)CGFloat buyPrice;  //!< name
@property(nonatomic,copy)NSString * buyTime;  //!< <#name#>
@property(nonatomic,assign)CGFloat sellPrice;  //!< name
@property(nonatomic,copy)NSString * sellTime;  //!< <#name#>

@property(nonatomic,copy)NSString * result;  //!< <#name#>

-(instancetype)initWithName:(NSString *)name buyPrice:(CGFloat)buyPrice buyTime:(NSString *)buyTime sellPrice:(CGFloat)sellPrice sellTime:(NSString *)sellTime;

@end

NS_ASSUME_NONNULL_END
