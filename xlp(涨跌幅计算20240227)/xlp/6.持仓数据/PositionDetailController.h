//
//  PositionDetailController.h
//  xlp
//
//  Created by xiaobao on 2023/7/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PositionDetailController : UIViewController
-(instancetype)initWithModel:(PositionModel *)model list:(NSMutableArray *)instrumentIDDatas index:(int)index;

@end

NS_ASSUME_NONNULL_END
