//
//  PositionViewController.m
//  xlp
//
//  Created by xiaobao on 2023/7/20.
//

#import "PositionViewController.h"
#import "GQBOneCell.h"
#import "PositionModel.h"

@interface PositionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *tableViewDatas;

@property (nonatomic,assign) int countNumber;

@end

@implementation PositionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableview];
    [self initData];
    // Do any additional setup after loading the view.
}

-(void)initData{
    self.tableViewDatas = [NSMutableArray array];
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale systemLocale];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    NSString *today = [formatter stringFromDate:date];
    

    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"深圳华强 000062" buyPrice:7.78 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"金螳螂 002081" buyPrice:3.04 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"美盈森 002303" buyPrice:2.26 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"富煌钢构 002743" buyPrice:3.32 buyTime:@"2024.2.7" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"木林森 002745" buyPrice:5.77 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"荣泰健康 603579" buyPrice:15.40 buyTime:@"2024.2.5" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"节能国祯 300388" buyPrice:4.82 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"勘设股份 603458" buyPrice:5.36 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"金海高科 603311" buyPrice:6.40 buyTime:@"2024.2.7" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"博深股份 002282" buyPrice:4.68 buyTime:@"2024.2.8" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"苏交科 300284" buyPrice:4.06 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"三力士 002224" buyPrice:3.62 buyTime:@"2024.2.8" sellPrice:0.00 sellTime:today]];
    [self.tableViewDatas addObject:[[PositionModel alloc] initWithName:@"兆丰股份 300695" buyPrice:31.72 buyTime:@"2024.2.6" sellPrice:0.00 sellTime:today]];


    
    
    
    
    NSMutableArray *tempMutarr = [NSMutableArray array];
    for (PositionModel *model in self.tableViewDatas) {
        NSString *gupiaoID = [[GQBSettingmanager shareInstance] returnMarketID:[model.name componentsSeparatedByString:@" "].lastObject];
        [tempMutarr addObject:gupiaoID];
        if([tempMutarr count] >= 10){
            self.countNumber++;
            if(self.countNumber >=19){
                self.countNumber = 0;
                [NSThread sleepForTimeInterval:1.5];
            }
            [self networkRequest:tempMutarr];
            [tempMutarr removeAllObjects];
        }
    }
    if([tempMutarr count] > 0){
        [self networkRequest:tempMutarr];
    }
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0*NSEC_PER_SEC)), dispatch_get_global_queue(0, 0), ^{

//    });
    
    

  
}

-(void)solveData{
    for (PositionModel *model in self.tableViewDatas) {
        NSString *gupiaoID = [model.name componentsSeparatedByString:@" "].lastObject;
        NSString *hanqing = [[GQBSettingmanager shareInstance].positionInfoDic objectForKey:gupiaoID];
        NSString *price = [hanqing componentsSeparatedByString:@","][3];
        model.sellPrice = [price floatValue];
        // [3] 最新 [4]最高  [5]最低
    }
    [self.tableView reloadData];

//    dispatch_async(dispatch_get_main_queue(), ^{
//    });
}

-(void)networkRequest:(NSArray *)arr {
    
    
    NSString *url = @"https://apis.tianapi.com/finance/index";
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSString *key = @"";
    
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale systemLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:date];
    NSString *timeStr = [dateStr componentsSeparatedByString:@" "].lastObject;
    
    //    if([timeStr compare:@"12:00:00"] == NSOrderedAscending){
    //        key = @"044359093026aa89903837ad2355a2ea";//huxu625  12点前
    //    } else {
    //        key = @"823f175452116a6f4bb7bdf327d65441";// 戈强宝
    //    }
    key = @"823f175452116a6f4bb7bdf327d65441";// 戈强宝
    NSString *code = [arr componentsJoinedByString:@","];
    NSString *list = @"1";
    
    //    sz002752  读取不出来
    [parameters setObject:key forKey:@"key"];
    [parameters setObject:code forKey:@"code"];
    [parameters setObject:list forKey:@"list"];
    
    NSLog(@"发送请求时间");
    [[HTTPEngine sharedEngine] getRequestWithURL:url parameters:parameters success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        NSError *error = nil;
        NSDictionary *response = responseObject[@"result"];
        NSArray *keys = response.allKeys;
        for (NSString *key in keys) {
            NSString *value = [response objectForKey:key];
            NSString *key2 =[key substringFromIndex:2];
            [[GQBSettingmanager shareInstance].positionInfoDic setObject:value forKey:key2];
        }
        if([GQBSettingmanager shareInstance].positionInfoDic.allKeys.count == self.tableViewDatas.count){
            [self solveData];
        }
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        
    }];
    
}



-(void)initTableview{
    self.view.backgroundColor = [UIColor whiteColor];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor colorWithHexString:@"979797" alpha:0.4];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource =self;
    _tableView.scrollEnabled = YES;
    _tableView.separatorStyle = UITableViewCellSelectionStyleGray;
    _tableView.userInteractionEnabled = YES;
    _tableView.layer.masksToBounds = YES;
    _tableView.tableFooterView = [UIView new];
//    [self.tableView registerClass:[OneCell class] forCellReuseIdentifier:@"lasdjlfa"];


    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavAndStatusBarHeight);
        make.left.equalTo(@(0));
        make.right.equalTo(@(0));
        make.bottom.equalTo(@(0));
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    return 10;
    if([GQBSettingmanager shareInstance].positionInfoDic.allKeys.count == self.tableViewDatas.count){
        return self.tableViewDatas.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GQBOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lasdjlfa"];
    if (cell == nil) {
        cell = [[GQBOneCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"lasdjlfa"];
    }
    PositionModel *model = self.tableViewDatas[indexPath.row];
    
    
    CGFloat buyPrice = model.buyPrice;
    CGFloat sellPrice = model.sellPrice;
    NSString* buyTime = model.buyTime;
    NSString* sellTime = model.sellTime;
    float result = 0;
    if (buyPrice < sellPrice) {
         result = (sellPrice/buyPrice -1)*100;
    } else{
        result = (1-sellPrice/buyPrice)*100;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    NSDate *beginDate = [formatter dateFromString:buyTime];
    NSDate *endDate = [formatter dateFromString:sellTime];
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long day = (int) ((endDay - beginDay) / (3600 * 24));
    
    double t1 = log2(result/100+1)*365;
    t1 = t1 - day;
    
    double speed = (pow(result/100+1, 1/(day/365.0))-1)*100;

    NSString *title = [NSString stringWithFormat:@"%@: %d天 上涨 %.0f%% 年化 %.0f%% %.0f天",model.name,day,result,speed,t1];
    
    NSString *message = @"";

    
    BOOL okSell = NO;
    
    
    BOOL b1 = result >= 44 && speed >= 94 && t1 >= -14 && day >= 28;
    BOOL b2 = result >= 65 || speed >= 412 || t1 >= 148;
    BOOL b3 =  result >= 161 || speed >= 1282617 || t1 >= 367;
    if(b1 && b2){
        okSell = YES;
        message = @"\n可以卖⭐️⭐️⭐️";
        if(b3){
            message = @"\n可以卖⭐️⭐️⭐️必须卖⭐️⭐️⭐️必须卖⭐️⭐️⭐️必须卖⭐️⭐️⭐️必须卖⭐️⭐️⭐️必须卖⭐️⭐️⭐️必须卖";
        }
    } else {
        message = @"\n不可以";
    }
   
//    
//    
//    
//    if(result < 44 || speed < 94 || day < 35){
//        okSell = NO;
//    }
//    
//    if(okSell){
//        if(result >= 61 || speed >= 520 || t1 >= 168){
//            
//        } else {
//            okSell = NO;
//        }
//    }
//    if(model.sellPrice < 1.0){
//        okSell = NO;
//    }
//    
//    NSString *message = @"";
//    if(okSell){
//        message = @"\n可以卖⭐️⭐️⭐️";
//    } else{
//        message = @"\n不可以";
//    }
    title = [NSString stringWithFormat:@"%@ %@",title,message];
    
    model.result = title;
    
    NSLog(@"%@",model.result);

    
    
    [cell refreshUI:model.name index:indexPath.row + 1 rightTitle:[NSString stringWithFormat:@"%.2f",model.sellPrice]];


    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor colorWithHexString:@"888888"].CGColor;
//    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PositionModel *model = self.tableViewDatas[indexPath.row];
    PositionDetailController *vc = [[PositionDetailController alloc] initWithModel:model list:self.tableViewDatas index:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
