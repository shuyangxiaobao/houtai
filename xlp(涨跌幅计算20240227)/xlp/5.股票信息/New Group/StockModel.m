//
//  StockModel.m
//  xlp
//
//  Created by xiaobao on 2022/8/26.
//

#import "StockModel.h"

@implementation StockModel

-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber
                                  name:(NSString *)name{
    self = [super init];
    if (self) {
        _currentDecline = currentDecline;
        _ninefall = ninefall;
        _ninefall_time = ninefall_time;
        _profitnumber = profitnumber;
        _name = name;
    }
    return self;
}

-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber
                                  name:(NSString *)name
                               buyTime:(NSString *)buyTime{

    self = [super init];
    if (self) {
        _currentDecline = currentDecline;
        _ninefall = ninefall;
        _ninefall_time = ninefall_time;
        _profitnumber = profitnumber;
        _name = name;
        _buyTime = buyTime;
    }
    return self;
}

-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber
                                  name:(NSString *)name
                               buyTime:(NSString *)buyTime
                          intervalTime:(NSString *)intervalTime{



    self = [super init];
    if (self) {
        _currentDecline = currentDecline;
        _ninefall = ninefall;
        _ninefall_time = ninefall_time;
        _profitnumber = profitnumber;
        _name = name;
        _buyTime = buyTime;
        _intervalTime = intervalTime;
    }
    return self;
}


-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber{
    self = [super init];
    if (self) {
        _currentDecline = currentDecline;
        _ninefall = ninefall;
        _ninefall_time = ninefall_time;
        _profitnumber = profitnumber;
    }
    return self;
}


- (NSString *)description{
    NSString *result = [NSString stringWithFormat:@"StockModel{currentDecline=%@%%,ninefall=%@%%,ninefall_time=%ld天,profitnumber=%@%%,name=%@",_currentDecline,_ninefall,_ninefall_time,_profitnumber,_name];
    
    if(_intervalTime.length > 0){
        result = [result stringByAppendingFormat:[NSString stringWithFormat:@",intervalTime=%@",_intervalTime]];
    }
    
    result = [result stringByAppendingFormat:[NSString stringWithFormat:@"}"]];
    
    

    
    
    
    if(_ninefall_time < 1){
        if(_buyTime.length > 0){
            result = [NSString stringWithFormat:@"StockModel{currentDecline= %@%% , name = %@ ,buyTime = %@ ",_currentDecline,_name,_buyTime];
            if(_intervalTime.length > 0){
                result = [result stringByAppendingFormat:[NSString stringWithFormat:@",intervalTime=%@",_intervalTime]];
            }
            
            result = [result stringByAppendingFormat:[NSString stringWithFormat:@"}"]];

            
            
        } else {
            result = [NSString stringWithFormat:@"StockModel{currentDecline= %@%% , name = %@}",_currentDecline,_name];

        }

    }
    return result;
}

@end
