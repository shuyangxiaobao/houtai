//
//  StockModel.h
//  xlp
//
//  Created by xiaobao on 2022/8/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StockModel : NSObject
@property(nonatomic,copy)NSString * currentDecline;  //!< 当前跌幅
@property(nonatomic,copy)NSString * ninefall;  //!< 90%盈利比例后下跌幅度
@property(nonatomic,assign)NSInteger ninefall_time;  //!< 90%盈利比例后下跌时间
@property(nonatomic,copy)NSString * profitnumber;  //!< 5%盈利比例后继续下跌幅度
@property(nonatomic,copy)NSString * name;  //!<
@property(nonatomic,copy)NSString * intervalTime;    //!< 第一次最低点到第二次最低点时间间隔
@property (nonatomic,copy) NSString *buyTime;  //!< 买入时间


-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber
                                  name:(NSString *)name;

-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber;

-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber
                                  name:(NSString *)name
                               buyTime:(NSString *)buyTime;

-(instancetype) initWithCurrentDecline:(NSString *)currentDecline
                              ninefall:(NSString *)ninefall
                         ninefall_time:(NSInteger)ninefall_time
                          profitnumber:(NSString *)profitnumber
                                  name:(NSString *)name
                               buyTime:(NSString *)buyTime
                          intervalTime:(NSString *)intervalTime;



@end

NS_ASSUME_NONNULL_END
