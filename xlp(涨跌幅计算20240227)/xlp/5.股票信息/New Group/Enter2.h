//
//  Enter2.h
//  gupiao
//
//  Created by xiaobao on 2023/2/6.
//

#import <Foundation/Foundation.h>
#import "InfoModel.h"
//#import "Enter2+method1.h"
NS_ASSUME_NONNULL_BEGIN

//#ifdef DEBUG
//#define NSLog( s, ... ) NSLog( @"<%@:(第%d行)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
//#else
//#define NSLog( s, ... )
//#endif


@interface Enter2 : NSObject

// 卖出点数据
@property (nonatomic,strong) NSMutableArray *paimingList; //!< 排名数据
@property (nonatomic,strong) NSMutableArray *originList; //!< 历史数据    413天 上涨 283% 年化 227%
@property (nonatomic,strong) NSMutableArray *mutArr1; //!< 按照上涨点数排序        187天 上涨 138% 年化 447%
@property (nonatomic,strong) NSMutableArray *mutArr2; //!< 按照年华收益率排序      187天 上涨 138% 年化 447%


// 买入点数据
@property (nonatomic,strong) NSMutableArray<InfoModel *> *infoDatas; // InfoModel
@property (nonatomic,strong) NSMutableArray<InfoModel *> *infoDatas1; // InfoModel 按照上涨点数排序
@property (nonatomic,strong) NSMutableArray<InfoModel *> *infoDatas2; // InfoModel 按照年华收益率排序
@property (nonatomic,strong) NSMutableArray *infoModelPaiming; //!< 排名数据



@property(nonatomic,assign) BOOL open;   //!< YES: 计算当前点位是否可买。 NO:历史最低数据,不参与
@property(nonatomic,copy)void(^showTableviewBlock)();


//按照上涨点数 + 按照上涨速度
-(int)infoModelSort:(NSMutableArray *)mutArr currentmodel:(InfoModel *)currentModel;

//按照上涨点数
-(int)infoModelSort1:(NSMutableArray *)mutArr currentmodel:(InfoModel *)currentModel;

//按照上涨速度
-(int)infoModelSort2:(NSMutableArray *)mutArr currentmodel:(InfoModel *)currentModel;

-(void)printInfoModelSort:(InfoModel *)currentmodel index:(NSInteger)index;


- (void)enter:(NSInteger)type;
@end

NS_ASSUME_NONNULL_END
