//
//  GupiaoHangqingModel.h
//  xlp
//
//  Created by xiaobao on 2023/4/2.
//

#import <JSONModel/JSONModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface GupiaoHangqingModel : JSONModel
@property (nonatomic,copy) NSString *code;
@property (nonatomic,copy) NSString *msg;
@property (nonatomic,copy) NSArray *result;

@end

NS_ASSUME_NONNULL_END
