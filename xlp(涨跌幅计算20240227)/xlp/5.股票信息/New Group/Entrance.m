 //  Entrance.m
//  gupiao
//
//  Created by xiaobao on 2022/11/15.
//

#import "Entrance.h"
#import "StockModel.h"
#import "GupiaoViewController.h"

@interface Entrance ()
@property (nonatomic,strong) NSMutableArray *resultList; //!< 排名数据
@property (nonatomic,strong) NSMutableArray *list; //!< 历史数据
@end



@implementation Entrance


- (void)enter:(GupiaoType) type{
    self.resultList = [NSMutableArray array];
    self.list = [NSMutableArray array];
    [self tiaojiandan];

    [self currentDown75];
    [self chicang];
    [self demo3];
    [self historyBuild];
    [self historyDown75];
    
    
// 2022年
    if(1){
        
//        currentDecline= -87.55% , name = 149.51%😄0.20%   中电兴发002298  -59.84%    2022.4.27                                                       -2
//        currentDecline= -76.32% , name = 124.62%😄-9.28%  神宇股份300563  -59.61%   2022.4.27
//        currentDecline= -81.69% , name = 135.06%😄-1.45%  东港股份002117  -58.08%   2022.4.27
//        currentDecline= -82.60% , name = 111.50%😄-8.88%  上海凤凰600679  -56.92%   2022.4.27
//        currentDecline= -76.07% , name = 118.16%😄-3.77%  金海高科603311  -55.89%
        
        [self printName:@"中电兴发002298" maxPrice:40.81 firstMinPrice:5.07 maxPrice2:12.65 currentPrice:5.08 buyTime:@"2022.4.27"];


        
        
        [self printName:@"三力士002224" maxPrice:25.56 firstMinPrice:4.89 maxPrice2:8.94 currentPrice:3.96 buyTime:@"2022.4.27"];
        [self printName:@"东兴证券601198" maxPrice:42.51 firstMinPrice:7.36 maxPrice2:14.98 currentPrice:7.27 buyTime:@"2022.10.10"];
        [self printName:@"东兴证券601198" maxPrice:42.51 firstMinPrice:7.36 maxPrice2:14.98 currentPrice:7.57 buyTime:@"2022.5.10"];

        [self printName:@"富煌钢构002743" maxPrice:25.62 firstMinPrice:4.82 maxPrice2:10.36 currentPrice:5.03 buyTime:@"2022.4.28"];
        [self printName:@"深圳华强000062" maxPrice:63.29 firstMinPrice:10.56 maxPrice2:20.16 currentPrice:9.95 buyTime:@"2022.4.27"];
        [self printName:@"木林森002745" maxPrice:38.39 firstMinPrice:9.28 maxPrice2:19.13 currentPrice:7.93 buyTime:@"2022.10.11"];
        [self printName:@"绿色动力601330" maxPrice:27.35 firstMinPrice:7.32 maxPrice2:12.29 currentPrice:6.42 buyTime:@"2022.10.31"];
        [self printName:@"三角轮胎601163" maxPrice:49.14 firstMinPrice:9.22 maxPrice2:19.22 currentPrice:9.84 buyTime:@"2022.4.27"];
        [self printName:@"东方证券600958" maxPrice:34.30 firstMinPrice:7.15 maxPrice2:16.96 currentPrice:7.52 buyTime:@"2022.10.28"];
        [self printName:@"富春环保002479" maxPrice:25.08 firstMinPrice:3.40 maxPrice2:8.55 currentPrice:4.16 buyTime:@"2022.4.27"];
        [self printName:@"北京利尔002392" maxPrice:13.67 firstMinPrice:2.88 maxPrice2:6.71 currentPrice:3.17 buyTime:@"2022.4.27"];
        [self printName:@"上海凤凰600679" maxPrice:45.98 firstMinPrice:8.78 maxPrice2:18.57 currentPrice:8.00 buyTime:@"2022.4.27"];
        [self printName:@"博深股份002282" maxPrice:26.27 firstMinPrice:6.24 maxPrice2:13.35 currentPrice:6.15 buyTime:@"2022.4.27"];
        [self printName:@"博深股份002282" maxPrice:26.27 firstMinPrice:6.24 maxPrice2:13.35 currentPrice:6.15 buyTime:@"2022.4.27"];
        [self printName:@"方正证券601901" maxPrice:16.97 firstMinPrice:4.35 maxPrice2:11.90 currentPrice:5.76 buyTime:@"2022.4.27"];
        [self printName:@"华通热力002893" maxPrice:25.47 firstMinPrice:6.11 maxPrice2:13.78 currentPrice:6.45 buyTime:@"2022.4.27"];
        [self printName:@"航天工程603698" maxPrice:54.04 firstMinPrice:9.65 maxPrice2:22.35 currentPrice:10.26 buyTime:@"2022.4.27"];
        [self printName:@"双箭股份002381" maxPrice:21.47 firstMinPrice:4.15 maxPrice2:10.83 currentPrice:5.15 buyTime:@"2022.4.27"];
        [self printName:@"浙江众成002522" maxPrice:49.60 firstMinPrice:3.68 maxPrice2:9.05 currentPrice:4.30 buyTime:@"2022.4.27"];
        [self printName:@"嘉泽新能601619" maxPrice:13.51 firstMinPrice:2.69 maxPrice2:6.40 currentPrice:3.27 buyTime:@"2022.4.27"];
        [self printName:@"华金资本000532" maxPrice:38.00 firstMinPrice:6.84 maxPrice2:18.60 currentPrice:8.99 buyTime:@"2022.4.27"];
        [self printName:@"冠豪高新600433" maxPrice:24.52 firstMinPrice:2.63 maxPrice2:6.22 currentPrice:2.75 buyTime:@"2022.4.27"];
        [self printName:@"远东传动002406" maxPrice:18.65 firstMinPrice:4.18 maxPrice2:8.22 currentPrice:4.07 buyTime:@"2022.4.28"];
        [self printName:@"德联集团002666" maxPrice:20.57 firstMinPrice:2.87 maxPrice2:8.04 currentPrice:3.92 buyTime:@"2022.4.27"];
        [self printName:@"东方精工002611" maxPrice:15.54 firstMinPrice:3.10 maxPrice2:7.31 currentPrice:3.54 buyTime:@"2022.4.27"];
        [self printName:@"杭电股份603618" maxPrice:18.85 firstMinPrice:3.89 maxPrice2:8.90 currentPrice:4.36 buyTime:@"2022.4.27"];
        [self printName:@"新华锦600735" maxPrice:28.53 firstMinPrice:3.71 maxPrice2:10.44 currentPrice:5.18 buyTime:@"2022.4.27"];
        [self printName:@"梅轮电梯603321" maxPrice:22.26 firstMinPrice:5.92 maxPrice2:10.95 currentPrice:5.48 buyTime:@"2022.4.28"];
        [self printName:@"山西证券002500" maxPrice:22.26 firstMinPrice:4.53 maxPrice2:9.74 currentPrice:4.60 buyTime:@"2022.4.27"];
        [self printName:@"合兴包装002228" maxPrice:14.39 firstMinPrice:2.92 maxPrice2:5.97 currentPrice:2.91 buyTime:@"2022.4.27"];
        [self printName:@"东港股份002117" maxPrice:33.32 firstMinPrice:6.19 maxPrice2:14.55 currentPrice:6.10 buyTime:@"2022.4.27"];
        [self printName:@"正裕工业603089" maxPrice:25.18 firstMinPrice:6.12 maxPrice2:14.09 currentPrice:6.34 buyTime:@"2022.4.27"];
        [self printName:@"国元证券000728" maxPrice:21.28 firstMinPrice:4.80 maxPrice2:11.05 currentPrice:5.42 buyTime:@"2022.4.28"];


    }
    [self printName:@"上海机电600835" maxPrice:43.52 firstMinPrice:11.33 maxPrice2:23.09 currentPrice:10.67 buyTime:@"2022.10.11"];
    [self printName:@"康尼机电603111" maxPrice:16.26 firstMinPrice:3.56 maxPrice2:8.50 currentPrice:4.00 buyTime:@"2022.10.11"];
    [self printName:@"康尼机电603111" maxPrice:16.26 firstMinPrice:3.56 maxPrice2:8.50 currentPrice:4.00 buyTime:@"2022.10.11"];
    [self printName:@"中国中冶601618" maxPrice:11.05 firstMinPrice:2.11 maxPrice2:6.22 currentPrice:2.86 buyTime:@"2022.11.1"];
    [self printName:@"重庆建工600939" maxPrice:22.69 firstMinPrice:3.08 maxPrice2:6.84 currentPrice:3.39 buyTime:@"2022.10.31"];
    [self printName:@"瑞茂通600180" maxPrice:47.64 firstMinPrice:4.55 maxPrice2:10.50 currentPrice:5.20 buyTime:@"2022.10.31"];
    [self printName:@"曲美家居603818" maxPrice:40.41 firstMinPrice:6.21 maxPrice2:14.45 currentPrice:5.85 buyTime:@"2022.10.31"];
    [self printName:@"正平股份603843" maxPrice:24.41 firstMinPrice:3.28 maxPrice2:8.08 currentPrice:3.77 buyTime:@"2022.10.31"];
    [self printName:@"东北证券000686" maxPrice:23.88 firstMinPrice:4.63 maxPrice2:12.08 currentPrice:5.97 buyTime:@"2022.5.10"];
    [self printName:@"华安证券600909" maxPrice:12.28 firstMinPrice:3.62 maxPrice2:8.10 currentPrice:3.97 buyTime:@"2022.5.9"];
    [self printName:@"西部证券002673" maxPrice:35.16 firstMinPrice:6.38 maxPrice2:12.47 currentPrice:5.79 buyTime:@"2022.10.10"];
    [self printName:@"传化智联002010" maxPrice:26.23 firstMinPrice:4.36 maxPrice2:10.06 currentPrice:4.90 buyTime:@"2022.10.31"];
    [self printName:@"精艺股份002295" maxPrice:32.52 firstMinPrice:4.89 maxPrice2:10.78 currentPrice:5.40 buyTime:@"2021.2.8"];
    [self printName:@"城市传媒600229" maxPrice:35.69 firstMinPrice:5.18 maxPrice2:9.63 currentPrice:5.20 buyTime:@"2021.7.28"];
    [self printName:@"陕天然气002267" maxPrice:19.19 firstMinPrice:4.71 maxPrice2:9.58 currentPrice:5.20 buyTime:@"2022.3.16"];
    [self printName:@"华明装备002270" maxPrice:20.40 firstMinPrice:2.68 maxPrice2:7.82 currentPrice:3.76 buyTime:@"2021.2.8"];
    [self printName:@"双良节能600481" maxPrice:15.39 firstMinPrice:2.68 maxPrice2:5.02 currentPrice:2.46 buyTime:@"2020.2.4"];
    [self printName:@"康缘药业600557" maxPrice:33.89 firstMinPrice:8.08 maxPrice2:18.58 currentPrice:8.40 buyTime:@"2021.2.4"];
    [self printName:@"第一医药600833" maxPrice:38.11 firstMinPrice:6.87 maxPrice2:15.55 currentPrice:7.74 buyTime:@"2021.11.2"];
    [self printName:@"人民同泰600829" maxPrice:27.88 firstMinPrice:4.76 maxPrice2:11.38 currentPrice:5.08 buyTime:@"2022.4.27"];
    [self printName:@"神宇股份300563" maxPrice:40.46 firstMinPrice:10.56 maxPrice2:23.72 currentPrice:9.58 buyTime:@"2022.4.27"];


    [self printName:@"中核科技000777" maxPrice:54.37 firstMinPrice:8.24 maxPrice2:17.98 currentPrice:8.81 buyTime:@"2022.4.27"];
    [self printName:@"安迪苏600299" maxPrice:31.05 firstMinPrice:8.93 maxPrice2:16.45 currentPrice:7.72 buyTime:@"2022.4.27"];
    [self printName:@"歌力思603808" maxPrice:44.94 firstMinPrice:9.16 maxPrice2:18.27 currentPrice:8.01 buyTime:@"2022.10.11"];
    [self printName:@"东方明珠600637" maxPrice:58.21 firstMinPrice:7.11 maxPrice2:12.57 currentPrice:5.93 buyTime:@"2022.10.31"];
    [self printName:@"北特科技603009" maxPrice:27.13 firstMinPrice:5.09 maxPrice2:11.44 currentPrice:4.80 buyTime:@"2022.4.27"];
    [self printName:@"浙数文化600633" maxPrice:30.55 firstMinPrice:6.66 maxPrice2:12.76 currentPrice:5.35 buyTime:@"2022.4.27"];
    [self printName:@"新华文轩601811" maxPrice:33.75 firstMinPrice:6.86 maxPrice2:15.76 currentPrice:7.48 buyTime:@"2021.2.4"];
    [self printName:@"南京港002040" maxPrice:20.69 firstMinPrice:5.27 maxPrice2:11.65 currentPrice:5.33 buyTime:@"2020.2.4"];
    [self printName:@"海立股份600619" maxPrice:18.87 firstMinPrice:6.13 maxPrice2:12.63 currentPrice:5.46 buyTime:@"2022.4.27"];
    [self printName:@"第一医院600833" maxPrice:38.11 firstMinPrice:6.87 maxPrice2:15.55 currentPrice:7.62 buyTime:@"2022.4.26"];
    [self printName:@"浙江东方600120" maxPrice:12.11 firstMinPrice:3.08 maxPrice2:6.63 currentPrice:3.30 buyTime:@"2022.4.27"];
    [self printName:@"福星股份000926" maxPrice:18.75 firstMinPrice:3.80 maxPrice2:7.65 currentPrice:3.70 buyTime:@"2022.10.31"];
    [self printName:@"歌华有线600037" maxPrice:51.52 firstMinPrice:7.13 maxPrice2:16.88 currentPrice:7.16 buyTime:@"2022.10.25"];
    [self printName:@"新大陆000997" maxPrice:42.58 firstMinPrice:11.27 maxPrice2:21.25 currentPrice:10.61 buyTime:@"2022.4.27"];
    [self printName:@"凤竹纺织600493" maxPrice:26.25 firstMinPrice:4.01 maxPrice2:8.40 currentPrice:3.95 buyTime:@"2021.2.8"];
    [self printName:@"美盈森002303" maxPrice:26.39 firstMinPrice:3.40 maxPrice2:7.02 currentPrice:2.82 buyTime:@"2022.4.27"];
    [self printName:@"新联电子002546" maxPrice:15.81 firstMinPrice:2.79 maxPrice2:6.30 currentPrice:3.11 buyTime:@"2022.4.27"];
    [self printName:@"宁波高发603788" maxPrice:44.55 firstMinPrice:10.88 maxPrice2:19.09 currentPrice:9.54 buyTime:@"2022.4.27"];
    [self printName:@"凯众股份603037" maxPrice:42.57 firstMinPrice:12.99 maxPrice2:25.38 currentPrice:12.59 buyTime:@"2022.4.27"];
    [self printName:@"荣泰健康603579" maxPrice:80.40 firstMinPrice:21.18 maxPrice2:43.79 currentPrice:18.75 buyTime:@"2022.10.25"];
    [self printName:@"华锋股份002806" maxPrice:49.10 firstMinPrice:7.96 maxPrice2:18.68 currentPrice:8.67 buyTime:@"2022.4.27"];
    [self printName:@"中亚股份300512" maxPrice:37.61 firstMinPrice:6.16 maxPrice2:14.56 currentPrice:6.21 buyTime:@"2021.1.14"];
    [self printName:@"洛凯股份603829" maxPrice:29.22 firstMinPrice:8.10 maxPrice2:16.05 currentPrice:8.00 buyTime:@"2022.4.27"];
    [self printName:@"畅联股份603648" maxPrice:32.54 firstMinPrice:6.73 maxPrice2:15.84 currentPrice:7.71 buyTime:@"2022.10.11"];
    [self printName:@"睿能科技603933" maxPrice:32.69 firstMinPrice:7.82 maxPrice2:17.54 currentPrice:8.21 buyTime:@"2022.4.27"];
    [self printName:@"海鸥股份603269" maxPrice:36.28 firstMinPrice:8.50 maxPrice2:19.71 currentPrice:9.76 buyTime:@"2022.4.27"];
    [self printName:@"汉嘉设计300746" maxPrice:43.00 firstMinPrice:8.89 maxPrice2:21.22 currentPrice:8.65 buyTime:@"2022.10.11"];
    [self printName:@"亚普股份603013" maxPrice:46.58 firstMinPrice:9.77 maxPrice2:20.59 currentPrice:9.58 buyTime:@"2022.4.27"];
    [self printName:@"锋龙股份002931" maxPrice:33.97 firstMinPrice:9.81 maxPrice2:20.35 currentPrice:7.92 buyTime:@"2022.4.27"];
    [self printName:@"今创集团603680" maxPrice:29.42 firstMinPrice:7.62 maxPrice2:17.49 currentPrice:6.45 buyTime:@"2022.4.27"];
    [self printName:@"新亚制程002388" maxPrice:16.93 firstMinPrice:4.06 maxPrice2:9.49 currentPrice:3.96 buyTime:@"2022.4.27"];
    [self printName:@"新华文轩601811" maxPrice:33.75 firstMinPrice:6.86 maxPrice2:15.76 currentPrice:7.77 buyTime:@"2022.3.23"];
    [self printName:@"正丹股份300641" maxPrice:17.10 firstMinPrice:3.88 maxPrice2:9.42 currentPrice:4.39 buyTime:@"2022.4.27"];
    [self printName:@"荣晟环保603165" maxPrice:36.49 firstMinPrice:8.69 maxPrice2:21.19 currentPrice:10.45 buyTime:@"2022.10.11"];

    NSLog(@"未记录");
    [self printName:@"先锋电子002767" maxPrice:53.76 firstMinPrice:8.95 maxPrice2:19.01 currentPrice:8.32 buyTime:@"2022.4.27"];
    [self printName:@"节能国祯300388" maxPrice:33.78 firstMinPrice:6.58 maxPrice2:13.60 currentPrice:5.96 buyTime:@"2022.4.27"];
    [self printName:@"重庆燃气600917" maxPrice:20.74 firstMinPrice:5.70 maxPrice2:13.77 currentPrice:6.01 buyTime:@"2022.4.27"];
    [self printName:@"宏辉果蔬603336" maxPrice:18.77 firstMinPrice:5.15 maxPrice2:12.86 currentPrice:5.64 buyTime:@"2022.4.27"];
    [self printName:@"东华能源002221" maxPrice:26.67 firstMinPrice:6.60 maxPrice2:15.93 currentPrice:6.66 buyTime:@"2022.4.28"];
    [self printName:@"方大集团000055" maxPrice:12.25 firstMinPrice:3.37 maxPrice2:8.37 currentPrice:3.61 buyTime:@"2022.4.27"];
    [self printName:@"光大嘉宝600622" maxPrice:12.57 firstMinPrice:2.42 maxPrice2:5.99 currentPrice:2.57 buyTime:@"2022.10.31"];
    [self printName:@"中亚股份300512" maxPrice:37.61 firstMinPrice:6.16 maxPrice2:14.56 currentPrice:6.58 buyTime:@"2022.10.11"];
    [self printName:@"海格通信002465" maxPrice:24.65 firstMinPrice:6.48 maxPrice2:15.68 currentPrice:7.57 buyTime:@"2022.10.12"];
    [self printName:@"浙江东日600113" maxPrice:24.65 firstMinPrice:5.24 maxPrice2:12.16 currentPrice:5.86 buyTime:@"2022.10.11"];
    [self printName:@"浙江仙通603239" maxPrice:33.81 firstMinPrice:7.33 maxPrice2:18.14 currentPrice:8.53 buyTime:@"2022.4.27"];
    [self printName:@"中远海科002401" maxPrice:31.05 firstMinPrice:6.66 maxPrice2:14.65 currentPrice:6.61 buyTime:@"2022.4.27"];
    [self printName:@"碧水源300070" maxPrice:25.72 firstMinPrice:5.77 maxPrice2:11.64 currentPrice:4.37 buyTime:@"2022.4.27"];
    [self printName:@"纽威股份603699" maxPrice:33.59 firstMinPrice:8.07 maxPrice2:16.67 currentPrice:6.72 buyTime:@"2022.4.27"];

    

//001 历史最低点买点数据
    
    [self summaryUp];
}
#pragma mark - 统计从底部到顶部上涨多少点
-(void)summaryUp{
    NSLog(@"");
    NSLog(@"=======统计从底部到顶部上涨多少点 start==========");
    NSMutableArray *mutArr = [NSMutableArray array];
    NSArray *arr1 = @[@"411",@"397",@"361",@"446",@"218",@"283",@"919",@"477",@"482",@"251"];
    NSArray *arr2 = @[@"205",@"238",@"257",@"326",@"198",@"479",@"275",@"276",@"368",@"173"];
    NSArray *arr3 = @[@"154",@"457",@"205",@"216",@"405",@"187",@"416",@"223",@"481",@"672"];
    NSArray *arr4 = @[@"224",@"390",@"314",@"389",@"297",@"250",@"247",@"206",@"591",@"357"];
    NSArray *arr5 = @[@"358",@"251",@"143",@"242",@"173",@"519",@"621",@"234",@"342",@"291"];
    NSArray *arr6 = @[@"306",@"379",@"1003",@"264",@"202",@"463",@"919",@"242",@"479",@"271"];
    NSArray *arr7 = @[@"205",@"189",@"213",@"259",@"264",@"351",@"178",@"482",@"296",@"257"];
    NSArray *arr8 = @[@"187",@"411",@"291",@"238",@"174",@"349",@"247",@"216",@"457",@"198"];
    NSArray *arr9 = @[@"257",@"482",@"449",@"374",@"368",@"351",@"251"];
    [mutArr addObjectsFromArray:arr1];
    [mutArr addObjectsFromArray:arr2];
    [mutArr addObjectsFromArray:arr3];
    [mutArr addObjectsFromArray:arr4];
    [mutArr addObjectsFromArray:arr5];
    [mutArr addObjectsFromArray:arr6];
    [mutArr addObjectsFromArray:arr7];
    [mutArr addObjectsFromArray:arr8];
    [mutArr addObjectsFromArray:arr9];
    [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
        int value1 = [obj1 intValue];
        int value2 = [obj2 intValue];
        if(value1 < value2){
            return NSOrderedAscending;
        } else if (value1 > value2){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSString * str1 in mutArr) {
        [tempArr addObject:str1];
        if(tempArr.count == 10){
            NSLog(@"%@",[tempArr componentsJoinedByString:@","]);
            [tempArr removeAllObjects];
        }
    }
    if([tempArr count] > 0 ){
        NSLog(@"%@",[tempArr componentsJoinedByString:@","]);
    }
    NSLog(@"=======统计从底部到顶部上涨多少点 end==========");
    
//    NSLog(@"%@",mutArr);
}



// currentDecline= -84.51% , name = 82.82%😄-19.02%  三力士002224  -55.70%   2022.4.27

#pragma mark - 买点数据
#pragma mark - currentDecline= -84.51% , name = 82.82%😄-19.02%  三力士002224  -55.70%   2022.4.27
-(void)printName:(NSString *)name
        maxPrice:(double)maxPrice
   firstMinPrice:(double)firstMinPrice
       maxPrice2:(double)maxPrice2
    currentPrice:(double)currentPrice
        buyTime:(NSString *)buyTime {
    double result1 =  ((currentPrice - maxPrice) / maxPrice * 100);
    double result2 =  ((maxPrice2 - firstMinPrice) / firstMinPrice * 100);
    double result3 =  ((currentPrice - firstMinPrice) / firstMinPrice * 100);
    double result4 =  ((currentPrice - maxPrice2) / maxPrice2 * 100);
    
    NSString *content = [NSString stringWithFormat:@"currentDecline= %.2f%% , name = %.2f%%😄%.2f%%  %@  %.2f%%   %@",result1,result2,result3,name,result4,buyTime];
    NSLog(@"%@",content);
}




#pragma mark - 历史下跌75
-(void)historyDown75{
    NSLog(@"\n历史下跌75========================================start");
    NSMutableArray *list = [NSMutableArray array];
//    [list addObject:[self maxValue:@"326.30" current:49.88 name:@"吉比特603444" buyTime:@"2018.10.19"]];
//    [list addObject:[self maxValue:@"643.60" current:231.51 name:@"吉比特603444" buyTime:@"2022.10.25"]];
    
    if(/* DISABLES CODE */ (0)){
        [list addObject:[self maxValue:@"519.7" current:154.3 name:@"金山办公688111" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"519.7" current:159 name:@"金山办公688111" buyTime:@"2022.8.29"]];
        [list addObject:[self maxValue:@"570.46" current:190.38 name:@"美迪西688202" buyTime:@"2022.10.12"]];
        [list addObject:[self maxValue:@"398.49" current:129.38 name:@"爱博医疗688050" buyTime:@"2022.3.16"]];
        [list addObject:[self maxValue:@"474.97" current:124.29 name:@"中望软件688083" buyTime:@"2022.4.28"]];
        [list addObject:[self maxValue:@"474.97" current:149.09 name:@"中望软件688083" buyTime:@"2022.10.12"]];
        [list addObject:[self maxValue:@"477.90" current:133.04 name:@"心脉医疗688016" buyTime:@"2022.8.24"]];
        [list addObject:[self maxValue:@"302.05" current:91.35 name:@"芯源微688037" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"402.19" current:140.12 name:@"键凯科技688356" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"629.13" current:158.88 name:@"极米科技688696" buyTime:@"2022.10.31"]];
        [list addObject:[self maxValue:@"521.4" current:130.86 name:@"长春高新000661" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"324.90" current:83.83 name:@"奥普特688686" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"417.18" current:89.01 name:@"恒玄科技688608" buyTime:@"2022.10.11"]];
        [list addObject:[self maxValue:@"123.32" current:37.86 name:@"瑞丰新材300910" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"421.99" current:110.02 name:@"通策医疗600763" buyTime:@"2022.9.7"]];
        [list addObject:[self maxValue:@"50.12" current:17.26 name:@"舍得酒业600702" buyTime:@"2018.10.30"]];
        [list addObject:[self maxValue:@"334.78" current:113.8 name:@"泰坦科技688133" buyTime:@"2022.9.26"]];
        [list addObject:[self maxValue:@"576.00" current:79.75 name:@"晶丰明源688368" buyTime:@"2022.10.11"]];
        [list addObject:[self maxValue:@"210.62" current:47.02 name:@"捷佳伟创300724" buyTime:@"2022.4.26"]];
        [list addObject:[self maxValue:@"27.97" current:9.05 name:@"永兴材料002756" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"225.3" current:77.60 name:@"兴齐眼药300573" buyTime:@"2022.4.25"]];
        [list addObject:[self maxValue:@"84.89" current:19.95 name:@"中科创达300496" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"37.8" current:10.25 name:@"振华科技000733" buyTime:@"2018.10.16"]];
        [list addObject:[self maxValue:@"22.21" current:4.79 name:@"阳光电源300274" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"314.09" current:100.51 name:@"华熙生物688363" buyTime:@"2022.3.16"]];
        [list addObject:[self maxValue:@"239.30" current:72.87 name:@"建龙微纳688357" buyTime:@"2022.10.28"]];
        [list addObject:[self maxValue:@"54.94" current:13.91 name:@"胜华新材603026" buyTime:@"2018.2.9"]];
        [list addObject:[self maxValue:@"302.02" current:70.62 name:@"乐鑫科技688018" buyTime:@"2022.10.11"]];
        [list addObject:[self maxValue:@"298.00" current:76.7 name:@"中微公司688012" buyTime:@"2022.10.12"]];
        [list addObject:[self maxValue:@"159.88" current:53.64 name:@"盐津铺子002847" buyTime:@"2022.3.16"]];
        [list addObject:[self maxValue:@"47.2" current:7.41 name:@"坚朗五金002791" buyTime:@"2018.10.18"]];
        [list addObject:[self maxValue:@"61.3" current:15.18 name:@"天齐锂业002466" buyTime:@"2020.4.29"]];
        [list addObject:[self maxValue:@"303.57" current:75.02 name:@"财富趋势688318" buyTime:@"2022.10.11"]];
        [list addObject:[self maxValue:@"230.65" current:64.02 name:@"南微医学688029" buyTime:@"2022.9.27"]];
        [list addObject:[self maxValue:@"48.23" current:13.74 name:@"赣锋锂业002460" buyTime:@"2019.8.15"]];
        [list addObject:[self maxValue:@"155.85" current:40.69 name:@"甘源食品002991" buyTime:@"2022.4.29"]];
        [list addObject:[self maxValue:@"21.33" current:4.65 name:@"东方电缆603606" buyTime:@"2018.2.9"]];
        [list addObject:[self maxValue:@"55.25" current:14.85 name:@"文灿股份603348" buyTime:@"2020.4.28"]];
        [list addObject:[self maxValue:@"33.41" current:9.32 name:@"鹏辉能源300438" buyTime:@"2019.8.9"]];
        [list addObject:[self maxValue:@"136.33" current:42.28 name:@"恒立液压601100" buyTime:@"2022.10.11"]];
        [list addObject:[self maxValue:@"137.85" current:39.98 name:@"安图生物603658" buyTime:@"2022.4.26"]];
        [list addObject:[self maxValue:@"164.10" current:38.63 name:@"小熊电器002959" buyTime:@"2022.3.16"]];
        [list addObject:[self maxValue:@"56.74" current:14.54 name:@"华友钴业603799" buyTime:@"2019.5.20"]];
        [list addObject:[self maxValue:@"260.59" current:53.42 name:@"华大基因300676" buyTime:@"2019.6.6"]];
        [list addObject:[self maxValue:@"171.36" current:43.45 name:@"闻泰科技600745" buyTime:@"2022.10.28"]];
        [list addObject:[self maxValue:@"47.82" current:12.12 name:@"健民集团600976" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"47.82" current:13.03 name:@"健民集团600976" buyTime:@"2019.8.6"]];
        [list addObject:[self maxValue:@"47.82" current:13.50 name:@"健民集团600976" buyTime:@"2020.5.22"]];
        [list addObject:[self maxValue:@"89.76" current:22.86 name:@"顶点软件603383" buyTime:@"2021.3.11"]];
        [list addObject:[self maxValue:@"147.26" current:21.8 name:@"卓易信息688258" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"159.38" current:32.58 name:@"映翰通688080" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"64.03" current:10.57 name:@"安孚科技603031" buyTime:@"2019.1.31"]];
        [list addObject:[self maxValue:@"39.90" current:7.5 name:@"钢研高纳300034" buyTime:@"2018.2.9"]];
        [list addObject:[self maxValue:@"39.90" current:8.04 name:@"钢研高纳300034" buyTime:@"2018.12.28"]];
        [list addObject:[self maxValue:@"48.14" current:9.31 name:@"迎驾贡酒603198" buyTime:@"2018.10.30"]]; // 6月
        [list addObject:[self maxValue:@"37.67" current:5.97 name:@"科华数据002335" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"37.67" current:7.96 name:@"科华数据002335" buyTime:@"2019.11.29"]];
        [list addObject:[self maxValue:@"39.58" current:9.95 name:@"华特达因000915" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"108.98" current:25.23 name:@"信捷电气603416" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"47.86" current:10.48 name:@"丰元股份002805" buyTime:@"2019.8.6"]];// 6月
        [list addObject:[self maxValue:@"81.44" current:18.76 name:@"航发动力600893" buyTime:@"2019.11.15"]];// 17月
        [list addObject:[self maxValue:@"81.44" current:18.67 name:@"航发动力600893" buyTime:@"2020.2.4"]]; //20月
        [list addObject:[self maxValue:@"63.78" current:11.15 name:@"金磊雷份300443" buyTime:@"2020.2.4"]];
        [list addObject:[self maxValue:@"102.37" current:14.73 name:@"嘉澳环保603822" buyTime:@""]];
        [list addObject:[self maxValue:@"80.68" current:21.65 name:@"盘龙药业002864" buyTime:@"2021.10.27"]]; // 8.5月
        [list addObject:[self maxValue:@"33.68" current:4.97 name:@"TCL中环002129" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"163.35" current:32.28 name:@"豪悦护理605009" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"17.68" current:2.99 name:@"大金重工002487" buyTime:@"2018.10.19"]];
        [list addObject:[self maxValue:@"155.15" current:26.96 name:@"康泰生物300601" buyTime:@"2022.10.12"]];
        [list addObject:[self maxValue:@"86.77" current:21.53 name:@"良品铺子603719" buyTime:@"2022.4.27"]];
        [list addObject:[self maxValue:@"42.91" current:12.23 name:@"华图山鼎300492" buyTime:@"2019.6.10"]];
        [list addObject:[self maxValue:@"42.91" current:12.40 name:@"华图山鼎300492" buyTime:@"2019.8.6"]];
        [list addObject:[self maxValue:@"46.47" current:9.54 name:@"多氟多002407" buyTime:@"2020.2.4"]];
        [list addObject:[self maxValue:@"46.47" current:9.57 name:@"多氟多002407" buyTime:@"2020.4.28"]];
        [list addObject:[self maxValue:@"52.28" current:11.84 name:@"康隆达603665" buyTime:@"2020.2.4"]];
        [list addObject:[self maxValue:@"101.64" current:14.35 name:@"欣锐科技300745" buyTime:@"2021.2.8"]];
        [list addObject:[self maxValue:@"64.75" current:13.69 name:@"赛腾股份603283" buyTime:@"2022.4.27"]];
    }
  
    
    [list addObject:[self maxValue:@"68.17" current:14.25 name:@"102.59%😄27.23% 安靠智电300617 -37.2% " buyTime:@"2019.11.28" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"33.49" current:5.67 name:@"58.82%😄-4.71% 海天精工601882 -40%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
//    [list addObject:[self maxValue:@"76.44" current:19.83 name:@"中科软603927" buyTime:@"2022.4.28" firstLowTime:@"2021.2.8"]];
    [list addObject:[self maxValue:@"50.34" current:15.06 name:@"106.82%😄16.74% 太极股份002368 -43.55%" buyTime:@"2018.12.20" firstLowTime:@"2018.2.7"]];
    [list addObject:[self maxValue:@"38.85" current:16.61 name:@"131.28%😄52.81% 莱克电气603355 -33.93%" buyTime:@"2021.2.26" firstLowTime:@"2019.8.12"]];
    [list addObject:[self maxValue:@"36.81" current:11.21 name:@"94.59%😄14.39% 国轩高科002074 -41.22%" buyTime:@"2019.8.15" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"36.6" current:6.6 name:@"87.70%😄15.99% 川仪股份603100 -38.2%" buyTime:@"2020.2.6" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"160.43" current:23.93 name:@"120.52%😄-10.71% 沃尔德688028 -59.51%" buyTime:@"2022.4.27" firstLowTime:@"2021.2.8"]];
    [list addObject:[self maxValue:@"88.71" current:21.98 name:@"118.30%😄11.12% 金牌橱柜603180 -49.10%" buyTime:@"2019.8.6" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"32.05" current:11.00 name:@"79.84%😄24.58% 快克股份603203 -30.73%" buyTime:@"2019.11.18" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"55.04" current:11.42 name:@"110.03%😄30.22% 索通发展603612 -38%" buyTime:@"2021.1.14" firstLowTime:@"2018.10.30"]];
    [list addObject:[self maxValue:@"22.34" current:6.16 name:@"136.88%😄17.11% 汉钟精机002158 -50.56%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"34.34" current:6.20 name:@"87.04%😄19.92% 永太科技002326 -35.88%" buyTime:@"2019.8.6" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"44.89" current:10.33 name:@"64.05%😄-4.53% 航发控制000738 -40.36%" buyTime:@"2020.2.4" firstLowTime:@"2018.2.7"]];
    [list addObject:[self maxValue:@"36.71" current:8.52 name:@"90.28%😄-3.73% 特一药业002728 -49.41%" buyTime:@"2021.2.5" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"49.23" current:14.35 name:@"微光股份002801 -26.18%" buyTime:@"2020.4.9"]];
    [list addObject:[self maxValue:@"40.08" current:12.22 name:@"70.99%😄16.60% 江苏雷利300660 -31.81%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"48.09" current:17.18 name:@"46.14%😄2.81% 奇正藏药002287 -29.65%" buyTime:@"2020.5.25" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"44.01" current:13.54 name:@"96.33%😄8.06% 恒锋工具300488 -44.96%" buyTime:@"2020.4.28" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"30.09" current:11.83 name:@"171.57%😄67.33% 天孚通信300394 -38.39%" buyTime:@"2019.5.20"]];
    [list addObject:[self maxValue:@"80.06" current:8.98 name:@"89.02%😄-22.39% 新开源300109 -53.76%" buyTime:@"2021.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"25.28" current:8.49 name:@"88.86%😄9.97% 金徽酒603919 -41.77%" buyTime:@"2020.3.19" firstLowTime:@"2019.2.1"]];
    [list addObject:[self maxValue:@"35.25" current:11.35 name:@"60.02%😄4.80% 大元泵业603757 -34.51%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"37.67" current:8.59 name:@"65.95%😄-2.63% 海思科002653 -38.16%" buyTime:@"2019.1.31" firstLowTime:@"2018.2.9"]];
    [list addObject:[self maxValue:@"38.38" current:8.19 name:@"120.18%😄20.62% 西藏珠峰600338 -45.22%" buyTime:@"2021.2.8" firstLowTime:@"2020.4.28"]];
    [list addObject:[self maxValue:@"34.69" current:5.81 name:@"79.97%😄-2.19% 泰嘉股份002843 -45%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"34.69" current:6.54 name:@"88.98%😄12.56% 泰嘉股份002843 -40.44%" buyTime:@"2021.2.9" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"34.69" current:7.31 name:@"99.85%😄11.77% 泰嘉股份002843 -44.07%" buyTime:@"2022.4.27" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"41.28" current:12.2 name:@"44.45%😄10.91% 四川双马000935 -23.22%" buyTime:@"2021.6.9" firstLowTime:@"2020.2.4"]];
 // start 2023.1.5 13:48
    [list addObject:[self maxValue:@"28.37" current:7.32 name:@"56.82%😄9.75% 金诚信603979 -30.02%" buyTime:@"2020.4.2" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"35.22" current:11.96 name:@"103.25%😄14.45% 中大力德002896 -43.69%" buyTime:@"2021.2.9" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"35.22" current:11.48 name:@"101.72%😄9.86% 中大力德002896 -45.95%" buyTime:@"2022.4.27" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"20.95" current:6.2 name:@"85.68%😄38.7% 华荣股份603855 -25.3%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"42.05" current:9.25 name:@"67.57%😄-7.13% 川恒股份002895 -44.58%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.30"]];
    [list addObject:[self maxValue:@"42.05" current:9.40 name:@"63.89%😄1.62% 川恒股份002895 -37.99%" buyTime:@"2021.4.29" firstLowTime:@"2018.10.30"]];
    [list addObject:[self maxValue:@"26.81" current:9.03 name:@"85.85%😄24.04% 巨星科技002444 -33.26%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"35.45" current:6.45 name:@"140.50%😄61.25% 昌红科技300151 -32.95%" buyTime:@"2020.3.24" firstLowTime:@"2018.10.182"]];
    [list addObject:[self maxValue:@"48.65" current:13.80 name:@"65.68%😄7.64% 长城科技603897 -35.03%" buyTime:@"2021.2.4" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"36.37" current:7.44 name:@"80.17%😄6.13% 许继电气000400 -41.09%" buyTime:@"2019.8.15" firstLowTime:@"2018.7.11"]];
    [list addObject:[self maxValue:@"45.37" current:10.26 name:@"43.16%😄-6.98% 威星智能002849 -35.02%" buyTime:@"2021.2.4" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"45.37" current:9.87 name:@"61.99%😄-3.80% 威星智能002849 -40.61%" buyTime:@"2022.4.27" firstLowTime:@"2021.2.4"]];
    [list addObject:[self maxValue:@"31.51" current:6.41 name:@"65.08%😄8.64% 银都股份603277 -34.19%" buyTime:@"2020.3.30" firstLowTime:@"2018.10.195"]];
    [list addObject:[self maxValue:@"59.05" current:11.77 name:@"133.66%😄43.01% 汉威科技300007 -38.79%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"85.96" current:13.69 name:@"83.72%😄-0.94% 丝路视觉300556 -46.08%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"85.96" current:14.79 name:@"121.11%😄8.04% 丝路视觉300556 -51.14%" buyTime:@"2021.5.10" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"37.01" current:11.30 name:@"150.38%😄43.40% 旺能环境002034 -42.73%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"21.88" current:6.81 name:@"57.23%😄8.27% 杭叉集团603298 -31.14%" buyTime:@"2019.8.13" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"21.88" current:6.91 name:@"57.23%😄9.86% 杭叉集团603298 -30.13%" buyTime:@"2020.3.30" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"46.66" current:8.42 name:@"64.80%😄0.12% 润和软件300339 -39.25%" buyTime:@"2019.1.29" firstLowTime:@"2018.2.7"]];
    [list addObject:[self maxValue:@"29.56" current:10.66 name:@"138.13%😄53.38% 贵航股份600523 -35.59%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"57.62" current:13.1 name:@"51.71%😄4.05% 大千生态603955 -31.41%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"57.62" current:11.49 name:@"60.99%😄12.29%  大千生态603955 -45.52%" buyTime:@"2021.2.4" firstLowTime:@"2019.1.31"]];
    // start 2023.1.5 15:18
    [list addObject:[self maxValue:@"57.62" current:11.27 name:@"55.61%😄-1.91% 大千生态603955 -36.97%" buyTime:@"2022.4.28" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"34.61" current:11.07 name:@"206.72%😄106.53% 日月股份603218 -32.66%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"50.85" current:9.76 name:@"108.10%😄8.32% 海兴电力603556 -47.95%" buyTime:@"2022.4.27" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"60.55" current:11.33 name:@"259.55%😄80.41% 焦点科技002315 -49.82%" buyTime:@"2020.5.25" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"27.35" current:7.52 name:@"78.24%😄20.32% 山河药铺300452 -32.50%" buyTime:@"2020.2.3" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"17.64" current:6.63 name:@"148.37%😄34.76% 华宏科技002645 -45.74%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"31.93" current:8.46 name:@"156.20%😄47.64% 浙农股份002758 -37.43%" buyTime:@"2021.7.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"36.72" current:10.15 name:@"105.66%😄40.19% 光大证券601788 -31.83%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.15"]];
    [list addObject:[self maxValue:@"39.86" current:6.84 name:@"90.73%😄3.95% 天奇股份002009 -45.5%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"22.51" current:4.39 name:@"120.35%😄-2.88% 振华股份603067 -55.92%" buyTime:@"2020.2.4" firstLowTime:@"2018.2.7"]];
    [list addObject:[self maxValue:@"19.35" current:2.86 name:@"135.77%😄10% 四方股份601126 -53.34%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"12.46" current:2.82 name:@"81.69%😄-0.7% 鄂尔多斯600295 -45.35%" buyTime:@"2020.4.28" firstLowTime:@"2019.1.29"]];
    [list addObject:[self maxValue:@"55.24" current:11.18 name:@"71.38%😄-3.62% 拉芳家化603630 -43.76%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"24.56" current:6.55 name:@"81.69%😄2.50% 众源新材603527 -43.58%" buyTime:@"2021.1.14" firstLowTime:@"2019.8.6"]];
    [list addObject:[self maxValue:@"24.56" current:7.22 name:@"107.79%😄10.23% 众源新材603527 -46.95%" buyTime:@"2022.4.27" firstLowTime:@"2019.8.6"]];
    [list addObject:[self maxValue:@"33.45" current:5.10 name:@"131.97%😄15.65% 湖南发展000722 -50.15%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"22.08" current:6.72 name:@"92.97%😄47.69% 康莱德603987 -23.46%" buyTime:@"2019.12.3" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"27.25" current:9.06 name:@"118.50%😄38.53% 中鼎股份000887 -36.6%" buyTime:@"2021.4.13" firstLowTime:@"2002.4.28"]];
    [list addObject:[self maxValue:@"24.00" current:5.39 name:@"82.12%😄12.06% 建发股份600153 -38.47%" buyTime:@"2020.3.19" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"25.71" current:5.07 name:@"158.41%😄15.23% 海德股份000567 -55.41%" buyTime:@"2021.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"24.23" current:7.64 name:@"76.70%😄17.90% 北大荒600598 -33.28%" buyTime:@"2020.2.3" firstLowTime:@"2019.1.31"]];
//    [list addObject:[self maxValue:@"35.45" current:6.46 name:@"创维数字000810 -60.49%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.16"]];
    [list addObject:[self maxValue:@"39.63" current:6.11 name:@"65.94%😄-4.98% 五洲新春603667 -42.74%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"46.89" current:12.15 name:@"97.50%😄16.83% 长缆科技002879 -40.85%" buyTime:@"2021.2.3" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"27.49" current:7.79 name:@"89.02%😄18.75% 神力股份603819 -37.18%" buyTime:@"2020.2.4" firstLowTime:@"2018.2.6"]];
    [list addObject:[self maxValue:@"33.8" current:8.02 name:@"159.63%😄86.08% 博实股份002698 -28.33%" buyTime:@"2020.3.24" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"39.87" current:9.59 name:@"70.40%😄11.77% 三星新材603578 -34.40%" buyTime:@"2021.2.5" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"27.91" current:7.92 name:@"110%😄27.74% 中科三环000970 -39.17%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"23.48" current:5.20 name:@"48.31%😄-12.16% 卧龙电驱600580 -40.77%" buyTime:@"2018.10.18" firstLowTime:@"2017.6.2"]];
    [list addObject:[self maxValue:@"40.81" current:5.08 name:@"149.51%😄0.2% 中电兴发002298 -59.84%" buyTime:@"2022.4.27" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"47.64" current:5.20 name:@"130.77%😄14.29% 瑞茂通600180 -50.48%" buyTime:@"2022.10.31" firstLowTime:@"2021.1.29"]];
    [list addObject:[self maxValue:@"20.65" current:3.05 name:@"131.86%😄-3.79% 天壕环境300332 -58.50%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"22.57" current:5.31 name:@"98.82%😄25.83% 三星医疗601567 -36.71%" buyTime:@"2021.1.29" firstLowTime:@"2018.10.26"]];
    [list addObject:[self maxValue:@"29.86" current:7.83 name:@"58.09%😄-7.56% 锡业股份000960 -41.52%" buyTime:@"2020.3.23" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"18.51" current:5.11 name:@"48.46%😄-7.26% 山东章鼓002598 -37.53%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"27.05" current:7.39 name:@"110.36%😄25.47% 江苏新能603693 -40.36%" buyTime:@"2021.2.8" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"26.93" current:7.30 name:@"97.89%😄10.11% 三祥新材603663 -44.36%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"21.49" current:6.46 name:@"114.13%😄18.53% 英飞特300582 -44.64%" buyTime:@"2019.11.29" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"22.86" current:6.49 name:@"71.98%😄0.46% 晨化股份300610 -41.58%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"29.66" current:12.85 name:@"77.88%😄46.52% 招商证券600999 -17.63%" buyTime:@"2020.3.19" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"72.75" current:11.11 name:@"88.95%😄4.03% 达威股份300535 -44.95%" buyTime:@"2021.1.13" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"33.47" current:7.26 name:@"85.36%😄19.41% 启明信息002232 -35.58%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"33.47" current:9.25 name:@"133.88%😄27.41% 启明信息002232 -45.52%" buyTime:@"2021.10.28" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"34.10" current:9.10 name:@"118.16%😄7.31% 金海高科603311 -50.81%" buyTime:@"2021.10.28" firstLowTime:@"2019.8.6"]];
     [list addObject:[self maxValue:@"35.14" current:9.76 name:@"121.26%😄43.11% 海信视像600060 -35.32%" buyTime:@"2021.1.12" firstLowTime:@"2019.8.15"]];
    [list addObject:[self maxValue:@"56.64" current:14.30 name:@"126.72%😄61.22% 网达软件603189 -28.89%" buyTime:@"2019.12.3" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"43.92" current:7.72 name:@"110.95%😄24.32% 海波重科300517 -41.07%" buyTime:@"2021.1.11" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"62.38" current:9.35 name:@"75.71%😄6.13% 上海亚虹603159 -39.60%" buyTime:@"2021.1.14" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"16.77" current:5.31 name:@"149.83%😄76.41% 四方达300179 -29.39%" buyTime:@"2020.4.1" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"43.52" current:12.36 name:@"74.32%😄9.09% 上海机电600835 -37.42%" buyTime:@"2020.3.19" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"35.23" current:7.42 name:@"87.37%😄-21.08% 易明医药002826 -55.35%" buyTime:@"2021.2.8" firstLowTime:@"2019.8.6"]];
    
    [list addObject:[self maxValue:@"41.85" current:10.04 name:@"183.93%😄56.63% 盛大资源000603 -44.84%" buyTime:@"2020.3.17" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"12.42" current:3.22 name:@"111.06%😄48.39% 东材科技601208 -29.69%" buyTime:@"2019.11.18" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"32.93" current:7.45 name:@"190.87%😄51.12% 拓尔思300229 -48.05%" buyTime:@"2021.1.8" firstLowTime:@"2018.10.25"]];
    [list addObject:[self maxValue:@"23.52" current:6.81 name:@"86.61%😄12.56% 北化股份002246 -39.68%" buyTime:@"2019.11.18" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"23.52" current:5.92 name:@"86.61%😄-2.15% 北化股份002246 -42.41%" buyTime:@"2021.2.9" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"20.97" current:6.34 name:@"99.17%😄32.08% 裕兴股份300305 -34.30%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.11"]];
    [list addObject:[self maxValue:@"26.57" current:6.79 name:@"255.26%😄98.54% 万向德农600371 -44.12%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"13.67" current:4.85 name:@"191.05%😄49.69% 雅本化学300261 -48.57%" buyTime:@"2021.2.8" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"40.10" current:8.96 name:@"115.28%😄15.02% 名雕股份002830 -46.57%" buyTime:@"2022.4.27" firstLowTime:@"2021.2.9"]];
    [list addObject:[self maxValue:@"30.42" current:10.83 name:@"184.60%😄79.30% 先进数通300541 -37.00%" buyTime:@"2020.4.1" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"53.54" current:7.39 name:@"71.64%😄-1.60% 华钰矿业601020 -42.67%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"39.09" current:7.02 name:@"104.19%😄22.51% 航天晨光600501 -40.00%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"39.09" current:8.31 name:@"102.28%😄18.38% 航天晨光600501 -41.48%" buyTime:@"2021.2.8" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.69" current:6.26 name:@"97.15%😄37.28% 丰原药业000153 -30.37%" buyTime:@"2020.3.19" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"37.98" current:12.15 name:@"174.62%😄53.41% 上海沪工603131 -44.14%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"21.33" current:7.76 name:@"82.86%😄22.01% 佛燃能源002911 -33.28%" buyTime:@"2021.2.1" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"21.33" current:7.75 name:@"101.42%😄21.86% 佛燃能源002911 -39.50%" buyTime:@"2022.4.27" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"68.90" current:13.58 name:@"103.29%😄24.02% 博迈科603727 -38.99%" buyTime:@"2020.4.28" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"41.14" current:6.76 name:@"78.78%😄-19.43% 纳尔股份002825 -54.93%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"41.14" current:8.46 name:@"102.81%😄25.15% 纳尔股份002825 -38.29%" buyTime:@"2020.5.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"32.22" current:9.87 name:@"130.61%😄43.88% 汇中股份300371 -37.61%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"24.04" current:5.42 name:@"158.33%😄29.05% 法兰泰克603966 -50.05%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"48.28" current:6.67 name:@"124.45%😄-1.77% 诚益通300430 -56.23%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.85" current:7.21 name:@"120.21%😄28.98% 万盛股份603010 -41.43%" buyTime:@"2020.4.2" firstLowTime:@"2019.1.30"]];
    [list addObject:[self maxValue:@"49.14" current:11.53 name:@"67.90%😄25.05% 三角轮胎601163 -25.52%" buyTime:@"2020.3.30" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"34.50" current:7.59 name:@"87.33%😄-5.71% 丽岛新材603937 -49.67%" buyTime:@"2021.2.4" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"34.50" current:7.73 name:@"114.36%😄1.84% 丽岛新材603937 -52.49%" buyTime:@"2022.4.27" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"39.63" current:8.95 name:@"137.63%😄51.69% 大恒科技600288 -36.16%" buyTime:@"2021.2.9" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"54.04" current:9.77 name:@"72.12%😄1.24% 航天工程603698 -41.18%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"31.47" current:9.58 name:@"162.87%😄28.41% 大丰实业603081 -46.84%" buyTime:@"2021.1.11" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"20.72" current:4.41 name:@"102.12%😄16.98% 宝光股份600379 -42.13%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"41.35" current:7.08 name:@"47.03%😄-17.58% 四方科技603339 -43.94%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"24.35" current:5.31 name:@"93.10%😄18.26% 苏盐井神603299 -38.75%" buyTime:@"2020.3.23" firstLowTime:@"2018.12.28"]];
    [list addObject:[self maxValue:@"30.28" current:6.60 name:@"93.17%😄12.63% 时代出版600551 -41.70%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"23.39" current:6.14 name:@"104.14%😄27.12% 千金药业600479 -37.73%" buyTime:@"2020.2.3" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"11.60" current:3.09 name:@"100.77%😄18.85% 滨江集团002244 -40.80%" buyTime:@"2019.9.2" firstLowTime:@"2018.10.16"]];
    [list addObject:[self maxValue:@"22.16" current:5.59 name:@"101.94%😄20.73% 天通股份600330 -40.21%" buyTime:@"2019.11.14" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"27.87" current:9.88 name:@"59.83%😄4.99% 东珠生态603359 -34.31%" buyTime:@"2019.12.4" firstLowTime:@"2019.1.30"]];
    [list addObject:[self maxValue:@"36.49" current:9.41 name:@"77.79%😄8.29% 荣晟环保603165 -39.09%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.30"]];
    [list addObject:[self maxValue:@"37.53" current:8.00 name:@"62.53%😄-7.19% 康普顿603798 -42.90%" buyTime:@"2021.2.8" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"38.00" current:9.43 name:@"116.96%😄37.87% 华金资本000532 -40.63%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"51.87" current:6.29 name:@"157.97%😄1.29% 长亮科技300348 -60.74%" buyTime:@"2019.1.31" firstLowTime:@"2016.3.1 "]];
    [list addObject:[self maxValue:@"20.45" current:6.14 name:@"55.86%😄5.86% 云南能投002053 -32.08%" buyTime:@"2021.2.1" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"23.06" current:6.39 name:@"80.07%😄18.99% 福能股份600483 -33.92%" buyTime:@"2020.5.26" firstLowTime:@"2018.2.9"]];
    [list addObject:[self maxValue:@"28.29" current:7.70 name:@"86.45%😄17.20% 万邦德002082 -37.14%" buyTime:@"2019.11.11" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"11.71" current:2.47 name:@"58.42%😄-11.47% 厦门象屿600057 -44.12%" buyTime:@"2020.2.4" firstLowTime:@"2018.12.25"]];
    [list addObject:[self maxValue:@"15.46" current:7.39 name:@"161.17%😄79.37% 国检集团603060 -31.32%" buyTime:@"2020.3.24" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"28.57" current:8.30 name:@"93.06%😄8.64% 永安药业002365 -43.73%" buyTime:@"2021.2.4" firstLowTime:@"2019.8.15"]];
    [list addObject:[self maxValue:@"53.76" current:8.95 name:@"82.66%😄-15.65% 先锋电子002767 -53.82%" buyTime:@"2021.2.8" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"17.09" current:6.62 name:@"103.34%😄47.44% 江苏神通002438 -27.49%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"31.05" current:7.27 name:@"65.17%😄9.16% 中远海科002401 -33.91%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"40.46" current:11.81 name:@"87.22%😄11.84% 神宇股份300563 -40.26%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"19.49" current:3.08 name:@"79.93%😄3.01% 云图控股002539 -42.75%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"13.81" current:3.23 name:@"164.38%😄5.56% 华贸物流603128 -60.07%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"43.41" current:6.57 name:@"62.96%😄10.61% 爱普股份603020 -32.13%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.15"]];
    [list addObject:[self maxValue:@"16.45" current:2.50 name:@"41.22%😄-39.02% 金发科技600143 -56.82%" buyTime:@"2018.10.19" firstLowTime:@"2017.5.24"]];
    [list addObject:[self maxValue:@"36.01" current:6.01 name:@"115.03%😄-7.82% 世民科技300522 -57.13%" buyTime:@"2019.8.15" firstLowTime:@"2018.2.6"]];
    [list addObject:[self maxValue:@"19.39" current:4.5 name:@"145.67%😄55.71% 凯盛科技600552 -36.62%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"24.17" current:6.00 name:@"84.96%😄17.19% 联环药业600513 -36.64%" buyTime:@"2019.8.15" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"42.99" current:8.89 name:@"74.65%😄-10.56% 汉嘉设计300746 -48.79%" buyTime:@"2021.2.8" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"29.02" current:4.86 name:@"130.35%😄-18.04% 罗莱生活002293 -64.42%" buyTime:@"2019.1.4" firstLowTime:@"2017.5.24"]];
    [list addObject:[self maxValue:@"21.50" current:7.93 name:@"102.25%😄27.70% 利民股份002734 -36.86%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"28.39" current:6.33 name:@"67.33%😄5.50% 开开实业600272 -36.95%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"41.35" current:9.1 name:@"105.23%😄44.22% 航天发展000547 -29.73%" buyTime:@"2019.11.25" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"14.79" current:4.13 name:@"75.26%😄5.36% 百川股份002455 -39.88%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"20.86" current:3.99 name:@"70.49%😄-2.68% 山东威达002026 -42.92%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"25.89" current:5.82 name:@"53.31%😄4.11% 飞亚达000026 -32.09%" buyTime:@"2019.8.9" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"21.14" current:5.01 name:@"75.91%😄1.42% 潍柴重机000880 -42.35%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"23.98" current:5.18 name:@"108.82%😄26.96% 黄山胶囊002817 -39.20%" buyTime:@"2021.2.8" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"19.56" current:5.61 name:@"69.18%😄20.91% 高能环境603588 -28.54%" buyTime:@"2019.11.25" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"30.78" current:7.35 name:@"139.16%😄41.07% 博彦科技002649 -41.01%" buyTime:@"2020.5.25" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"23.63" current:7.27 name:@"147.90%😄45.69% 浙商证券601878 -41.23%" buyTime:@"2019.8.15" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"30.30" current:6.02 name:@"163.20%😄15.99% 汇金通603577 -55.93%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"36.91" current:10.85 name:@"73.18%😄29.32% 诺邦股份603238 -25.33%" buyTime:@"2019.11.27" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"21.61" current:5.26 name:@"99.80%😄6.69% 惠泉啤酒600573 -46.60%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"20.05" current:5.91 name:@"103.47%😄28.20% 创元科技000551 -36.99%" buyTime:@"2019.11.29" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.01" current:4.05 name:@"73.81%😄7.14% 兔宝宝002043 -38.36%" buyTime:@"2019.10.23" firstLowTime:@"2019.1.4"]];
    [list addObject:[self maxValue:@"13.28" current:2.90 name:@"109.87%😄30.04% 梅花生物600873 -38.03%" buyTime:@"2020.2.3" firstLowTime:@"2018.7.6"]];
    [list addObject:[self maxValue:@"38.69" current:4.98 name:@"112.47%😄12.93% 沃顿科技000920 -46.85%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.4"]];
    [list addObject:[self maxValue:@"14.59" current:4.11 name:@"70.36%😄5.93% 华光环能600475 -37.82%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"30.73" current:6.28 name:@"123.38%😄23.38% 万讯自控300112 -44.77%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"29.19" current:6.79 name:@"76.04%😄4.30% 铁流股份603926 -40.75%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"56.17" current:12.15 name:@"126.98%😄45.68% 盛天网络300494 -35.82%" buyTime:@"2020.4.2" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"25.18" current:7.39 name:@"96.90%😄20.75% 正裕工业603089 -38.67%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"17.85" current:4.61 name:@"67.87%😄10.55% 宁波联合600051 -34.14%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"37.14" current:8.09 name:@"117.41%😄43.69% 朗科智能300543 -33.91%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"26.66" current:5.39 name:@"71.59%😄2.08% 益盛药业002566 -40.51%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.86" current:4.00 name:@"123.78%😄21.95% 浙商中拓000906 -45.50%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"44.92" current:5.87 name:@"173.47%😄37.79% 万马股份002276 -49.61%" buyTime:@"2021.2.8" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"30.47" current:5.30 name:@"47.15%😄12.05% 百大集团600865 -23.85" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.88" current:4.93 name:@"76.34%😄6.02% 东方铁塔002545 -39.88%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.50" current:3.70 name:@"72.29%😄5.71% 诺德股份600110 -38.64%" buyTime:@"2019.11.18" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"43.81" current:8.97 name:@"165.85%😄46.57% 福龙马603686 -44.87%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"14.84" current:3.59 name:@"91.05%😄10.80% 林洋能源601222 -42.00%" buyTime:@"2019.8.15" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"37.49" current:5.87 name:@"73.96%😄22.29% 众业达002441 -29.70%" buyTime:@"2019.12.3" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"22.74" current:4.58 name:@"87.69%😄0.66% 瑞尔特002790 -46.37%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"23.39" current:4.10 name:@"102.27%😄33.12% 辉隆股份002556 -34.19%" buyTime:@"2019.11.13" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"33.57" current:7.21 name:@"121.78%😄30.85% 国金证券600109 -41%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"20.81" current:6.03 name:@"90.25%😄17.54% 东莞控股000828 -38.22%" buyTime:@"2020.3.23" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"30.41" current:5.64 name:@"59.11%😄-9.90% 瑞风高材300243 -43.37%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.25"]];
    [list addObject:[self maxValue:@"26.51" current:5.21 name:@"100.50%😄28.96% 海洋王002724 -35.68%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"14.34" current:4.52 name:@"131.28%😄26.26% 江苏国泰002091 -45.41%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"25.12" current:5.61 name:@"64.60%😄12.20% 中原传媒000719 -31.83%" buyTime:@"2020.4.2" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"15.90" current:4.54 name:@"142.25%😄27.89% 太阳电缆002300 -47.21%" buyTime:@"2020.2.4" firstLowTime:@"2018.6.22"]];
    [list addObject:[self maxValue:@"24.38" current:5.01 name:@"101.22%😄21.90% 凤凰传媒601928 -39.42%" buyTime:@"2020.4.28" firstLowTime:@"2018.6.19"]];
    [list addObject:[self maxValue:@"40.30" current:4.18 name:@"89.47%😄-18.52% 雪迪龙002658 -57.00%" buyTime:@"2021.2.5" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"42.03" current:4.87 name:@"154.94%😄4.51% 恒宝股份002104 -59.01%" buyTime:@"2021.7.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"31.49" current:6.07 name:@"95.16%😄1.34% 金桥信息603918 -48.08%" buyTime:@"2021.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"31.49" current:6.39 name:@"95.16%😄6.68% 金桥信息603918 -45.34%" buyTime:@"2021.11.2" firstLowTime:@"2019.1.31"]];

    
    [list addObject:[self maxValue:@"20.58" current:4.36 name:@"107.54%😄6.08% 国泰集团603977 -48.89%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.25"]];
    [list addObject:[self maxValue:@"23.02" current:5.24 name:@"127.10%😄22.43% 黑牡丹600510 -46.09%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.18"]];
//    [list addObject:[self maxValue:@"19.32" current:3.17 name:@"中国电建601669 -46.45%" buyTime:@"2020.6.12" firstLowTime:@"2018.10.30"]];
    [list addObject:[self maxValue:@"23.53" current:7.13 name:@"79.17%😄17.85% 北部湾港000582 -34.23%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"26.00" current:3.66 name:@"96.76%😄-8.98% 中材节能603126 -53.74%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.19" current:5.20 name:@"103.40%😄10.40% 陕天然气002267 -45.72%" buyTime:@"2022.3.16" firstLowTime:@"2020.3.19"]];
    [list addObject:[self maxValue:@"18.55" current:6.15 name:@"115.64%😄18.73% 上海梅林600073 -44.94%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"19.69" current:6.70 name:@"207.99%😄84.57% 楚江新材002171 -40.07%" buyTime:@"2021.3.31" firstLowTime:@"2018.10.19"]];
//    [list addObject:[self maxValue:@"14.07" current:4.31 name:@"湖南海利600731 -27.44%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.68" current:4.41 name:@"145.91%😄28.95% 亚威股份002559 -47.56%" buyTime:@"2021.1.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"20.01" current:4.14 name:@"83.75%😄15.97% 松芝股份002454 -36.89%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"26.84" current:5.51 name:@"73.24%😄-0.36% 汇洁股份002763 -42.48%" buyTime:@"2020.4.28" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"30.68" current:6.41 name:@"108.11%😄13.05% 国脉科技002093 -45.68%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.58" current:4.74 name:@"98.47%😄20.92% 仁和药业000650 -39.07%" buyTime:@"2019.1.4" firstLowTime:@"2018.2.9"]];
    [list addObject:[self maxValue:@"16.58" current:5.02 name:@"151.28%😄28.06% 仁和药业000650 -49.04%" buyTime:@"2021.2.4" firstLowTime:@"2018.2.9"]];
    [list addObject:[self maxValue:@"13.16" current:2.83 name:@"45.83%😄-1.74% 通宝能源600780 -32.62%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.25"]];
    [list addObject:[self maxValue:@"31.44" current:3.98 name:@"120.59%😄30.07% 沃华医药002107 -41.04%" buyTime:@"2019.11.29" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"17.60" current:5.80 name:@"79.87%😄21.59% 鲁北化工600727 -32.40%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"37.19" current:3.94 name:@"93.49%😄16.57% 隆基机械002363 -39.76%" buyTime:@"2021.5.18" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"37.19" current:4.33 name:@"157.11%😄9.90% 隆基机械002363 -57.26%" buyTime:@"2021.5.18" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"17.71" current:4.58 name:@"89.20%😄17.74% 展鹏科技603488 -37.77%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"17.48" current:4.01 name:@"66.21%😄9.26% 皖通高速600012 -34.26%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"30.10" current:4.68 name:@"78.26%😄-7.51% 弘讯科技603015 -48.12%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"26.43" current:3.47 name:@"69.80%😄-1.14% 利君股份002651 -41.78%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"11.99" current:3.41 name:@"72.08%😄10.71% 南玻A000012 -35.66%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"8.48" current:2.25 name:@"63.44%😄-0.88% 粤水电002060 -39.35%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"13.24" current:3.51 name:@"102.22%😄10.76% 山东路桥000498 -45.23%" buyTime:@"2020.2.4" firstLowTime:@"2018.7.12"]];
    [list addObject:[self maxValue:@"20.00" current:4.32 name:@"74.65%😄0.47% 同德化工002360 -42.48%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"13.78" current:3.58 name:@"102.34%😄19.73% 金杯电工002533 -40.83%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"29.83" current:5.37 name:@"95.81%😄18.28% 经纬辉开300120 -39.60%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"10.37" current:3.03 name:@"49.32%😄3.06% 江南水务601199 -30.98%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.7"]];
    [list addObject:[self maxValue:@"26.71" current:3.95 name:@"61.22%😄-13.94% 富临运业002357 -53.69%" buyTime:@"2021.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.56" current:3.64 name:@"130.47%😄30.47% 越秀资本000987 -43.39%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"18.61" current:3.74 name:@"75.64%😄19.87% 华邦健康002004 -31.75%" buyTime:@"2020.2.3" firstLowTime:@"2018.10.18"]];
    [list addObject:[self maxValue:@"13.20" current:3.04 name:@"134.92%😄3.05% 中钢国际000928 -56.13%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"22.37" current:3.33 name:@"78.98%😄12.88% 迦南科技300412 -36.93%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"43.10" current:4.17 name:@"74.25%😄4.25% 福达股份603166 -40.17%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"15.57" current:3.89 name:@"94.71%😄-2.02% 汉宇集团300403 -49.86%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.72" current:4.07 name:@"215.66%😄63.45% 天威视讯002238 -48.22%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"19.10" current:3.22 name:@"75.30%😄-1.83% 史丹利002588 -44.00%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"18.86" current:4.19 name:@"98.43%😄9.40% 金洲管道002443 -44.87%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"14.27" current:1.97 name:@"60.20%😄-1.99% 节能风电601016 -38.82%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"13.98" current:4.45 name:@"162.54%😄44.95% 中国出版601949 -44.79%" buyTime:@"2020.5.28" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"9.14" current:2.80 name:@"104.71%😄9.80% 宁波建工601789 -46.36%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"15.17" current:3.08 name:@"110.27%😄17.11% 乔治白002687 -44.30%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"11.36" current:3.21 name:@"95.09%😄21.13% 宏昌电子603002 -37.91%" buyTime:@"2020.2.4" firstLowTime:@"2019.1.31"]];
    [list addObject:[self maxValue:@"13.95" current:2.92 name:@"62.24%😄-0.68% 中国西电601179 -38.78%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.17"]];
    [list addObject:[self maxValue:@"14.63" current:2.80 name:@"81.75%😄-2.11% 天润工业002283 -46.14%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"11.64" current:2.13 name:@"54.76%😄1.43% 旷达科技002516 -34.46%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"12.35" current:3.04 name:@"148.82%😄44.08% 新朋股份002328 -42.10%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.56" current:4.00 name:@"141.29%😄29.03% 润邦股份002483 -46.52%" buyTime:@"2021.2.5" firstLowTime:@"2018.10.12"]];
    [list addObject:[self maxValue:@"13.57" current:2.16 name:@"58.47%😄-12.90% 宁波能源600982 -45.04%" buyTime:@"2020.2.4" firstLowTime:@"2018.6.22"]];
    [list addObject:[self maxValue:@"13.03" current:3.68 name:@"157.09%😄30.50% 沧州明珠002108 -49.24%" buyTime:@"2021.2.8" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"10.04" current:2.58 name:@"96.88%😄0.78% 安徽建工600502 -48.81%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"16.23" current:4.63 name:@"50.82%😄8.43% 浙江交科002061 -28.11%" buyTime:@"2021.1.28" firstLowTime:@"2020.2.4"]];
    [list addObject:[self maxValue:@"8.66" current:2.51 name:@"125.37%😄-6.34% 岳阳林纸600963 -58.44%" buyTime:@"2020.2.4" firstLowTime:@"2018.10.19"]];
    [list addObject:[self maxValue:@"21.25" current:5.17 name:@"77.89%😄8.84% 中钨高新000657 -38.82%" buyTime:@"2020.3.19" firstLowTime:@"2018.10.19"]];

//    [list addObject:[self maxValue:@"sssss" current:8888 name:@"" buyTime:@"" firstLowTime:@""]];

//

//  价格:  16 -20   历史数据有遗漏
    
//    300452    002645   002758   601788   002009   603067   002585   601126   300480   300378
//    600295    603633   600750   300257   603630   603527   000722   002646   603987   300351
//    002048    000887   002090   600153   600452   000567   002125   300031   300501   002149
//    300435    600598   000810   603667   603239   300515   002879   603819   000657   002698
//    603578    603339   000970   300183   002899   600580   300332   601567   000960   002598
//    603693    600739   603663   300582   300610   600999   300535   002232   603311   600206
//    002079    600060   300542   603189   300517   603159   300179   600835   002826   000603
//    601208    300229   002246   300305   600371   300261   002830   300541   601020   600501
//    300771    000153   603131   002911   603727   002825   300371   300606   603966   603909
//    300430    603010   601163   603937   300224   600288   603698   603081   600379   603339
//    603299    300320   600551   600479   002244   002674   600330   603359   603165   603798
//    000532    300348   603123   002053   300434   600483   002082   600057   603060   002365
//    002767    002637   002438   002401   300563   002539   603898   603128   600573   000829
//    603020    002152   600143   300522   300288   600552   600513   300746   002293   002734
//    600272    000547   002455   600478   002026   000026   600366   000880   002817   603588
//    002649    601878   603577   600613   603238   600573   000551   000919   002043   600873
//    603357    000920   600475   300112   603926   300494   603089   600051   300543   002566
//    000906    002276   600865   002545   600110   603686   601200   601222   601222   002441
//    002790    600101   002556   600109   000828   603977   300243   002724   002091   000719
    
//    002763    600335   002730   000719   002300   601928   002658   002104   603918   603977
//    600510    601669   000582   603126   002267   600073   002171   600731   002559   002454
//    603088    002763   600510   601669   600602   000582   002093   000650   600780   002107
    
//    600727    002363   603488   600012   603015   002651   002651   000012   002060   000498
//    002360    002533   300120   600063   601199   601019   002357   300026   000987   002004
//    000928    300412   603166   300403   002238   601555   002588   002443   601016   601949
    
//    002743    601789   002687   603002   601179   002283   002516   002328   002483   600982
//    002108    600502   002061   600963   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx
//    xxxxxx    xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx
//    xxxxxx    xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx
//    xxxxxx    xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx
//    xxxxxx    xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx
//    xxxxxx    xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx
//    xxxxxx    xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx   xxxxxx

    
//    个股底部特征(12个月内没有创下新低)
// 1.下跌75点
// 2.12个月后,下跌75点买入 (如果首次最大下跌 85, 则 80点买入)

//    NSArray *years = @[@"2015",@"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2022"];
    
    
    // 300746    9.54(20222.11.30 条件单)
    
    
    
    NSMutableArray *years = [NSMutableArray array];
    for(int i = 2015 ; i <= 2022; i++){
        NSString *year = [NSString stringWithFormat:@"%d",i];
        [years addObject:year];
    }
    
    
//    for (NSString * year in years) {
//        NSMutableArray *tempList = [NSMutableArray array];
//        for (StockModel *model in list) {
//            if([model.buyTime containsString:year]){
//                [tempList addObject:model];
//            }
//        }
//        [self historyDown75_subMethod:tempList year:year];
//    }
    
    
    
    for (NSString * year in years) {
        NSMutableArray *tempList = [NSMutableArray array];
        for (StockModel *model in list) {
            NSString *zhangfu = [[model.name componentsSeparatedByString:@" "].firstObject componentsSeparatedByString:@"😄"].firstObject;
            if([zhangfu doubleValue] > 40 && [zhangfu doubleValue] < 150 && [model.buyTime containsString:year]){
                [tempList addObject:model];
            }
        }
        [self historyDown75_subMethod:tempList year:year];
    }
    
    
    // 70 130
   

    

    
    

    
   
    NSLog(@"\n历史下跌75========================================end");
}

-(void)historyDown75_subMethod:(NSMutableArray *)list year:(NSString *)year{
    
    [list sortUsingComparator:^NSComparisonResult(StockModel *  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        if ([obj1.buyTime containsString:@"."]){
            [formatter setDateFormat:@"yyyy.MM.dd"];
        }
        NSDate *obj1Date = [formatter dateFromString:obj1.buyTime];
        NSDate *obj2Date = [formatter dateFromString:obj2.buyTime];
        long obj1Day = obj1Date.timeIntervalSinceReferenceDate;
        long obj2Day = obj2Date.timeIntervalSinceReferenceDate;
        long result = (int) ((obj1Day - obj2Day) / (3600 * 24));
        
//        if(result > 0){
//            return NSOrderedDescending;
//        } else if (result < 0){
//            return NSOrderedAscending;
//        } else {
//           double result = [[obj1.name componentsSeparatedByString:@" "].lastObject doubleValue] - [[obj2.name componentsSeparatedByString:@" "].lastObject doubleValue];
////            result = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
//            if(result > 0){
//                return NSOrderedDescending;
//            } else if (result < 0){
//                return NSOrderedAscending;
//            } else {
//                return NSOrderedSame;
//            }
//        }
//
        
        //  根据二次跌幅排序
        double  result1 = [[obj1.name componentsSeparatedByString:@" "].lastObject doubleValue] - [[obj2.name componentsSeparatedByString:@" "].lastObject doubleValue];
        //            result = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
        if(result1 > 0){
            return NSOrderedDescending;
        } else if (result1 < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
        
        
        
//       根据最大跌幅排序
//        double result2 = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
//        if(result2 > 0){
//            return NSOrderedDescending;
//        } else if (result2 < 0){
//            return NSOrderedAscending;
//        } else {
//            return NSOrderedSame;
//        }
        
////        根据第一次最小值到第二次最小值涨跌幅排序
//        double  result3 = [[[obj1.name componentsSeparatedByString:@" "].firstObject componentsSeparatedByString:@"😄"].lastObject doubleValue] - [[[obj2.name componentsSeparatedByString:@" "].firstObject componentsSeparatedByString:@"😄"].lastObject doubleValue];
//
//        if(result3 > 0){
//            return NSOrderedDescending;
//        } else if (result3 < 0){
//            return NSOrderedAscending;
//        } else {
//            return NSOrderedSame;
//        }
        
        
    }];
      
    NSLog(@"%@年",year);
    for (int i = 0; i < list.count; i++) {
        int index = i + 1;
        StockModel *model = list[i];
        NSLog(@"序号: %02d %@",index,model);
    }
    NSLog(@"");
}


#pragma mark -  历史建仓
-(void)historyBuild{
    NSLog(@"\n历史建仓========================================start");
    [self chackRank:@"24.16" ninePer:@"12.40" fivePer:@"8.06" current:5.64 time:289 name:@"南极电商" list:_list resultList:_resultList];
    [self chackRank:@"64.750" ninePer:@"33.150" fivePer:@"23.850" current:18.52 beginTime:@"2021.5.10" endTime:@"2022.6.22" name:@"赛腾股份" list:_list resultList:_resultList];
    [self chackRank:@"15.00" ninePer:@"8.68" fivePer:@"6.92" current:4.64 beginTime:@"2021.8.9" endTime:@"2022.5.24" name:@"世纪华通" list:_list resultList:_resultList];
    [self chackRank:@"67.80" ninePer:@"34.67" fivePer:@"23.18" current:21.76 beginTime:@"2021.2.8" endTime:@"2022.3.11" name:@"朗博科技" list:_list resultList:_resultList];
    [self chackRank:@"25.65" ninePer:@"15.93" fivePer:@"13.20" current:12.95 beginTime:@"2021.2.8" endTime:@"2022.1.19" name:@"保龄宝" list:_list resultList:_resultList];
    [self chackRank:@"24.56" ninePer:@"15.29" fivePer:@"11.88" current:7.14 beginTime:@"2021.9.29" endTime:@"2022.4.25" name:@"天下秀" list:_list resultList:_resultList];
    [self chackRank:@"196.1" ninePer:@"125.71" fivePer:@"93.11" current:89.6 beginTime:@"2021.5.10" endTime:@"2022.7.5" name:@"深南电路" list:_list resultList:_resultList];
    [self chackRank:@"34.310" ninePer:@"18.980" fivePer:@"14.130" current:10.56 beginTime:@"2021.1.29" endTime:@"2022.6.13" name:@"诚志股份" list:_list resultList:_resultList];
    [self chackRank:@"26.600" ninePer:@"17.940" fivePer:@"12.940" current:9.12 beginTime:@"2020.3.19" endTime:@"2022.9.1" name:@"通化东宝" list:_list resultList:_resultList];
    [self chackRank:@"49.24" ninePer:@"25.740" fivePer:@"18.510" current:11.60 beginTime:@"2021.10.28" endTime:@"2022.10.24" name:@"吉宏股份" list:_list resultList:_resultList];
    [self chackRank:@"85.09" ninePer:@"36.02" fivePer:@"25" current:16.89 beginTime:@"2021.9.29" endTime:@"2022.10.24" name:@"新经典" list:_list resultList:_resultList];
    [self chackRank:@"75.50" ninePer:@"24.34" fivePer:@"17.73" current:17.72 beginTime:@"2022.4.27" endTime:@"2022.11.2" name:@"华兰生物" list:_list resultList:_resultList];
    [self chackRank:@"57.350" ninePer:@"29.270" fivePer:@"19.770" current:16.56 beginTime:@"2022.4.27" endTime:@"2022.11.11" name:@"新宝股份" list:_list resultList:_resultList];
    NSLog(@"\n历史建仓========================================end");

}


#pragma mark - 下跌75点候选
-(void)currentDown75{
    NSMutableArray *list = [NSMutableArray array];
    [list addObject:[self maxValue:@"155.15" current:35.10 name:@"康泰生物_300601"]];
    [list addObject:[self maxValue:@"82.4" current:35.00 name:@"中炬高新_600872"]];
    [list addObject:[self maxValue:@"163.35" current:43.11 name:@"豪悦护理_605009"]];
    [list addObject:[self maxValue:@"181.22" current:39.50 name:@"西藏药业_600211"]];
    [list addObject:[self maxValue:@"40.65" current:11.20 name:@"中银证券_601696"]];
    [list addObject:[self maxValue:@"171.36" current:55.00 name:@"闻泰科技_600745"]];
    [list addObject:[self maxValue:@"137.85" current:60.53 name:@"安图生物_603658"]];
    [list addObject:[self maxValue:@"259.36" current:71.78 name:@"传音控股_688036"]];
    [list addObject:[self maxValue:@"177.27" current:70.00 name:@"金域医学_603882"]];
    [list addObject:[self maxValue:@"185.13" current:79.66 name:@"瑞芯微_603893"]];
    [list addObject:[self maxValue:@"24.160" current:4.59 name:@"南极电商⭐_002127"]];
    [list addObject:[self maxValue:@"85.090" current:18.91 name:@"新经典⭐_603096"]];
    [list addObject:[self maxValue:@"75.500" current:19.84 name:@"华兰生物⭐⭐_002007"]];
    [list addObject:[self maxValue:@"65.150" current:15.48 name:@"艾迪精密_603638"]];
    [list addObject:[self maxValue:@"66.130" current:16.78 name:@"信维通信_创业板"]];
    [list addObject:[self maxValue:@"15.000" current:3.91 name:@"世纪华通"]];
    [list addObject:[self maxValue:@"41.970" current:11.00 name:@"中贝通信"]];
    [list addObject:[self maxValue:@"49.240" current:13.98 name:@"吉宏股份⭐"]];
    [list addObject:[self maxValue:@"32.430" current:8.72 name:@"双鹭药业⭐"]];
    [list addObject:[self maxValue:@"24.560" current:7.09 name:@"天下秀"]];
    [list addObject:[self maxValue:@"41.800" current:13.35 name:@"完美世界⭐⭐_002624"]];
    [list addObject:[self maxValue:@"35.020" current:11.11 name:@"中顺洁柔"]];
    [list addObject:[self maxValue:@"57.150" current:16.99 name:@"新宝股份⭐⭐_002705"]];
    [list addObject:[self maxValue:@"49.250" current:15.74 name:@"三一重工"]];
    [list addObject:[self maxValue:@"68.950" current:21.45 name:@"华正新材⭐"]];
    [list addObject:[self maxValue:@"48.690" current:14.41 name:@"拉卡拉⭐"]];
    [list addObject:[self maxValue:@"49.710" current:17.32 name:@"三七互娱⭐⭐_002555"]];
    [list addObject:[self maxValue:@"43.330" current:13.35 name:@"圣达生物⭐"]];
    [list addObject:[self maxValue:@"33.690" current:11.33 name:@"有友食品⭐"]];
    [list addObject:[self maxValue:@"44.380" current:14.13 name:@"新城市⭐"]];
    [list addObject:[self maxValue:@"29.170" current:9.14 name:@"福龙马"]];
    [list addObject:[self maxValue:@"33.090" current:10.66 name:@"煌上煌"]];
    [list addObject:[self maxValue:@"39.050" current:12.19 name:@"奥美医疗⭐"]];
    [list addObject:[self maxValue:@"30.780" current:10.25 name:@"深物业A⭐"]];
    [list addObject:[self maxValue:@"63.560" current:20.82 name:@"电魂网络⭐"]];
    [list addObject:[self maxValue:@"40.930" current:13.77 name:@"西麦食品⭐"]];
    [list addObject:[self maxValue:@"14.880" current:4.85 name:@"领益智造"]];
    [list addObject:[self maxValue:@"139.70" current:49.75 name:@"浙江鼎力⭐"]];
    [list addObject:[self maxValue:@"78.640" current:22.95 name:@"王府井⭐"]];
    [list addObject:[self maxValue:@"25.650" current:9.18 name:@"保龄宝⭐"]];
    [list addObject:[self maxValue:@"17.470" current:5.99 name:@"东华软件⭐"]];
    [list addObject:[self maxValue:@"43.980" current:15.08 name:@"九阳股份⭐"]];
    [list addObject:[self maxValue:@"34.310" current:12.61 name:@"亿帆医药"]];
    [list addObject:[self maxValue:@"45.200" current:14.18 name:@"姚记科技⭐"]];
    [list addObject:[self maxValue:@"30.940" current:11.02 name:@"航天信息"]];
    [list addObject:[self maxValue:@"33.690" current:13.03 name:@"桃李面包"]];
    [list addObject:[self maxValue:@"38.570" current:17.80 name:@"淳中科技⭐"]];
    [list addObject:[self maxValue:@"120.03" current:46.68 name:@"江山欧派⭐"]];
    [list addObject:[self maxValue:@"23.400" current:8.47 name:@"大亚圣象⭐"]];
    [list addObject:[self maxValue:@"38.240" current:14.17 name:@"宁水集团"]];
    [list addObject:[self maxValue:@"27.270" current:9.87 name:@"武商集团⭐"]];
    [list addObject:[self maxValue:@"108.98" current:45.03 name:@"信捷电气⭐"]];
    [list addObject:[self maxValue:@"13.330" current:5.09 name:@"红旗连锁⭐"]];
    [list addObject:[self maxValue:@"36.460" current:14.83 name:@"华阳国际⭐"]];
    [list addObject:[self maxValue:@"38.470" current:15.12 name:@"招商积余⭐"]];
    [list addObject:[self maxValue:@"196.10" current:75.55 name:@"深南电路⭐⭐"]];
    [list addObject:[self maxValue:@"164.10" current:60.88 name:@"小熊电器⭐⭐_002959"]];
    [list addObject:[self maxValue:@"61.430" current:24.96 name:@"双汇发展⭐"]];
    [list addObject:[self maxValue:@"35.400" current:15.32 name:@"生益科技⭐"]];
    [list addObject:[self maxValue:@"33.210" current:13.41 name:@"中国重汽"]];
    [list addObject:[self maxValue:@"76.250" current:30.12 name:@"山东药玻"]];
    [list addObject:[self maxValue:@"91.960" current:37.05 name:@"丸美股份⭐"]];
    [list addObject:[self maxValue:@"19.540" current:8.12 name:@"力合科创"]];
    [list addObject:[self maxValue:@"27.320" current:13.08 name:@"日丰股份"]];
    [list addObject:[self maxValue:@"49.370" current:21.05 name:@"星网锐捷⭐⭐"]];
    [list addObject:[self maxValue:@"28.100" current:12.63 name:@"沪电股份⭐"]];
    [list addObject:[self maxValue:@"35.220" current:14.39 name:@"网达软件"]];
    [list addObject:[self maxValue:@"70.960" current:39.04 name:@"老百姓"]];

    
    [list sortUsingComparator:^NSComparisonResult(StockModel *  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        double result = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
        if(result > 0){
            return NSOrderedDescending;
        } else if (result < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
        
    for (int i = 0; i < list.count; i++) {
        int index = i + 1;
        StockModel *model = list[i];
        NSLog(@"序号: %02d %@",index,model);
    }
}

#pragma mark - 当前持仓
-(void)chicang{
    NSLog(@"");
    NSLog(@"当前持仓---------------start");
    NSMutableArray *list = [NSMutableArray array];
    [list addObject:[self maxValue:@"63.79" current:32.07 name:@"格力电器"]];
    [list addObject:[self maxValue:@"34.31" current:9.51 name:@"诚志股份"]];
    [list addObject:[self maxValue:@"75.5" current:19.06 name:@"华兰生物⭐⭐_002007"]];
    [list addObject:[self maxValue:@"24.16" current:4.78 name:@"南极电商"]];
    [list addObject:[self maxValue:@"25.65" current:9.04 name:@"保龄宝"]];
    [list addObject:[self maxValue:@"15.00" current:3.99 name:@"世纪华通"]];
    [list addObject:[self maxValue:@"43.58" current:4.96 name:@"中公教育"]];
    [list addObject:[self maxValue:@"57.15" current:17.51 name:@"新宝股份"]];
    [list addObject:[self maxValue:@"49.24" current:14.06 name:@"吉宏股份"]];
    [list addObject:[self maxValue:@"196.1" current:78.75 name:@"深南电路⭐⭐"]];
    [list addObject:[self maxValue:@"2.699" current:0.857 name:@"中国互联网164906"]];
    [list addObject:[self maxValue:@"0.907" current:0.474 name:@"医药ETF"]];
    [list addObject:[self maxValue:@"2.650" current:0.960 name:@"中概互联网ETF"]];
    [list addObject:[self maxValue:@"49.25" current:15.98 name:@"三一重工"]];
    [list addObject:[self maxValue:@"24.56" current:8.47 name:@"天下秀"]];
    [list addObject:[self maxValue:@"26.35" current:9.24 name:@"通化东宝"]];
    [list addObject:[self maxValue:@"85.09" current:19.16 name:@"新经典⭐"]];
    [list addObject:[self maxValue:@"386.53" current:52.19 name:@"汇顶科技"]];
    [list addObject:[self maxValue:@"67.80" current:21.96 name:@"朗博科技"]];
    
    [list sortUsingComparator:^NSComparisonResult(StockModel *  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        double result = [obj1.currentDecline doubleValue] - [obj2.currentDecline doubleValue];
        if(result > 0){
            return NSOrderedDescending;
        } else if (result < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        int index = i + 1;
        StockModel *model = list[i];
        NSLog(@"序号: %02d %@",index,model);
    }
    NSLog(@"当前持仓---------------end");
    NSLog(@"");
 
}

#pragma mark - 再跌多少触发条件单
-(void)tiaojiandan:(NSString *)name
     tiaojiandan:(NSString *)tiaojiandan_price
         current:(double)current_price
          maxPrice:(double)maxPrice
         maxPrice2:(double)maxPrice2{
    [self tiaojiandan:name tiaojiandan:tiaojiandan_price current:current_price maxPrice:maxPrice maxPrice2:maxPrice2 trigger:NO];
}


-(NSString *)origin:(NSString *)origin targetLength:(NSInteger)length{
    NSInteger len = origin.length;
    for(int i = 0;i<length-len;i++){
        origin = [NSString stringWithFormat:@"%@ ",origin];
    }
    return origin;
}

#pragma mark - 再跌多少触发条件单
-(void)tiaojiandan:(NSString *)name
     tiaojiandan:(NSString *)tiaojiandan_price
         current:(double)current_price
          maxPrice:(double)maxPrice
         maxPrice2:(double)maxPrice2
     firstMinPrice:(double)firstMinPrice {
    double result =  ((current_price - [tiaojiandan_price floatValue]) / current_price * 100);
    
    double maxResult =  ((current_price - maxPrice) / maxPrice * 100);
    double maxResult2 =  ((current_price - maxPrice2) / maxPrice2 * 100);

    double result3 =  ((current_price - firstMinPrice) / firstMinPrice * 100);

    if ( result3 < 10 && ((maxResult < -80 && maxResult2 < -48) || (maxResult < -70 && maxResult2 < -48)) ){
        
        NSString *str1 = [self origin:[NSString stringWithFormat:@"%@",name] targetLength:20];
        NSString *str2 = [self origin:[NSString stringWithFormat:@"😄😄😄😄😄😄下跌 %.2f%% 触发",result] targetLength:28];
        NSString *str3 = [self origin:[NSString stringWithFormat:@"%.2f%%",maxResult] targetLength:11];
        NSString *str4 = [self origin:[NSString stringWithFormat:@"%.2f%%",maxResult2] targetLength:11];
        NSString *str5 = [self origin:[NSString stringWithFormat:@"%.2f%%",result3] targetLength:11];
        NSLog(@"%@:%@ %@ %@ %@   🍺🍺🍺🍺🍺",str1,str2,str3,str4,str5);
        
//        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发🍺🍺🍺🍺🍺  %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
        return;
    }
    
    if ( maxResult2 < -40 ){
        NSString *str1 = [self origin:[NSString stringWithFormat:@"%@",name] targetLength:20];
        NSString *str2 = [self origin:[NSString stringWithFormat:@"😄😄😄😄😄😄下跌 %.2f%% 触发",result] targetLength:28];
        NSString *str3 = [self origin:[NSString stringWithFormat:@"%.2f%%",maxResult] targetLength:11];
        NSString *str4 = [self origin:[NSString stringWithFormat:@"%.2f%%",maxResult2] targetLength:11];
        NSString *str5 = [self origin:[NSString stringWithFormat:@"%.2f%%",result3] targetLength:11];
        NSLog(@"%@:%@ %@ %@ %@   🍺🍺",str1,str2,str3,str4,str5);
        
//        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发🍺🍺🍺🍺🍺  %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
        return;
    }
    
//    63.68% , name = 120.21%😄28.98% 万盛股份603010 -41.43%
    NSString *str1 = [self origin:[NSString stringWithFormat:@"%@",name] targetLength:20];
    NSString *str2 = [self origin:[NSString stringWithFormat:@"😄😄😄😄😄😄下跌 %.2f%% 触发",result] targetLength:28];
    NSString *str3 = [self origin:[NSString stringWithFormat:@"%.2f%%",maxResult] targetLength:11];
    NSString *str4 = [self origin:[NSString stringWithFormat:@"%.2f%%",maxResult2] targetLength:11];
    NSString *str5 = [self origin:[NSString stringWithFormat:@"%.2f%%",result3] targetLength:11];
    NSLog(@"%@:%@ %@ %@ %@",str1,str2,str3,str4,str5);
    
//        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发              %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
    return;
    
    if (result < 0){
        NSLog(@"%@ : 触发后下跌 %.2f%% %.2f%%    %.2f%%  %.2f%%",name,fabs(result),maxResult,maxResult2,result3);
    } else  if (result < 10){
//            0-10
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐⭐⭐⭐  %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
    } else if (result < 15){
//            10 - 15
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐  %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
    }else if (result < 25){
//            15 - 25
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐  %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
    } else {
//            > 25
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发  %.2f%%    %.2f%%  %.2f%%",name,result,maxResult,maxResult2,result3);
    }
}


-(void)tiaojiandan:(NSString *)name
     tiaojiandan:(NSString *)tiaojiandan_price
         current:(double)current_price
          maxPrice:(double)maxPrice
          maxPrice2:(double)maxPrice2
           trigger:(BOOL)trigger
{
    double result =  ((current_price - [tiaojiandan_price floatValue]) / current_price * 100);

    double maxResult =  -((maxPrice - current_price) / maxPrice * 100);
    double maxResult2 =  -((maxPrice2 - current_price) / maxPrice2 * 100);


    if(trigger){
        result =  ((current_price - [tiaojiandan_price floatValue]) / [tiaojiandan_price floatValue] * 100);
        NSLog(@"%@ : %.2f%% 🍺🍺🍺🍺🍺已触发  %.2f%%    %.2f%%",name,result,maxResult,maxResult2);
        return;
    }

    if ((maxResult < -80 && maxResult2 < -50) || (maxResult < -70 && maxResult2 < -50)){
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发🍺🍺🍺🍺🍺  %.2f%%    %.2f%%",name,result,maxResult,maxResult2);
        return;
    }


    if (result < 0){
        NSLog(@"%@ : 触发后下跌 %.2f%% %.2f%%    %.2f%%",name,fabs(result),maxResult,maxResult2);
    } else  if (result < 10){
//            0-10
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐⭐⭐⭐  %.2f%%    %.2f%%",name,result,maxResult,maxResult2);
    } else if (result < 15){
//            10 - 15
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐  %.2f%%    %.2f%%",name,result,maxResult,maxResult2);
    }else if (result < 25){
//            15 - 25
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐  %.2f%%    %.2f%%",name,result,maxResult,maxResult2);
    } else {
//            > 25
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发  %.2f%%    %.2f%%",name,result,maxResult,maxResult2);
    }


}




#pragma mark - 再跌多少触发条件单

-(void)tiaojiandan:(NSString *)name
     tiaojiandan:(NSString *)tiaojiandan_price
         current:(double)current_price{
    [self tiaojiandan:name tiaojiandan_price:tiaojiandan_price current_price:current_price trigger:false];
}


-(void) tiaojiandan{
    
    
    NSLog(@"\n tiaojiandan 历史建仓========================================start \n");
    [self tiaojiandan:@"中电兴发002298 149.51%" tiaojiandan:@"5.93" current:6.32 maxPrice:40.81 maxPrice2:12.65 firstMinPrice:5.07];
    [self tiaojiandan:@"三力士002224 82.82%" tiaojiandan:@"4.48" current:4.76 maxPrice:25.55 maxPrice2:8.94 firstMinPrice:4.89];
    [self tiaojiandan:@"木林森002745 106.14%" tiaojiandan:@"8.50" current:8.81 maxPrice:38.39 maxPrice2:19.13 firstMinPrice:9.28];
    [self tiaojiandan:@"深圳华强000062 94.97%" tiaojiandan:@"8.29" current:11.88 maxPrice:63.29 maxPrice2:20.16 firstMinPrice:10.34];
    [self tiaojiandan:@"东兴证券601198 103.53%" tiaojiandan:@"7.27" current:7.79 maxPrice:42.51 maxPrice2:14.98 firstMinPrice:7.36];
    [self tiaojiandan:@"富煌钢构002743 114.94%" tiaojiandan:@"5.38" current:5.36 maxPrice:25.62 maxPrice2:10.36 firstMinPrice:4.82];
    
    NSLog(@"\n tiaojiandan 历史建仓========================================end\n\n");

    NSLog(@"");
    NSLog(@"再跌多少触发条件单========================================start");
    
    [self tiaojiandan:@"三力士002224 82.82%" tiaojiandan:@"4.47" current:4.81 maxPrice:25.56 maxPrice2:8.94 firstMinPrice:3.96];
    [self tiaojiandan:@"中电兴发002298 149.51%" tiaojiandan:@"6.32" current:7.03 maxPrice:40.81 maxPrice2:12.65 firstMinPrice:5.08];
    [self tiaojiandan:@"东兴证券601198 103.53%" tiaojiandan:@"7.49" current:8.36 maxPrice:42.51 maxPrice2:14.98 firstMinPrice:7.27];
    [self tiaojiandan:@"富煌钢构002743 114.94%" tiaojiandan:@"5.18" current:5.80 maxPrice:25.62 maxPrice2:10.36 firstMinPrice:5.03];
    [self tiaojiandan:@"深圳华强000062 94.97%" tiaojiandan:@"10.08" current:12.62 maxPrice:63.29 maxPrice2:20.16 firstMinPrice:9.95];
    [self tiaojiandan:@"木林森002745 106.14%" tiaojiandan:@"8.21" current:9.01 maxPrice:38.39 maxPrice2:19.13 firstMinPrice:7.93];
    [self tiaojiandan:@"富春环保002479 151.47%" tiaojiandan:@"4.27" current:4.76 maxPrice:25.08 maxPrice2:8.55 firstMinPrice:3.40];
    [self tiaojiandan:@"重庆建工600939 122.08%" tiaojiandan:@"3.42" current:3.67 maxPrice:22.69 maxPrice2:6.84 firstMinPrice:3.08];
    [self tiaojiandan:@"瑞茂通600180 130.77%" tiaojiandan:@"5.81" current:6.02 maxPrice:47.64 maxPrice2:10.50 firstMinPrice:4.55];
    [self tiaojiandan:@"上海凤凰600679 111.50%" tiaojiandan:@"9.10" current:8.99 maxPrice:45.98 maxPrice2:16.85 firstMinPrice:8.78];
    [self tiaojiandan:@"曲美家居603818 132.69%" tiaojiandan:@"6.39" current:7.28 maxPrice:40.41 maxPrice2:14.45 firstMinPrice:6.21];
    [self tiaojiandan:@"节能国祯300388 106.69%" tiaojiandan:@"6.29" current:6.62 maxPrice:33.78 maxPrice2:13.60 firstMinPrice:6.58];
    [self tiaojiandan:@"绿色动力601330 69.26%" tiaojiandan:@"6.81" current:7.10 maxPrice:27.35 maxPrice2:12.39 firstMinPrice:7.32];
    [self tiaojiandan:@"信邦制药002390 261.82%" tiaojiandan:@"4.81" current:5.08 maxPrice:26.50 maxPrice2:11.94 firstMinPrice:3.30];
    [self tiaojiandan:@"上海机电600835 103.80%" tiaojiandan:@"10.97" current:12.02 maxPrice:43.52 maxPrice2:23.09 firstMinPrice:11.33];
    [self tiaojiandan:@"康尼机电603111 138.76%" tiaojiandan:@"4.06" current:4.64 maxPrice:16.26 maxPrice2:8.5 firstMinPrice:3.56];
    [self tiaojiandan:@"浙富控股002266 149.09%" tiaojiandan:@"3.69" current:4.07 maxPrice:16.66 maxPrice2:8.22 firstMinPrice:3.30];
    [self tiaojiandan:@"中国中冶601618 196.19%" tiaojiandan:@"3.00" current:3.32 maxPrice:11.05 maxPrice2:6.22 firstMinPrice:2.10];
    [self tiaojiandan:@"联明股份603006 162.89%" tiaojiandan:@"6.90" current:10.00 maxPrice:35.41 maxPrice2:19.48 firstMinPrice:7.41];
    [self tiaojiandan:@"神宇股份300563 124.62%" tiaojiandan:@"10.62" current:12.46 maxPrice:40.45 maxPrice2:23.72 firstMinPrice:10.56];
    [self tiaojiandan:@"龙元建设600491 82.78%" tiaojiandan:@"4.61" current:5.46 maxPrice:18.24 maxPrice2:9.98 firstMinPrice:5.46];
    [self tiaojiandan:@"东南网架002135 202.85%" tiaojiandan:@"5.95" current:6.65 maxPrice:20.30 maxPrice2:13.81 firstMinPrice:4.56];
    [self tiaojiandan:@"博深股份002282 113.94%" tiaojiandan:@"6.50" current:7.78 maxPrice:26.27 maxPrice2:13.35 firstMinPrice:6.24];
    [self tiaojiandan:@"北京利尔002392 132.99%" tiaojiandan:@"3.30" current:3.83 maxPrice:13.67 maxPrice2:6.71 firstMinPrice:2.88];
    [self tiaojiandan:@"正平股份603843 146.34%" tiaojiandan:@"4.02" current:4.48 maxPrice:24.41 maxPrice2:8.08 firstMinPrice:3.28];
    [self tiaojiandan:@"宏辉果蔬603336 130.29%" tiaojiandan:@"5.64" current:6.93 maxPrice:18.77 maxPrice2:11.86 firstMinPrice:5.15];
    [self tiaojiandan:@"中文传媒600373 174.60%" tiaojiandan:@"7.43" current:9.94 maxPrice:36.52 maxPrice2:17.08 firstMinPrice:6.22];
    [self tiaojiandan:@"荣晟环保603165 144.99%" tiaojiandan:@"9.12" current:12.55 maxPrice:36.49 maxPrice2:21.19 firstMinPrice:8.69];
    [self tiaojiandan:@"歌力思603808 99.45%" tiaojiandan:@"8.55" current:10.50 maxPrice:44.94 maxPrice2:18.27 firstMinPrice:9.16];//8888
    [self tiaojiandan:@"武汉控股600168 126.09%" tiaojiandan:@"5.82" current:6.44 maxPrice:18.49 maxPrice2:10.92 firstMinPrice:4.83];
    [self tiaojiandan:@"合兴包装002228 104.45%" tiaojiandan:@"3.15" current:3.54 maxPrice:14.39 maxPrice2:5.97 firstMinPrice:2.92];
    [self tiaojiandan:@"东方证券600958 137.20%" tiaojiandan:@"8.28" current:9.93 maxPrice:34.3 maxPrice2:16.96 firstMinPrice:7.15];
    [self tiaojiandan:@"国信证券002736 172.26%" tiaojiandan:@"8.77" current:9.23 maxPrice:32.74 maxPrice2:15.41 firstMinPrice:5.66];
    [self tiaojiandan:@"方正证券601901 173.56%" tiaojiandan:@"6.13" current:6.67 maxPrice:16.97 maxPrice2:11.90 firstMinPrice:4.35];
    [self tiaojiandan:@"东北证券000686 160.91%" tiaojiandan:@"5.80" current:6.66 maxPrice:23.88 maxPrice2:12.08 firstMinPrice:4.63];
    [self tiaojiandan:@"山西证券002500 115.01%" tiaojiandan:@"4.87" current:5.67 maxPrice:22.26 maxPrice2:9.74 firstMinPrice:4.53];
    [self tiaojiandan:@"华安证券600909 123.76%" tiaojiandan:@"4.41" current:4.85 maxPrice:12.28 maxPrice2:8.1 firstMinPrice:3.62];
    [self tiaojiandan:@"西部证券002673 95.45%" tiaojiandan:@"5.84" current:6.58 maxPrice:35.16 maxPrice2:11.78 firstMinPrice:6.38];


    NSLog(@"");
    [self tiaojiandan:@"安诺其300067 153.74%" tiaojiandan:@"2.87" current:3.29 maxPrice:11.51 maxPrice2:7.46 firstMinPrice:2.94];
    [self tiaojiandan:@"珠江钢琴002678 81.89%" tiaojiandan:@"5.23" current:6.36 maxPrice:22.35 maxPrice2:9.24 firstMinPrice:5.08];
    [self tiaojiandan:@"双箭股份002381 162.17%" tiaojiandan:@"5.43" current:6.86 maxPrice:21.47 maxPrice2:10.83 firstMinPrice:4.15];
    [self tiaojiandan:@"上海环境601200 80.19%" tiaojiandan:@"8.16" current:9.31 maxPrice:32.20 maxPrice2:15.19 firstMinPrice:8.43];
    [self tiaojiandan:@"华数传媒000156 100.00%" tiaojiandan:@"7.01" current:7.98 maxPrice:61.76 maxPrice2:12.76 firstMinPrice:6.38];
    [self tiaojiandan:@"读者传媒603999 130.53%" tiaojiandan:@"4.74" current:5.89 maxPrice:32.42 maxPrice2:8.76 firstMinPrice:3.80];
    [self tiaojiandan:@"浙江众成002522 145.92%" tiaojiandan:@"4.50" current:5.53 maxPrice:49.60 maxPrice2:9.05 firstMinPrice:3.68];
    [self tiaojiandan:@"绿城水务601368 90.42%" tiaojiandan:@"5.13" current:5.36 maxPrice:25.73 maxPrice2:8.55 firstMinPrice:4.49];
    [self tiaojiandan:@"申万宏源000166 90.83%" tiaojiandan:@"3.99" current:4.11 maxPrice:14.65 maxPrice2:6.66 firstMinPrice:3.49];
    [self tiaojiandan:@"嘉泽新能601619 137.92%" tiaojiandan:@"3.84" current:4.03 maxPrice:13.51 maxPrice2:6.40 firstMinPrice:2.69];
    [self tiaojiandan:@"广州港601228 66.19%" tiaojiandan:@"3.03" current:3.18 maxPrice:11.38 maxPrice2:4.67 firstMinPrice:2.81];
    [self tiaojiandan:@"中贝通信603220 63.39%" tiaojiandan:@"10.65" current:12.09 maxPrice:41.97 maxPrice2:17.76 firstMinPrice:10.87];
    [self tiaojiandan:@"新疆交建002941 128.94%" tiaojiandan:@"12.39" current:13.29 maxPrice:44.28 maxPrice2:20.65 firstMinPrice:9.02];
    [self tiaojiandan:@"东方明珠600637 76.79%" tiaojiandan:@"6.15" current:7.03 maxPrice:58.21 maxPrice2:10.25 firstMinPrice:7.11];
   
    
    NSLog(@"---------end");
    [self tiaojiandan:@"航天工程603698 131.61%" tiaojiandan:@"12.29" current:15.89 maxPrice:54.04 maxPrice2:22.35 firstMinPrice:9.65];
    [self tiaojiandan:@"华金资本000532 120.79%" tiaojiandan:@"10.23" current:11.82 maxPrice:38.00 maxPrice2:18.60 firstMinPrice:6.84];
    [self tiaojiandan:@"建发合城603909 120.79%" tiaojiandan:@"8.97" current:12.99 maxPrice:42.81 maxPrice2:17.95 firstMinPrice:8.13];
    [self tiaojiandan:@"众业达002441 162.50%" tiaojiandan:@"5.67" current:10.98 maxPrice:37.49 maxPrice2:12.60 firstMinPrice:4.80];
    [self tiaojiandan:@"设计总院603357 90.16%" tiaojiandan:@"7.86" current:9.77 maxPrice:25.80 maxPrice2:14.30 firstMinPrice:7.52];
    [self tiaojiandan:@"三角轮胎601163 108.46%" tiaojiandan:@"10.57" current:13.35 maxPrice:49.14 maxPrice2:19.22 firstMinPrice:9.22];
    [self tiaojiandan:@"传化智联002010 130.73%" tiaojiandan:@"5.57" current:6.05 maxPrice:26.23 maxPrice2:10.06 firstMinPrice:4.36];
    [self tiaojiandan:@"七匹狼002029 58.73%" tiaojiandan:@"4.36" current:5.60 maxPrice:32.98 maxPrice2:7.27 firstMinPrice:4.58];
    [self tiaojiandan:@"冠豪高新600433 136.12%" tiaojiandan:@"3.10" current:4.48 maxPrice:24.52 maxPrice2:6.21 firstMinPrice:2.63];
    [self tiaojiandan:@"广宇集团002133 93.20%" tiaojiandan:@"2.89" current:3.53 maxPrice:16.27 maxPrice2:4.83 firstMinPrice:2.50];
    [self tiaojiandan:@"景兴纸业002067 69.40%" tiaojiandan:@"2.85" current:3.60 maxPrice:14.97 maxPrice2:4.76 firstMinPrice:2.81];
    [self tiaojiandan:@"楚天高速600035 70.00%" tiaojiandan:@"2.34" current:3.33 maxPrice:9.67 maxPrice2:3.91 firstMinPrice:2.30];
    [self tiaojiandan:@"日照港600017 51.56%" tiaojiandan:@"2.21" current:2.83 maxPrice:11.60 maxPrice2:3.41 firstMinPrice:2.25];
    [self tiaojiandan:@"中原内配002448 101.97%" tiaojiandan:@"4.51" current:5.86 maxPrice:23.23 maxPrice2:8.20 firstMinPrice:4.06];
    [self tiaojiandan:@"诺普信002215" tiaojiandan:@"4.90" current:6.60];
    [self tiaojiandan:@"皖新传媒601801 60.59%" tiaojiandan:@"4.15" current:5.55 maxPrice:18.84 maxPrice2:6.52 firstMinPrice:4.06];
    [self tiaojiandan:@"远东传动002406 96.65%" tiaojiandan:@"4.25" current:6.06 maxPrice:18.65 maxPrice2:8.22 firstMinPrice:4.18];
    [self tiaojiandan:@"海宁皮城002344 58.89%" tiaojiandan:@"3.21" current:4.52 maxPrice:31.08 maxPrice2:5.35 firstMinPrice:3.43];
    [self tiaojiandan:@"濮耐股份002225 75.62%" tiaojiandan:@"3.41" current:4.32 maxPrice:13.32 maxPrice2:5.69 firstMinPrice:3.15];
    [self tiaojiandan:@"宁波港601018 69.52%" tiaojiandan:@"2.73" current:3.61 maxPrice:12.86 maxPrice2:4.56 firstMinPrice:2.69];
    [self tiaojiandan:@"龙江交通601188 93.15%" tiaojiandan:@"2.33" current:3.15 maxPrice:9.32 maxPrice2:4.23 firstMinPrice:2.19];
    [self tiaojiandan:@"清新环境002573 102.67%" tiaojiandan:@"5.01" current:5.84 maxPrice:29.25 maxPrice2:8.35 firstMinPrice:4.12];
    [self tiaojiandan:@"德联集团002666 127.04%" tiaojiandan:@"4.45" current:5.37 maxPrice:20.57 maxPrice2:8.9 firstMinPrice:2.87];
    [self tiaojiandan:@"隆鑫通用603766 132.48%" tiaojiandan:@"3.42" current:5.32 maxPrice:14.87 maxPrice2:6.37 firstMinPrice:2.74];
    [self tiaojiandan:@"奥瑞金002701 122.99%" tiaojiandan:@"3.35" current:5.06 maxPrice:13.42 maxPrice2:7.76 firstMinPrice:3.48];
    [self tiaojiandan:@"东方精工002611 135.81%" tiaojiandan:@"3.80" current:4.53 maxPrice:15.54 maxPrice2:7.31 firstMinPrice:3.10];
    [self tiaojiandan:@"丰林集团601996 107.66%" tiaojiandan:@"2.17" current:2.77 maxPrice:8.29 maxPrice2:4.34 firstMinPrice:2.09];
    [self tiaojiandan:@"杭电股份603618 128.79%" tiaojiandan:@"4.62" current:5.73 maxPrice:18.85 maxPrice2:8.9 firstMinPrice:3.89];
    [self tiaojiandan:@"无锡银行600908 85.37%" tiaojiandan:@"4.56" current:5.59 maxPrice:22.82 maxPrice2:7.6 firstMinPrice:4.10];
    [self tiaojiandan:@"银龙股份603969 117.20%" tiaojiandan:@"3.41" current:5.15 maxPrice:13.82 maxPrice2:6.82 firstMinPrice:3.14];
    [self tiaojiandan:@"苏农银行603323 52.09%" tiaojiandan:@"3.77" current:4.77 maxPrice:14.85 maxPrice2:5.81 firstMinPrice:3.82];
    [self tiaojiandan:@"张家港行002839 77.08%" tiaojiandan:@"4.22" current:4.79 maxPrice:24.74 maxPrice2:5.95 firstMinPrice:3.36];
    [self tiaojiandan:@"国芳集团601086 101.77%" tiaojiandan:@"3.42" current:4.16 maxPrice:12.33 maxPrice2:5.71 firstMinPrice:2.83];
    [self tiaojiandan:@"江阴银行002807 45.93%" tiaojiandan:@"3.26" current:4.12 maxPrice:19.22 maxPrice2:5.02 firstMinPrice:3.44];
    [self tiaojiandan:@"秦港股份601326 39.04%" tiaojiandan:@"2.48" current:2.81 maxPrice:12.04 maxPrice2:3.17 firstMinPrice:2.28];
    [self tiaojiandan:@"珠海港000507 100%" tiaojiandan:@"5.02" current:5.80 maxPrice:13.78 maxPrice2:8.92 firstMinPrice:4.46];
    [self tiaojiandan:@"中国中铁601390 63.30%" tiaojiandan:@"5.00" current:5.79 maxPrice:22.96 maxPrice2:7.12 firstMinPrice:4.36];
    [self tiaojiandan:@"史丹利002588 142.86%" tiaojiandan:@"4.69" current:6.23 maxPrice:19.10 maxPrice2:7.82 firstMinPrice:3.22];
    [self tiaojiandan:@"天威视讯002238 258.23%" tiaojiandan:@"4.90" current:7.70 maxPrice:19.72 maxPrice2:8.92 firstMinPrice:2.49];
    [self tiaojiandan:@"新华锦600735 181.40%" tiaojiandan:@"5.69" current:7.15 maxPrice:28.53 maxPrice2:10.44 firstMinPrice:3.71];
    [self tiaojiandan:@"北特科技603009 93.54%" tiaojiandan:@"4.70" current:7.33 maxPrice:27.13 maxPrice2:9.29 firstMinPrice:4.80];
    [self tiaojiandan:@"金洲管道002443 154.31%" tiaojiandan:@"4.38" current:6.42 maxPrice:18.86 maxPrice2:9.74 firstMinPrice:3.83];
    [self tiaojiandan:@"嘉欣丝绸002404 133.99%" tiaojiandan:@"4.90" current:7.03 maxPrice:14.72 maxPrice2:8.33 firstMinPrice:3.56];
    [self tiaojiandan:@"世荣兆业002016 79.68%" tiaojiandan:@"4.47" current:7.11 maxPrice:17.90 maxPrice2:7.96 firstMinPrice:4.43];
    [self tiaojiandan:@"恒基达鑫002492 97.61%" tiaojiandan:@"4.78" current:6.72 maxPrice:17.06 maxPrice2:8.28 firstMinPrice:4.19];
    [self tiaojiandan:@"山东出版601019 46.27%" tiaojiandan:@"4.25" current:6.53 maxPrice:17.00 maxPrice2:6.86 firstMinPrice:4.69];
    [self tiaojiandan:@"创业环保600874 55.39%" tiaojiandan:@"5.34" current:6.33 maxPrice:24.36 maxPrice2:8.22 firstMinPrice:5.29];
    [self tiaojiandan:@"中粮科技000930 113.01%" tiaojiandan:@"7.27" current:8.59 maxPrice:28.85 maxPrice2:12.12 firstMinPrice:5.69];
    [self tiaojiandan:@"国药现代600420 63.45%" tiaojiandan:@"7.62" current:10.28 maxPrice:29.62 maxPrice2:12.70 firstMinPrice:7.70];
    [self tiaojiandan:@"浙数文化600633 97.22%" tiaojiandan:@"6.06" current:8.56 maxPrice:30.55 maxPrice2:12.76 firstMinPrice:6.47];
    [self tiaojiandan:@"吉宏股份002803" tiaojiandan:@"10.13" current:16.78 maxPrice:49.24 maxPrice2:16.89 firstMinPrice:9.60];
    [self tiaojiandan:@"博彦科技002649 184.84%" tiaojiandan:@"7.42" current:14.38 maxPrice:30.78 maxPrice2:14.84 firstMinPrice:5.21];
    [self tiaojiandan:@"红墙股份002809 95.49%" tiaojiandan:@"7.57" current:10.65 maxPrice:39.21 maxPrice2:15.17 firstMinPrice:7.76];
    [self tiaojiandan:@"新华文轩601811 129.74%" tiaojiandan:@"6.83" current:10.36 maxPrice:33.75 maxPrice2:15.76 firstMinPrice:6.86];
    [self tiaojiandan:@"锦江在线600650 81.64%" tiaojiandan:@"8.25" current:10.58 maxPrice:52.25 maxPrice2:13.75 firstMinPrice:7.57];
    [self tiaojiandan:@"完美世界002624" tiaojiandan:@"10.45" current:14.25 maxPrice:41.8 maxPrice2:16.28 firstMinPrice:9.85];
    [self tiaojiandan:@"勘设股份603458 98.52%" tiaojiandan:@"8.05" current:9.99 maxPrice:33.71 maxPrice2:16.10 firstMinPrice:8.11];
     [self tiaojiandan:@"中南传媒601098 98.52%" tiaojiandan:@"7.29" current:10.00 maxPrice:29.19 maxPrice2:11.72 firstMinPrice:7.55];
    [self tiaojiandan:@"好莱客603898 65.01%" tiaojiandan:@"9.15" current:12.48 maxPrice:44.14 maxPrice2:12.59 firstMinPrice:7.63];
    [self tiaojiandan:@"深物业A000011 72.73%" tiaojiandan:@"7.69" current:11.07 maxPrice:30.78 maxPrice2:15.01 firstMinPrice:8.69];
    [self tiaojiandan:@"康普顿603798 79.88%" tiaojiandan:@"9.26" current:13.06 maxPrice:37.53 maxPrice2:14.39 firstMinPrice:8.00];
    [self tiaojiandan:@"大商股份600694 32.42%" tiaojiandan:@"14.99" current:17.52 maxPrice:58.45 maxPrice2:19.85 firstMinPrice:14.99];
    [self tiaojiandan:@"西藏药业600211" tiaojiandan:@"31.20" current:45.68 maxPrice:181.22 maxPrice2:45.98 firstMinPrice:29.55];
    [self tiaojiandan:@"世纪华通002602" tiaojiandan:@"2.78" current:4.50 maxPrice:15.00 maxPrice2:4.39 firstMinPrice:3.55];
    [self tiaojiandan:@"诚志股份000990" tiaojiandan:@"6.32" current:9.42 maxPrice:34.31 maxPrice2:13.37];
    [self tiaojiandan:@"保龄宝002286" tiaojiandan:@"7.77" current:9.18];
    [self tiaojiandan:@"深南电路002916" tiaojiandan:@"53.79" current:81.51];
    [self tiaojiandan:@"招商积余001914" tiaojiandan:@"9.61" current:16.19];
    [self tiaojiandan:@"三七互娱002555" tiaojiandan:@"12.42" current:22.36];
    [self tiaojiandan:@"宝新能源000690 79.81%" tiaojiandan:@"3.85" current:6.43 maxPrice:17.29 maxPrice2:7.75 firstMinPrice:4.31];
    [self tiaojiandan:@"仁和药业000650" tiaojiandan:@"5.31" current:6.73];
    [self tiaojiandan:@"上海九百600838 100%" tiaojiandan:@"5.95" current:6.71 maxPrice:21.5 maxPrice2:10.82 firstMinPrice:4.58];
    [self tiaojiandan:@"广日股份600894 123.83%" tiaojiandan:@"5.05" current:6.94 maxPrice:29.02 maxPrice2:9.11 firstMinPrice:4.07];
    [self tiaojiandan:@"福达股份603166 174.75%" tiaojiandan:@"5.49" current:6.90 maxPrice:43.1 maxPrice2:10.99 firstMinPrice:4.00];
    [self tiaojiandan:@"海峡环保603817 62.55%" tiaojiandan:@"5.21" current:6.14 maxPrice:29.67 maxPrice2:8.03 firstMinPrice:4.94];
    [self tiaojiandan:@"新奥股份603889 124.29%" tiaojiandan:@"4.63" current:7.51 maxPrice:17.86 maxPrice2:8.68 firstMinPrice:3.87];
    [self tiaojiandan:@"梅轮电梯603321 84.97%" tiaojiandan:@"5.48" current:7.65 maxPrice:22.26 maxPrice2:10.95 firstMinPrice:5.92];
    [self tiaojiandan:@"中山公用000685 90.40%" tiaojiandan:@"5.95" current:7.19 maxPrice:20.91 maxPrice2:9.92 firstMinPrice:5.21];
    [self tiaojiandan:@"渤海轮渡603167" tiaojiandan:@"5.33" current:7.49 maxPrice:21.32 maxPrice2:8.21];
    [self tiaojiandan:@"精艺股份002295 79.63%" tiaojiandan:@"5.46" current:7.45 maxPrice:32.52 maxPrice2:9.7 firstMinPrice:5.40];
    [self tiaojiandan:@"南京港002040 121.06%" tiaojiandan:@"5.52" current:7.20 maxPrice:20.69 maxPrice2:11.65 firstMinPrice:5.27];
    [self tiaojiandan:@"杭州解百600814 116.50%" tiaojiandan:@"5.34" current:7.37 maxPrice:19.72 maxPrice2:8.79 firstMinPrice:4.06];
    [self tiaojiandan:@"城市传媒600229 85.91%" tiaojiandan:@"4.98" current:7.28 maxPrice:35.69 maxPrice2:9.63 firstMinPrice:5.18];
    [self tiaojiandan:@"国元证券000728 130.21%" tiaojiandan:@"5.42" current:6.77 maxPrice:21.28 maxPrice2:11.05 firstMinPrice:4.80];
    [self tiaojiandan:@"福成股份600965" tiaojiandan:@"5.52" current:7.41 maxPrice:21.77 maxPrice2:9.78];
    [self tiaojiandan:@"合众思壮002383 104.39%" tiaojiandan:@"5.37" current:7.71 maxPrice:23.45 maxPrice2:9.77 firstMinPrice:4.86];
    [self tiaojiandan:@"长青股份002391 104.86%" tiaojiandan:@"6.10" current:7.52 maxPrice:18.32 maxPrice2:11.37 firstMinPrice:5.55];
    [self tiaojiandan:@"中国铁建601186 38.57%" tiaojiandan:@"6.66" current:7.83 maxPrice:26.64 maxPrice2:9.27 firstMinPrice:6.69];
    [self tiaojiandan:@"陕天然气002267 103.40%" tiaojiandan:@"5.20" current:7.15 maxPrice:19.19 maxPrice2:9.58 firstMinPrice:4.71];
    [self tiaojiandan:@"中信海直000099 92.95%" tiaojiandan:@"5.41" current:7.75 maxPrice:26.50 maxPrice2:9.30 firstMinPrice:4.82];
    [self tiaojiandan:@"国泰建团603977 205.84%" tiaojiandan:@"6.28" current:9.25 maxPrice:20.58 maxPrice2:12.57 firstMinPrice:4.11];
    [self tiaojiandan:@"金桥信息603918 95.16%" tiaojiandan:@"6.12" current:9.78 maxPrice:31.49 maxPrice2:11.69 firstMinPrice:5.99];
    [self tiaojiandan:@"东港股份002117 135.06%" tiaojiandan:@"5.80" current:9.71 maxPrice:33.32 maxPrice2:14.55 firstMinPrice:6.19];
    [self tiaojiandan:@"中马传动603767 133.91%" tiaojiandan:@"5.86" current:7.89 maxPrice:18.84 maxPrice2:12.21 firstMinPrice:5.22];
    [self tiaojiandan:@"中国核建601611 106.39%" tiaojiandan:@"6.97" current:7.80 maxPrice:23.31 maxPrice2:11.62 firstMinPrice:5.63];
    [self tiaojiandan:@"中铁工业600528" tiaojiandan:@"6.64" current:8.09];
    [self tiaojiandan:@"汇洁股份002763 135.57%" tiaojiandan:@"7.15" current:8.10 maxPrice:26.84 maxPrice2:12.65 firstMinPrice:5.37];
    [self tiaojiandan:@"经纬纺机000666 90.99%" tiaojiandan:@"7.09" current:10.26 maxPrice:32.38 maxPrice2:12.51 firstMinPrice:6.55];
    [self tiaojiandan:@"中持股份603903 94.38%" tiaojiandan:@"7.86" current:9.75 maxPrice:36.80 maxPrice2:14.52 firstMinPrice:7.47];
    [self tiaojiandan:@"南极电商002127" tiaojiandan:@"3.94" current:4.90];
    [self tiaojiandan:@"双鹭药业002038 55.42%" tiaojiandan:@"7.67" current:8.93 maxPrice:32.43 maxPrice2:12.79 firstMinPrice:8.21];
    [self tiaojiandan:@"雪天盐业600929 149.89%" tiaojiandan:@"5.63" current:8.37 maxPrice:24.11 maxPrice2:11.27 firstMinPrice:4.51];
    [self tiaojiandan:@"嘉化能源600273" tiaojiandan:@"7.69" current:8.76 maxPrice:18.11 maxPrice2:13.99 firstMinPrice:6.93];
    [self tiaojiandan:@"中工国际002051 75.13%" tiaojiandan:@"6.16" current:8.51 maxPrice:25.12 maxPrice2:10.28 firstMinPrice:5.87];
    [self tiaojiandan:@"鹭燕医药002788 89.70%" tiaojiandan:@"6.40" current:9.04 maxPrice:26.91 maxPrice2:10.68 firstMinPrice:5.63];
    [self tiaojiandan:@"皖天然气603689" tiaojiandan:@"5.83" current:8.18 maxPrice:17.92 maxPrice2:10.09 firstMinPrice:6.03];
    [self tiaojiandan:@"万马股份002276 186.85%" tiaojiandan:@"7.33" current:10.38 maxPrice:44.92 maxPrice2:12.22 firstMinPrice:4.26];
    [self tiaojiandan:@"益盛药业002566 133.14%" tiaojiandan:@"7.38" current:8.66 maxPrice:26.66 maxPrice2:12.31 firstMinPrice:5.28];
    [self tiaojiandan:@"北方国际000065 119.79%" tiaojiandan:@"8.30" current:9.68 maxPrice:20.76 maxPrice2:12.77 firstMinPrice:5.81];
    [self tiaojiandan:@"正裕工业603089 130.23%" tiaojiandan:@"6.00" current:9.57 maxPrice:25.18 maxPrice2:14.09 firstMinPrice:6.12];
    [self tiaojiandan:@"交建股份603815 68.18%" tiaojiandan:@"6.88" current:8.02 maxPrice:31.59 maxPrice2:11.47 firstMinPrice:6.82];
    [self tiaojiandan:@"三江购物601116 85.05%" tiaojiandan:@"7.36" current:10.62 maxPrice:53.76 maxPrice2:17.58 firstMinPrice:9.50];
  
    
    
//    [self tiaojiandan:@"xxxxxx" tiaojiandan:@"8888" current:88.888];
//    [self tiaojiandan:@"xxxxxx" tiaojiandan:@"8888" current:88.888];
//    [self tiaojiandan:@"xxxxxx" tiaojiandan:@"8888" current:88.888];
//    [self tiaojiandan:@"xxxxxx" tiaojiandan:@"8888" current:88.888];
//    [self tiaojiandan:@"xxxxxx" tiaojiandan:@"8888" current:88.888];
//    [self tiaojiandan:@"xxxxxx" tiaojiandan:@"8888" current:88.888];

    NSLog(@"再跌多少触发条件单========================================end");
    NSLog(@"");
}


#pragma mark - 历史数据
-(void)demo3{
    NSMutableArray *list = [NSMutableArray array];
    
    // 三七互娱 002555 (2018.10.12  2019.1.3)    1个涨停   8.32月100%     5%以下下跌： -9.12% 市盈率：11.78/43.25(最大)
    // （-9.12%   10.74%  ⭐️⭐️⭐️⭐️⭐️）
    [list addObject:[self maxValue:@"25.74" ninePer:@"11.92" fivePer:@"8.55" current:7.77 time:@"82" name:@"三七互娱"]];
    
//     中牧股份 600195 (2016.2.29 2018.10.29)    1个涨停   9.5月100%     5%以下下跌： -27.55% 市盈率：12.89/57.53(最大) （底部与大盘同步）
    [list addObject:[self maxValue:@"16.39" ninePer:@"9.64" fivePer:@"7.95" current:5.76 time:@"976" name:@"中牧股份"]];
    
    // 帝欧家居 002798 (2017.5.23 2019.1.3)     x个涨停   3月100%     5%以下下跌： -23.52%  市盈率：16.61/155.14(最大)
    [list addObject:[self maxValue:@"97.88" ninePer:@"63.12" fivePer:@"48.93" current:37.42 time:@"590" name:@"帝欧家居"]];
    
    // 星网锐捷 002396 (2015.9.16 2018.10.16)    x个涨停   11月100%     5%以下下跌： -30.69x%   市盈率：15.98/104.65(最大)
    [list addObject:[self maxValue:@"45.13" ninePer:@"25.34" fivePer:@"20.07" current:13.91 time:@"1128" name:@"星网锐捷"]];

    // 掌阅科技 603533 (2019.1.31  2019.8.15)    3个涨停   8月100%     5%以下下跌： -25.86x% 市盈率：40.32/233.01(最大)
    [list addObject:[self maxValue:@"73.40" ninePer:@"24.84" fivePer:@"17.21" current:12.76 time:@"197" name:@"掌阅科技"]];

    // 欧菲光   002456 (2019.1.16 2019.7.22)   x个涨停   6月100%     5%以下下跌： -30.10%   市盈率：-19.25/59.9(最大)
    [list addObject:[self maxValue:@"26.05" ninePer:@"15.77" fivePer:@"10.83" current:7.57 time:@"122" name:@"欧菲光"]];

    // 沪电股份 002463 (2016.2.29 2018.6.26)    x个涨停   4月100%     5%以下下跌: -24.40%  市盈率：28.34/-315(最大)
    [list addObject:[self maxValue:@"11.73" ninePer:@"4.89" fivePer:@"3.73" current:2.82 time:@"854" name:@"沪电股份"]];

    // 实丰文化 002862(2018.10.11 2019.1.31)  1.4月100%     5%以下下跌： -16.63x%   市盈率：37.74/77.88
    [list addObject:[self maxValue:@"42.94" ninePer:@"24.72" fivePer:@"20.44" current:17.04 time:@"42" name:@"实丰文化"]];


    // 游族网络 002174 (2018.10.12 2019.8.15)    x个涨停   6月天100%     5%以下下跌： -24.36% 市盈率：12.04/98.08
    [list addObject:[self maxValue:@"53.02" ninePer:@"25" fivePer:@"17.08" current:12.92 time:@"308" name:@"游族网络"]];
    
    // 新国脉 600640 (2016.3.4 2018.10.18) 2017.6.1    x个涨停   10月100%     5%以下下跌： -49.31%  市盈率：29.61/241.99 (第1轮)
    [list addObject:[self maxValue:@"38.5" ninePer:@"21.41" fivePer:@"15.94" current:8.08 time:@"959" name:@"新国脉"]];

    // 宁波飞翔 002048 (2018.10.18  2019.8.9)   x个涨停  5月100%  5%以下下跌： -15.55%  市盈率：7.87/16.77
    [list addObject:[self maxValue:@"23.83" ninePer:@"10.67" fivePer:@"6.56" current:5.54 time:@"296" name:@"宁波飞翔"]];

    // 双鹭药业 002038 (2016.2.29  2017.5.10)    x个涨停  12月100%  5%以下下跌： -17.93%  市盈率 37.4/47.48
    [list addObject:[self maxValue:@"32.49" ninePer:@"21.83" fivePer:@"17.9" current:14.69 time:@"438" name:@"双鹭药业"]];

    // 新易盛 300502 (2017.7.24 2018.8.6 )     x个涨停  16月100%  5%以下下跌： -60.99%  市盈率 37.79/119.97 -68.5%
    [list addObject:[self maxValue:@"28.35" ninePer:@"19.84" fivePer:@"15.20" current:5.93 time:@"377" name:@"新易盛"]];

    // 盘龙药业 002864 (2019.1.31 2021.2.5 )     x个涨停  13.8月100%  5%以下下跌： -27.38%  市盈率: 23.03/135.09 -82.95%
    [list addObject:[self maxValue:@"80.86" ninePer:@"32.53" fivePer:@"28.71" current:@"20.85" name:@"盘龙药业" beginTime:@"2019.1.31" endTime:@"2021.2.5"]];
    
    // 德赛电池 000049 (2018.10.12 2019.6.6)  7.5月100%  5%以下下跌： -16.83%  市盈率: 12.27/40.54 -69.73%
    [list addObject:[self maxValue:@"39.28" ninePer:@"23.34" fivePer:@"17.17" current:@"14.28" name:@"德赛电池" beginTime:@"2018.10.12" endTime:@"2019.6.6"]];

    // 伊力特 600197  9月100%  5%以下下跌： -17.25%  市盈率: 12.27/41.36  -70.33%
    [list addObject:[self maxValue:@"28.56" ninePer:@"21.28" fivePer:@"14.03" current:@"11.61" name:@"伊力特" beginTime:@"2018.10.29" endTime:@"2020.3.23"]];

    // 爱乐达 300696  4.3月100%  5%以下下跌： -19.12%  市盈率: 40.8/71.79  -43.17%
    [list addObject:[self maxValue:@"30.35" ninePer:@"16.87" fivePer:@"13.7" current:@"11.08" name:@"爱乐达" beginTime:@"2019.1.31" endTime:@"2019.12.30"]];

    // 天孚通信 25.84  4.3月100%  5%以下下跌： -39.37%  市盈率: 25/113  -77.88%
    [list addObject:[self maxValue:@"30.49" ninePer:@"16.46" fivePer:@"12.32" current:@"7.47" name:@"天孚通信" beginTime:@"2017.7.24" endTime:@"2018.2.7"]];
    [self.list addObjectsFromArray:list];
    
    NSMutableArray *list2 = [NSMutableArray array];
    [list2 addObjectsFromArray:list];
    
//    NSLogv(@"1234567890987654321234567890");
    NSMutableArray *resultList = [NSMutableArray array];
    
//    if (1) {
//        int resu =  [self demo2:list model:list[0]];
//        NSLog(@"%d",resu);
//    }
    
    if (1) {
        for (StockModel *model in list2) {
            BOOL print = NO;
            
            //            if (model == list2[0]){
            //                print = true;
            //            }
            
            int paiming = [self demo2:list model:model isPrint:print];
            [resultList addObject:[NSString stringWithFormat:@"%d",paiming]];
        }
        
        [resultList sortUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int result = [obj2 intValue] - [obj1 intValue];
            if (result > 0) {
                return NSOrderedAscending;
            } else if (result < 0){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        NSLog(@"resultList: %@",resultList);
    }
    
    [self.resultList addObjectsFromArray:resultList];
    
    
    NSLog(@"================");
    if (1) {
        for (StockModel *model in list2) {
           int pm = [self demo2:list model:model];
            [self checkBuyName:model.name resultList:resultList paiming:pm];
            NSLog(@"%@",model);
        }
    }
    
    
    if (YES) {
        for (int i = 0; i < 2; i++) {
            NSLog(@" ");
        }
        NSLog(@"条件单进行时========================================start");
        int n = 0;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [formatter dateFromString:@"2021-12-31"];
        NSDate *today = [NSDate new];
        long day = today.timeIntervalSinceReferenceDate;
        long old = date.timeIntervalSinceReferenceDate;
        n = (int) ((day - old) / (3600 * 24));
        NSLog(@"今天: %d",n);
        NSLog(@"市盈率正常--------start🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
        [self chackRank:@"18.100" ninePer:@"10.980" fivePer:@"7.0900" current:5.57 time:358+n name:@"格力地产" list:list resultList:resultList];
        [self chackRank:@"45.500" ninePer:@"32.810" fivePer:@"18.690" current:14.73 time:122+n name:@"姚记科技" list:list resultList:resultList];
        [self chackRank:@"50.060" ninePer:@"29.870" fivePer:@"20.540" current:20.18 time:122+n name:@"三七互娱" list:list resultList:resultList];
        [self chackRank:@"181.22" ninePer:@"72.660" fivePer:@"53.920" current:35.10 time:280+n name:@"西藏药业" list:list resultList:resultList];
        [self chackRank:@"26.900" ninePer:@"18.240" fivePer:@"13.230" current:9.11 time:652+n name:@"通化东宝" list:list resultList:resultList];
        [self chackRank:@"33.070" ninePer:@"17.940" fivePer:@"13.380" current:16.29 time:181+n name:@"亨通光电" list:list resultList:resultList];
        [self chackRank:@"34.310" ninePer:@"18.980" fivePer:@"14.130" current:9.82 time:697+n name:@"诚志股份" list:list resultList:resultList];
        [self chackRank:@"49.470" ninePer:@"32.500" fivePer:@"21.890" current:20.80 time:234+n name:@"星网锐捷" list:list resultList:resultList];
        [self chackRank:@"26.420" ninePer:@"15.220" fivePer:@"10.590" current:8.63 time:155+n name:@"力合科创" list:list resultList:resultList];
        [self chackRank:@"24.580" ninePer:@"15.310" fivePer:@"11.650" current:7.52 time:93+n name:@"天下秀" list:list resultList:resultList];
        [self chackRank:@"25.710" ninePer:@"15.990" fivePer:@"13.260" current:11.30 time:328+n name:@"保龄宝" list:list resultList:resultList];
        [self chackRank:@"41.800" ninePer:@"22.470" fivePer:@"13.430" current:15.00 time:122+n name:@"完美世界" list:list resultList:resultList];
        [self chackRank:@"57.350" ninePer:@"29.270" fivePer:@"19.770" current:19.68 time:93+n name:@"新宝股份" list:list resultList:resultList];
        [self chackRank:@"13.330" ninePer:@"5.8800" fivePer:@"4.7600" current:4.80 time:155+n name:@"红旗连锁" list:list resultList:resultList];
        [self chackRank:@"89.760" ninePer:@"62.830" fivePer:@"38.300" current:30.30 time:582+n name:@"顶点软件" list:list resultList:resultList];
        [self chackRank:@"82.700" ninePer:@"40.560" fivePer:@"31.280" current:36.50 time:100+n name:@"中炬高新" list:list resultList:resultList];
        [self chackRank:@"64.390" ninePer:@"33.000" fivePer:@"24.170" current:20.37 time:122+n name:@"电魂网络" list:list resultList:resultList];
        [self chackRank:@"64.750" ninePer:@"33.150" fivePer:@"23.850" current:26.33 time:233+n name:@"赛腾股份" list:list resultList:resultList];
        [self chackRank:@"15.000" ninePer:@"8.6800" fivePer:@"6.9200" current:4.57 time:144+n name:@"世纪华通" list:list resultList:resultList];
        [self chackRank:@"49.240" ninePer:@"25.740" fivePer:@"18.510" current:11.63 time:64+n name:@"吉宏股份" list:list resultList:resultList];
        [self chackRank:@"31.100" ninePer:@"14.590" fivePer:@"11.950" current:10.38 time:332+n name:@"航天信息" list:list resultList:resultList];
        [self chackRank:@"24.160" ninePer:@"12.400" fivePer:@"8.0600" current:4.85 time:238+n name:@"南极电商" list:list resultList:resultList];
        [self chackRank:@"197.05" ninePer:@"123.74" fivePer:@"94.060" current:88.25 time:233+n name:@"深南电路" list:list resultList:resultList];
        [self chackRank:@"60.120" ninePer:@"30.730" fivePer:@"23.760" current:32.63 time:155+n name:@"国恩股份" list:list resultList:resultList];
        NSLog(@"市盈率正常--------end🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
    }
    
    if (YES) {
        for (int i = 0; i < 2; i++) {
            NSLog(@" ");
        }
        NSLog(@"条件单进行时========================================start");
        int n = 0;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [formatter dateFromString:@"2021-12-31"];
        NSDate *today = [formatter dateFromString:@"2022-04-27"];
        long day = today.timeIntervalSinceReferenceDate;
        long old = date.timeIntervalSinceReferenceDate;
        n = (int) ((day - old) / (3600 * 24));
        NSLog(@"今天: 2022-04-27");
        NSLog(@"市盈率正常--------start🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
        [self chackRank:@"18.100" ninePer:@"10.980" fivePer:@"7.0900" current:4.50 time:358+n name:@"格力地产" list:list resultList:resultList];
        [self chackRank:@"45.500" ninePer:@"32.810" fivePer:@"18.690" current:13.16 time:122+n name:@"姚记科技" list:list resultList:resultList];
        [self chackRank:@"50.060" ninePer:@"29.870" fivePer:@"20.540" current:19.33 time:122+n name:@"三七互娱" list:list resultList:resultList];
        [self chackRank:@"181.22" ninePer:@"72.660" fivePer:@"53.920" current:29.55 time:280+n name:@"西藏药业" list:list resultList:resultList];
        [self chackRank:@"26.900" ninePer:@"18.240" fivePer:@"13.230" current:8.9 time:652+n name:@"通化东宝" list:list resultList:resultList];
        [self chackRank:@"33.070" ninePer:@"17.940" fivePer:@"13.380" current:9.23 time:181+n name:@"亨通光电" list:list resultList:resultList];
        [self chackRank:@"34.310" ninePer:@"18.980" fivePer:@"14.130" current:8.8 time:697+n name:@"诚志股份" list:list resultList:resultList];
        [self chackRank:@"49.470" ninePer:@"32.500" fivePer:@"21.890" current:19.60 time:234+n name:@"星网锐捷" list:list resultList:resultList];
        [self chackRank:@"26.420" ninePer:@"15.220" fivePer:@"10.590" current:6.92 time:155+n name:@"力合科创" list:list resultList:resultList];
        [self chackRank:@"24.580" ninePer:@"15.310" fivePer:@"11.650" current:6.29 time:93+n name:@"天下秀" list:list resultList:resultList];
        [self chackRank:@"25.710" ninePer:@"15.990" fivePer:@"13.260" current:8.95 time:328+n name:@"保龄宝" list:list resultList:resultList];
        [self chackRank:@"41.800" ninePer:@"22.470" fivePer:@"13.430" current:11.72 time:122+n name:@"完美世界" list:list resultList:resultList];
        [self chackRank:@"57.350" ninePer:@"29.270" fivePer:@"19.770" current:13.85 time:93+n name:@"新宝股份" list:list resultList:resultList];
        [self chackRank:@"13.330" ninePer:@"5.8800" fivePer:@"4.7600" current:4.54 time:155+n name:@"红旗连锁" list:list resultList:resultList];
        [self chackRank:@"89.760" ninePer:@"62.830" fivePer:@"38.300" current:21.60 time:582+n name:@"顶点软件" list:list resultList:resultList];
        [self chackRank:@"82.700" ninePer:@"40.560" fivePer:@"31.280" current:23.75 time:100+n name:@"中炬高新" list:list resultList:resultList];
        [self chackRank:@"64.390" ninePer:@"33.000" fivePer:@"24.170" current:19.14 time:122+n name:@"电魂网络" list:list resultList:resultList];
        [self chackRank:@"64.750" ninePer:@"33.150" fivePer:@"23.850" current:13.69 time:233+n name:@"赛腾股份" list:list resultList:resultList];
        [self chackRank:@"15.000" ninePer:@"8.6800" fivePer:@"6.9200" current:4.39 time:144+n name:@"世纪华通" list:list resultList:resultList];
        [self chackRank:@"49.240" ninePer:@"25.740" fivePer:@"18.510" current:9.60 time:64+n name:@"吉宏股份" list:list resultList:resultList];
        [self chackRank:@"31.100" ninePer:@"14.590" fivePer:@"11.950" current:9.33 time:332+n name:@"航天信息" list:list resultList:resultList];
        [self chackRank:@"24.160" ninePer:@"12.400" fivePer:@"8.0600" current:3.96 time:238+n name:@"南极电商" list:list resultList:resultList];
        [self chackRank:@"197.05" ninePer:@"123.74" fivePer:@"94.060" current:84.95 time:233+n name:@"深南电路" list:list resultList:resultList];
        [self chackRank:@"60.120" ninePer:@"30.730" fivePer:@"23.760" current:19.43 time:155+n name:@"国恩股份" list:list resultList:resultList];
        NSLog(@"市盈率正常--------end🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺🍺");
    }
    
}


-(void)tiaojiandan:(NSString *)name
     tiaojiandan_price:(NSString *)tiaojiandan_price
     current_price:(double)current_price
           trigger:(BOOL)trigger{
    double result =  ((current_price - [tiaojiandan_price floatValue]) / current_price * 100);
    
    if(trigger){
        result =  ((current_price - [tiaojiandan_price floatValue]) / [tiaojiandan_price floatValue] * 100);
        NSLog(@"%@ : %.2f%% 🍺🍺🍺🍺🍺已触发",name,result);
        return;
    }
    if (result < 0){
        NSLog(@"%@ : 触发后下跌 %.2f%%",name,fabs(result));
    } else  if (result < 10){
//            0-10
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐⭐⭐⭐",name,result);
    } else if (result < 15){
//            10 - 15
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐⭐",name,result);
    }else if (result < 25){
//            15 - 25
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发⭐",name,result);
    } else {
//            > 25
        NSLog(@"%@ : 😄😄😄😄😄😄下跌 %.2f%% 触发",name,result);
    }
}



-(int)demo2:(NSMutableArray *)list
             model:(StockModel *)model{
    return [self demo2:list model:model isPrint:NO];
}

-(int)demo2:(NSMutableArray *)list
             model:(StockModel *)model
           isPrint:(BOOL)print{
    int a1 = 1;
    int a2 = 1;
    int a3 = 1;
    int a4 = 1;
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        return [obj2.currentDecline compare:obj1.currentDecline];
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if ([stockModel.currentDecline floatValue] == [model.currentDecline floatValue]) {
            a1 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %@ %%",i,stockModel.currentDecline);
        }
    }
    
    if (print) {
        NSLog(@"根据最大跌幅排序--------end" );
        NSLog(@"================================" );
        NSLog(@"根据90%盈利到二次探底最大跌幅排序--------start" );
    }
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        return [obj2.ninefall compare:obj1.ninefall];
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if ([stockModel.ninefall floatValue] == [model.ninefall floatValue]) {
            a2 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %@ %%",i,stockModel.ninefall);
        }
    }
    if (print) {
        NSLog(@"根据90%%盈利到二次探底最大跌幅排序--------end" );
        NSLog(@"================================" );
        NSLog(@"根据90%%盈利到二次探底时间--------start" );
    }
    
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        int result = obj1.ninefall_time - obj2.ninefall_time;
        if (result > 0) {
            return NSOrderedAscending;
        } else if (result < 0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if (stockModel.ninefall_time == model.ninefall_time) {
            a3 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %ld天",i,stockModel.ninefall_time);
        }
    }
    if (print) {
        NSLog(@"根据90%%盈利到二次探底时间--------end" );
        NSLog(@"================================" );
        NSLog(@"根据5%%盈利比例后继续下跌幅度排序----------start" );
    }
    
    [list sortUsingComparator:^NSComparisonResult(StockModel*  _Nonnull obj1, StockModel*  _Nonnull obj2) {
        float result = [obj2.profitnumber floatValue] - [obj1.profitnumber floatValue];
        if (result > 0) {
            return NSOrderedAscending;
        } else if (result < 0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    for (int i = 0; i < list.count; i++) {
        StockModel* stockModel = list[i];
        if ([stockModel.profitnumber floatValue] == [model.profitnumber floatValue]) {
            a4 = i + 1;
        }
        if (print) {
            NSLog(@"序号：%d %@ %%",i,stockModel.profitnumber);
        }
    }
    
    if (print) {
         NSLog(@"根据5%%盈利比例后继续下跌幅度排序----------end" );
    }
    int sum = (a1*2  + a3*4  + a4*3);
    return sum;
}

-(void)checkBuyName:(NSString *)name
         resultList:(NSMutableArray *)resultList
            paiming:(int)paiming{
    for (int i = 0; i < resultList.count; i++) {
        if ([resultList[i] floatValue] >= paiming) {
            i++;
            if (i <= resultList.count / 2) {
                NSLog(@"%@  %d   %d%/%d  建议购买😄",name,paiming,i,resultList.count);
//                NSLog(@ name + "  " + paiming + "    " + i + "/" + resultList.size () + "    建议购买😄😄😄😄😄😄" );
            } else {
//                NSLog(@ name + "  " + paiming + "    " + i + "/" + resultList.size () + "    不买" );
                NSLog(@"%@  %d   %d%/%d  不买",name,paiming,i,resultList.count);

            }
            return;
        }
    }
    NSLog(@"%@  %d   %d%/%d不买",name,paiming,resultList.count,resultList.count);

}

-(StockModel *)maxValue:(NSString *)max_value
                       current:(double )current_value
                                name:(NSString *)name{
    return [self maxValue:max_value ninePer:@"0" fivePer:@"0" current:current_value time:@"0" name:name];
}

-(StockModel *)maxValue:(NSString *)max_value
                current:(double )current_value
                   name:(NSString *)name
                buyTime:(NSString *)buyTime{
    return [self maxValue:max_value ninePer:@"0" fivePer:@"0" current:current_value time:@"0" name:name buyTime:buyTime];
}

-(StockModel *)maxValue:(NSString *)max_value
                current:(double )current_value
                   name:(NSString *)name
                buyTime:(NSString *)buyTime
           firstLowTime:(NSString *)firstLowTime

{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if ([buyTime containsString:@"."]){
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    NSDate *beginDate = [formatter dateFromString:firstLowTime];
    NSDate *endDate = [formatter dateFromString:buyTime];
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long n = (int) ((endDay - beginDay) / (3600 * 24));

    NSString *intervalTime = [NSString stringWithFormat:@"%d",n];
    return [self maxValue:max_value ninePer:@"0" fivePer:@"0" current:current_value time:@"0" name:name buyTime:buyTime intervalTime:intervalTime];
}



-(StockModel *)maxValue:(NSString *)max_value
                       ninePer:(NSString *)ninePer_value
                       fivePer:(NSString *)fivePer_value
                       current:(double)current_value
                       time:(NSString *)ninefall_time
                                name:(NSString *)name
                buyTime:(NSString *)buyTime{
    double max_value1 = (current_value / [max_value doubleValue] - 1) * 100;
    double max_value2 = (current_value / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = (current_value / [fivePer_value doubleValue] - 1) * 100;
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:[ninefall_time intValue]  profitnumber:[NSString stringWithFormat:@"%.1f",max_value3] name:name buyTime:buyTime];
}

-(StockModel *)maxValue:(NSString *)max_value
                       ninePer:(NSString *)ninePer_value
                       fivePer:(NSString *)fivePer_value
                       current:(double)current_value
                       time:(NSString *)ninefall_time
                                name:(NSString *)name
                buyTime:(NSString *)buyTime
           intervalTime:(NSString *)intervalTime{
    double max_value1 = (current_value / [max_value doubleValue] - 1) * 100;
    double max_value2 = (current_value / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = (current_value / [fivePer_value doubleValue] - 1) * 100;
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.2f",max_value1] ninefall:[NSString stringWithFormat:@"%.2f",max_value2] ninefall_time:[ninefall_time intValue]  profitnumber:[NSString stringWithFormat:@"%.2f",max_value3] name:name buyTime:buyTime intervalTime:intervalTime];
}




-(StockModel *)maxValue:(NSString *)max_value
                       ninePer:(NSString *)ninePer_value
                       fivePer:(NSString *)fivePer_value
                       current:(double)current_value
                       time:(NSString *)ninefall_time
                                name:(NSString *)name{
    double max_value1 = (current_value / [max_value doubleValue] - 1) * 100;
    double max_value2 = (current_value / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = (current_value / [fivePer_value doubleValue] - 1) * 100;
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:[ninefall_time intValue]  profitnumber:[NSString stringWithFormat:@"%.1f",max_value3] name:name];
}

-(StockModel *)createModle_max_value:(NSString *)max_value
                       ninePer_value:(NSString *)ninePer_value
                       fivePer_value:(NSString *)fivePer_value
                       current_value:(NSString *)current_value
                       ninefall_time:(NSInteger)ninefall_time{
    double max_value1 = ([current_value doubleValue] / [max_value doubleValue] - 1) * 100;
    double max_value2 = ([current_value doubleValue] / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = ([current_value doubleValue] / [fivePer_value doubleValue] - 1) * 100;
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:ninefall_time profitnumber:[NSString stringWithFormat:@"%.1f",max_value3]];
}


-(StockModel *)maxValue:(NSString *)max_value
                       ninePer:(NSString *)ninePer_value
                       fivePer:(NSString *)fivePer_value
                       current:(NSString *)current_value
                                name:(NSString *)name
                                beginTime:(NSString *)beginTime
                                endTime:(NSString *)endTime {
    double max_value1 = ([current_value doubleValue] / [max_value doubleValue] - 1) * 100;
    double max_value2 = ([current_value doubleValue] / [ninePer_value doubleValue] - 1) * 100;
    double max_value3 = ([current_value doubleValue] / [fivePer_value doubleValue] - 1) * 100;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if ([beginTime containsString:@"."]){
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    NSDate *beginDate = [formatter dateFromString:beginTime];
    NSDate *endDate = [formatter dateFromString:endTime];
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long n = (int) ((endDay - beginDay) / (3600 * 24));
    return [[StockModel alloc] initWithCurrentDecline:[NSString stringWithFormat:@"%.1f",max_value1] ninefall:[NSString stringWithFormat:@"%.1f",max_value2] ninefall_time:n profitnumber:[NSString stringWithFormat:@"%.1f",max_value3] name:name];
}


-(void)chackRank:(NSString *)max_value
         ninePer:(NSString *)ninePer_value
         fivePer:(NSString *)fivePer_value
         current:(double)current_value
       beginTime:(NSString *)beginTime
         endTime:(NSString *)endTime
            name:(NSString *)name
            list:(NSMutableArray *)list
      resultList:(NSMutableArray *)resultList{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if ([beginTime containsString:@"."]){
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    NSDate *beginDate = [formatter dateFromString:beginTime];
    NSDate *endDate = [formatter dateFromString:endTime];
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long ninefall_time = (int) ((endDay - beginDay) / (3600 * 24));
    
//    NSString *ninefall_time = [NSString stringWithFormat:@"%d",n];
    [self chackRank:max_value ninePer:ninePer_value fivePer:fivePer_value current:current_value time:ninefall_time name:name list:list resultList:resultList];
}



-(void)chackRank:(NSString *)max_value
             ninePer:(NSString *)ninePer_value
             fivePer:(NSString *)fivePer_value
             current:(double)current_value
             time:(NSInteger)ninefall_time
                      name:(NSString *)name
                      list:(NSMutableArray *)list
                resultList:(NSMutableArray *)resultList{
    StockModel *targetmodel = [self maxValue:max_value ninePer:ninePer_value fivePer:fivePer_value current:current_value time:[NSString stringWithFormat:@"%ld",ninefall_time] name:name];
    [list addObject:targetmodel];
    int paiming = [self demo2:list model:targetmodel];
    if ([targetmodel.profitnumber floatValue] < 30) {
        [self checkBuyName:name resultList:resultList paiming:paiming];
        NSLog(@"%@",targetmodel);
    }
    [list removeObject:targetmodel];
}




@end















