//
//  Enter2.m
//  gupiao
//
//  Created by xiaobao on 2023/2/6.
//

#import "Enter2.h"
#import "HTTPEngine.h"
#import "GupiaoHangqingModel.h"
#import "GupiaoViewController.h"

@interface Enter2 ()
@property (nonatomic,strong) NSMutableArray *instrumentIDDatas; //!< 股票代码  用于网络请求

@property (nonatomic,strong) NSMutableArray *instrumentIDDatas2; //!< 股票代码 用于页面显示

@property (nonatomic,assign) int countNumber;
@end

@implementation Enter2

- (void)enter:(GupiaoType)type{
    
    self.paimingList = [NSMutableArray array];
    self.originList = [NSMutableArray array];
    self.mutArr1 = [NSMutableArray array];
    self.mutArr2 = [NSMutableArray array];
    self.infoDatas = [NSMutableArray array];
    self.infoDatas1 = [NSMutableArray array];
    self.infoDatas2 = [NSMutableArray array];
    self.infoModelPaiming = [NSMutableArray array];
    self.instrumentIDDatas = [NSMutableArray array];
    self.instrumentIDDatas2 = [NSMutableArray array];
    
    //    历史卖出点数据
    [self fun1];
    
    
    
    // 历史 最低点以后出现的最佳买点
    
    [self fun2];
    
    
    
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSString *title in self.originList) {
        NSString *day = [title componentsSeparatedByString:@"天"].firstObject;
        if([day intValue] < 10000){
            [tempArr addObject:title];
        }
    }
    [self sortType:@"按照上涨点数001" data:tempArr];
    [self sortType:@"按照年华收益率002" data:tempArr];
    [self sortType:@"按照上涨天数003" data:tempArr];
    // 如港股hk00001，沪市sh000001，深市sz000001
    
    // 当前条件单数据
//    [self crrrentTianjianDan];
    
    if(type == GupiaoTypeCommon){
        [self crrrentTianjianDan_best];
    } else if(type == GupiaoType_55_best){
        [self crrrentTianjianDan_55_best];
    }
    
    NSMutableArray *tempMutarr = [NSMutableArray array];
    
    // 每次发送。10只股票数据 每秒可以请求20次,为了保险起见,每19次间隔1.5秒
    for (NSString *title in self.instrumentIDDatas) {
        [tempMutarr addObject:title];
        if([tempMutarr count] >= 10){
            self.countNumber++;
            if(self.countNumber >=19){
                self.countNumber = 0;
                [NSThread sleepForTimeInterval:1.5];
            }
            [self networkRequest:tempMutarr];
            [tempMutarr removeAllObjects];
        }else {
            NSLog(@"2876872364587236458236485: %@",title);
        }
    }
    if([tempMutarr count] > 0){
        [self networkRequest:tempMutarr];
    }
    
    
    if(self.instrumentIDDatas2.count != self.instrumentIDDatas.count){
        [NSThread sleepForTimeInterval:1.5];
        [self.instrumentIDDatas2 removeAllObjects];
        
        if(type == GupiaoTypeCommon){
            [self crrrentTianjianDan_best];
        } else if(type == GupiaoType_55_best){
            [self crrrentTianjianDan_55_best];
        }
    }
    
    
    
    
    [[GQBSettingmanager shareInstance].instrumentIDDatas removeAllObjects];
    [[GQBSettingmanager shareInstance].instrumentIDDatas addObjectsFromArray:self.instrumentIDDatas];
    if(self.showTableviewBlock){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.showTableviewBlock();
        });
    }
}

#pragma mark - 根据股票代码请求行情



-(void)networkRequest:(NSArray *)arr {
    
    
    NSString *url = @"https://apis.tianapi.com/finance/index";
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSString *key = @"";
    
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale systemLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:date];
    NSString *timeStr = [dateStr componentsSeparatedByString:@" "].lastObject;
    
    //    if([timeStr compare:@"12:00:00"] == NSOrderedAscending){
    //        key = @"044359093026aa89903837ad2355a2ea";//huxu625  12点前
    //    } else {
    //        key = @"823f175452116a6f4bb7bdf327d65441";// 戈强宝
    //    }
    key = @"823f175452116a6f4bb7bdf327d65441";// 戈强宝
    NSString *code = [arr componentsJoinedByString:@","];
    NSString *list = @"1";
    
    //    sz002752  读取不出来
    [parameters setObject:key forKey:@"key"];
    [parameters setObject:code forKey:@"code"];
    [parameters setObject:list forKey:@"list"];
    
    NSLog(@"发送请求时间");
    [[HTTPEngine sharedEngine] getRequestWithURL:url parameters:parameters success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        NSError *error = nil;
        NSDictionary *response = responseObject[@"result"];
        NSArray *keys = response.allKeys;
        for (NSString *key in keys) {
            NSString *value = [response objectForKey:key];
            NSString *key2 =[key substringFromIndex:2];
            
            
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key2];
            AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if(model == nil){
                model = [AllInfoModel new];
            }
            
            
            //            AllInfoModel *model = [[AllInfoModel alloc] init];
            model.hanqing = value;
            data = [NSKeyedArchiver archivedDataWithRootObject:model];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:key2];
                [[NSUserDefaults standardUserDefaults] synchronize];
            });
        }
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        
    }];
    
}


#pragma mark - 55最佳版

-(void)crrrentTianjianDan_55_best{
    self.open = YES;
    [self name:@"木林森002745 -55%" max:38.29 min:9.18 max2:19.03 low:5.77 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.56];
    [self name:@"新联电子002546 -55%" max:15.69 min:2.99 max2:6.578 low:3.02 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:2.63];
    [self name:@"读者传媒603999 -50%" max:32.38 min:3.75 max2:8.25 low:4.08 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:3.71];// 4.38 -50%
    [self name:@"人民同泰600829 -55%" max:27.88 min:5.08 max2:10.50 low:4.96 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.72];
    [self name:@"荣晟环保603165 -55%" max:36.03 min:8.23 max2:18.106 low:8.28 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.14];
    [self name:@"日丰股份002953 -55%" max:27.26 min:7.79 max2:17.13 low:7.61 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.71];// 转换120%
    [self name:@"中行机载600372 -55%" max:49.44 min:10.05 max2:22.11 low:9.81 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:9.94];
    [self name:@"佳讯飞鸿300213 -55%" max:25.83 min:4.53 max2:9.96 low:4.26 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.48];
    [self name:@"智能自控002877 -55%" max:17.30 min:5.28 max2:11.616 low:5.63 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.22];
    [self name:@"深振业A000006 -55%" max:18.46 min:3.62 max2:7.96 low:3.29 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.58];// 转换 120%
    [self name:@"银信科技300231 -55%" max:32.94 min:6.10 max2:13.42 low:5.94 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.04];
    [self name:@"苏州高新600736 -55%" max:16.99 min:3.90 max2:8.28 low:3.71 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.72];// 转换 120%
    [self name:@"富春环保002479 -55%" max:24.88 min:3.20 max2:7.04 low:3.28 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.36];// 转换 120%
    [self name:@"兴业股份603928 -55%" max:32.37 min:8.55 max2:14.96 low:7.01 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:6.732];
    [self name:@"盛视科技002990 -55%" max:58.45 min:18.45 max2:40.59 low:17.03 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:18.26];
    [self name:@"城市传媒600229 -55%" max:35.49 min:5.00 max2:11.00 low:5.04 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.95];
    [self name:@"宁波高发603788 -55%" max:44.05 min:10.38 max2:18.59 low:8.69 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.36];
    [self name:@"正裕工业603089 -50%" max:25.08 min:6.02 max2:13.24 low:6.22 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:5.95 ];// 7.04 -50%
    [self name:@"联明股份603006 -55%" max:35.03 min:6.39 max2:14.07 low:6.60 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.32];
    [self name:@"盈峰环境000967 -55%" max:14.29 min:4.75 max2:10.14 low:3.86 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.56];
    [self name:@"铁流股份603926 -55%" max:29.08 min:6.40 max2:14.08 low:6.55 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.33];// 转换120%
    [self name:@"唐源电气300789 -55%" max:43.69 min:12.24 max2:26.18 low:11.65 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:11.78];
    [self name:@"宏达高科002144 -50%" max:47.95 min:7.86 max2:15.39 low:6.96 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.92];
    [self name:@"汉森制药002412 -55%" max:23.15 min:4.56 max2:9.28 low:4.08 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.17];
    [self name:@"东港股份002117 -50%" max:33.10 min:5.97 max2:13.134 low:5.41 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.00];// 7.27 -50%
    [self name:@"金科环境688466 -55%" max:48.90 min:10.94 max2:22.00 low:9.32 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:9.90];
    [self name:@"深圳华强000062 -55%" max:62.99 min:10.04 max2:19.86 low:7.78 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.93];
    [self name:@"中持股份603903 -55%" max:36.61 min:7.29 max2:14.33 low:6.44 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:6.45];// 7.26 -50%
    [self name:@"赛科希德688338 -55%" max:142.38 min:21.60 max2:45.19 low:18.49 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:20.33];
    [self name:@"光电股份600184 -55%" max:40.66 min:8.63 max2:17.41 low:7.11 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.83];
    [self name:@"益盛药业002566 -55%" max:26.61 min:5.23 max2:11.50 low:5.20 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.17];//转换120%
    [self name:@"奇精机械603677 -55%" max:28.45 min:8.69 max2:17.61 low:7.96 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.92];
    [self name:@"泰永长征002927 -55%" max:30.61 min:7.72 max2:16.98 low:7.67 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.64];// 转换 120%
    [self name:@"先锋电子002767 -50%" max:53.76 min:8.32 max2:18.304 low:8.52 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:8.2368];
    [self name:@"雅运股份603790 -55%" max:26.96 min:7.68 max2:15.70 low:7.04 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.06];
    [self name:@"京华激光603607 -50%" max:34.75 min:9.91 max2:21.802 low:9.56 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:9.81];
    [self name:@"北京利尔002392 -55%" max:13.64 min:2.84 max2:6.24 low:2.67 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:2.81];// 转换 120%
    [self name:@"广宇集团002133 -55%" max:16.20 min:2.43 max2:4.76 low:2.15 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:2.14];
    [self name:@"天地数码300743 -55%" max:49.73 min:8.13 max2:17.02 low:7.08 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.65];
    [self name:@"杭电股份603618 -55%" max:18.80 min:3.84 max2:8.44 low:3.63 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.80];
    [self name:@"恒基达鑫002492 -55%" max:16.98 min:4.11 max2:8.20 low:3.56 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:3.69];// 4.14 -50%
    [self name:@"新湖中宝600208 -55%" max:10.40 min:2.62 max2:5.05 low:1.88 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:2.27];
    [self name:@"苏交科300284 -55%" max:20.89 min:5.45 max2:10.69 low:4.06 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.81];
    [self name:@"日上集团002593 -55%" max:14.36 min:2.76 max2:5.76 low:2.51 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:2.59];
    [self name:@"名雕股份002830 -55%" max:39.98 min:7.67 max2:16.65 low:7.22 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.49];
    [self name:@"欣天科技300615 -55%" max:31.54 min:8.45 max2:18.59 low:8.06 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:8.36];
    [self name:@"瑞茂通600180 -55%" max:47.51 min:4.41 max2:9.70 low:3.99 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.36];// 5.25 -50%
    [self name:@"绿城水务601368 -55%" max:25.68 min:4.43 max2:8.49 low:3.67 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:3.82];
    [self name:@"佳禾智能300793 -55%" max:45.50 min:12.07 max2:25.98 low:10.27 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:11.69];
    [self name:@"碧水源300070 -60%" max:25.72 min:5.77 max2:11.64 low:4.18 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:4.65];
    [self name:@"大丰实业603081 -55%" max:31.32 min:9.43 max2:19.13 low:7.62 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.60];
    [self name:@"南京港002040 -50%" max:20.63 min:5.20 max2:11.44 low:4.88 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.14];// 转换 120%
    [self name:@"三联虹普300384 -55%" max:77.58 min:11.20 max2:24.64 low:9.24 lowTime:@"22024.2.6" nowPrice:0.00 now:@"" tjd:11.08];
    [self name:@"美思德603041 -55%" max:30.49 min:8.62 max2:17.59 low:7.56 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.91];
    [self name:@"烽火电子000561 -55%" max:20.97 min:5.87 max2:12.73 low:5.04 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.72];
    [self name:@"梅轮电梯603321 -55%" max:22.16 min:5.82 max2:10.85 low:4.60 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:4.88];// 5.47 -50%
    [self name:@"泰瑞机器603289 -55%" max:21.26 min:6.47 max2:14.234 low:5.90 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.40];
    [self name:@"辉煌科技002296 -55%" max:38.38 min:5.38 max2:11.69 low:4.82 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.26];
    [self name:@"红墙股份002809 -55%" max:39.11 min:7.66 max2:15.07 low:6.16 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:6.78];// 7.58  -50%
    [self name:@"长青股份002391 -55%" max:18.12 min:5.35 max2:11.17 low:4.32 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.02];// 5.68 -50%
    [self name:@"镇海股份603637 -55%" max:20.95 min:6.18 max2:12.55 low:5.04 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:5.64];
    [self name:@"天邑股份300504 -55%" max:61.53 min:13.24 max2:26.37 low:10.24 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:11.86];
    [self name:@"三力士002224 -55%" max:25.51 min:4.84 max2:8.89 low:3.62 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:4.00];
    [self name:@"苏利股份603585 -55%" max:39.58 min:12.77 max2:22.58 low:9.04 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:10.16];
    [self name:@"中环环保300692 -55%" max:23.22 min:5.09 max2:11.07 low:4.38 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.98];
    [self name:@"兆丰股份300695 -55%" max:139.65 min:42.32 max2:81.81 low:31.72 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:36.81];
    [self name:@"浙江众成002522 -50%" max:49.57 min:3.65 max2:8.03 low:3.02 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.61];// 4.52 -50%
    [self name:@"福成股份600965 -55%" max:21.72 min:4.41 max2:9.70 low:3.84 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:4.36];
    [self name:@"建研院603183 -50%" max:17.42 min:3.37 max2:6.80 low:2.66 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:3.06];
    [self name:@"华神科技000790 -55%" max:16.91 min:3.41 max2:7.502 low:3.02 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.37];
    [self name:@"双飞股份300817 -55%" max:27.35 min:9.14 max2:20.108 low:7.85 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:9.04];
    [self name:@"金河生物002688 -55%" max:12.40 min:3.41 max2:7.48 low:2.91 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.36];// 转换 120%
    [self name:@"绿茵生态002887 -55%" max:25.65 min:6.28 max2:13.81 low:5.23 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.21]; // 转换 120%
    [self name:@"闽发铝业002578 -55%" max:9.12 min:2.64 max2:5.80 low:2.22 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:2.61];// 转换 120%
    [self name:@"福达合金603045 -55%" max:57.37 min:9.64 max2:21.208 low:8.13 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:9.54];
    [self name:@"荣泰健康603579 -55%" max:79.90 min:20.68 max2:43.29 low:15.40 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:19.48];
    [self name:@"节能国祯300388 -55%" max:33.60 min:6.40 max2:13.42 low:4.82 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.03];// 6.20 -54.4%
    [self name:@"金海高科603311 -55%" max:51.13 min:8.36 max2:18.38 low:6.40 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:8.27];
    [self name:@"勘设股份603458 -55%" max:33.54 min:7.95 max2:15.94 low:5.36 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.17];
    [self name:@"润欣科技300493 -55%" max:23.35 min:5.38 max2:11.27 low:4.28 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.07];
    [self name:@"沃尔德688028 -55%" max:114.38 min:16.88 max2:37.136 low:11.80 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:16.71];
    [self name:@"博深股份002282 -55%" max:26.24 min:6.21 max2:13.32 low:4.68 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:5.99];
    [self name:@"神宇股份300563 -55%" max:40.37 min:10.47 max2:23.03 low:7.80 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:10.36];//  转换 120%
    [self name:@"易明医药002826 -55%" max:35.13 min:7.32 max2:14.29 low:5.57 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.43];
    [self name:@"美盈森002303 -55%" max:26.33 min:3.33 max2:6.95 low:2.26 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.12];
    [self name:@"中亚股份300512 -55%" max:37.58 min:6.13 max2:13.486 low:4.50 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.06];// 转换 120%
    [self name:@"今创集团603680 -55%" max:29.34 min:7.54 max2:16.58 low:5.48 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.46 ];// 转换 120%
    [self name:@"富煌钢构002743 -55%" max:25.58 min:4.77 max2:10.32 low:3.32 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:4.33];
    [self name:@"华盛昌002980 -55%" max:89.31 min:21.81 max2:47.982 low:14.97 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:21.59];
    [self name:@"华瑞股份300626 -55%" max:21.23 min:6.65 max2:14.63 low:4.39 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.58];
    [self name:@"中胤时尚300901 -55%" max:49.80 min:8.01 max2:17.14 low:5.03 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.71];
    [self name:@"南京聚隆300644 -55%" max:45.57 min:11.90 max2:22.40 low:8.98 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:10.08];// 转换 120%

    self.open = NO;
}

#pragma mark - 价格提醒best

-(void)crrrentTianjianDan_best{
    
    self.open = YES;
    [self name:@"木林森002745 -55%" max:38.29 min:9.18 max2:19.03 low:5.77 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.56];
    [self name:@"新联电子002546 -55%" max:15.69 min:2.99 max2:6.578 low:3.02 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:2.63];
    [self name:@"读者传媒603999 -50%" max:32.38 min:3.75 max2:8.25 low:4.08 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:3.71];// 4.38 -50%
    [self name:@"人民同泰600829 -55%" max:27.88 min:5.08 max2:10.50 low:4.96 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.72];
    [self name:@"荣晟环保603165 -55%" max:36.03 min:8.23 max2:18.106 low:8.28 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.14];
    [self name:@"日丰股份002953 -55%" max:27.26 min:7.79 max2:17.13 low:7.61 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.71];// 转换120%
    [self name:@"中行机载600372 -55%" max:49.44 min:10.05 max2:22.11 low:9.81 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:9.94];
    [self name:@"佳讯飞鸿300213 -55%" max:25.83 min:4.53 max2:9.96 low:4.26 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.48];
    [self name:@"智能自控002877 -55%" max:17.30 min:5.28 max2:11.616 low:5.63 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.22];
    [self name:@"深振业A000006 -55%" max:18.46 min:3.62 max2:7.96 low:3.29 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.58];// 转换 120%
    [self name:@"银信科技300231 -55%" max:32.94 min:6.10 max2:13.42 low:5.94 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.04];
    [self name:@"苏州高新600736 -55%" max:16.99 min:3.90 max2:8.28 low:3.71 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.72];// 转换 120%
    [self name:@"富春环保002479 -55%" max:24.88 min:3.20 max2:7.04 low:3.28 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.36];// 转换 120%
    [self name:@"兴业股份603928 -55%" max:32.37 min:8.55 max2:14.96 low:7.01 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:6.732];
    [self name:@"盛视科技002990 -55%" max:58.45 min:18.45 max2:40.59 low:17.03 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:18.26];
    [self name:@"城市传媒600229 -55%" max:35.49 min:5.00 max2:11.00 low:5.04 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.95];
    [self name:@"宁波高发603788 -55%" max:44.05 min:10.38 max2:18.59 low:8.69 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.36];
    [self name:@"正裕工业603089 -50%" max:25.08 min:6.02 max2:13.24 low:6.22 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:5.95 ];// 7.04 -50%
    [self name:@"联明股份603006 -55%" max:35.03 min:6.39 max2:14.07 low:6.60 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.32];
    [self name:@"盈峰环境000967 -55%" max:14.29 min:4.75 max2:10.14 low:3.86 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.56];
    [self name:@"铁流股份603926 -55%" max:29.08 min:6.40 max2:14.08 low:6.55 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.33];// 转换120%
    [self name:@"唐源电气300789 -55%" max:43.69 min:12.24 max2:26.18 low:11.65 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:11.78];
    [self name:@"宏达高科002144 -50%" max:47.95 min:7.86 max2:15.39 low:6.96 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.92];
    [self name:@"汉森制药002412 -55%" max:23.15 min:4.56 max2:9.28 low:4.08 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.17];
    [self name:@"东港股份002117 -50%" max:33.10 min:5.97 max2:13.134 low:5.41 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.00];// 7.27 -50%
    [self name:@"金科环境688466 -55%" max:48.90 min:10.94 max2:22.00 low:9.32 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:9.90];
    [self name:@"深圳华强000062 -55%" max:62.99 min:10.04 max2:19.86 low:7.78 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.93];
    [self name:@"中持股份603903 -55%" max:36.61 min:7.29 max2:14.33 low:6.44 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:6.45];// 7.26 -50%
    [self name:@"赛科希德688338 -55%" max:142.38 min:21.60 max2:45.19 low:18.49 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:20.33];
    [self name:@"光电股份600184 -55%" max:40.66 min:8.63 max2:17.41 low:7.11 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.83];
    [self name:@"益盛药业002566 -55%" max:26.61 min:5.23 max2:11.50 low:5.20 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.17];//转换120%
    [self name:@"奇精机械603677 -55%" max:28.45 min:8.69 max2:17.61 low:7.96 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.92];
    [self name:@"泰永长征002927 -55%" max:30.61 min:7.72 max2:16.98 low:7.67 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.64];// 转换 120%
    [self name:@"先锋电子002767 -50%" max:53.76 min:8.32 max2:18.304 low:8.52 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:8.2368];
    [self name:@"雅运股份603790 -55%" max:26.96 min:7.68 max2:15.70 low:7.04 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.06];
    [self name:@"京华激光603607 -50%" max:34.75 min:9.91 max2:21.802 low:9.56 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:9.81];
    [self name:@"北京利尔002392 -55%" max:13.64 min:2.84 max2:6.24 low:2.67 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:2.81];// 转换 120%
    [self name:@"广宇集团002133 -55%" max:16.20 min:2.43 max2:4.76 low:2.15 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:2.14];
    [self name:@"天地数码300743 -55%" max:49.73 min:8.13 max2:17.02 low:7.08 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.65];
    [self name:@"杭电股份603618 -55%" max:18.80 min:3.84 max2:8.44 low:3.63 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.80];
    [self name:@"恒基达鑫002492 -55%" max:16.98 min:4.11 max2:8.20 low:3.56 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:3.69];// 4.14 -50%
    [self name:@"新湖中宝600208 -55%" max:10.40 min:2.62 max2:5.05 low:1.88 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:2.27];
    [self name:@"苏交科300284 -55%" max:20.89 min:5.45 max2:10.69 low:4.06 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.81];
    [self name:@"日上集团002593 -55%" max:14.36 min:2.76 max2:5.76 low:2.51 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:2.59];
    [self name:@"名雕股份002830 -55%" max:39.98 min:7.67 max2:16.65 low:7.22 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.49];
    [self name:@"欣天科技300615 -55%" max:31.54 min:8.45 max2:18.59 low:8.06 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:8.36];
    [self name:@"瑞茂通600180 -55%" max:47.51 min:4.41 max2:9.70 low:3.99 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.36];// 5.25 -50%
    [self name:@"绿城水务601368 -55%" max:25.68 min:4.43 max2:8.49 low:3.67 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:3.82];
    [self name:@"佳禾智能300793 -55%" max:45.50 min:12.07 max2:25.98 low:10.27 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:11.69];
    [self name:@"碧水源300070 -60%" max:25.72 min:5.77 max2:11.64 low:4.18 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:4.65];
    [self name:@"大丰实业603081 -55%" max:31.32 min:9.43 max2:19.13 low:7.62 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:8.60];
    [self name:@"南京港002040 -50%" max:20.63 min:5.20 max2:11.44 low:4.88 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.14];// 转换 120%
    [self name:@"三联虹普300384 -55%" max:77.58 min:11.20 max2:24.64 low:9.24 lowTime:@"22024.2.6" nowPrice:0.00 now:@"" tjd:11.08];
    [self name:@"美思德603041 -55%" max:30.49 min:8.62 max2:17.59 low:7.56 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.91];
    [self name:@"烽火电子000561 -55%" max:20.97 min:5.87 max2:12.73 low:5.04 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.72];
    [self name:@"梅轮电梯603321 -55%" max:22.16 min:5.82 max2:10.85 low:4.60 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:4.88];// 5.47 -50%
    [self name:@"泰瑞机器603289 -55%" max:21.26 min:6.47 max2:14.234 low:5.90 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.40];
    [self name:@"辉煌科技002296 -55%" max:38.38 min:5.38 max2:11.69 low:4.82 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.26];
    [self name:@"红墙股份002809 -55%" max:39.11 min:7.66 max2:15.07 low:6.16 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:6.78];// 7.58  -50%
    [self name:@"长青股份002391 -55%" max:18.12 min:5.35 max2:11.17 low:4.32 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.02];// 5.68 -50%
    [self name:@"镇海股份603637 -55%" max:20.95 min:6.18 max2:12.55 low:5.04 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:5.64];
    [self name:@"天邑股份300504 -55%" max:61.53 min:13.24 max2:26.37 low:10.24 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:11.86];
    [self name:@"三力士002224 -55%" max:25.51 min:4.84 max2:8.89 low:3.62 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:4.00];
    [self name:@"苏利股份603585 -55%" max:39.58 min:12.77 max2:22.58 low:9.04 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:10.16];
    [self name:@"中环环保300692 -55%" max:23.22 min:5.09 max2:11.07 low:4.38 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.98];
    [self name:@"兆丰股份300695 -55%" max:139.65 min:42.32 max2:81.81 low:31.72 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:36.81];
    [self name:@"浙江众成002522 -50%" max:49.57 min:3.65 max2:8.03 low:3.02 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.61];// 4.52 -50%
    [self name:@"福成股份600965 -55%" max:21.72 min:4.41 max2:9.70 low:3.84 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:4.36];
    [self name:@"建研院603183 -50%" max:17.42 min:3.37 max2:6.80 low:2.66 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:3.06];
    [self name:@"华神科技000790 -55%" max:16.91 min:3.41 max2:7.502 low:3.02 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.37];
    [self name:@"双飞股份300817 -55%" max:27.35 min:9.14 max2:20.108 low:7.85 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:9.04];
    [self name:@"金河生物002688 -55%" max:12.40 min:3.41 max2:7.48 low:2.91 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.36];// 转换 120%
    [self name:@"绿茵生态002887 -55%" max:25.65 min:6.28 max2:13.81 low:5.23 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.21]; // 转换 120%
    [self name:@"闽发铝业002578 -55%" max:9.12 min:2.64 max2:5.80 low:2.22 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:2.61];// 转换 120%
    [self name:@"福达合金603045 -55%" max:57.37 min:9.64 max2:21.208 low:8.13 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:9.54];
    [self name:@"荣泰健康603579 -55%" max:79.90 min:20.68 max2:43.29 low:15.40 lowTime:@"2024.2.5" nowPrice:0.00 now:@"" tjd:19.48];
    [self name:@"节能国祯300388 -55%" max:33.60 min:6.40 max2:13.42 low:4.82 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.03];// 6.20 -54.4%
    [self name:@"金海高科603311 -55%" max:51.13 min:8.36 max2:18.38 low:6.40 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:8.27];
    [self name:@"勘设股份603458 -55%" max:33.54 min:7.95 max2:15.94 low:5.36 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:7.17];
    [self name:@"润欣科技300493 -55%" max:23.35 min:5.38 max2:11.27 low:4.28 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:5.07];
    [self name:@"沃尔德688028 -55%" max:114.38 min:16.88 max2:37.136 low:11.80 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:16.71];
    [self name:@"博深股份002282 -55%" max:26.24 min:6.21 max2:13.32 low:4.68 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:5.99];
    [self name:@"神宇股份300563 -55%" max:40.37 min:10.47 max2:23.03 low:7.80 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:10.36];//  转换 120%
    [self name:@"易明医药002826 -55%" max:35.13 min:7.32 max2:14.29 low:5.57 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.43];
    [self name:@"美盈森002303 -55%" max:26.33 min:3.33 max2:6.95 low:2.26 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:3.12];
    [self name:@"中亚股份300512 -55%" max:37.58 min:6.13 max2:13.486 low:4.50 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:6.06];// 转换 120%
    [self name:@"今创集团603680 -55%" max:29.34 min:7.54 max2:16.58 low:5.48 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.46 ];// 转换 120%
    [self name:@"富煌钢构002743 -55%" max:25.58 min:4.77 max2:10.32 low:3.32 lowTime:@"2024.2.7" nowPrice:0.00 now:@"" tjd:4.33];
    [self name:@"华盛昌002980 -55%" max:89.31 min:21.81 max2:47.982 low:14.97 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:21.59];
    [self name:@"华瑞股份300626 -55%" max:21.23 min:6.65 max2:14.63 low:4.39 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:6.58];
    [self name:@"中胤时尚300901 -55%" max:49.80 min:8.01 max2:17.14 low:5.03 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:7.71];
    [self name:@"南京聚隆300644 -55%" max:45.57 min:11.90 max2:22.40 low:8.98 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:10.08];// 转换 120%
    
    
    
    
    NSLog(@"------------分割线---------------")
    [self name:@"安诺其300067 -55%" max:11.51 min:2.94 max2:6.468 low:2.80 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:2.91];
    [self name:@"维维股份600300 -55%" max:15.04 min:2.24 max2:4.928 low:2.85 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:2.21];
    [self name:@"浙富控股002266 -55%" max:16.60 min:3.24 max2:7.128 low:3.58 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:3.20];
    [self name:@"浙江交科002061 -55%" max:11.47 min:2.93 max2:6.446 low:3.61 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:2.90];
    [self name:@"北巴传媒600386 -55%" max:12.71 min:2.77 max2:5.61 low:3.58 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:2.52];
    [self name:@"金螳螂002081 -55%" max:24.85 min:6.43 max2:13.20 low:4.00 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.62];
    [self name:@"中国联通600050 -55%" max:10.22 min:3.19 max2:6.41 low:4.40 lowTime:@"2023.11.2" nowPrice:0.00 now:@"" tjd:2.88];
    [self name:@"中金岭南000060 -55%" max:19.07 min:3.03 max2:6.666 low:3.66 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.99];
    [self name:@"东方精工002611 -55%" max:15.54 min:3.10 max2:6.82 low:3.54 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.06];
    [self name:@"华安证券600909 -55%" max:12.18 min:3.52 max2:7.744 low:3.87 lowTime:@"2022.5.9" nowPrice:0.00 now:@"" tjd:3.48];
    [self name:@"中储股份600787 -55%" max:18.26 min:3.86 max2:7.54 low:4.44 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.39];
    [self name:@"重药控股000950 -55%" max:14.79 min:4.31 max2:8.27 low:4.82 lowTime:@"2023.10.27" nowPrice:0.00 now:@"" tjd:3.72];
    [self name:@"三峡旅游002627 -55%" max:14.44 min:3.23 max2:7.106 low:4.22 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:3.19];
    [self name:@"隆鑫通用603766 -55%" max:14.79 min:2.66 max2:5.852 low:3.46 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.63];
    [self name:@"广州发展600098 -55%" max:19.06 min:4.46 max2:9.23 low:4.77 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.1535];
    [self name:@"中国中铁601390 -55%" max:22.76 min:4.16 max2:9.152 low:5.73 lowTime:@"2023.11.10" nowPrice:0.00 now:@"" tjd:4.1184];
    [self name:@"武汉控股600168 -55%" max:18.49 min:4.83 max2:10.626 low:5.64 lowTime:@"2022.5.6" nowPrice:0.00 now:@"" tjd:4.78];
    [self name:@"海峡环保603817 -55%" max:29.62 min:4.88 max2:7.97 low:4.12 lowTime:@"2024.2.6" nowPrice:0.00 now:@"" tjd:4.88];
    [self name:@"铁龙物流600125 -55%" max:19.60 min:4.21 max2:9.262 low:5.63 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.16];
    [self name:@"百洋股份002696 -55%" max:18.45 min:4.51 max2:8.94 low:5.81 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.023];
    [self name:@"史丹利002588 -55%" max:19.05 min:3.17 max2:6.974 low:5.29 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:3.13];
    [self name:@"浦东建设600284 -55%" max:18.04 min:4.64 max2:10.208 low:5.87 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:4.5936];
    [self name:@"世荣兆业002016 -55%" max:17.90 min:4.43 max2:9.746 low:5.08 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.3857];
    [self name:@"中粮科技000930 -55%" max:28.28 min:5.12 max2:11.264 low:6.20 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:5.0688];
    [self name:@"海欣股份600851 -55%" max:19.60 min:5.67 max2:10.96 low:5.57 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:4.932];
    [self name:@"厦门港务000905 -55%" max:26.18 min:5.30 max2:9.84 low:6.70 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.428];
    [self name:@"瑞凌股份300154 -55%" max:19.35 min:3.30 max2:7.26 low:4.40 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.267];
    [self name:@"中山公用000685 -55%" max:20.69 min:4.99 max2:9.70 low:6.20 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.365];
    [self name:@"广日股份600894 -55%" max:28.84 min:3.89 max2:8.558 low:5.52 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:3.85];
    [self name:@"东北证券000686 -55%" max:23.78 min:4.53 max2:9.966 low:5.87 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:4.48];
    [self name:@"浙江东日600113 -55%" max:19.16 min:5.12 max2:11.264 low:5.74 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:5.06];
    [self name:@"国脉科技002093 -55%" max:30.67 min:4.69 max2:10.318 low:6.85 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:4.64];
    [self name:@"金陵饭店601007 -55%" max:27.55 min:5.02 max2:11.044 low:7.38 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.96];
    [self name:@"恒丰纸业600356 -55%" max:16.08 min:4.64 max2:10.208 low:5.77 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.59];
    [self name:@"中铁工业600528 -55%" max:28.15 min:6.98 max2:11.15 low:7.70 lowTime:@"2023.11.10" nowPrice:0.00 now:@"" tjd:5.01];
    [self name:@"方正证券601901 -55%" max:16.96 min:4.34 max2:9.548 low:5.75 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.29];
    [self name:@"东吴证券601555 -55%" max:19.59 min:5.20 max2:10.58 low:5.87 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:4.76];
    [self name:@"中国铁建601186 -55%" max:26.36 min:6.41 max2:12.80 low:7.72 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:5.76];
    [self name:@"中信海直000099 -55%" max:26.44 min:4.76 max2:10.472 low:7.48 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.71];
    [self name:@"中国交建601800 -55%" max:22.60 min:5.69 max2:12.518 low:7.79 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:5.63];
    [self name:@"中国凤凰600679 -55%" max:45.98 min:8.78 max2:18.57 low:7.73 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:8.35];
    [self name:@"皖天然气603689 -55%" max:17.72 min:5.83 max2:12.826 low:7.53 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:5.77];
    [self name:@"宁波中百600857 -55%" max:35.64 min:7.29 max2:14.96 low:8.55 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:6.73];
    [self name:@"五矿发展600058 -55%" max:42.42 min:5.89 max2:12.958 low:8.68 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:5.83];
    [self name:@"锦江在线600650 -55%" max:51.96 min:7.28 max2:13.46 low:8.90 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:6.05];
    [self name:@"白云电器603861 -55%" max:46.26 min:6.87 max2:14.24 low:6.54 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.40];
    [self name:@"开创国际600097 -55%" max:29.53 min:7.61 max2:14.10 low:8.51 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:6.34];
    [self name:@"音飞储存603066 -55%" max:22.11 min:6.84 max2:13.51 low:9.27 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:6.07];
    [self name:@"弘讯科技603015 -55%" max:30.05 min:4.83 max2:10.626 low:7.74 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.78];
    [self name:@"甘咨询000779 -55%" max:31.55 min:7.72 max2:16.984 low:8.24 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:7.64];
    [self name:@"奥普家居603551 -55%" max:23.45 min:5.97 max2:13.134 low:10.11 lowTime:@"2023.9.14" nowPrice:0.00 now:@"" tjd:5.91];
    [self name:@"国光股份002749 -55%" max:21.22 min:6.95 max2:14.98 low:7.48 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:6.74];
    [self name:@"城发环境000885 -55%" max:29.15 min:7.84 max2:15.28 low:10.13 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:6.87];
    [self name:@"时代出版600551 -55%" max:30.03 min:5.68 max2:12.496 low:10.02 lowTime:@"2023.10.30" nowPrice:0.00 now:@"" tjd:5.62];
    [self name:@"格林精密300968 -55%" max:25.91 min:7.19 max2:14.82 low:10.47 lowTime:@"2023.11.2" nowPrice:0.00 now:@"" tjd:6.66];
    [self name:@"华金资本000532 -55%" max:37.94 min:6.78 max2:14.916 low:8.93 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.71];
    [self name:@"浦东金桥600639 -55%" max:34.10 min:7.33 max2:16.126 low:9.54 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:7.25];
    [self name:@"北方导航600435 -55%" max:32.49 min:6.61 max2:13.14 low:9.22 lowTime:@"2023.10.26" nowPrice:0.00 now:@"" tjd:5.91];
    [self name:@"中设股份002883 -55%" max:23.32 min:7.16 max2:15.752 low:9.14 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:7.08];
    [self name:@"天润乳业600419 -55%" max:29.19 min:9.85 max2:20.45 low:11.66 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:9.20];
    [self name:@"如通股份603036 -55%" max:56.41 min:6.46 max2:13.40 low:9.68 lowTime:@"2023.5.8" nowPrice:0.00 now:@"" tjd:6.03];
    [self name:@"中核科技000777 -55%" max:54.25 min:8.12 max2:17.86 low:8.69 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.03];
    [self name:@"泰禾智能603656 -55%" max:37.61 min:8.82 max2:16.50 low:10.61 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:7.42];
    [self name:@"富森美002818 -55%" max:52.30 min:7.44 max2:15.84 low:12.94 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:7.12];
    [self name:@"迪普科技300768 -55%" max:38.74 min:10.93 max2:22.75 low:13.47 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:10.23];
    [self name:@"迪贝电气603320 -55%" max:39.30 min:8.50 max2:16.39 low:9.66 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:7.37];
    [self name:@"亚盛集团600108 -55%" max:14.52 min:2.34 max2:4.45 low:2.38 lowTime:@"2021.2.4" nowPrice:0.00 now:@"" tjd:2.00];
    [self name:@"首创环保600008 -55%" max:7.49 min:2.30 max2:4.33 low:2.45 lowTime:@"2022.9.29" nowPrice:0.00 now:@"" tjd:1.94];
    [self name:@"好当家600467 -55%" max:6.68 min:1.86 max2:3.58 low:2.25 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:1.61];
    [self name:@"金证股份600446 -55%" max:89.42 min:8.69 max2:19.118 low:11.50 lowTime:@"2023.10.27" nowPrice:0.00 now:@"" tjd:8.60];
    [self name:@"双鹭药业002038 -55%" max:32.33 min:7.68 max2:16.09 low:11.18 lowTime:@"2023.11.3" nowPrice:0.00 now:@"" tjd:7.24];
    [self name:@"新宏泽002836 -55%" max:31.98 min:4.39 max2:9.658 low:6.93 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.34];
    [self name:@"健盛集团603558 -55%" max:31.78 min:5.95 max2:13.09 low:7.49 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:5.89];
    [self name:@"汇洁股份002763 -55%" max:26.34 min:4.87 max2:10.714 low:6.38 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.82];
    [self name:@"凤竹纺织600493 -55%" max:26.22 min:3.92 max2:7.39 low:4.66 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:3.32];
    [self name:@"三星新材603578 -55%" max:39.69 min:8.40 max2:17.50 low:13.72 lowTime:@"2023.11.2" nowPrice:0.00 now:@"" tjd:7.87];
    [self name:@"大千生态603955 -55%" max:57.62 min:11.27 max2:24.35 low:13.89 lowTime:@"2023.6.27" nowPrice:0.00 now:@"" tjd:10.95];
    [self name:@"香飘飘603711 -55%" max:37.52 min:10.07 max2:22.154 low:14.74 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:9.96];
    [self name:@"东方环宇603706 -55%" max:54.93 min:10.87 max2:19.18 low:12.75 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:8.63];
    [self name:@"凯众股份603037 -55%" max:32.28 min:9.22 max2:20.284 low:17.39 lowTime:@"2023.11.1" nowPrice:0.00 now:@"" tjd:9.12];
    [self name:@"贵州三力603439 -55%" max:43.75 min:10.64 max2:21.18 low:14.90 lowTime:@"2023.8.24" nowPrice:0.00 now:@"" tjd:9.53];
    [self name:@"长江通信600345 -55%" max:43.78 min:12.02 max2:26.444 low:17.80 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:11.89];
    [self name:@"国恩股份002768 -55%" max:59.87 min:18.80 max2:38.29 low:19.71 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:17.23];
    [self name:@"华兴源创688001 -55%" max:77.75 min:18.69 max2:41.118 low:27.62 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:18.50];
    [self name:@"西藏药业600211 -55%" max:179.27 min:27.60 max2:60.72 low:43.96 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:27.32];
    [self name:@"小熊电器002959 -55%" max:163.30 min:37.83 max2:83.226 low:52.31 lowTime:@"2023.10.24" nowPrice:0.00 now:@"" tjd:37.45];
    [self name:@"新亚制程002388 -55%" max:16.93 min:3.96 max2:8.712 low:5.59 lowTime:@"2023.9.21" nowPrice:0.00 now:@"" tjd:3.92];
    [self name:@"安迪苏600299 -55%" max:30.90 min:8.78 max2:16.30 low:7.45 lowTime:@"2023.10.30" nowPrice:0.00 now:@"" tjd:7.33];
    [self name:@"兴蓉环境000598 -55%" max:12.44 min:3.33 max2:6.32 low:4.35 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.84];
    [self name:@"中油资本000617 -55%" max:16.41 min:4.07 max2:8.954 low:5.81 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.02];
    [self name:@"南京医药600713 -55%" max:19.78 min:3.47 max2:6.31 low:4.61 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:2.83];
    [self name:@"方大集团000055 -55%" max:12.25 min:3.37 max2:7.414 low:3.51 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:3.33];
    [self name:@"合肥百货000417 -55%" max:18.17 min:3.77 max2:7.29 low:3.91 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:3.28];
    [self name:@"华建集团600629 -55%" max:25.61 min:4.15 max2:9.13 low:5.22 lowTime:@"2023.10.23" nowPrice:0.00 now:@"" tjd:4.10];
    [self name:@"万向钱潮000559 -55%" max:25.47 min:3.63 max2:6.84 low:4.31 lowTime:@"2022.4.26" nowPrice:0.00 now:@"" tjd:3.07];
    [self name:@"第一医院600833 -55%" max:37.91 min:6.67 max2:14.674 low:7.43 lowTime:@"2022.4.26" nowPrice:0.00 now:@"" tjd:6.60];
    [self name:@"海通证券600837 -55%" max:29.32 min:6.41 max2:14.102 low:8.04 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:6.34];
    [self name:@"映翰通688080 -55%" max:113.74 min:23.17 max2:50.097 low:36.21 lowTime:@"2023.8.29" nowPrice:0.00 now:@"" tjd:22.93];
    [self name:@"奥海科技002993 -55%" max:85.95 min:24.11 max2:53.042 low:26.78 lowTime:@"2023.5.5" nowPrice:0.00 now:@"" tjd:23.86];
    [self name:@"康拓医疗688314 -55%" max:94.53 min:23.61 max2:44.99 low:27.88 lowTime:@"2023.8.28" nowPrice:0.00 now:@"" tjd:20.24];
    [self name:@"三友医疗688085 -55%" max:73.93 min:13.95 max2:30.69 low:20.08 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:13.81];
    [self name:@"新媒股份300770 -55%" max:138.34 min:27.53 max2:60.42 low:33.67 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:27.18];
    [self name:@"因赛集团300781 -55%" max:49.77 min:13.58 max2:29.876 low:19.02 lowTime:@"2023.9.18" nowPrice:0.00 now:@"" tjd:13.44];
    [self name:@"赛特新材688398 -55%" max:76.99 min:16.06 max2:35.332 low:29.01 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:15.89];
    [self name:@"博瑞医药688166 -55%" max:72.11 min:16.99 max2:37.378 low:20 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:16.82];
    [self name:@"新乳业002946 -55%" max:25.63 min:9.78 max2:17.90 low:13.49 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:8.05];
    [self name:@"锦州港600190 -55%" max:11.64 min:2.37 max2:5.214 low:2.83 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.34];
    [self name:@"秦港股份601326 -55%" max:11.97 min:2.21 max2:4.86 low:2.95 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:2.18];
    [self name:@"中油工程600339 -55%" max:12.86 min:2.10 max2:4.62 low:3.45 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:2.07];
    [self name:@"宁波海运600798 -55%" max:14.79 min:2.59 max2:5.698 low:3.36 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:2.56];
    [self name:@"大禹节水300021 -55%" max:10.75 min:3.39 max2:7.458 low:4.26 lowTime:@"2022.10.28" nowPrice:0.00 now:@"" tjd:3.35];
    [self name:@"天地源600665 -55%" max:11.75 min:2.37 max2:5.214 low:3.67 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:2.34];
    [self name:@"仙乐健康300791 -55%" max:81.93 min:20.58 max2:39.50 low:24.24 lowTime:@"2023.10.10" nowPrice:0.00 now:@"" tjd:17.77];
    [self name:@"宇信科技300674 -55%" max:42.96 min:12.62 max2:22.39 low:15.51 lowTime:@"2023.5.25" nowPrice:0.00 now:@"" tjd:10.07];
    [self name:@"数据港603881 -55%" max:58.52 min:14.45 max2:29.15 low:18.90 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:13.11];
    [self name:@"鼎信通讯603421 -55%" max:43.75 min:5.64 max2:12.05 low:8.57 lowTime:@"2023.5.15" nowPrice:0.00 now:@"" tjd:5.42];
    [self name:@"康斯特300445 -55%" max:48.13 min:9.02 max2:19.76 low:14.46 lowTime:@"2023.8.29" nowPrice:0.00 now:@"" tjd:8.89];
    [self name:@"友讯达300514 -55%" max:23.89 min:7.55 max2:16.61 low:11.56 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:7.47];
    [self name:@"鹭燕医药002788 -55%" max:26.61 min:5.33 max2:10.38 low:7.88 lowTime:@"2022.12.29" nowPrice:0.00 now:@"" tjd:4.67];
    [self name:@"柳药集团603368 -55%" max:47.66 min:12.40 max2:25.50 low:19.46 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:11.47];
    [self name:@"三角轮胎601163 -55%" max:48.86 min:8.94 max2:18.94 low:9.56 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.52];
    [self name:@"神马电力603530 -55%" max:36.35 min:8.68 max2:19.096 low:11.99 lowTime:@"2022.10.10" nowPrice:0.00 now:@"" tjd:8.59];
    [self name:@"彭鹞环保300664 -55%" max:19.51 min:5.73 max2:11.47 low:4.72 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.16];
    [self name:@"南华期货603093 -55%" max:33.72 min:8.32 max2:15.38 low:12.33 lowTime:@"2023.10.16" nowPrice:0.00 now:@"" tjd:6.92];
    [self name:@"新城市300778 -55%" max:38.37 min:10.01 max2:22.022 low:13.45 lowTime:@"2023.10.19" nowPrice:0.00 now:@"" tjd:9.90];
    [self name:@"招商蛇口001979 -55%" max:23.75 min:8.25 max2:17.20 low:11.44 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:7.74];
    [self name:@"三川智慧300066 -55%" max:10.30 min:2.83 max2:6.226 low:3.95 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.80];
    [self name:@"嘉欣丝绸002404 -55%" max:14.47 min:3.31 max2:7.282 low:4.29 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.27];
    [self name:@"海澜之家600398 -55%" max:17.90 min:3.63 max2:7.90 low:7.42 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:3.55];
    [self name:@"诺普信002215 -55%" max:32.02 min:4.65 max2:8.57 low:6.03 lowTime:@"2023.8.24" nowPrice:0.00 now:@"" tjd:3.85];
    [self name:@"云意电气300304 -55%" max:19.96 min:2.99 max2:6.578 low:3.87 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.96];
    [self name:@"楚天高速600035 -55%" max:9.53 min:2.11 max2:4.47 low:3.66 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:1.92];
    [self name:@"北矿科技600980 -55%" max:36.02 min:10.49 max2:21.92 low:11.41 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.86];
    [self name:@"开山股份300257 -55%" max:41.42 min:8.18 max2:17.996 low:13.05 lowTime:@"2023.8.17" nowPrice:0.00 now:@"" tjd:8.09];
    [self name:@"九州通600998 -55%" max:16.09 min:4.90 max2:9.25 low:4.74 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.16];
    [self name:@"方大炭素600516 -55%" max:17.38 min:5.23 max2:11.506 low:5.66 lowTime:@"2023.9.27" nowPrice:0.00 now:@"" tjd:5.17];
    [self name:@"航天信息600271 -55%" max:49.60 min:9.16 max2:16.67 low:11.55 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:7.50];
    [self name:@"国药现代600420 -55%" max:29.52 min:7.67 max2:14.20 low:9.53 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:6.39];
    [self name:@"陕天然气002267 -55%" max:18.89 min:4.41 max2:9.28 low:4.90 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:4.17];
    [self name:@"中原内配002448 -55%" max:23.12 min:3.95 max2:8.09 low:4.97 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.04];
    [self name:@"网宿科技300017 -55%" max:25.28 min:4.24 max2:9.328 low:6.26 lowTime:@"2023.9.21" nowPrice:0.00 now:@"" tjd:4.19];
    [self name:@"富临运业002357 -55%" max:26.72 min:3.95 max2:8.69 low:5.59 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:3.91];
    [self name:@"三江购物601116 -55%" max:53.56 min:7.11 max2:15.642 low:10.70 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:7.03];
    [self name:@"清新环境002573 -55%" max:29.15 min:4.02 max2:8.25 low:4.90 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:3.71];

    [self name:@"凌志软件688588 -55%" max:54.00 min:8.30 max2:20.30 low:12.13 lowTime:@"2023.9.22" nowPrice:0.00 now:@"" tjd:8.21];
    [self name:@"维康药业300878 -55%" shichangID:@"sz" max:60.14 min:18.12 max2:33.58 low:20.10 lowTime:@"2023.8.24" nowPrice:0.00 now:@"" tjd:15.11];
    [self name:@"特宝生物688278 -55%" max:81.14 min:21.01 max2:49.39 low:31.01 lowTime:@"2023.8.29" nowPrice:0.00 now:@"" tjd:22.22];
    [self name:@"金宏气体688106 -55%" max:58.65 min:14.46 max2:28.88 low:22.70 lowTime:@"2023.8.29" nowPrice:0.00 now:@"" tjd:12.99];
    [self name:@"良品铺子603719 -55%" max:86.52 min:21.28 max2:41.05 low:21.85 lowTime:@"2023.9.8" nowPrice:0.00 now:@"" tjd:18.47];
    [self name:@"翔宇医疗688626 -55%" max:124.25 min:24.58 max2:52.46 low:37.00 lowTime:@"2023.8.24" nowPrice:0.00 now:@"" tjd:23.67];
    [self name:@"豪悦护理605009 -55%" max:161.25 min:30.18 max2:56.79 low:37.87 lowTime:@"2023.10.19" nowPrice:0.00 now:@"" tjd:25.55];
    [self name:@"狄耐克300884 -55%" max:39.42 min:8.01 max2:16.32 low:10.37 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:7.34];
    [self name:@"大中矿业001203 -55%" max:28.74 min:9.33 max2:17.28 low:10.35 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:7.77];
    [self name:@"仲景食品300908 -55%" max:125.25 min:28.72 max2:50.84 low:39.22 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:22.87];
    [self name:@"熊猫乳品300898 -55%" max:87.45 min:15.39 max2:31.99 low:23.28 lowTime:@"2023.9.27" nowPrice:0.00 now:@"" tjd:14.39];
    [self name:@"万邦德002082 -55%" max:28.17 min:6.45 max2:14.19 low:6.98 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.38]; // 转换 120%
    [self name:@"城投控股600649 -55%" max:25.95 min:4.25 max2:8.38 low:3.41 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.77];
    [self name:@"道恩股份002838 -55%" max:61.81 min:13.66 max2:30.05 low:15.37 lowTime:@"2023.7.7" nowPrice:0.00 now:@"" tjd:13.52];// 转换 120%
    [self name:@"歌华有线600037 -55%" max:51.45 min:7.06 max2:15.53 low:7.09 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:6.98]; // 转换120%
    [self name:@"重庆建工600939 -55%" max:22.66 min:3.06 max2:6.732 low:3.18 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:3.03];// 转换 120%
    [self name:@"西部证券002673 -55%" max:35.11 min:6.33 max2:12.42 low:5.74 lowTime:@"2022.10.10" nowPrice:0.00 now:@"" tjd:5.58];
    [self name:@"锋龙股份002931 -55%" max:33.97 min:9.81 max2:20.35 low:7.92 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.15];
    [self name:@"合兴包装002228 -55%" max:14.27 min:2.70 max2:5.94 low:2.79 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.67];// 转换 120%
    [self name:@"东兴证券601198 -55%" max:42.46 min:7.31 max2:14.93 low:7.22 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.71];
    [self name:@"腾龙股份603158 -55%" max:20.93 min:6.16 max2:13.55 low:5.26 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.09];// 转换 120%
    [self name:@"重庆燃气600917 -55%" max:20.61 min:5.57 max2:12.25 low:5.88 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.51];// 转换 120%
    [self name:@"光大嘉宝600622 -55%" max:12.57 min:2.42 max2:5.32 low:2.57 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.39];// 转换 120%
    [self name:@"传化智联002010 -55%" max:26.13 min:4.26 max2:9.37 low:4.80 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.21];// 转换 120%
    [self name:@"永安药业002365 -50%" max:28.47 min:7.54 max2:16.58 low:9.04 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:7.46];// 转换120%
    [self name:@"丰林集团601996 -55%" max:8.29 min:2.09 max2:4.34 low:2.37 lowTime:@"2022.6.21" nowPrice:0.00 now:@"" tjd:1.95];// 2.17 -50%
    [self name:@"汉嘉设计300746 -55%" max:42.66 min:8.55 max2:18.81 low:8.31 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:8.46];// 转换 120%
    [self name:@"绿色动力601330 -55%" max:27.23 min:7.20 max2:12.27 low:6.30 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:5.52];
    [self name:@"奥瑞金002701 -55%" max:13.30 min:3.36 max2:7.64 low:4.12 lowTime:@"2022.6.26" nowPrice:0.00 now:@"" tjd:3.43];// 转换 120%
    [self name:@"泸天化000912 -55%" max:17.20 min:3.58 max2:7.87 low:4.39 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:3.54];// 转换 120%
    [self name:@"宏辉果蔬603336 -55%" max:14.41 min:3.93 max2:8.64 low:4.31 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.89];// 转换 120%
    [self name:@"四创电子600990 -55%" max:83.77 min:18.21 max2:37.40 low:18.89 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:16.83];
    [self name:@"康尼机电603111 -55%" max:16.26 min:3.56 max2:7.83 low:4.00 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.52];// 4.25 -50%
    [self name:@"浙江东方600120 -55%" max:12.03 min:3.00 max2:6.55 low:3.22 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.94];
    [self name:@"山西证券002500 -55%" max:22.18 min:4.45 max2:9.66 low:4.52 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.34];
    [self name:@"福星股份000926 -50%" max:18.72 min:3.77 max2:7.62 low:3.67 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.42];
    [self name:@"北部湾港000582 -55%" max:23.36 min:5.88 max2:12.93 low:6.61 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:5.82];// 转换 120%
    [self name:@"珠江钢琴002678 -55%" max:22.32 min:5.05 max2:9.22 low:4.94 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.14];
//    [self name:@"宁波海运600798 -55%" max:14.79 min:2.59 max2:5.69 low:3.42 lowTime:@"2023.6.28" nowPrice:0.00 now:@"" tjd:2.56]; // 转换 120%
//    [self name:@"东方明珠600637 -50%" max:58.21 min:7.11 max2:12.57 low:5.93 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:6.28];// 6.28 -50%
    [self name:@"龙元建设600491 -55%" max:18.24 min:3.99 max2:7.69 low:4.46 lowTime:@"2023.7.17" nowPrice:0.00 now:@"" tjd:3.46];//  转换 120%
    [self name:@"正丹股份300641 -50%" max:17.05 min:3.83 max2:8.426 low:4.34 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.79];// 转换 120%
    [self name:@"中国天楹000035 -55%" max:13.33 min:3.46 max2:7.61 low:4.45 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:3.42];// 3.81 -50%
//    [self name:@"江苏舜天600287 -55%" max:21.75 min:4.01 max2:8.39 low:3.85 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:3.77];// 转换 120%
//    [self name:@"国恩股份002768 -55%" max:59.87 min:18.80 max2:38.29 low:22.55 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:17.23];
    [self name:@"珠海港000507 -55%" max:13.71 min:4.39 max2:8.85 low:4.75 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:3.98];
    [self name:@"新华锦600735 -55%" max:28.50 min:4.33 max2:9.52 low:5.60 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.28];//转换120%
//    [self name:@"维维股份600300 -55%" max:15.06 min:2.26 max2:4.97 low:2.99 lowTime:@"2022.10.28" nowPrice:0.00 now:@"" tjd:2.23]; // 转换120%
//    [self name:@"万通智控300643 -55%" max:28.13 min:9.16 max2:20.15 low:10.73 lowTime:@"2023.4.26" nowPrice:0.00 now:@"" tjd:9.06];// 转换 120%
//    [self name:@"华安证券600909 -55%" max:12.18 min:3.52 max2:7.74 low:3.87 lowTime:@"2022.5.9" nowPrice:0.00 now:@"" tjd:3.48];// 转换 120%
//    [self name:@"健盛集团603558 -55%" max:31.78 min:5.95 max2:13.09 low:7.49 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:5.89];// 转换 120%
//    [self name:@"锌业股份000751 -55%" max:12.89 min:2.50 max2:5.50 low:2.80 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.47];// 转换 120%
    [self name:@"国海证券000750 -55%" max:12.48 min:2.63 max2:5.78 low:3.09 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.60];// 转换 120%
    [self name:@"东方证券600958 -55%" max:34.15 min:7.00 max2:15.40 low:7.37 lowTime:@"2022.10.28" nowPrice:0.00 now:@"" tjd:6.93];// 转换 120%
//    [self name:@"顺发恒业000631 -55%" max:7.12 min:2.11 max2:4.64 low:2.82 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:2.09];// 转换 120%
    [self name:@"畅联股份603648 -55%" max:32.19 min:6.38 max2:14.036 low:7.36 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.31];// 转换 120%
//    [self name:@"龙星化工002442 -55%" max:18.00 min:3.63 max2:7.98 low:3.93 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.59];// 转换 120%
//    [self name:@"蒙草生态300355 -55%" max:14.88 min:2.57 max2:5.65 low:2.97 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:2.54];// 转换 120%
//    [self name:@"飞力达300240 -55%" max:22.15 min:4.59 max2:10.09 low:5.80 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.54];// 转换 120%
    [self name:@"庄园牧场002910 -50%" max:33.36 min:8.73 max2:17.61 low:10.31 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:8.83];
    [self name:@"陆家嘴600663 -55%" max:32.40 min:7.99 max2:16.26 low:8.86 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:7.31];
    [self name:@"广聚能源000096 -55%" max:32.45 min:7.01 max2:13.10 low:6.85 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:5.89];
//    [self name:@"克来机电603960 -55%" max:55.27 min:12.40 max2:26.93 low:15.45 lowTime:@"2023.6.12" nowPrice:0.00 now:@"" tjd:12.11];
    [self name:@"冠豪高新600433 -55%" max:24.27 min:2.38 max2:5.23 low:2.50 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.09];// 转换 120%
    [self name:@"上海环境601200 -55%" max:32.10 min:8.33 max2:15.09 low:8.46 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:6.79];
    [self name:@"郴电国际600969 -55%" max:20.87 min:4.96 max2:10.71 low:6.59 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.81];// 转换 120%
    [self name:@"湖北能源000883 -55%" max:13.89 min:2.88 max2:6.336 low:3.50 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.85];
    [self name:@"国元证券000728 -55%" max:21.13 min:4.65 max2:10.23 low:5.27 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.60];//转换 120%
    [self name:@"精艺股份002295 -55%" max:32.52 min:4.89 max2:10.75 low:5.40 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:4.84];// 转换120%
    [self name:@"上海机电600835 -55%" max:43.09 min:10.90 max2:22.66 low:9.90 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.39];
    [self name:@"华数传媒000156 -55%" max:61.54 min:6.16 max2:12.54 low:6.30 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:5.64];
    [self name:@"依顿电子603328 -55%" max:19.97 min:5.15 max2:11.33 low:4.89 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.09];// 转换 120%
    [self name:@"上海建工600170 -55%" max:9.40 min:2.18 max2:4.15 low:2.45 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:1.86];
    [self name:@"佛山照明000541 -55%" max:16.83 min:3.93 max2:8.04 low:4.72 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:3.61];
    [self name:@"中航产融600705 -55%" max:16.69 min:2.93 max2:5.70 low:3.63 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:2.56];
    [self name:@"大胜达603687 -55%" max:24.80 min:7.11 max2:13.37 low:7.32 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.01];
    [self name:@"贵州百灵002424 -55%" max:37.05 min:5.68 max2:11.97 low:7.40 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.38];
    [self name:@"完美世界002624 -55%" max:41.45 min:9.50 max2:20.90 low:16.69 lowTime:@"2023.5.30" nowPrice:0.00 now:@"" tjd:9.40];// 转换 120%
    [self name:@"科瑞技术002957 -55%" max:44.89 min:10.62 max2:23.36 low:13.94 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:10.51];
    [self name:@"嘉事堂002462 -55%" max:83.77 min:10.40 max2:20.39 low:13.08 lowTime:@"2023.6.27" nowPrice:0.00 now:@"" tjd:9.17];
    [self name:@"上海九百600838 -55%" max:21.46 min:4.54 max2:9.988 low:5.66 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.49];// 转换 120%
    [self name:@"中国核建601611 -55%" max:23.23 min:5.55 max2:11.54 low:7.23 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.19];
    [self name:@"光弘科技300735 -55%" max:26.48 min:7.55 max2:15.30 low:8.61 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:6.88];
    [self name:@"信捷电气603416 -55%" max:108.80 min:25.15 max2:55.33 low:32.90 lowTime:@"2022.10.10" nowPrice:0.00 now:@"" tjd:24.89];// 转换 120%
    [self name:@"新华股份600785 -50%" max:36.66 min:9.67 max2:21.27 low:13.30 lowTime:@"2023.7.7" nowPrice:0.00 now:@"" tjd:9.57];// 转换 120%
    [self name:@"联诚精密002921 -55%" max:38.90 min:9.79 max2:21.53 low:12.00 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:9.69];
    [self name:@"中电环保300172 -55%" max:14.11 min:3.23 max2:7.10 low:4.03 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.20];// 转换 120%
    [self name:@"道明光学002632 -55%" max:16.72 min:4.81 max2:9.81 low:4.42 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.41];
    [self name:@"远东传动002406 -55%" max:18.55 min:4.08 max2:8.12 low:3.97 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:3.65];
    [self name:@"新经典603096 -55%" max:84.29 min:13.60 max2:27.27 low:16.55 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:12.27];
    [self name:@"丸美股份603983 -55%" max:91.71 min:18.66 max2:41.05 low:28.16 lowTime:@"2023.7.17" nowPrice:0.00 now:@"" tjd:18.47];// 转换120%
    [self name:@"安图生物603658 -55%" max:137.05 min:39.18 max2:80.20 low:50.70 lowTime:@"2023.6.28" nowPrice:0.00 now:@"" tjd:36.09];
    [self name:@"中粮糖业600737 -55%" max:27.11 min:5.34 max2:11.17 low:5.97 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.02];
    [self name:@"五方光电002962 -55%" max:31.93 min:8.04 max2:17.68 low:9.85 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:7.95];// 转换 120%
    [self name:@"新疆交建002941 -50%" max:44.19 min:8.92 max2:19.624 low:14.10 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:8.83];// 10.32 -50%
    [self name:@"新疆火炬603080 -50%" max:54.85 min:8.84 max2:19.448 low:10.33 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.75];
    [self name:@"海格通信002465 -55%" max:24.50 min:6.33 max2:13.92 low:7.42 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:6.26];//转换 120%
    [self name:@"银龙股份603969 -55%" max:17.72 min:3.08 max2:6.76 low:3.68 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.04];
    [self name:@"长飞光纤601869 -55%" max:72.91 min:20.14 max2:44.308 low:29.79 lowTime:@"2023.8.25" nowPrice:0.00 now:@"" tjd:19.93];
    [self name:@"航天工程603698 -50%" max:53.94 min:9.55 max2:21.01 low:10.16 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.45];
    [self name:@"无锡银行600908 -55%" max:22.62 min:3.90 max2:7.40 low:4.80 lowTime:@"2022.11.4" nowPrice:0.00 now:@"" tjd:3.33];
    [self name:@"康普顿603798 -55%" max:28.82 min:6.10 max2:12.17 low:8.81 lowTime:@"2023.10.19" nowPrice:0.00 now:@"" tjd:5.47];// 7.19 -50%
    [self name:@"珍宝岛603567 -50%" max:59.02 min:9.94 max2:19.09 low:10.83 lowTime:@"2022.8.2" nowPrice:0.00 now:@"" tjd:8.59];
    [self name:@"中科软603927 -55%" max:75.89 min:19.28 max2:42.416 low:30.94 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:19.08];
    [self name:@"嘉泽新能601619 -55%" max:13.43 min:2.61 max2:5.74 low:3.19 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.58];
    [self name:@"浙江震元000705 -50%" max:27.84 min:5.19 max2:11.14 low:7.60 lowTime:@"2023.6.27" nowPrice:0.00 now:@"" tjd:5.01];
    [self name:@"湖南投资000548 -55%" max:21.25 min:3.42 max2:7.05 low:4.00 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.17];
    [self name:@"亚世光电002952 -50%" max:34.10 min:9.07 max2:19.954 low:14.37 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:8.97];
    [self name:@"桂林三金002275 -55%" max:33.51 min:9.44 max2:20.768 low:14.47 lowTime:@"2023.10.20" nowPrice:0.00 now:@"" tjd:9.34];
    [self name:@"亚普股份603013 -50%" max:46.08 min:9.27 max2:20.09 low:9.08 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.04];
    [self name:@"设计总院603357 -50%" max:21.06 min:5.83 max2:12.826 low:8.56 lowTime:@"2023.10.19" nowPrice:0.00 now:@"" tjd:5.77];// 7.15 -50%
    [self name:@"国信证券002736 -50%" max:32.47 min:5.39 max2:11.858 low:8.15 lowTime:@"2022.5.19" nowPrice:0.00 now:@"" tjd:5.33];// 转换 120%
    [self name:@"亨通光电600487 -55%" max:32.92 min:9.15 max2:20.13 low:13.80 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:9.05];// 转换 120%
    [self name:@"国泰君安601211 -55%" max:33.46 min:10.90 max2:20.04 low:12.62 lowTime:@"2023.1.3" nowPrice:0.00 now:@"" tjd:9.01];
    [self name:@"新兴铸管000778 -50%" max:11.93 min:2.72 max2:5.37 low:3.31 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.41];
    [self name:@"中南传媒601098 -55%" max:28.59 min:6.83 max2:15.02 low:11.20 lowTime:@"2023.7.27" nowPrice:0.00 now:@"" tjd:6.76];//转换120%
    

    self.open = NO;

}

#pragma mark - 当前条件单数据
-(void)crrrentTianjianDan{
    
    if(1){
        NSLog(@"xlpxlp");
        NSLog(@"候选ok转换 ----- start\n");
        self.open = YES;
        [self name:@"哈尔斯002615 -55%" max:16.48 min:3.36 max2:7.39 low:6.36 lowTime:@"2023.4.27" nowPrice:0.00 now:@"" tjd:3.32];// 转换 120%
        [self name:@"方正证券601901 -55%" max:16.97 min:4.35 max2:9.57 low:5.76 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.30];// 转换 120%
        [self name:@"金洲管道002443 -55%" max:18.86 min:3.83 max2:8.42 low:5.70 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:3.79];// 转换 120%
        [self name:@"京能电力600578 -50%" max:9.05 min:2.30 max2:4.34 low:2.56 lowTime:@"2022.4.25" nowPrice:0.00 now:@"" tjd:2.17];
        [self name:@"鸿达兴业002002 -58%" max:15.95 min:2.72 max2:5.98 low:2.03 lowTime:@"2023.6.1" nowPrice:0.00 now:@"" tjd:2.51];// 转换 120%
        [self name:@"思维列控603508 -50%" max:68.18 min:10.60 max2:20.34 low:16.83 lowTime:@"2023.5.16" nowPrice:0.00 now:@"" tjd:10.17];// 转换 120%
        [self name:@"三联虹普300384 -55%" max:77.90 min:10.05 max2:22.11 low:13.52 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.94];// 转换 120%
        [self name:@"信质集团002664 -55%" max:53.33 min:9.54 max2:20.98 low:9.89 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.44];// 转换 120%
        [self name:@"康斯特300445 -50%" max:48.13 min:7.20 max2:15.84 low:9.02 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.92];// 转换 120%
        [self name:@"佛慈制药002644 -55%" max:26.96 min:6.03 max2:13.26 low:12.50 lowTime:@"2023.6.5" nowPrice:0.00 now:@"" tjd:5.96];// 转换 120%
        [self name:@"中衡设计603017 -50%" max:36.37 min:6.42 max2:14.12 low:7.52 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.06];// 转换 120%
        [self name:@"凯发电气300407 -55%" max:34.74 min:5.51 max2:11.51 low:5.78 lowTime:@"2021.6.4" nowPrice:0.00 now:@"" tjd:5.17];
        [self name:@"江河集团601886 -50%" max:20.40 min:4.41 max2:9.68 low:6.51 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.84];// 转换 120%
        [self name:@"新澳股份603889 -50%" max:12.47 min:2.48 max2:5.45 low:5.80 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:2.72];// 转换 120%
        [self name:@"健盛集团603558 -55%" max:31.78 min:5.95 max2:13.09 low:7.49 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:5.89];// 转换 120%
        [self name:@"瑞尔特002790 -55%" max:22.54 min:4.35 max2:9.57 low:7.91 lowTime:@"2023.5.5" nowPrice:0.00 now:@"" tjd:4.30];// 转换 120%
        [self name:@"富邦股份300387 -55%" max:16.89 min:4.80 max2:10.56 low:5.56 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:4.75];// 转换 120%
        [self name:@"腾龙股份603158 -55%" max:20.93 min:6.16 max2:13.55 low:5.26 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.09];// 转换 120%
        [self name:@"多伦科技603528 -55%" max:37.56 min:4.96 max2:10.91 low:4.66 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.91];// 转换 120%
        [self name:@"飞力达300240 -55%" max:22.15 min:4.59 max2:10.09 low:5.80 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.54];// 转换 120%
        [self name:@"新美星300509 -55%" max:24.73 min:4.32 max2:9.50 low:4.66 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.27];// 转换 120%
        [self name:@"亿利达002686 -55%" max:24.79 min:4.78 max2:8.76 low:4.38 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:3.94];
        [self name:@"慈星股份300307 -55%" max:24.92 min:3.90 max2:8.58 low:4.00 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.86];// 转换 120%
        [self name:@"东宝生物300239 -55%" max:14.51 min:3.30 max2:7.26 low:4.60 lowTime:@"2021.2.4" nowPrice:0.00 now:@"" tjd:3.26];// 转换 120%
        
        
        
        self.open = NO;
        NSLog(@"候选ok转换 ------ end\n");
    }
    
    
    if(1){
        NSLog(@"xlpxlp");
        NSLog(@"候选ok ----- start\n");
        self.open = YES;
        [self name:@"恒瑞医药600276 -55%" max:96.91 min:26.68 max2:58.69 low:44.82 lowTime:@"2023.6.14" nowPrice:0.00 now:@"" tjd:26.41];// 转换 120%
        [self name:@"山东药玻600529 -55%" max:76.25 min:18.33 max2:32.97 low:23.23 lowTime:@"2023.5.10" nowPrice:0.00 now:@"" tjd:14.83];
        [self name:@"烽火通信600498 -55%" max:41.45 min:12.15 max2:22.60 low:17.35 lowTime:@"2023.5.50" nowPrice:0.00 now:@"" tjd:10.17];
        [self name:@"天润乳业600419 -55%" max:29.38 min:10.03 max2:20.64 low:17.50 lowTime:@"2023.6.14" nowPrice:0.00 now:@"" tjd:9.28];
        [self name:@"九阳股份002242 -55%" max:43.18 min:11.53 max2:19.98 low:13.99 lowTime:@"2023.6.13" nowPrice:0.00 now:@"" tjd:8.99];
        [self name:@"金证股份600446 -55%" max:32.16 min:8.69 max2:19.11 low:13.03 lowTime:@"2023.5.30" nowPrice:0.00 now:@"" tjd:8.60];// 转换 120%
        [self name:@"天士力600535 -55%" max:38.72 min:8.87 max2:16.81 low:13.80 lowTime:@"2023.5.15" nowPrice:0.00 now:@"" tjd:7.56];
        [self name:@"招商积余001914 -55%" max:38.47 min:11.00 max2:20.82 low:13.17 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:9.36];
        [self name:@"九芝堂000989 -55%" max:38.72 min:5.89 max2:12.95 low:11.41 lowTime:@"2023.5.24" nowPrice:0.00 now:@"" tjd:5.83];// 转换 120%
        [self name:@"沙河股份000014 -55%" max:32.01 min:5.36 max2:11.79 low:8.53 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.30];// 转换 120%
        [self name:@"宇通客车600066 -55%" max:23.26 min:5.40 max2:11.88 low:12.54 lowTime:@"2023.6.8" nowPrice:0.00 now:@"" tjd:5.34];// 转换 120%
        [self name:@"甘咨询000779 -55%" max:31.55 min:7.72 max2:16.98 low:8.24 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:7.64];// 转换 120%
        [self name:@"大恒科技600288 -55%" max:39.63 min:5.90 max2:12.98 low:10.10 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.84];// 转换 120%
        [self name:@"北方导航600435 -55%" max:32.53 min:6.65 max2:13.18 low:10.00 lowTime:@"2023.5.15" nowPrice:0.00 now:@"" tjd:5.93];// 转换 120%
        [self name:@"生物股份600201 -55%" max:30.92 min:7.49 max2:12.66 low:9.90 lowTime:@"2023.6.12" nowPrice:0.00 now:@"" tjd:5.69];
        [self name:@"金陵饭店601007 -55%" max:27.65 min:5.12 max2:11.26 low:8.46 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.06];// 转换 120%
        [self name:@"经纬纺机000666 -55%" max:32.38 min:6.55 max2:12.51 low:8.65 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.62];
        [self name:@"五矿发展600058 -55%" max:42.42 min:5.89 max2:12.95 low:7.93 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:5.83];// 转换 120%
        [self name:@"冠农股份600251 -55%" max:20.95 min:4.50 max2:9.90 low:7.98 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.45];// 转换 120%
        [self name:@"北化股份002246 -55%" max:23.52 min:5.92 max2:13.02 low:7.37 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.86];// 转换 120%
        [self name:@"国脉科技002093 -55%" max:30.67 min:5.66 max2:12.45 low:4.69 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.61];// 转换 120%
        [self name:@"网宿科技300017 -55%" max:25.28 min:4.24 max2:9.32 low:6.51 lowTime:@"2023.7.24" nowPrice:0.00 now:@"" tjd:4.19];// 转换 120%
        [self name:@"中粮糖业600737 -55%" max:27.35 min:5.58 max2:11.41 low:6.21 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.13];
        [self name:@"东方创业600278 -55%" max:27.93 min:6.75 max2:14.85 low:6.20 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.68];// 转换 120%
        [self name:@"航民股份600987 -55%" max:12.34 min:4.01 max2:8.82 low:7.15 lowTime:@"2023.5.25" nowPrice:0.00 now:@"" tjd:3.96];// 转换 120%
        [self name:@"国投资本600061 -55%" max:25.07 min:3.94 max2:8.68 low:5.62 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.90];// 转换 120%
        [self name:@"实益达002137 -55%" max:25.97 min:4.02 max2:8.84 low:6.41 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:3.97];// 转换 120%
        [self name:@"中广核技000881 -55%" max:29.52 min:5.87 max2:12.91 low:6.93 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:5.81];// 转换 120%
        [self name:@"郴电国际600969 -55%" max:20.87 min:4.96 max2:10.71 low:6.59 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.81];// 转换 120%
        [self name:@"诺普信002215 -55%" max:32.02 min:4.67 max2:10.27 low:4.61 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:4.62];// 转换 120%
        [self name:@"重药控股000950 -55%" max:14.85 min:4.37 max2:8.33 low:6.90 lowTime:@"2023.6.13" nowPrice:0.00 now:@"" tjd:3.74];
        [self name:@"华帝股份002035 -55%" max:22.52 min:4.35 max2:7.53 low:6.35 lowTime:@"2023.7.24" nowPrice:0.00 now:@"" tjd:3.38];
        [self name:@"汉森制药002412 -55%" max:23.15 min:4.56 max2:9.28 low:4.75 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.17];
        [self name:@"重庆水务601158 -55%" max:12.82 min:3.70 max2:6.29 low:4.77 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.83];
        [self name:@"三川智慧300066 -55%" max:10.30 min:2.83 max2:6.22 low:3.95 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.80];// 转换 120%
        [self name:@"天地科技600582 -55%" max:12.17 min:2.04 max2:4.48 low:3.26 lowTime:@"2021.11.10" nowPrice:0.00 now:@"" tjd:2.01];// 转换 120%
        [self name:@"碧水源300070 -55%" max:25.72 min:5.77 max2:11.64 low:4.37 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.23];
        [self name:@"江苏舜天600287 -55%" max:21.75 min:4.01 max2:8.39 low:3.85 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:3.77];// 转换 120%
        [self name:@"中国联通600050 -55%" max:10.26 min:3.23 max2:6.45 low:4.65 lowTime:@"2023.5.25" nowPrice:0.00 now:@"" tjd:2.90];
        [self name:@"金螳螂0002081 -65%" max:24.85 min:6.43 max2:13.20 low:4.17 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:4.62];
        [self name:@"盈峰环境000967 -55%" max:14.29 min:4.75 max2:10.14 low:3.90 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.56];
        [self name:@"深振业A000006 -55%" max:18.46 min:3.62 max2:7.96 low:3.55 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.58];// 转换 120%
        [self name:@"中油工程600339 -55%" max:12.90 min:2.44 max2:5.25 low:3.81 lowTime:@"2023.6.13" nowPrice:0.00 now:@"" tjd:2.36];
        [self name:@"城投控股600649 -55%" max:26.06 min:4.36 max2:8.49 low:3.52 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.82];
        [self name:@"泸天化000912 -55%" max:17.20 min:3.58 max2:7.87 low:4.39 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:3.54];// 转换 120%
        [self name:@"湖南投资000548 -55%" max:21.27 min:3.44 max2:7.07 low:4.02 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.18];
        [self name:@"佛塑科技000973 -55%" max:16.00 min:2.77 max2:6.09 low:3.58 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.74];// 转换 120%
        [self name:@"菲达环保600526 -55%" max:30.52 min:3.43 max2:7.54 low:4.58 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:3.43];// 转换 120%
        [self name:@"中原高速600020 -55%" max:9.74 min:2.78 max2:6.11 low:2.62 lowTime:@"2022.11.1" nowPrice:0.00 now:@"" tjd:2.75];// 转换 120%
        [self name:@"阳光照明600261 -55%" max:10.37 min:1.82 max2:4.00 low:2.80 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:1.80];// 转换 120%
        [self name:@"胜利股份000407 -55%" max:12.76 min:2.69 max2:5.58 low:3.19 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.51];
        [self name:@"吉林高速601518 -55%" max:10.48 min:1.99 max2:4.06 low:3.39 lowTime:@"2023.6.13" nowPrice:0.00 now:@"" tjd:1.82];
        [self name:@"国海证券000750 -55%" max:12.48 min:2.63 max2:5.78 low:3.09 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.60];// 转换 120%
        [self name:@"锌业股份000751 -55%" max:12.89 min:2.50 max2:5.50 low:2.80 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.47];// 转换 120%
        [self name:@"陕国投A000563 -55%" max:7.87 min:2.15 max2:4.73 low:2.70 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.12];// 转换 120%
        [self name:@"锦州港600190 -55%" max:11.64 min:2.37 max2:5.21 low:2.83 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.34];// 转换 120%
        [self name:@"顺发恒业000631 -55%" max:7.12 min:2.11 max2:4.64 low:2.82 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:2.09];// 转换 120%
        [self name:@"新湖中宝600208 -55%" max:10.42 min:2.64 max2:5.06 low:2.40 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:2.27];
        [self name:@"腾达建设600512 -55%" max:9.62 min:1.90 max2:4.18 low:2.44 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:1.88];// 转换 120%
        [self name:@"首创环保600008 -55%" max:7.49 min:2.30 max2:4.33 low:2.45 lowTime:@"2022.9.30" nowPrice:0.00 now:@"" tjd:1.94];// 转换 120%
        [self name:@"亚盛集团600108 -55%" max:14.53 min:2.34 max2:4.46 low:2.39 lowTime:@"2021.2.4" nowPrice:0.00 now:@"" tjd:2.00];
        [self name:@"柳药集团603368 -55%" max:47.66 min:12.40 max2:25.50 low:20.25 lowTime:@"2023.6.13" nowPrice:0.00 now:@"" tjd:11.47];
        [self name:@"中南传媒601098 -55%" max:28.59 min:6.83 max2:15.02 low:11.20 lowTime:@"2023.7.27" nowPrice:0.00 now:@"" tjd:6.76];//转换120%
        [self name:@"领益智造002600 -55%" max:14.88 min:3.83 max2:8.42 low:6.46 lowTime:@"2023.6.13" nowPrice:0.00 now:@"" tjd:3.79];
        
        
        
        
        [self name:@"广宇集团002133 -50%" max:16.27 min:2.50 max2:4.83 low:2.94 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.41];
        [self name:@"濮耐股份002225 -55%" max:13.26 min:3.09 max2:5.74 low:3.18 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.58];
        [self name:@"三房港600370 -55%" max:8.30 min:1.85 max2:3.95 low:2.26 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:1.77];
        [self name:@"好当家600467 -55%" max:6.68 min:1.86 max2:3.58 low:2.33 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:1.61];
        [self name:@"浙江鼎力603338 -55%" max:139.70 min:31.82 max2:61.58 low:46.24 lowTime:@"2023.5.15" nowPrice:0.00 now:@"" tjd:27.71];
        [self name:@"老百姓603883 -55%" max:70.46 min:20.61 max2:45.34 low:27.00 lowTime:@"2023.7.21" nowPrice:0.00 now:@"" tjd:20.40];
        [self name:@"国恩股份002768 -55%" max:59.87 min:18.80 max2:38.29 low:22.55 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:17.23];
        [self name:@"四方科技603339 -55%" max:41.35 min:7.08 max2:16.26 low:13.26 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.96];
        [self name:@"完美世界002624 -55%" max:41.45 min:9.50 max2:20.90 low:16.69 lowTime:@"2023.5.30" nowPrice:0.00 now:@"" tjd:9.40];// 转换 120%
        [self name:@"航新科技300424 -55%" max:110.88 min:12.69 max2:27.91 low:8.88 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:12.56];
        [self name:@"三江购物601116 -55%" max:53.56 min:7.11 max2:15.85 low:10.58 lowTime:@"2023.5.25" nowPrice:0.00 now:@"" tjd:7.13];
        [self name:@"中国交建601800 -55%" max:22.81 min:5.91 max2:13.15 low:9.82 lowTime:@"2023.5.26" nowPrice:0.00 now:@"" tjd:5.91];
        [self name:@"鹭燕医药002788 -55%" max:26.91 min:5.63 max2:10.68 low:8.18 lowTime:@"2022.12.29" nowPrice:0.00 now:@"" tjd:4.80];
        [self name:@"依顿电子603328 -55%" max:19.97 min:5.15 max2:11.33 low:4.89 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.09];// 转换 120%
        [self name:@"银信科技300231 -55%" max:32.94 min:4.96 max2:11.87 low:6.10 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.34];
        [self name:@"贵州百灵002424 -55%" max:37.05 min:5.68 max2:11.97 low:7.40 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.38];
        [self name:@"万安科技002590 -55%" max:34.14 min:5.48 max2:12.27 low:5.78 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.52];
        [self name:@"润欣科技300493 -55%" max:23.35 min:5.50 max2:12.56 low:5.38 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.65];
        [self name:@"利亚德300296 -55%" max:17.14 min:4.98 max2:10.95 low:5.29 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:4.93];
        [self name:@"兴业证券601377 -50%" max:13.41 min:3.75 max2:8.25 low:5.14 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:4.12];// 转换 120%
        [self name:@"运达科技300440 -55%" max:41.63 min:4.65 max2:10.23 low:5.44 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.60];// 转换 120%
        [self name:@"佳讯飞鸿300213 -55%" max:25.83 min:4.53 max2:9.96 low:4.50 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:4.48];
        [self name:@"百洋股份002696 -55%" max:18.45 min:4.51 max2:8.94 low:4.50 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:4.02];
        [self name:@"道明光学002632 -55%" max:16.72 min:4.81 max2:9.81 low:4.42 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.41];
        [self name:@"中国核电601985 -55%" max:13.55 min:3.62 max2:8.77 low:5.78 lowTime:@"2022.8.2" nowPrice:0.00 now:@"" tjd:3.94];
        [self name:@"苏交科300284 -55%" max:20.89 min:5.45 max2:10.69 low:4.72 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:4.81];
        [self name:@"迪森股份300335 -55%" max:28.16 min:3.91 max2:8.60 low:4.23 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.87];
        [self name:@"第一创业002797 -55%" max:28.23 min:3.90 max2:8.58 low:4.95 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.86];// 转换 120%
        [self name:@"金河生物002688 -55%" max:12.40 min:3.41 max2:7.48 low:3.60 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.36];// 转换 120%
        [self name:@"龙星化工002442 -55%" max:18.00 min:3.63 max2:7.98 low:3.93 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.59];// 转换 120%
        [self name:@"鲍斯股份300441 -55%" max:20.87 min:5.47 max2:12.03 low:5.10 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:5.41];// 转换 120%
        [self name:@"云意电气300304 -55%" max:19.96 min:2.99 max2:6.57 low:4.08 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:2.96];// 转换 120%
        [self name:@"开能健康300272 -55%" max:18.60 min:3.49 max2:7.67 low:4.81 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.45];// 转换 120%
        [self name:@"中建环能300425 -55%" max:13.53 min:3.61 max2:6.82 low:3.74 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:3.06];
        [self name:@"通源石油300164 -55%" max:17.18 min:2.98 max2:6.55 low:3.82 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.94];// 转换 120%
        [self name:@"中电环保300172 -55%" max:14.11 min:3.23 max2:7.10 low:4.03 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.20];// 转换 120%
        //        [self name:@"昇兴股份002752 -55%" max:45.74 min:4.96 max2:8.73 low:3.78 lowTime:@"2022.4.27" nowPrice:0.00 now:@"4.84" tjd:3.92];
        [self name:@"金字火腿002515 -55%" max:15.67 min:3.87 max2:8.16 low:3.65 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.67];
        [self name:@"隆鑫通用603766 -55%" max:14.87 min:2.74 max2:6.37 low:3.54 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.86];
        [self name:@"山东矿机002526 -55%" max:7.33 min:1.72 max2:3.68 low:1.79 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:1.65];
        [self name:@"蒙草生态300355 -55%" max:14.88 min:2.57 max2:5.65 low:2.97 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:2.54];// 转换 120%
        [self name:@"长江通信600345 -55%" max:43.86 min:12.10 max2:21.34 low:19.57 lowTime:@"2023.5.26" nowPrice:0.00 now:@"" tjd:9.60];
        [self name:@"金鹰股份600232 -55%" max:14.78 min:2.92 max2:6.42 low:3.84 lowTime:@"2021.2.9" nowPrice:0.00 now:@"" tjd:2.89];// 转换 120%
        [self name:@"方大炭素600516 -55%" max:17.38 min:5.23 max2:11.50 low:5.90 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:5.17];// 转换 120%
        [self name:@"方正证券601901 -55%" max:16.97 min:4.35 max2:9.57 low:5.76 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.30];// 转换 120%
        [self name:@"鹏翎股份300375 -55%" max:12.96 min:2.71 max2:5.96 low:3.20 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.68];// 转换 120%
        [self name:@"安图生物603658 -55%" max:137.05 min:39.18 max2:80.20 low:50.70 lowTime:@"2023.6.28" nowPrice:0.00 now:@"" tjd:36.09];
        [self name:@"兆丰股份300695 -55%" max:139.65 min:42.32 max2:81.81 low:41.62 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:36.81];
        [self name:@"金牌橱柜603180 -55%" max:87.99 min:19.06 max2:41.93 low:20.99 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:18.86];// 转换 120%
        [self name:@"数据港603881 -55%" max:58.52 min:14.45 max2:29.15 low:21.76 lowTime:@"2022.5.25" nowPrice:0.00 now:@"" tjd:13.11];
        [self name:@"信捷电气603416 -55%" max:108.80 min:25.15 max2:55.33 low:32.90 lowTime:@"2022.10.10" nowPrice:0.00 now:@"" tjd:24.89];// 转换 120%
        [self name:@"联得装备300545 -55%" max:55.64 min:14.76 max2:29.52 low:11.94 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:13.28];// 转换 100%
        [self name:@"新经典603096 -55%" max:85.09 min:14.40 max2:28.07 low:19.52 lowTime:@"2023.5.25" nowPrice:0.00 now:@"" tjd:12.63];
        [self name:@"香飘飘603711 -55%" max:37.68 min:10.23 max2:23.70 low:19.63 lowTime:@"2023.5.19" nowPrice:0.00 now:@"" tjd:10.66];
        [self name:@"桂林三金002275 -55%" max:33.91 min:9.69 max2:21.67 low:17.35 lowTime:@"2023.5.17" nowPrice:0.00 now:@"" tjd:9.75];
        [self name:@"欣天科技300615 -55%" max:31.64 min:8.55 max2:18.60 low:15.24 lowTime:@"2023.5.15" nowPrice:0.00 now:@"" tjd:8.37];
        [self name:@"双一科技300690 -55%" max:45.82 min:12.13 max2:26.68 low:13.74 lowTime:@"2023.4.26" nowPrice:0.00 now:@"" tjd:12.00];// 转换 120%
        [self name:@"苏利股份603585 -55%" max:39.58 min:12.77 max2:22.58 low:14.03 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:10.16];
        [self name:@"克来机电603960 -55%" max:55.27 min:12.40 max2:26.93 low:15.45 lowTime:@"2023.6.12" nowPrice:0.00 now:@"" tjd:12.11];
        [self name:@"道恩股份002838 -55%" max:61.81 min:13.66 max2:30.05 low:15.37 lowTime:@"2023.7.7" nowPrice:0.00 now:@"" tjd:13.52];// 转换 120%
        [self name:@"南京聚隆300644 -55%" max:45.57 min:11.90 max2:22.40 low:8.98 lowTime:@"2024.2.8" nowPrice:0.00 now:@"" tjd:10.08];// 转换 120%
        [self name:@"天邑股份300504 -55%" max:61.53 min:13.24 max2:26.37 low:13.68 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:11.86];
        [self name:@"英派斯002899 -50%" max:49.30 min:10.85 max2:17.10 low:10.24 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.55];
        
        [self name:@"建发合诚603909 -50%" max:32.89 min:6.21 max2:13.76 low:7.92 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:6.88];
        [self name:@"中设股份002883 -50%" max:23.40 min:7.24 max2:18.47 low:9.22 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:9.23];
        [self name:@"名雕股份002830 -50%" max:40.10 min:7.79 max2:16.77 low:8.96 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.38];
        [self name:@"万通智控300643 -55%" max:28.13 min:9.16 max2:20.15 low:10.73 lowTime:@"2023.4.26" nowPrice:0.00 now:@"" tjd:9.06];// 转换 120%
        [self name:@"如通股份603036 -50%" max:56.60 min:6.65 max2:13.59 low:6.68 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.79];
        [self name:@"光弘科技300735 -55%" max:26.48 min:7.55 max2:15.30 low:8.61 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:6.88];
        [self name:@"泰瑞机器603289 -55%" max:21.41 min:6.50 max2:14.30 low:8.59 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.43];
        [self name:@"鼎信通讯603421 -50%" max:43.81 min:5.70 max2:12.10 low:8.73 lowTime:@"2023.5.12" nowPrice:0.00 now:@"" tjd:6.05];
        [self name:@"智能自控002877 -50%" max:17.33 min:5.31 max2:13.34 low:8.29 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:6.67];
        [self name:@"今飞凯达002863 -50%" max:14.46 min:4.37 max2:11.42 low:4.22 lowTime:@"2022.4.29" nowPrice:0.00 now:@"" tjd:5.13];
        [self name:@"雪天盐业600929 -55%" max:23.86 min:4.26 max2:9.37 low:7.24 lowTime:@"2023.5.26" nowPrice:0.00 now:@"" tjd:4.21]; //转换120%
        [self name:@"国芳集团601086 -50%" max:12.33 min:2.83 max2:5.71 low:3.46 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.85];
        [self name:@"绿茵生态002887 -55%" max:25.65 min:6.28 max2:13.81 low:6.66 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.21]; // 转换 120%
        [self name:@"江阴银行002807 -50%" max:19.22 min:3.44 max2:5.02 low:3.67 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.51];
        [self name:@"财通证券601108 -50%" max:20.99 min:5.98 max2:15.28 low:6.57 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.41];
        [self name:@"中环环保300692 -55%" max:23.22 min:5.09 max2:11.07 low:6.16 lowTime:@"2022.9.29" nowPrice:0.00 now:@"" tjd:4.98];
        [self name:@"华瑞股份300626 -55%" max:21.23 min:6.65 max2:14.63 low:6.51 lowTime:@"2021.2.9" nowPrice:0.00 now:@"" tjd:6.58]; // 转换 120%
        [self name:@"小熊电器002959 -55%" max:164.10 min:38.63 max2:91.24 low:80.62 lowTime:@"2023.5.12" nowPrice:0.00 now:@"" tjd:41.05];
        [self name:@"新产业300832 -50%" max:106.28 min:31.16 max2:64.20 low:51.36 lowTime:@"2023.5.12" nowPrice:0.00 now:@"" tjd:32.10];
        [self name:@"中科软603927 -50%" max:76.44 min:19.83 max2:45.97 low:37.45 lowTime:@"2023.5.4" nowPrice:0.00 now:@"" tjd:22.98];
        [self name:@"长飞光纤601869 -50%" max:73.38 min:20.60 max2:48.12 low:30.94 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:24.06];
        [self name:@"特宝生物688278 -50%" max:81.35 min:21.22 max2:49.60 low:42.81 lowTime:@"2023.5.12" nowPrice:0.00 now:@"" tjd:24.80];
        [self name:@"丸美股份603983 -55%" max:91.71 min:18.66 max2:41.05 low:28.16 lowTime:@"2023.7.17" nowPrice:0.00 now:@"" tjd:18.47];// 转换120%
        [self name:@"华盛昌002980 -55%" max:89.31 min:21.81 max2:47.96 low:27.64 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:21.58];
        [self name:@"三友医疗688085 -50%" max:81.40 min:15.43 max2:37.79 low:24.73 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:18.89];
        [self name:@"唐源电气300789 -55%" max:43.69 min:12.24 max2:24.84 low:18.86 lowTime:@"2023.6.28" nowPrice:0.00 now:@"" tjd:11.17];
        [self name:@"瑞玛精密002976 -50%" max:43.95 min:13.60 max2:31.77 low:20.50 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:15.88];
        [self name:@"奥普家居603551 -50%" max:24.20 min:6.72 max2:12.66 low:10.92 lowTime:@"2023.5.10" nowPrice:0.00 now:@"" tjd:6.33];
        [self name:@"佳禾智能300793 -50%" max:45.65 min:12.22 max2:21.70 low:16.25 lowTime:@"2023.4.28" nowPrice:0.00 now:@"" tjd:10.85];
        [self name:@"天味食品603317 -55%" max:48.54 min:10.59 max2:22.27 low:14.36 lowTime:@"2023.6.28" nowPrice:0.00 now:@"" tjd:10.02];//转换120%
        [self name:@"宇信科技300674 -50%" max:43.10 min:12.76 max2:22.53 low:17.30 lowTime:@"2023.5.12" nowPrice:0.00 now:@"" tjd:11.26];
        [self name:@"贵州三力603439 -50%" max:43.85 min:10.74 max2:21.28 low:13.57 lowTime:@"2022.12.28" nowPrice:0.00 now:@"" tjd:10.64];
        [self name:@"东方环宇603706 -50%" max:55.63 min:11.57 max2:19.88 low:13.45 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:9.94];
        [self name:@"福达合金603045 -50%" max:57.41 min:9.68 max2:24.75 low:11.66 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:12.37];
        [self name:@"科瑞技术002957 -55%" max:44.89 min:10.62 max2:23.36 low:13.94 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:10.51];
        [self name:@"神驰机电603109 -55%" max:35.64 min:11.13 max2:25.33 low:13.35 lowTime:@"2022.12.26" nowPrice:0.00 now:@"" tjd:11.39];
        [self name:@"日丰股份002953 -55%" max:27.26 min:7.79 max2:17.13 low:10.10 lowTime:@"2022.10.10" nowPrice:0.00 now:@"" tjd:7.71];// 转换120%
        [self name:@"迪普科技300768 -55%" max:38.74 min:10.93 max2:22.75 low:15.75 lowTime:@"2023.7.24" nowPrice:0.00 now:@"" tjd:10.23];
        [self name:@"重庆燃气600917 -55%" max:20.61 min:5.57 max2:12.25 low:5.88 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.51];// 转换 120%
        [self name:@"北矿科技600980 -50%" max:36.07 min:8.72 max2:21.97 low:11.46 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.98];// 10.98 -50%
        [self name:@"浦东金桥600639 -50%" max:34.60 min:7.83 max2:16.63 low:10.04 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:8.31];// 8.31 -50%
        [self name:@"华建集团600629 -50%" max:25.63 min:4.38 max2:8.04 low:4.17 lowTime:@"2022.10.28" nowPrice:0.00 now:@"" tjd:4.02];// 4.02 -50%
        [self name:@"光大嘉宝600622 -55%" max:12.57 min:2.42 max2:5.32 low:2.57 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.39];// 转换 120%
        [self name:@"海立股份600619 -55%" max:18.86 min:6.12 max2:12.62 low:5.45 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.67];
        [self name:@"人民同泰600829 -55%" max:27.88 min:4.76 max2:10.47 low:5.08 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.71];// 5.69 -50%
        [self name:@"第一医院600833 -50%" max:38.11 min:6.87 max2:15.85 low:7.62 lowTime:@"2022.4.26" nowPrice:0.00 now:@"" tjd:7.92];// 7.92 -50%
        [self name:@"隧道股份600820 -50%" max:21.11 min:4.35 max2:7.09 low:4.37 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:3.54];// 3.54 -50%
        [self name:@"万向钱潮000559 -50%" max:25.74 min:3.90 max2:7.11 low:4.58 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.55];// 3.55 -50%
        [self name:@"佛山照明000541 -55%" max:16.83 min:3.93 max2:8.04 low:4.72 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:3.61];
        [self name:@"天地源600665 -50%" max:11.87 min:2.49 max2:5.15 low:2.84 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.57];// 2.57 -50%
        [self name:@"陆家嘴600663 -55%" max:32.40 min:7.99 max2:16.26 low:8.86 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:7.31];
        [self name:@"博闻科技600883 -50%" max:26.68 min:4.82 max2:12.36 low:6.48 lowTime:@"2021.1.26" nowPrice:0.00 now:@"" tjd:6.18];// 6.18 -50%
        [self name:@"北部湾港000582 -55%" max:23.36 min:5.88 max2:12.93 low:6.61 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:5.82];// 转换 120%
        [self name:@"烽火电子000561 -50%" max:20.97 min:5.12 max2:11.60 low:9.18 lowTime:@"2023.4.10" nowPrice:0.00 now:@"" tjd:5.80];// 5.80 -50%
        [self name:@"宁波中百600857 -55%" max:35.64 min:7.29 max2:14.96 low:8.07 lowTime:@"2021.9.29" nowPrice:0.00 now:@"" tjd:6.73];
        [self name:@"渝三峡A000565 -55%" max:23.10 min:3.74 max2:8.22 low:5.47 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.70];// 4.60 -50%
        [self name:@"中国天楹000035 -55%" max:13.33 min:3.46 max2:7.61 low:4.45 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:3.42];// 3.81 -50%
        [self name:@"海欣股份600851 -55%" max:19.65 min:5.71 max2:11.01 low:5.65 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.95];// 5.50 -50%
        [self name:@"中金岭南000060 -50%" max:19.17 min:3.13 max2:6.87 low:3.76 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.43];// 3.43 -50%
        [self name:@"中储股份600787 -50%" max:18.36 min:3.96 max2:7.64 low:4.54 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.82];// 3.82 -50%
        [self name:@"新华股份600785 -50%" max:36.66 min:9.67 max2:21.27 low:13.30 lowTime:@"2023.7.7" nowPrice:0.00 now:@"" tjd:9.57];// 转换 120%
        [self name:@"ST实华000637 -55%" max:13.86 min:3.08 max2:6.77 low:3.00 lowTime:@"2023.5.29" nowPrice:0.00 now:@"" tjd:3.04];// 转换 120%
        [self name:@"苏州高新600736 -55%" max:17.04 min:3.95 max2:8.33 low:4.31 lowTime:@"2022.11.1" nowPrice:0.00 now:@"" tjd:3.74];// 转换 120%
        [self name:@"合肥百货000417 -50%" max:18.24 min:3.84 max2:7.36 low:3.98 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:3.68];// 3.68 -50%
        [self name:@"南京医药600713 -50%" max:19.92 min:3.61 max2:6.15 low:5.02 lowTime:@"2022.12.29" nowPrice:0.00 now:@"" tjd:3.07];// 3.07 -50%
        [self name:@"兴蓉环境000598 -50%" max:12.55 min:3.44 max2:6.43 low:4.46 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.21];// 3.21 -50%
        [self name:@"中航产融600705 -55%" max:16.69 min:2.93 max2:5.70 low:3.63 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:2.56];
        [self name:@"方大集团000055 -50%" max:12.25 min:3.37 max2:7.41 low:3.51 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:3.33];// 4.21 -50%
        [self name:@"浙江东方600120 -55%" max:12.03 min:3.00 max2:6.55 low:3.22 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.94];
        [self name:@"浙江东日600113 -50%" max:19.16 min:4.88 max2:10.73 low:5.74 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:4.83];// 6.08 -50%
        [self name:@"广州发展600098 -55%" max:19.06 min:4.46 max2:9.23 low:4.77 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.15];
        [self name:@"中核科技000777 -50%" max:54.37 min:8.24 max2:17.98 low:8.81 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.99];// 8.99 -50%
        [self name:@"开创国际600097 -55%" max:29.53 min:7.61 max2:14.10 low:8.18 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.34];
        [self name:@"新兴铸管000778 -50%" max:12.06 min:2.85 max2:5.50 low:3.44 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.75];
        [self name:@"宁波海运600798 -55%" max:14.79 min:2.59 max2:5.69 low:3.42 lowTime:@"2023.6.28" nowPrice:0.00 now:@"" tjd:2.56]; // 转换 120%
        [self name:@"浙江震元000705 -50%" max:27.90 min:5.25 max2:11.20 low:7.81 lowTime:@"2022.12.27" nowPrice:0.00 now:@"" tjd:5.60];
        [self name:@"城市环境000885 -50%" max:29.32 min:6.64 max2:12.98 low:9.30 lowTime:@"2022.12.29" nowPrice:0.00 now:@"" tjd:6.49];
        [self name:@"上海建工600170 -55%" max:9.45 min:2.23 max2:4.20 low:2.50 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:1.89];
        [self name:@"湖北能源000883 -55%" max:13.95 min:2.94 max2:6.72 low:3.56 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.02];
        [self name:@"铁龙物流600125 -50%" max:19.68 min:4.29 max2:6.91 low:4.31 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.45];
        [self name:@"华神科技000790 -55%" max:16.91 min:3.41 max2:7.83 low:4.04 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.52];
        [self name:@"福星股份000926 -50%" max:18.75 min:3.80 max2:7.65 low:3.70 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.44];
        [self name:@"厦门港务000905 -50%" max:26.24 min:5.36 max2:9.90 low:6.52 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.95];
        [self name:@"大西洋600558 -50%" max:9.76 min:2.47 max2:4.12 low:2.63 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.06];
        [self name:@"北巴传媒600386 -50%" max:12.79 min:2.29 max2:5.39 low:2.80 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:2.69];
        [self name:@"歌华有线600037 -55%" max:51.45 min:7.06 max2:15.53 low:7.09 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:6.98]; // 转换120%
        [self name:@"西南证券600369 -55%" max:13.18 min:2.54 max2:5.58 low:3.34 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.51]; // 转换 120%
        [self name:@"振华重工600320 -50%" max:9.70 min:2.71 max2:4.69 low:2.84 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.34];
        [self name:@"新大陆000997 -50%" max:42.58 min:11.27 max2:21.25 low:10.61 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.62];
        [self name:@"广聚能源000096 -55%" max:32.45 min:7.01 max2:13.10 low:6.85 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:5.89];
        [self name:@"维维股份600300 -55%" max:15.06 min:2.26 max2:4.97 low:2.99 lowTime:@"2022.10.28" nowPrice:0.00 now:@"" tjd:2.23]; // 转换120%
        [self name:@"中航电子600372 -55%" max:49.56 min:10.17 max2:22.37 low:13.93 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.06];
        [self name:@"百利电气600468 -50%" max:16.90 min:2.82 max2:6.56 low:3.34 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.28];
        [self name:@"恒丰纸业600356 -50%" max:16.21 min:4.77 max2:10.72 low:5.90 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.36];
        [self name:@"金自天正600560 -50%" max:31.92 min:5.83 max2:14.12 low:8.81 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:7.06];
        [self name:@"时代出版600551 -50%" max:30.28 min:5.86 max2:13.13 low:9.34 lowTime:@"2022.9.30" nowPrice:0.00 now:@"" tjd:6.56];
        [self name:@"栖霞建设600533 -50%" max:10.15 min:2.28 max2:5.01 low:3.02 lowTime:@"2023.5.30" nowPrice:0.00 now:@"" tjd:2.25];
        [self name:@"浦东建设600284 -55%" max:18.04 min:3.67 max2:8.07 low:5.58 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:3.63];
        [self name:@"广安爱众600979 -50%" max:9.82 min:2.38 max2:4.33 low:3.10 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.16];
        [self name:@"四创电子600990 -55%" max:83.77 min:18.21 max2:37.40 low:18.89 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:16.83];
        [self name:@"凤竹纺织600493 -55%" max:26.22 min:3.98 max2:8.37 low:3.92 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:3.76];
        [self name:@"浙江交科002061 -55%" max:11.47 min:2.93 max2:5.54 low:3.55 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.49];
        [self name:@"东华能源002221 -55%" max:26.67 min:6.60 max2:14.52 low:6.66 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:6.53]; //转换 120%
        [self name:@"宏达高科002144 -50%" max:48.05 min:7.96 max2:15.49 low:11.10 lowTime:@"2023.1.20" nowPrice:0.00 now:@"" tjd:7.74];
        [self name:@"华泰证券601688 -50%" max:48.05 min:7.96 max2:15.49 low:11.10 lowTime:@"2023.1.20" nowPrice:0.00 now:@"" tjd:7.74];
        [self name:@"美盈森002303 -55%" max:26.33 min:3.33 max2:6.95 low:2.75 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.12];
        [self name:@"海通证券600837 -50%" max:29.53 min:6.62 max2:14.56 low:8.25 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:6.55];//转换 120%
        [self name:@"新联电子002546 -50%" max:15.81 min:2.79 max2:6.30 low:3.11 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.15];
        [self name:@"瑞凌股份300154 -55%" max:19.50 min:3.45 max2:7.59 low:4.39 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:3.41];//转换 120%
        [self name:@"九州通600998 -50%" max:22.53 min:6.86 max2:12.95 low:6.63 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.82];
        [self name:@"海格通信002465 -55%" max:24.50 min:6.33 max2:13.92 low:7.42 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:6.26];//转换 120%
        [self name:@"嘉事堂002462 -55%" max:83.77 min:10.40 max2:20.39 low:13.08 lowTime:@"2023.6.27" nowPrice:0.00 now:@"" tjd:9.17];
        [self name:@"新亚制程002388 -50%" max:16.93 min:3.96 max2:10.50 low:6.26 lowTime:@"2023.1.6" nowPrice:0.00 now:@"" tjd:5.25];
        [self name:@"东吴证券601555 -55%" max:19.59 min:5.20 max2:10.58 low:5.87 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:4.76];
        [self name:@"三峡旅游002627 -55%" max:14.44 min:3.23 max2:7.10 low:4.22 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:3.19];//转换120%
        [self name:@"日上集团002593 -55%" max:14.36 min:2.76 max2:5.76 low:3.33 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:2.59];
        [self name:@"闽发铝业002578 -55%" max:9.12 min:2.64 max2:5.80 low:3.88 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.61];// 转换 120%
        [self name:@"宁波高发603788 -55%" max:44.05 min:10.38 max2:18.59 low:9.04 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.36];
        [self name:@"国泰君安601211 -55%" max:33.46 min:10.90 max2:20.04 low:12.62 lowTime:@"2023.1.3" nowPrice:0.00 now:@"" tjd:9.01];
        [self name:@"先锋电子002767 -50%" max:53.76 min:8.32 max2:18.99 low:13.17 lowTime:@"2023.4.4" nowPrice:0.00 now:@"" tjd:9.49];
        [self name:@"音飞储存603066 -50%" max:22.20 min:5.92 max2:13.60 low:10.45 lowTime:@"2023.3.20" nowPrice:0.00 now:@"" tjd:6.80];
        [self name:@"珍宝岛603567 -50%" max:59.05 min:9.96 max2:19.11 low:10.86 lowTime:@"2022.8.2" nowPrice:0.00 now:@"" tjd:9.55];
        [self name:@"国光股份002749 -50%" max:21.42 min:7.15 max2:15.18 low:7.68 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:7.59];
        [self name:@"大千生态603955 -55%" max:57.62 min:11.27 max2:24.35 low:15.54 lowTime:@"2023.3.28" nowPrice:0.00 now:@"" tjd:12.17];
        [self name:@"三星新材603578 -50%" max:39.87 min:8.58 max2:17.28 low:14.50 lowTime:@"2023.4.4" nowPrice:0.00 now:@"" tjd:8.64];
        [self name:@"威星智能002849 -50%" max:32.38 min:7.02 max2:15.44 low:12.71 lowTime:@"2023.5.15" nowPrice:0.00 now:@"" tjd:6.94];
        [self name:@"镇海股份603637 -55%" max:20.95 min:6.18 max2:12.55 low:6.76 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:5.64];
        [self name:@"凯众股份603037 -55%" max:32.28 min:9.53 max2:19.06 low:9.22 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.57];
        [self name:@"荣泰健康603579 -55%" max:79.90 min:20.68 max2:43.29 low:18.25 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:19.48];
        [self name:@"新宏泽002836 -55%" max:31.98 min:4.39 max2:9.65 low:6.93 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.34];
        [self name:@"兴业股份603928 -50%" max:32.37 min:8.55 max2:14.96 low:7.98 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.48];
        [self name:@"步长制药603858 -55%" max:86.31 min:14.19 max2:30.62 low:16.63 lowTime:@"2021.10.28" nowPrice:0.00 now:@"" tjd:13.77];
        [self name:@"富森美002818 -50%" max:53.38 min:8.52 max2:15.68 low:11.51 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:7.84];
        [self name:@"达威股份300535 -50%" max:72.75 min:10.68 max2:20.18 low:11.11 lowTime:@"2021.1.13" nowPrice:0.00 now:@"" tjd:10.09];
        [self name:@"华锋股份002806 -55%" max:49.10 min:7.96 max2:17.51 low:8.67 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.88];// 转换 120%
        [self name:@"中亚股份300512 -55%" max:37.61 min:6.16 max2:13.55 low:6.21 lowTime:@"2021.1.14" nowPrice:0.00 now:@"" tjd:6.09];// 转换 120%
        [self name:@"永泰长征002927 -55%" max:30.61 min:7.72 max2:16.98 low:11.21 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:7.64];// 转换 120%
        [self name:@"新疆火炬603080 -50%" max:55.06 min:9.05 max2:20.46 low:10.54 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.23];
        [self name:@"联诚精密002921 -55%" max:38.90 min:9.79 max2:21.53 low:12.00 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:9.69];
        [self name:@"庄园牧场002910 -50%" max:33.36 min:8.73 max2:17.61 low:10.31 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:8.83];
        [self name:@"京华激光603607 -50%" max:35.25 min:9.59 max2:19.33 low:10.41 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.66];
        [self name:@"洛凯股份603829 -50%" max:29.22 min:8.10 max2:16.05 low:8.00 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.02];
        [self name:@"畅联股份603648 -55%" max:32.54 min:6.73 max2:14.80 low:7.71 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.66];// 转换 120%
        [self name:@"建研院603183 -50%" max:17.46 min:3.41 max2:6.84 low:4.33 lowTime:@"2022.9.27" nowPrice:0.00 now:@"" tjd:3.42];
        [self name:@"睿能科技603933 -50%" max:32.69 min:7.82 max2:17.54 low:8.21 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.77];
        [self name:@"泰禾智能603656 -50%" max:37.71 min:8.92 max2:16.60 low:10.71 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:8.30];
        [self name:@"海鸥股份603269 -50%" max:25.74 min:5.89 max2:12.95 low:6.79 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.83]; // 转换 120%
        [self name:@"迪贝电气603320 -50%" max:39.38 min:8.58 max2:16.47 low:9.74 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:8.23];
        [self name:@"正丹股份300641 -50%" max:17.10 min:3.88 max2:8.53 low:4.39 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.84];// 转换 120%
        [self name:@"美思德603041 -55%" max:30.49 min:8.62 max2:17.59 low:9.37 lowTime:@"2022.4.26" nowPrice:0.00 now:@"" tjd:7.91];
        [self name:@"快意电梯002774 -50%" max:21.89 min:4.66 max2:12.28 low:5.80 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.14];
        [self name:@"利通电子603629 -50%" max:39.28 min:10.53 max2:22.81 low:15.78 lowTime:@"2022.9.30" nowPrice:0.00 now:@"" tjd:11.40];
        [self name:@"汉嘉设计300746 -55%" max:42.78 min:8.67 max2:19.07 low:8.43 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:8.58];// 转换 120%
        [self name:@"亚普股份603013 -50%" max:46.58 min:9.77 max2:20.59 low:9.58 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.29];
        [self name:@"天地数码300743 -50%" max:49.98 min:8.38 max2:16.35 low:9.23 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:8.17];
        [self name:@"锋龙股份002931 -55%" max:33.97 min:9.81 max2:20.35 low:7.92 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.15];
        [self name:@"五方光电002962 -55%" max:31.93 min:8.04 max2:17.68 low:9.85 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:7.95];// 转换 120%
        [self name:@"大胜达603687 -55%" max:24.80 min:7.11 max2:13.37 low:7.32 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.01];
        [self name:@"华兴源创688001 -50%" max:78.05 min:18.99 max2:42.50 low:25.28 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:21.25];
        [self name:@"亚世光电002952 -50%" max:34.50 min:9.47 max2:22.30 low:14.79 lowTime:@"2022.12.30" nowPrice:0.00 now:@"" tjd:11.15];
        [self name:@"金海高科603311 -55%" max:51.13 min:8.36 max2:18.38 low:8.04 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.27];
        [self name:@"史丹利002588 -50%" max:19.10 min:3.22 max2:7.82 low:5.07 lowTime:@"2021.11.10" nowPrice:0.00 now:@"" tjd:3.91];
        [self name:@"航天工程603698 -50%" max:54.04 min:9.65 max2:22.35 low:10.26 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:11.17];
        [self name:@"富临运业002357 -50%" max:26.72 min:3.95 max2:9.01 low:5.85 lowTime:@"2023.4.6" nowPrice:0.00 now:@"" tjd:4.50];
        [self name:@"铁流股份603926 -55%" max:29.08 min:6.40 max2:14.08 low:7.83 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.33];// 转换120%
        [self name:@"永安药业002365 -50%" max:28.47 min:7.54 max2:16.58 low:9.04 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:7.46];// 转换120%
        [self name:@"丽岛新材603937 -50%" max:34.50 min:7.59 max2:16.39 low:10.73 lowTime:@"2022.9.19" nowPrice:0.00 now:@"" tjd:8.19];
        [self name:@"易明医药002826 -55%" max:35.13 min:8.77 max2:16.52 low:7.32 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:7.42];
        [self name:@"沃尔德688028 -50%" max:160.43 min:23.93 max2:57.55 low:29.88 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:28.77];
        [self name:@"世荣兆业002016 -50%" max:17.90 min:4.43 max2:7.96 low:5.05 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.98];
        [self name:@"今创集团603680 -55%" max:29.34 min:7.54 max2:16.58 low:6.37 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.46 ];// 转换 120%
        
        
        
        
        
        
        self.open = NO;
        NSLog(@"候选ok ------ end\n");
        
    }
    
    if(1){
        NSLog(@"股价提醒10点内 xlpxlp");
        self.open = YES;
        [self name:@"联明股份603006 -65.25%" max:35.41 min:7.41 max2:19.48 low:6.77 lowTime:@"2022.4.29" nowPrice:0.00 now:@"" tjd:6.77];// 6.77 -65.25%
        
        
        //        [self printName:@"三力士002224 -50%" maxPrice:25.56 firstMinPrice:4.89 maxPrice2:8.94 lowestPrice:3.96 lowestTime:@"2022.4.27" currentPrice:4.78 currentTime:@"" tiaojiandan:4.47 isPrint:YES]; // 4.47 -50%
        [self name:@"东方证券600958 -55%" max:34.30 min:7.15 max2:15.73 low:7.52 lowTime:@"2022.10.28" nowPrice:0.00 now:@"" tjd:7.07];// 转换 120%
        [self name:@"正平股份603843 -55%" max:24.41 min:3.28 max2:7.21 low:3.53 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:3.24]; // 转换 120%
        [self name:@"安迪苏600299 -55%" max:30.90 min:8.78 max2:16.30 low:7.56 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:7.33];
        [self name:@"万邦德002082 -55%" max:28.17 min:6.45 max2:14.19 low:6.98 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.38]; // 转换 120%
        [self name:@"三力士002224 -55%" max:25.51 min:4.84 max2:8.89 low:3.91 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.00];
        [self name:@"中电兴发002298 -55%" max:40.81 min:5.07 max2:11.15 low:5.08 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.01]; // 转换 120%
        [self name:@"东兴证券601198 -55%" max:42.51 min:7.36 max2:14.98 low:7.27 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.74]; // 7.49 -50%
        [self name:@"富煌钢构002743 -55%" max:25.62 min:4.82 max2:10.36 low:5.03 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.66];
        [self name:@"深圳华强000062 -55%" max:62.99 min:10.04 max2:19.86 low:9.65 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.93];
        [self name:@"木林森002745 -55%" max:38.29 min:9.18 max2:19.03 low:7.83 lowTime:@"2022.10.12" nowPrice:0.00 now:@"" tjd:8.56];
        [self name:@"富春环保002479 -55%" max:24.88 min:3.20 max2:7.04 low:3.96 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:3.16];// 转换 120%
        [self name:@"重庆建工600939 -55%" max:22.69 min:3.08 max2:6.77 low:3.39 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.04];// 转换 120%
        [self name:@"瑞茂通600180 -55%" max:47.51 min:4.41 max2:9.70 low:5.07 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.36];// 5.25 -50%
        [self name:@"上海凤凰600679 -55%" max:45.98 min:8.78 max2:18.57 low:8.00 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:8.35];// 8.54 -54%
        [self name:@"曲美家居603818 -58%" max:40.41 min:6.21 max2:12.42 low:5.21 lowTime:@"2023.6.1" nowPrice:0.00 now:@"" tjd:5.21];// 转换 100%
        [self name:@"节能国祯300388 -55%" max:33.60 min:6.40 max2:13.42 low:5.78 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.03];// 6.20 -54.4%
        [self name:@"绿色动力601330 -55%" max:27.35 min:7.32 max2:12.39 low:6.42 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:5.57];
        [self name:@"上海机电600835 -55%" max:43.09 min:10.90 max2:22.66 low:9.90 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.39];
        [self name:@"康尼机电603111 -55%" max:16.26 min:3.56 max2:7.83 low:4.00 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.52];// 4.25 -50%
        [self name:@"浙富控股002266 -55%" max:16.60 min:3.24 max2:7.12 low:3.78 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.20];//  转换 120%
        [self name:@"神宇股份300563 -55%" max:40.37 min:10.47 max2:23.03 low:9.49 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:10.36];//  转换 120%
        [self name:@"龙元建设600491 -55%" max:18.24 min:3.99 max2:7.69 low:4.46 lowTime:@"2023.7.17" nowPrice:0.00 now:@"" tjd:3.46];//  转换 120%
        [self name:@"博深股份002282 -55%" max:26.24 min:6.21 max2:13.32 low:6.12 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.99];
        [self name:@"北京利尔002392 -55%" max:13.64 min:2.84 max2:6.24 low:3.14 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.81];// 转换 120%
        [self name:@"宏辉果蔬603336 -55%" max:14.41 min:3.93 max2:8.64 low:4.31 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.89];// 转换 120%
        //        [self name:@"中文传媒600373 -50%" max:36.52 min:6.22 max2:17.08 low:8.13 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:8.54];// 8.54 -50%
        [self name:@"荣晟环保603165 -50%" max:36.49 min:8.69 max2:21.19 low:10.45 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:10.59];// 10.59 -50%
        [self name:@"歌力思603808 -51.83%" max:44.94 min:9.16 max2:18.27 low:8.01 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:8.80];// 8.80 -51.83%
        [self name:@"武汉控股600168 -55%" max:18.49 min:4.83 max2:10.62 low:5.64 lowTime:@"2022.5.6" nowPrice:0.00 now:@"" tjd:4.78];// 转换120%
        [self name:@"合兴包装002228 -55%" max:14.27 min:2.70 max2:5.94 low:2.79 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.67];// 转换 120%
        [self name:@"国信证券002736 -50%" max:32.74 min:5.66 max2:12.45 low:8.42 lowTime:@"2022.5.19" nowPrice:0.00 now:@"" tjd:5.60];// 转换 120%
        [self name:@"方正证券601901 -50%" max:16.97 min:4.35 max2:11.90 low:5.76 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.95];// 5.95 -50%
        [self name:@"东北证券000686 -50%" max:23.88 min:4.63 max2:12.08 low:5.97 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:6.04];// 6.04 -50%
        [self name:@"山西证券002500 -55%" max:22.18 min:4.45 max2:9.66 low:4.52 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.34];
        [self name:@"亨通光电600487 -55%" max:32.92 min:9.15 max2:20.13 low:13.80 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:9.05];// 转换 120%
        [self name:@"华安证券600909 -55%" max:12.18 min:3.52 max2:7.74 low:3.87 lowTime:@"2022.5.9" nowPrice:0.00 now:@"" tjd:3.48];// 转换 120%
        [self name:@"西部证券002673 -50%" max:35.11 min:6.33 max2:12.42 low:5.74 lowTime:@"2022.10.10" nowPrice:0.00 now:@"" tjd:5.58];
        [self name:@"弘讯科技603015 -50%" max:30.10 min:4.68 max2:11.48 low:6.32 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:5.74];// 5.74 -50%
        [self name:@"华能热力002893 -50%" max:25.47 min:6.11 max2:13.78 low:6.45 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.89];// 6.89 -50%
        [self name:@"航天工程603698 -55%" max:54.04 min:9.65 max2:21.23 low:10.26 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.55];// 11.17 -50%
        [self name:@"双箭股份002381 -50%" max:21.47 min:4.15 max2:10.88 low:5.15 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.41];// 5.41 -50%
        [self name:@"上海环境601200 -55%" max:32.20 min:8.43 max2:15.19 low:8.56 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:6.83];
        [self name:@"华数传媒000156 -55%" max:61.76 min:6.38 max2:12.76 low:6.52 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:5.74];
        [self name:@"读者传媒603999 -50%" max:32.42 min:3.80 max2:8.76 low:4.50 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.38];// 4.38 -50%
        [self name:@"浙江众成002522 -50%" max:49.57 min:3.65 max2:8.03 low:4.27 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.61];// 4.52 -50%
        [self name:@"绿城水务601368 -55%" max:25.68 min:4.43 max2:8.49 low:5.02 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.82];
        [self name:@"申万宏源000166 -50%" max:15.04 min:3.49 max2:6.66 low:3.80 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.33];
        [self name:@"嘉泽新能601619 -55%" max:13.43 min:2.61 max2:5.74 low:3.19 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.58];
        [self name:@"广州港601228 -50%" max:11.38 min:2.81 max2:4.67 low:3.01 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.33];// 2.33 -50%
        [self name:@"中贝通信603220 -50%" max:41.83 min:10.73 max2:17.62 low:9.44 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:8.81];// 8.88 -50%
        [self name:@"新疆交建002941 -50%" max:44.29 min:9.02 max2:20.65 low:11.50 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:10.32];// 10.32 -50%
        [self name:@"东方明珠600637 -50%" max:58.21 min:7.11 max2:12.57 low:5.93 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:6.28];// 6.28 -50%
        // 提醒
        [self name:@"华金资本000532 -50%" max:38.00 min:6.84 max2:18.60 low:8.99 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:9.30];// 9.30 -50%
        [self name:@"设计总院603357 -50%" max:25.80 min:7.52 max2:14.30 low:8.77 lowTime:@"2022.9.29" nowPrice:0.00 now:@"" tjd:7.15];// 7.15 -50%
        [self name:@"传化智联002010 -55%" max:26.13 min:4.26 max2:9.37 low:4.80 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.21];// 转换 120%
        [self name:@"七匹狼002029 -50%" max:32.98 min:4.58 max2:7.27 low:4.75 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.63];//3.63 -50%
        [self name:@"冠豪高新600433 -55%" max:24.27 min:2.38 max2:5.23 low:2.50 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.09];// 转换 120%
        [self name:@"景兴纸业002067 -50%" max:14.98 min:2.81 max2:4.76 low:3.25 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:2.38];// 2.38 -50%
        [self name:@"中原内配002448 -50%" max:23.23 min:4.06 max2:8.20 low:5.25 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:4.10];// 4.10 -50%
        [self name:@"皖新传媒601801" max:18.84 min:4.06 max2:6.52 low:4.11 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.26];// 3.26
        [self name:@"远东传动002406 -55%" max:18.55 min:4.08 max2:8.12 low:3.97 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:3.65];
        [self name:@"宁波港601018 -55%" max:12.77 min:2.84 max2:5.19 low:2.60 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:2.33];
        [self name:@"龙江交通601188 -50%" max:9.32 min:2.19 max2:4.23 low:2.56 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.11];// 2.11 -50%
        [self name:@"清新环境002573 -55%" max:29.15 min:4.02 max2:8.25 low:5.08 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:3.71];
        [self name:@"德联集团002666 -50%" max:20.57 min:2.87 max2:8.90 low:4.80 lowTime:@"2023.1.3" nowPrice:0.00 now:@"" tjd:4.45];// 4.45 -50%
        [self name:@"奥瑞金002701 -55%" max:13.30 min:3.36 max2:7.64 low:4.12 lowTime:@"2022.6.26" nowPrice:0.00 now:@"" tjd:3.43];// 转换 120%
        [self name:@"东方精工002611 -50%" max:15.54 min:3.10 max2:7.31 low:3.54 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.65];// 3.65 -50%
        [self name:@"丰林集团601996 -55%" max:8.29 min:2.09 max2:4.34 low:2.37 lowTime:@"2022.6.21" nowPrice:0.00 now:@"" tjd:1.95];// 2.17 -50%
        [self name:@"杭电股份603618 -55%" max:18.80 min:3.84 max2:8.44 low:4.31 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.80];
        [self name:@"无锡银行600908 -55%" max:22.62 min:3.90 max2:7.40 low:4.80 lowTime:@"2022.11.4" nowPrice:0.00 now:@"" tjd:3.33];
        [self name:@"银龙股份603969 -55%" max:17.72 min:3.08 max2:6.76 low:3.68 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:3.04];
        [self name:@"苏农银行603323" max:14.85 min:3.82 max2:5.81 low:4.41 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:2.90];// 2.90 -50%
        [self name:@"珠海港000507 -55%" max:13.71 min:4.39 max2:8.85 low:4.75 lowTime:@"2022.5.10" nowPrice:0.00 now:@"" tjd:3.98];
        [self name:@"史丹利002588" max:19.10 min:3.22 max2:7.82 low:5.07 lowTime:@"2021.11.9" nowPrice:0.00 now:@"" tjd:3.91];// 3.91 -50%
        [self name:@"新华锦600735 -55%" max:28.50 min:4.33 max2:9.52 low:5.60 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.28];//转换120%
        [self name:@"北特科技603009 55%" max:27.09 min:5.05 max2:9.85 low:4.76 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.43];// 5.43 -50%
        [self name:@"金洲管道002443" max:18.86 min:3.83 max2:8.42 low:5.70 lowTime:@"2022.12.23" nowPrice:0.00 now:@"" tjd:3.78];// 转换 120%
        [self name:@"嘉欣丝绸002404" max:14.72 min:3.56 max2:8.33 low:4.54 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.16];// 4.16 -50%
        [self name:@"恒基达鑫002492" max:17.06 min:4.19 max2:8.28 low:4.30 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:4.14];// 4.14 -50%
        [self name:@"创业环保600874" max:24.36 min:5.29 max2:8.22 low:5.91 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.11];// 4.11 -50%
        [self name:@"中粮科技000930 -55%" max:28.28 min:5.12 max2:11.26 low:6.98 lowTime:@"2022.4.25" nowPrice:0.00 now:@"" tjd:5.06];// 转换 120%
        [self name:@"国药现代600420 55%" max:29.52 min:7.67 max2:14.20 low:10.50 lowTime:@"2023.7.10" nowPrice:0.00 now:@"" tjd:6.39];// 6.98  -45%
        [self name:@"浙数文化600633 -50%" max:30.55 min:6.66 max2:12.76 low:5.35 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.38];// 6.38  -50%
        [self name:@"红墙股份002809 -50%" max:39.21 min:7.76 max2:15.17 low:8.36 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.58];// 7.58  -50%
        [self name:@"新华文轩601811 -50%" max:33.75 min:6.86 max2:15.76 low:7.48 lowTime:@"2021.2.4" nowPrice:0.00 now:@"" tjd:8.66];// 8.66  -45%
        [self name:@"锦江在线600650 -50%" max:52.25 min:7.57 max2:13.75 low:9.08 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:6.87];// 6.87 -50%
        [self name:@"勘设股份603458 -55%" max:33.54 min:7.95 max2:15.94 low:8.84 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:7.17];
        [self name:@"康普顿603798 -50%" max:37.53 min:8.00 max2:14.39 low:8.37 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.19];// 7.19 -50%
        [self name:@"上海九百600838 -55%" max:21.46 min:4.54 max2:9.988 low:5.66 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.49];// 转换 120%
        [self name:@"广日股份600894 -50%" max:29.02 min:4.07 max2:9.11 low:5.28 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:4.55];// 4.55 -50%
        [self name:@"福达股份603166 -50%" max:43.00 min:3.90 max2:8.58 low:5.90 lowTime:@"2022.10.11" nowPrice:0.00 now:@"" tjd:3.43];// 转换 120%
        [self name:@"海峡环保603817 -50%" max:29.67 min:4.94 max2:8.03 low:5.77 lowTime:@"2023.1.13" nowPrice:0.00 now:@"" tjd:4.01];// 4.01 -50%
        [self name:@"梅轮电梯603321 -50%" max:22.26 min:5.92 max2:10.95 low:5.48 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:5.47];// 5.47 -50%
        [self name:@"中山公用000685 -50%" max:20.91 min:5.21 max2:9.92 low:6.42 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:4.96];// 4.96 -50%
        [self name:@"精艺股份002295 -55%" max:32.52 min:4.89 max2:10.75 low:5.40 lowTime:@"2021.2.8" nowPrice:0.00 now:@"" tjd:4.84];// 转换120%
        [self name:@"南京港002040 -50%" max:20.63 min:5.20 max2:11.44 low:5.26 lowTime:@"2020.2.4" nowPrice:0.00 now:@"" tjd:5.14];// 转换 120%
        [self name:@"杭州解百600814 -50%" max:19.72 min:4.06 max2:8.79 low:5.34 lowTime:@"2022.10.25" nowPrice:0.00 now:@"" tjd:4.39];// 4.39 -50%
        [self name:@"城市传媒600229 -50%" max:35.69 min:5.18 max2:9.63 low:5.20 lowTime:@"2021.7.28" nowPrice:0.00 now:@"" tjd:4.81];// 4.81 -50%
        [self name:@"国元证券000728 -55%" max:21.13 min:4.65 max2:10.23 low:5.27 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.60];//转换 120%
        [self name:@"福成股份600965 -55%" max:21.72 min:4.41 max2:9.70 low:6.12 lowTime:@"2023.6.26" nowPrice:0.00 now:@"" tjd:4.36];
        [self name:@"长青股份002391 -55%" max:18.12 min:5.35 max2:11.17 low:5.90 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.02];// 5.68 -50%
        [self name:@"中国铁建601186 -50%" max:26.64 min:6.94 max2:11.62 low:6.69 lowTime:@"2021.7.28" nowPrice:0.00 now:@"" tjd:5.81];// 5.81 -50%
        [self name:@"陕天然气002267 -50%" max:19.19 min:4.71 max2:9.58 low:5.20 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:4.79];// 4.79 -50%
        [self name:@"中信海直000099 -50%" max:26.51 min:4.82 max2:9.31 low:5.83 lowTime:@"2021.2.9" nowPrice:0.00 now:@"" tjd:4.65];// 4.65 -50%
        [self name:@"东港股份002117 -50%" max:33.32 min:6.19 max2:14.55 low:6.10 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:7.27];// 7.27 -50%
        [self name:@"中马传动603767 -50%" max:18.54 min:4.92 max2:10.82 low:6.15 lowTime:@"2023.4.25" nowPrice:0.00 now:@"" tjd:4.87];// 转换 120%
        [self name:@"中国核建601611 -55%" max:23.23 min:5.55 max2:11.54 low:7.23 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.19];
        [self name:@"中铁工业600528 -55%" max:28.30 min:8.33 max2:14.00 low:7.13 lowTime:@"2022.3.16" nowPrice:0.00 now:@"" tjd:6.3];
        [self name:@"汇洁股份002763 -50%" max:26.34 min:4.87 max2:10.71 low:6.48 lowTime:@"2022.4.28" nowPrice:0.00 now:@"" tjd:4.82];//转换120%
        [self name:@"中持股份603903 -55%" max:36.80 min:7.47 max2:14.52 low:8.29 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:6.53];// 7.26 -50%
        [self name:@"万马股份002276 -50%" max:44.92 min:4.26 max2:12.22 low:5.89 lowTime:@"2022.4.26" nowPrice:0.00 now:@"" tjd:6.11];// 6.11 -50%
        [self name:@"益盛药业002566 -55%" max:26.61 min:5.23 max2:11.50 low:6.43 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.17];//转换120%
        [self name:@"北方国际000065 -50%" max:21.07 min:5.81 max2:12.77 low:7.61 lowTime:@"2022.10.31" nowPrice:0.00 now:@"" tjd:6.38];// 6.38 -50%
        [self name:@"正裕工业603089 -50%" max:25.08 min:6.02 max2:13.24 low:6.24 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:5.95 ];// 7.04 -50%
        [self name:@"安诺其300067 -60%" max:11.51 min:2.94 max2:7.46 low:2.96 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:2.98];// 2.98 -60%
        [self name:@"珠江钢琴002678 -55%" max:22.32 min:5.05 max2:9.22 low:4.94 lowTime:@"2022.4.27" nowPrice:0.00 now:@"" tjd:4.14];
        NSLog(@"当前持仓336\n");
    }
    
}


#pragma mark - 历史 卖出点数据
-(void)fun1{
    NSLog(@"%s",__func__);
    NSMutableArray *mutArr = [NSMutableArray array];
    {
        [mutArr addObject:@"216天 上涨 127% 年化 300%"];
        [mutArr addObject:@"208天 上涨 81% 年化 184%"];
        [mutArr addObject:@"415天 上涨 187% 年化 153%"];
        [mutArr addObject:@"526天 上涨 397% 年化 204%"];
        [mutArr addObject:@"26天 上涨 62% 年化 88554%"];
        [mutArr addObject:@"461天 上涨 347% 年化 227%"];
        [mutArr addObject:@"188天 上涨 105% 年化 306%"];
        [mutArr addObject:@"657天 上涨 227% 年化 93%"];
        [mutArr addObject:@"926天 上涨 446% 年化 95%"];
        [mutArr addObject:@"75天 上涨 102% 年化 3022%"];
        [mutArr addObject:@"588天 上涨 111% 年化 59%"];
        [mutArr addObject:@"685天 上涨 218% 年化 85%"];
        [mutArr addObject:@"413天 上涨 283% 年化 227%"];
        [mutArr addObject:@"83天 上涨 149% 年化 5488%"];
        [mutArr addObject:@"680天 上涨 826% 年化 230%"];
        [mutArr addObject:@"30天 上涨 181% 年化 29610000%"];
        [mutArr addObject:@"659天 上涨 392% 年化 141%"];
        [mutArr addObject:@"203天 上涨 77% 年化 180%"];
        [mutArr addObject:@"463天 上涨 86% 年化 63%"];
        [mutArr addObject:@"689天 上涨 92% 年化 41.3%"];
        [mutArr addObject:@"833天 上涨 403% 年化 103%"];
        [mutArr addObject:@"274天 上涨 203% 年化 338%"];
        [mutArr addObject:@"216天 上涨 102% 年化 228%"];
        [mutArr addObject:@"584天 上涨 188% 年化 93%"];
        [mutArr addObject:@"197天 上涨 89% 年化 228%"];
        [mutArr addObject:@"846天 上涨 235% 年化 68%"];
        [mutArr addObject:@"171天 上涨 75% 年化 232%"];
        [mutArr addObject:@"573天 上涨 101% 年化 56%"];
        [mutArr addObject:@"926天 上涨 145% 年化 42%"];
        [mutArr addObject:@"674天 上涨 113% 年化 50%"];
        [mutArr addObject:@"904天 上涨 141% 年化 42%"];
        [mutArr addObject:@"294天 上涨 103% 年化 140%"];
        [mutArr addObject:@"591天 上涨 145% 年化 74%"];
        [mutArr addObject:@"324天 上涨 175% 年化 213%"];
        [mutArr addObject:@"96天  上涨 113% 年化 1701%"];
        [mutArr addObject:@"752天 上涨 225% 年化 77%"];
        [mutArr addObject:@"197天 上涨 182% 年化 583%"];
        [mutArr addObject:@"664天 上涨 326% 年化 121%"];
        [mutArr addObject:@"267天 上涨 121% 年化 195%"];
        [mutArr addObject:@"701天 上涨 198% 年化 76%"];
        [mutArr addObject:@"79天  上涨 90% 年化 1864%"];
        [mutArr addObject:@"661天 上涨 329% 年化 123%"];
        [mutArr addObject:@"30天  上涨 76% 年化 103345%"];
        [mutArr addObject:@"889天 上涨 238% 年化 64%"];
        [mutArr addObject:@"338天 上涨 88% 年化 97%"];
        [mutArr addObject:@"759天 上涨 276% 年化 89%"];
        [mutArr addObject:@"205天 上涨 89% 年化 210%"];
        [mutArr addObject:@"707天 上涨 125% 年化 52%"];
        [mutArr addObject:@"1004天上涨 368% 年化 75%"];
        [mutArr addObject:@"211天 上涨 130% 年化 324%"];
        [mutArr addObject:@"197天 上涨 81% 年化 200%"];
        [mutArr addObject:@"702天 上涨 132% 年化 55%"];
        [mutArr addObject:@"195天 上涨 63% 年化 152%"];
        [mutArr addObject:@"587天 上涨 457% 年化 191%"];
        [mutArr addObject:@"203天 上涨 115% 年化 297%"];
        [mutArr addObject:@"699天 上涨 177% 年化 70%"];
        [mutArr addObject:@"562天 上涨 170% 年化 90%"];
        [mutArr addObject:@"196天 上涨 195% 年化 650%"];
        [mutArr addObject:@"1038天 上涨 405% 年化 76%"];
        [mutArr addObject:@"188天 上涨 109% 年化 319%"];
        [mutArr addObject:@"590天 上涨 142% 年化 73%"];
        [mutArr addObject:@"177天 上涨 75% 年化 220%"];
        [mutArr addObject:@"679天 上涨 108% 年化 48%"];
        [mutArr addObject:@"167天 上涨 112% 年化 420%"];
        [mutArr addObject:@"484天 上涨 187% 年化 121%"];
        [mutArr addObject:@"158天 上涨 296% 年化 2316%"];
        [mutArr addObject:@"366天 上涨 259% 年化 257%"];
        [mutArr addObject:@"664天 上涨 481% 年化 163%"];
        [mutArr addObject:@"595天 上涨 650% 年化 244%"];
        [mutArr addObject:@"183天 上涨 146% 年化 502%"];
        [mutArr addObject:@"796天 上涨 161% 年化 55%"];
        [mutArr addObject:@"187天 上涨 95% 年化 268%"];
        [mutArr addObject:@"366天 上涨 177% 年化 176%"];
        [mutArr addObject:@"252天 上涨 116% 年化 205%"];
        [mutArr addObject:@"868天 上涨 165% 年化 50%"];
        [mutArr addObject:@"694天 上涨 319% 年化 112%"];
        [mutArr addObject:@"17天 上涨 186% 年化 21000000%"];
        [mutArr addObject:@"229天 上涨 252% 年化 644%"];
        [mutArr addObject:@"337天 上涨 326% 年化 381%"];
        [mutArr addObject:@"552天 上涨 393% 年化 187%"];
        [mutArr addObject:@"764天 上涨 98% 年化 38%"];
        [mutArr addObject:@"866天 上涨 121% 年化 39%"];
        [mutArr addObject:@"267天 上涨 155% 年化 260%"];
        [mutArr addObject:@"702天 上涨 253% 年化 92%"];
        [mutArr addObject:@"896天 上涨 295% 年化 75%"];
        [mutArr addObject:@"348天 上涨 109% 年化 117%"];
        [mutArr addObject:@"474天 上涨 128% 年化 89%"];
        [mutArr addObject:@"301天 上涨 167% 年化 230%"];
        [mutArr addObject:@"555天 上涨 190% 年化 101%"];
        [mutArr addObject:@"658天 上涨 113% 年化 52%"];
        [mutArr addObject:@"534天 上涨 97% 年化 59%"];
        [mutArr addObject:@"1014天 上涨 171% 年化 43%"];
        [mutArr addObject:@"187天 上涨 138% 年化 447%"];
        [mutArr addObject:@"343天 上涨 176% 年化 195%"];
        [mutArr addObject:@"926天 上涨 206% 年化 55%"];
        [mutArr addObject:@"259天 上涨 92% 年化 151%"];
        [mutArr addObject:@"447天 上涨 100% 年化 76%"];
        [mutArr addObject:@"197天 上涨 91% 年化 232%"];
        [mutArr addObject:@"667天 上涨 139% 年化 61%"];
        [mutArr addObject:@"667天 上涨 139% 年化 61%"];
    }
    
    NSMutableArray *tempArr1 = [NSMutableArray array];
    for (NSString *title in mutArr) {
        NSString *day = [title componentsSeparatedByString:@"天"].firstObject;
        if([day intValue] < 10000){
            [tempArr1 addObject:title];
        }
    }
    mutArr = tempArr1;
    
    
    
    NSMutableArray *ceshiMutarr = [NSMutableArray array];
    int index = 0;
    NSLog(@"2387465283765823746582347");
    for (NSString *title in mutArr) {
        int _ = [[title componentsSeparatedByString:@"天"].firstObject intValue];
        int upper = [[[title componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
        int speed = [[title componentsSeparatedByString:@"年化"].lastObject intValue];
        BOOL b1 = upper > 146 && speed > 150; // 上涨146个点以上 并且 年华收益150点以上 24/100
        BOOL b2 = upper > 100 && speed > 100; // 上涨100个点以上 并且 年华收益100点以上 46/100
        BOOL b3 = upper > 90 && speed > 150; // 上涨90个点以上 并且 年华收益150点以上 40/100
        BOOL b4 = upper > 80 && speed > 100; // 上涨80个点以上 并且 年华收益150点以上 54/100
        BOOL b5 = upper > 70 && speed > 200; // 上涨70个点以上 并且 年华收益200点以上 38/100
        BOOL b6 = upper > 100 ;              // 上涨100个点以上  80/100
        
        // b1 || b2 ||b3 || b4 || b5  57/100
        // b1 || b2 ||b3 || b4 || b5 || b6  91/100
        
        if(b1 || b2 || b3 || b4 || b5 || b6){
            index++;
            //            NSLog(@"序号:%2d %@",index,title);
            [ceshiMutarr addObject:title];
        }
    }
    
    
    
    
    
    [self.originList addObjectsFromArray:mutArr];
    for (NSString *current in mutArr) {
        int score = [self rank:self.originList current:current];
        [self.paimingList addObject:[NSString stringWithFormat:@"%d",score]];
    }
    
    [self.paimingList sortUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
        int value1 = [obj1 intValue];
        int value2 = [obj2 intValue];
        if(value1 < value2){
            return NSOrderedAscending;
        } else if (value1 > value2){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    [mutArr sortUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, id  _Nonnull obj2) {
        int value1 = [self rank:self.originList current:obj1];
        int value2 = [self rank:self.originList current:obj2];
        if(value1 < value2){
            return NSOrderedAscending;
        } else if (value1 > value2){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    NSLog(@"\n\n按照上涨点数和上涨速度综合排序\n");
    for (NSString *current in mutArr) {
        [self current:current];
    }
    
    
    
    
    
}


#pragma mark - 历史 最低点以后出现的最佳买点
-(void)fun2 {
    //        最低点以后出现的最佳买点
    //2020年
    [self printName:@"中钢国际000928" maxPrice:17.02 firstMinPrice:2.95 maxPrice2:6.93 lowestPrice:3.04 lowestTime:@"2020.2.4" currentPrice:3.38 currentTime:@"2020.3.23" isPrint:NO];
    [self printName:@"中钢国际000928" maxPrice:17.02 firstMinPrice:2.95 maxPrice2:6.93 lowestPrice:3.04 lowestTime:@"2020.2.4" currentPrice:3.43 currentTime:@"2020.4.28" isPrint:NO];
    [self printName:@"中钢国际000928" maxPrice:17.02 firstMinPrice:2.95 maxPrice2:6.93 lowestPrice:3.04 lowestTime:@"2020.2.4" currentPrice:3.34 currentTime:@"2020.5.22" isPrint:NO];
    [self printName:@"中钢国际000928" maxPrice:17.02 firstMinPrice:2.95 maxPrice2:6.93 lowestPrice:3.04 lowestTime:@"2020.2.4" currentPrice:3.39 currentTime:@"2020.6.29" isPrint:NO];
    [self printName:@"振华股份603067" maxPrice:22.51 firstMinPrice:4.52 maxPrice2:9.96 lowestPrice:4.39 lowestTime:@"2020.2.4" currentPrice:4.68 currentTime:@"2020.4.2" isPrint:NO];
    [self printName:@"纳尔股份002825" maxPrice:41.14 firstMinPrice:8.39 maxPrice2:15.00 lowestPrice:6.76 lowestTime:@"2020.2.4" currentPrice:8.46 currentTime:@"2020.5.28" isPrint:NO];
    [self printName:@"中材节能603126" maxPrice:26.00 firstMinPrice:4.02 maxPrice2:7.90 lowestPrice:3.66 lowestTime:@"2020.2.4" currentPrice:3.81 currentTime:@"2020.6.29" isPrint:NO];
    [self printName:@"安徽建工600502" maxPrice:10.04 firstMinPrice:2.56 maxPrice2:5.04 lowestPrice:2.58 lowestTime:@"2020.2.4" currentPrice:3.08 currentTime:@"2020.5.29" isPrint:NO];
    [self printName:@"弘讯科技603015" maxPrice:30.10 firstMinPrice:5.06 maxPrice2:9.02 lowestPrice:4.68 lowestTime:@"2020.2.4" currentPrice:5.10 currentTime:@"2020.4.28" isPrint:NO];
    [self printName:@"华宏科技002645" maxPrice:17.64 firstMinPrice:4.92 maxPrice2:12.63 lowestPrice:6.63 lowestTime:@"2020.2.4" currentPrice:7.75 currentTime:@"2020.7.1" isPrint:NO];
    [self printName:@"华宏科技002645" maxPrice:17.64 firstMinPrice:4.92 maxPrice2:12.63 lowestPrice:6.63 lowestTime:@"2020.2.4" currentPrice:7.80 currentTime:@"2020.11.2" isPrint:NO];
    [self printName:@"中钢国际000928" maxPrice:13.20 firstMinPrice:2.95 maxPrice2:6.93 lowestPrice:3.04 lowestTime:@"2020.2.4" currentPrice:3.41 currentTime:@"2020.11.2" isPrint:NO];
    
    
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    
    
    //        < 45
    //        [self printName:@"华钰矿业601020" maxPrice:53.54 firstMinPrice:7.51 maxPrice2:12.89 lowestPrice:7.39 lowestTime:@"2020.2.4" currentPrice:7.45 currentTime:@"2020.5.25" isPrint:NO];
    
    
    
    // 2021
    [self printName:@"纳尔股份002825" maxPrice:41.14 firstMinPrice:8.39 maxPrice2:15.00 lowestPrice:6.76 lowestTime:@"2020.2.4" currentPrice:8.69 currentTime:@"2021.1.14" isPrint:NO];
    [self printName:@"纳尔股份002825" maxPrice:41.14 firstMinPrice:8.39 maxPrice2:15.00 lowestPrice:6.76 lowestTime:@"2020.2.4" currentPrice:8.93 currentTime:@"2021.7.28" isPrint:NO];
    [self printName:@"四方股份601126" maxPrice:19.35 firstMinPrice:2.60 maxPrice2:7.14 lowestPrice:2.86 lowestTime:@"2020.2.4" currentPrice:4.17 currentTime:@"2021.2.9" isPrint:NO];//跨
    [self printName:@"湖南发展000722" maxPrice:33.45 firstMinPrice:4.41 maxPrice2:10.23 lowestPrice:5.10 lowestTime:@"2020.2.4" currentPrice:5.37 currentTime:@"2021.2.5" isPrint:NO];
    [self printName:@"湖南发展000722" maxPrice:33.45 firstMinPrice:4.41 maxPrice2:10.23 lowestPrice:5.10 lowestTime:@"2020.2.4" currentPrice:5.96 currentTime:@"2021.7.28" isPrint:NO];
    [self printName:@"国泰集团603977" maxPrice:20.58 firstMinPrice:4.11 maxPrice2:8.81 lowestPrice:4.36 lowestTime:@"2020.2.4" currentPrice:5.95 currentTime:@"2021.1.14" isPrint:NO]; //跨
    [self printName:@"安徽建工600502" maxPrice:10.04 firstMinPrice:2.56 maxPrice2:5.04 lowestPrice:2.58 lowestTime:@"2020.2.4" currentPrice:2.97 currentTime:@"2021.2.4" isPrint:NO];
    [self printName:@"弘讯科技603015" maxPrice:30.10 firstMinPrice:5.06 maxPrice2:9.02 lowestPrice:4.68 lowestTime:@"2020.2.4" currentPrice:5.11 currentTime:@"2021.2.4" isPrint:NO];
    [self printName:@"太阳电缆002300" maxPrice:15.90 firstMinPrice:3.55 maxPrice2:8.60 lowestPrice:4.54 lowestTime:@"2020.2.4" currentPrice:4.92 currentTime:@"2021.2.8" isPrint:NO];
    [self printName:@"沃顿科技000920" maxPrice:38.69 firstMinPrice:4.41 maxPrice2:10.11 lowestPrice:4.98 lowestTime:@"2020.2.4" currentPrice:5.31 currentTime:@"2021.2.5" isPrint:NO]; //跨
    [self printName:@"宁波建工601789" maxPrice:9.14 firstMinPrice:2.55 maxPrice2:5.99 lowestPrice:2.80 lowestTime:@"2020.2.4" currentPrice:3.23 currentTime:@"2021.2.4" isPrint:NO]; // 跨
    [self printName:@"宁波建工601789" maxPrice:9.14 firstMinPrice:2.55 maxPrice2:5.99 lowestPrice:2.80 lowestTime:@"2020.2.4" currentPrice:3.23 currentTime:@"2021.7.28" isPrint:NO]; // 跨
    [self printName:@"宁波建工601789" maxPrice:9.14 firstMinPrice:2.55 maxPrice2:5.99 lowestPrice:2.80 lowestTime:@"2020.2.4" currentPrice:3.28 currentTime:@"2021.10.28" isPrint:NO]; // 跨
    [self printName:@"丝路视觉300556" maxPrice:85.96 firstMinPrice:13.69 maxPrice2:30.27 lowestPrice:13.69 lowestTime:@"2020.2.4" currentPrice:14.79 currentTime:@"2021.5.10" isPrint:NO];// 跨
    [self printName:@"山东路桥000498" maxPrice:13.24 firstMinPrice:3.16 maxPrice2:6.39 lowestPrice:3.51 lowestTime:@"2020.2.4" currentPrice:4.36 currentTime:@"2021.1.27" isPrint:NO];
    [self printName:@"宁波能源600982" maxPrice:13.57 firstMinPrice:2.48 maxPrice2:4.08 lowestPrice:2.16 lowestTime:@"2020.2.4" currentPrice:2.63 currentTime:@"2021.2.8" isPrint:NO];
    [self printName:@"宁波能源600982" maxPrice:13.57 firstMinPrice:2.48 maxPrice2:4.12 lowestPrice:2.16 lowestTime:@"2020.2.4" currentPrice:2.96 currentTime:@"2021.7.28" isPrint:NO];
    [self printName:@"泰嘉股份002843" maxPrice:34.69 firstMinPrice:5.94 maxPrice2:10.98 lowestPrice:5.81 lowestTime:@"2020.2.4" currentPrice:6.54 currentTime:@"2021.2.9" isPrint:NO]; // 跨
    [self printName:@"诚益通300430" maxPrice:48.28 firstMinPrice:6.79 maxPrice2:15.24 lowestPrice:6.67 lowestTime:@"2020.4.28" currentPrice:6.66 currentTime:@"2021.2.9" isPrint:NO];
    [self printName:@"浙商中拓000906" maxPrice:19.86 firstMinPrice:3.28 maxPrice2:7.34 lowestPrice:4.00 lowestTime:@"2020.4.28" currentPrice:5.19 currentTime:@"2021.2.8" isPrint:NO];
    [self printName:@"江苏国泰002091" maxPrice:14.34 firstMinPrice:3.58 maxPrice2:8.28 lowestPrice:4.52 lowestTime:@"2020.4.28" currentPrice:5.47 currentTime:@"2021.1.29" isPrint:NO];
    [self printName:@"易明医药002826" maxPrice:35.23 firstMinPrice:8.87 maxPrice2:16.62 lowestPrice:7.42 lowestTime:@"2021.2.8" currentPrice:8.15 currentTime:@"2021.7.28" isPrint:NO];
    [self printName:@"易明医药002826" maxPrice:35.23 firstMinPrice:8.87 maxPrice2:16.62 lowestPrice:7.42 lowestTime:@"2021.2.8" currentPrice:8.22 currentTime:@"2021.9.29" isPrint:NO];
    [self printName:@"易明医药002826" maxPrice:35.23 firstMinPrice:8.87 maxPrice2:16.62 lowestPrice:7.42 lowestTime:@"2021.2.8" currentPrice:8.28 currentTime:@"2021.10.28" isPrint:NO];
    [self printName:@"富临运业002357" maxPrice:26.72 firstMinPrice:4.63 maxPrice2:8.53 lowestPrice:3.95 lowestTime:@"2021.2.4" currentPrice:4.45 currentTime:@"2021.11.2" isPrint:NO];
    [self printName:@"恒锋工具300488" maxPrice:44.01 firstMinPrice:12.53 maxPrice2:26.98 lowestPrice:13.06 lowestTime:@"2021.2.9" currentPrice:13.78 currentTime:@"2021.4.13" isPrint:NO];
    [self printName:@"金桥信息603918" maxPrice:31.49 firstMinPrice:5.99 maxPrice2:11.69 lowestPrice:6.07 lowestTime:@"2021.2.4" currentPrice:6.39 currentTime:@"2021.11.2" isPrint:NO];
    [self printName:@"北化股份002246" maxPrice:23.65 firstMinPrice:6.06 maxPrice2:11.30 lowestPrice:5.92 lowestTime:@"2021.2.8" currentPrice:6.34 currentTime:@"2021.10.28" isPrint:NO];
    [self printName:@"时代出版600551" maxPrice:30.28 firstMinPrice:5.86 maxPrice2:11.32 lowestPrice:5.93 lowestTime:@"2021.2.8" currentPrice:6.55 currentTime:@"2021.11.2" isPrint:NO];
    [self printName:@"大千生态603955" maxPrice:57.62 firstMinPrice:12.59 maxPrice2:21.09 lowestPrice:11.49 lowestTime:@"2021.2.4" currentPrice:11.65 currentTime:@"2021.11.2" isPrint:NO];
    
    
    //        <45
    //        [self printName:@"金洲管道002443" maxPrice:18.86 firstMinPrice:3.83 maxPrice2:7.60 lowestPrice:4.19 lowestTime:@"2020.2.4" currentPrice:5.02 currentTime:@"2021.8.2" isPrint:NO];
    //        [self printName:@"金洲管道002443" maxPrice:18.86 firstMinPrice:3.83 maxPrice2:7.60 lowestPrice:4.19 lowestTime:@"2020.2.4" currentPrice:5.03 currentTime:@"2021.10.28" isPrint:NO];
    //        [self printName:@"川恒股份002895" maxPrice:42.05 firstMinPrice:9.96 maxPrice2:16.69 lowestPrice:9.25 lowestTime:@"2020.2.4" currentPrice:9.40 currentTime:@"2021.4.29" isPrint:NO];
    //        [self printName:@"三祥新材603663" maxPrice:26.93 firstMinPrice:6.63 maxPrice2:15.73 lowestPrice:7.30 lowestTime:@"2020.2.4" currentPrice:9.24 currentTime:@"2021.1.29" isPrint:NO];//跨
    //        [self printName:@"乔治白002687" maxPrice:15.17 firstMinPrice:2.63 maxPrice2:5.53 lowestPrice:3.08 lowestTime:@"2020.2.4" currentPrice:3.43 currentTime:@"2021.3.9" isPrint:NO];
    //        [self printName:@"厦门象屿600057" maxPrice:11.71 firstMinPrice:2.79 maxPrice2:7.29 lowestPrice:2.47 lowestTime:@"2020.2.4" currentPrice:4.19 currentTime:@"2021.2.4" isPrint:NO]; //跨
    //        [self printName:@"史丹利002588" maxPrice:19.10 firstMinPrice:3.28 maxPrice2:6.74 lowestPrice:3.22 lowestTime:@"2020.2.4" currentPrice:4.84 currentTime:@"2021.2.5" isPrint:NO];
    //        [self printName:@"四方科技603339" maxPrice:41.35 firstMinPrice:8.59 maxPrice2:12.63 lowestPrice:7.08 lowestTime:@"2020.2.4" currentPrice:8.46 currentTime:@"2021.2.8" isPrint:NO];
    //        [self printName:@"拉芳家化603630" maxPrice:55.24 firstMinPrice:11.60 maxPrice2:23.77 lowestPrice:11.18 lowestTime:@"2020.2.4" currentPrice:13.32 currentTime:@"2021.2.3" isPrint:NO];//跨
    
    
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    //        [self printName:@"" maxPrice:888 firstMinPrice:8888 maxPrice2:8888 lowestPrice:8888 lowestTime:@"sssss" currentPrice:888888 currentTime:@"sssssss" isPrint:NO];
    
    
    // 2022年
    [self printName:@"湖南发展000722" maxPrice:33.45 firstMinPrice:4.41 maxPrice2:10.23 lowestPrice:5.10 lowestTime:@"2020.2.4" currentPrice:7.01 currentTime:@"2022.1.27" isPrint:NO];
    [self printName:@"亚威股份002559" maxPrice:19.68 firstMinPrice:3.42 maxPrice2:10.63 lowestPrice:4.41 lowestTime:@"2021.1.28" currentPrice:4.96 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"易明医药002826" maxPrice:35.23 firstMinPrice:8.87 maxPrice2:16.62 lowestPrice:7.42 lowestTime:@"2021.2.8" currentPrice:7.75 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"新开源300109" maxPrice:80.06 firstMinPrice:11.57 maxPrice2:27.04 lowestPrice:8.98 lowestTime:@"2021.2.4" currentPrice:11.01 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"富临运业002357" maxPrice:26.72 firstMinPrice:4.63 maxPrice2:8.78 lowestPrice:3.95 lowestTime:@"2021.2.4" currentPrice:4.39 currentTime:@"2022.4.28" isPrint:NO];
    [self printName:@"富临运业002357" maxPrice:26.72 firstMinPrice:4.63 maxPrice2:8.78 lowestPrice:3.95 lowestTime:@"2021.2.4" currentPrice:5.00 currentTime:@"2022.10.12" isPrint:NO];
    [self printName:@"丽岛新材603937" maxPrice:34.50 firstMinPrice:8.05 maxPrice2:16.27 lowestPrice:7.59 lowestTime:@"2021.2.4" currentPrice:7.73 currentTime:@"2022.4.27" isPrint:NO]; //跨
    [self printName:@"特一药业002728" maxPrice:41.76 firstMinPrice:8.85 maxPrice2:18.93 lowestPrice:8.52 lowestTime:@"2021.2.5" currentPrice:10.77 currentTime:@"2022.4.26" isPrint:NO];
    [self printName:@"特一药业002728" maxPrice:41.76 firstMinPrice:8.85 maxPrice2:18.93 lowestPrice:8.52 lowestTime:@"2021.2.5" currentPrice:11.08 currentTime:@"2022.9.26" isPrint:NO];
    [self printName:@"金桥信息603918" maxPrice:31.49 firstMinPrice:5.99 maxPrice2:11.69 lowestPrice:6.07 lowestTime:@"2021.2.4" currentPrice:6.12 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"金桥信息603918" maxPrice:31.49 firstMinPrice:5.99 maxPrice2:11.69 lowestPrice:6.07 lowestTime:@"2021.2.4" currentPrice:6.37 currentTime:@"2022.10.10" isPrint:NO];
    [self printName:@"时代出版600551" maxPrice:30.28 firstMinPrice:5.86 maxPrice2:11.76 lowestPrice:5.93 lowestTime:@"2021.2.8" currentPrice:6.89 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"瑞尔特002790" maxPrice:22.74 firstMinPrice:4.55 maxPrice2:10.58 lowestPrice:4.58 lowestTime:@"2021.2.4" currentPrice:4.98 currentTime:@"2022.4.27" isPrint:NO];//跨
    [self printName:@"黑牡丹600510" maxPrice:23.02 firstMinPrice:4.28 maxPrice2:9.96 lowestPrice:5.24 lowestTime:@"2021.2.4" currentPrice:6.54 currentTime:@"2022.1.28" isPrint:NO];
    [self printName:@"大千生态603955" maxPrice:57.62 firstMinPrice:12.59 maxPrice2:21.09 lowestPrice:11.49 lowestTime:@"2021.2.4" currentPrice:11.27 currentTime:@"2022.4.28" isPrint:NO];
    [self printName:@"金海高科603311" maxPrice:51.25 firstMinPrice:8.48 maxPrice2:18.50 lowestPrice:9.10 lowestTime:@"2021.10.28" currentPrice:8.16 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"弘讯科技603015" maxPrice:30.10 firstMinPrice:5.06 maxPrice2:9.45 lowestPrice:4.68 lowestTime:@"2020.2.4" currentPrice:4.88 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"众源新材603527" maxPrice:24.56 firstMinPrice:6.39 maxPrice2:13.61 lowestPrice:6.55 lowestTime:@"2021.1.14" currentPrice:7.22 currentTime:@"2022.4.27" isPrint:NO]; // 跨
    [self printName:@"泰嘉股份002843" maxPrice:34.69 firstMinPrice:5.94 maxPrice2:13.07 lowestPrice:5.81 lowestTime:@"2020.2.4" currentPrice:7.31 currentTime:@"2022.4.27" isPrint:NO]; // 跨
    [self printName:@"诚益通300430" maxPrice:48.28 firstMinPrice:6.79 maxPrice2:15.24 lowestPrice:6.67 lowestTime:@"2020.4.28" currentPrice:8.06 currentTime:@"2022.4.27" isPrint:NO];
    [self printName:@"诚益通300430" maxPrice:48.28 firstMinPrice:6.79 maxPrice2:15.24 lowestPrice:6.67 lowestTime:@"2020.4.28" currentPrice:8.50 currentTime:@"2022.9.26" isPrint:NO];
    
    
    
    [self printName:@"三力士002224" maxPrice:25.56 firstMinPrice:4.89 maxPrice2:8.94 lowestPrice:3.96 lowestTime:@"2022.4.27" currentPrice:4.19 currentTime:@"2022.10.11" isPrint:NO];
    [self printName:@"三力士002224" maxPrice:25.56 firstMinPrice:4.89 maxPrice2:8.94 lowestPrice:3.96 lowestTime:@"2022.4.27" currentPrice:4.27 currentTime:@"2022.12.29" isPrint:NO];
    [self printName:@"中电兴发002298" maxPrice:40.81 firstMinPrice:5.07 maxPrice2:11.15 lowestPrice:5.08 lowestTime:@"2022.4.27" currentPrice:5.01 currentTime:@"2022.10.10" isPrint:NO]; // 转换 120%
    [self printName:@"中电兴发002298" maxPrice:40.81 firstMinPrice:5.07 maxPrice2:12.65 lowestPrice:5.08 lowestTime:@"2022.4.27" currentPrice:6.05 currentTime:@"2022.12.23" isPrint:NO];
    [self printName:@"东兴证券601198" maxPrice:42.51 firstMinPrice:7.36 maxPrice2:14.98 lowestPrice:7.27 lowestTime:@"2022.10.11" currentPrice:7.57 currentTime:@"2023.1.3" isPrint:NO];
    [self printName:@"富煌钢构002743" maxPrice:25.58 firstMinPrice:4.77 maxPrice2:10.32 lowestPrice:3.32 lowestTime:@"2024.2.7" currentPrice:5.20 currentTime:@"2022.10.11" isPrint:NO];
    [self printName:@"富煌钢构002743" maxPrice:25.62 firstMinPrice:4.82 maxPrice2:10.36 lowestPrice:5.03 lowestTime:@"2022.4.28" currentPrice:5.29 currentTime:@"2022.12.29" isPrint:NO];
    [self printName:@"深圳华强000062" maxPrice:63.29 firstMinPrice:10.34 maxPrice2:20.16 lowestPrice:9.95 lowestTime:@"2022.4.27" currentPrice:10.63 currentTime:@"2022.10.31" isPrint:NO];
    [self printName:@"木林森002745" maxPrice:38.39 firstMinPrice:9.28 maxPrice2:19.13 lowestPrice:7.93 lowestTime:@"2022.10.12" currentPrice:8.05 currentTime:@"2022.12.29" isPrint:NO];
    
    [self printName:@"中原高速600020" maxPrice:9.74 firstMinPrice:2.78 maxPrice2:6.11 lowestPrice:2.62 lowestTime:@"2022.10.31" currentPrice:2.62 currentTime:@"2022.10.31" isPrint:NO];
    
    
    
    
    NSMutableArray *oldInfoDatas = [NSMutableArray array];
    
    NSMutableArray *infoModelPaiming = [NSMutableArray array];
    [oldInfoDatas addObjectsFromArray:self.infoDatas];
    
    for (InfoModel *model in self.infoDatas) {
        int score = [self infoModelSort:oldInfoDatas currentmodel:model];
        [infoModelPaiming addObject:[NSString stringWithFormat:@"%d",score]];
    }
    
    self.infoModelPaiming = infoModelPaiming;
    
    [infoModelPaiming sortUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
        int value1 = [obj1 intValue];
        int value2 = [obj2 intValue];
        if(value1 < value2){
            return NSOrderedAscending;
        } else if (value1 > value2){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    
    [oldInfoDatas sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
        int value1 = [self infoModelSort:self.infoDatas currentmodel:obj1];
        int value2 = [self infoModelSort:self.infoDatas currentmodel:obj2];
        if(value1 < value2){
            return NSOrderedAscending;
        } else if (value1 > value2){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    NSLog(@"\n\n最低点以后出现的最佳买点🍺🍺🍺🍺 start \n");
    int k =0;
    for (InfoModel *model in oldInfoDatas) {
        k++;
        [self printInfoModelSort:model index:k];
    }
    NSLog(@"\n\n最低点以后出现的最佳买点🍺🍺🍺🍺 end\n");
    
    
    
    
    if(0){
        k = 0;
        for (int i = 0; i < oldInfoDatas.count; i++) {
            InfoModel *currentmodel = oldInfoDatas[i];
            int score2 = [self infoModelSort1:nil currentmodel:currentmodel];
            int score3 = [self infoModelSort2:nil currentmodel:currentmodel];
            BOOL b1 = i * 2 < oldInfoDatas.count;    // 36/72    上涨速度和上涨点数综合排名一半
            BOOL b2 = score2 * 2 < self.infoDatas1.count; // 37/72 上涨速度前面一半
            BOOL b3 = score3 * 2 < self.infoDatas1.count; // 35/72 上涨点数前面一半
            
            //            b2                    36/72    50%
            //            b1                    36/72    50%
            //            b2 || b3              46/72    64%  上涨速度前面一半 或者 上涨点数前面一半
            //            b1 || b2 || b3        46/72    64%
            //            b2 && b3              26/72    36%  上涨速度前面一半 并且 上涨点数前面一半   必须可以买
            if(b2 && b3){
                k++;
            }
        }
        NSLog(@"k = %d",k);
    }
    
    
    
    
    
    [self.infoDatas sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
        int result = obj1.upper - obj2.upper;
        if(result > 0){
            return NSOrderedDescending;
        } else if (result < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    NSLog(@"\n\n按照上涨点数排序  start 🍺🍺🍺🍺🍺 \n");
    k =0;
    for (InfoModel *model in self.infoDatas) {
        k++;
        [self printInfoModelSort:model index:k];
    }
    
    NSLog(@"\n按照上涨点数排序  end  🍺🍺🍺🍺🍺\n");
    
    [self.infoDatas sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
        int result = obj1.speed - obj2.speed;
        if(result > 0){
            return NSOrderedDescending;
        } else if (result < 0){
            return NSOrderedAscending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    NSLog(@"\n\n按照上涨速度排序  start 🍺🍺🍺🍺🍺 \n");
    k = 0;
    for (InfoModel *model in self.infoDatas) {
        k++;
        [self printInfoModelSort:model index:k];
    }
    NSLog(@"\n按照上涨速度排序  end  🍺🍺🍺🍺🍺\n");
}

-(void)name:(NSString *)name
 shichangID:(NSString *)shichangID
        max:(double)maxPrice
        min:(double)firstMinPrice
       max2:(double)maxPrice2
        low:(double)lowestPrice
    lowTime:(NSString *)lowestTime
   nowPrice:(double)currentPrice
        now:(NSString *)currentTime
        tjd:(double)tiaojiandan{
    if(self.open){
        double result5 =  ((currentPrice - tiaojiandan) / currentPrice * 100);
        if(result5 < 90){
            
        } else {
            return;
        }
        
        // 跌幅
        double result6 =  ((maxPrice2 - lowestPrice) / maxPrice2 * 100);
        double result7 =  ((maxPrice2 - firstMinPrice) / firstMinPrice * 100);
        if(result6 >= 50 && result7 <= 150 && result7 >= 140){
            [[GQBSettingmanager shareInstance].satisArr addObject:name];
        }
        
        double result8 =  ((maxPrice2 - tiaojiandan) / maxPrice2 * 100) + 0.5;
        int resultInt8 = (int)(0-result8);
        NSArray *nameArr = [name componentsSeparatedByString:@" "];
        name = [NSString stringWithFormat:@"%@ %d%%",nameArr.firstObject,resultInt8];
        
        
        // 涨幅超过140的数据不要
        if(result7 > 150){
            return;
        }
        
        //        // 跌幅小于50的不要
        //        if(result6 < 50){
        //            return;
        //        }
        
    }
    
    
    if(self.open){
        //        弘讯科技603015
        NSString *str1 = [name componentsSeparatedByString:@" "].firstObject;
        int length = str1.length;
        NSString *daima = [str1 substringWithRange:NSMakeRange(length-6, 6)];
        
        NSString *head3 = [daima substringToIndex:3];
        NSString *head2 = [daima substringToIndex:2];
        
        NSData *data =  [[NSUserDefaults standardUserDefaults] objectForKey:daima];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSString *hangqing = model.hanqing;
        
        if(hangqing.length > 0){
            NSString *price = [hangqing componentsSeparatedByString:@","][5];
            
            
            NSDate *date = [NSDate date];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.locale = [NSLocale systemLocale];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateStr = [formatter stringFromDate:date];
            NSString *timeStr = [dateStr componentsSeparatedByString:@" "].lastObject;
            if([timeStr compare:@"15:00:00"] == NSOrderedAscending){
                price = [hangqing componentsSeparatedByString:@","][3];//huxu625  15点前 最新价
            } else {
                price = [hangqing componentsSeparatedByString:@","][5];//huxu625  15点后 最低价
            }
            if([GQBSettingmanager shareInstance].minestPrice){
                price = [hangqing componentsSeparatedByString:@","][5];//huxu625  最低价
            } else {
                price = [hangqing componentsSeparatedByString:@","][3];//huxu625  最新价
            }
            
            currentPrice = [price doubleValue];
        } else {
            // 没有行情
            if(shichangID.length > 0){
                [self.instrumentIDDatas addObject:[NSString stringWithFormat:@"%@%@",shichangID,daima]];
            } else {
                if([head3 isEqualToString:@"600"] ||
                   [head3 isEqualToString:@"601"] ||
                   [head3 isEqualToString:@"603"] ||
                   [head3 isEqualToString:@"605"] ||
                   [head3 isEqualToString:@"688"]){
                    [self.instrumentIDDatas addObject:[NSString stringWithFormat:@"sh%@",daima]];
                }  else if ([head3 isEqualToString:@"300"] || [head3 isEqualToString:@"301"] || [head2 isEqualToString:@"00"]){
                    [self.instrumentIDDatas addObject:[NSString stringWithFormat:@"sz%@",daima]];
                } else {
                    NSLog(@"怪胎: %@",daima);
                }
                if(daima.length != 6){
                    NSLog(@"怪胎: %@",daima);
                }
            }
            NSLog(@"以下股票ID没有行情 = %@",daima);
            return;
        }
        
        
        if(shichangID.length > 0){
            [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"%@%@",shichangID,daima]];
        } else {
            if([head3 isEqualToString:@"600"] ||
               [head3 isEqualToString:@"601"] ||
               [head3 isEqualToString:@"603"] ||
               [head3 isEqualToString:@"605"] ){
                [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"sh%@",daima]];
            } else if ([head2 isEqualToString:@"00"]){
                [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"sz%@",daima]];
            } else if ([head3 isEqualToString:@"300"]){
                [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"sz%@",daima]];
            }
        }
    }
    
    
    
    
    
    
    
    
    //    0-10: 9
    //    10-20: 28
    //    20-35: 60
    //    35-50: 20
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    if(currentTime.length < 1){
        NSDate *today = [NSDate date];
        currentTime = [formatter stringFromDate:today];
    }
    if(lowestTime.length < 1){
        NSDate *today = [NSDate date];
        lowestTime = [formatter stringFromDate:today];
    }

    //    三力士002224 100+100=200  100/100    167天 上涨 5% 年化 13%  继续持有

    NSString *title1 = [self current:[self buyTime:lowestTime buyPrice:lowestPrice sellTime:currentTime sellPrice:currentPrice] gupiaoName:name isPrint:YES];


    //    currentDecline= -80.14% , name = 134.92%😄14.58%  中钢国际000928  -51.23%   2020.3.23.............🍺🍺🍺⭐️⭐️⭐️

    NSString *title2 =  [self printName:name maxPrice:maxPrice firstMinPrice:firstMinPrice maxPrice2:maxPrice2 currentPrice:currentPrice buyTime:currentTime tiaojiandan:tiaojiandan isPrint:YES];
    if(YES){
        NSLog(@"\n.");
    }
    InfoModel *model = [[InfoModel alloc] initwithTitle1:title1 title2:title2];
    //    if(!isPrint){
    //        [self.infoDatas addObject:model];
    //    }

}


-(void)name:(NSString *)name
        max:(double)maxPrice
        min:(double)firstMinPrice
       max2:(double)maxPrice2
        low:(double)lowestPrice
    lowTime:(NSString *)lowestTime
   nowPrice:(double)currentPrice
        now:(NSString *)currentTime
        tjd:(double)tiaojiandan
{
//
    [self name:name shichangID:@"" max:maxPrice min:firstMinPrice max2:maxPrice2 low:lowestPrice lowTime:lowestTime nowPrice:currentPrice now:currentTime tjd:tiaojiandan];
    return;
    
    
    if(self.open){
        double result5 =  ((currentPrice - tiaojiandan) / currentPrice * 100);
        if(result5 < 90){
            
        } else {
            return;
        }
        
        // 跌幅
        double result6 =  ((maxPrice2 - lowestPrice) / maxPrice2 * 100);
        double result7 =  ((maxPrice2 - firstMinPrice) / firstMinPrice * 100);
        if(result6 >= 50 && result7 <= 150 && result7 >= 140){
            [[GQBSettingmanager shareInstance].satisArr addObject:name];
        }
        
        double result8 =  ((maxPrice2 - tiaojiandan) / maxPrice2 * 100) + 0.5;
        int resultInt8 = (int)(0-result8);
        NSArray *nameArr = [name componentsSeparatedByString:@" "];
        name = [NSString stringWithFormat:@"%@ %d%%",nameArr.firstObject,resultInt8];
        
        
        // 涨幅超过140的数据不要
        if(result7 > 150){
            return;
        }
        
        //        // 跌幅小于50的不要
        //        if(result6 < 50){
        //            return;
        //        }
        
    }
    
    
    if(self.open){
        //        弘讯科技603015
        NSString *str1 = [name componentsSeparatedByString:@" "].firstObject;
        int length = str1.length;
        NSString *daima = [str1 substringWithRange:NSMakeRange(length-6, 6)];
        
        NSString *head3 = [daima substringToIndex:3];
        NSString *head2 = [daima substringToIndex:2];
        
        NSData *data =  [[NSUserDefaults standardUserDefaults] objectForKey:daima];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSString *hangqing = model.hanqing;
        
        if(hangqing.length > 0){
            NSString *price = [hangqing componentsSeparatedByString:@","][5];
            
            
            NSDate *date = [NSDate date];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.locale = [NSLocale systemLocale];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateStr = [formatter stringFromDate:date];
            NSString *timeStr = [dateStr componentsSeparatedByString:@" "].lastObject;
            if([timeStr compare:@"15:00:00"] == NSOrderedAscending){
                price = [hangqing componentsSeparatedByString:@","][3];//huxu625  15点前 最新价
            } else {
                price = [hangqing componentsSeparatedByString:@","][5];//huxu625  15点后 最低价
            }
            
            if([GQBSettingmanager shareInstance].minestPrice){
                price = [hangqing componentsSeparatedByString:@","][5];//huxu625  最低价
            } else {
                price = [hangqing componentsSeparatedByString:@","][3];//huxu625  最新价
            }            currentPrice = [price doubleValue];
        } else {
            // 没有行情数据
            if([head3 isEqualToString:@"600"] ||
               [head3 isEqualToString:@"601"] ||
               [head3 isEqualToString:@"603"] ||
               [head3 isEqualToString:@"605"] ||
               [head3 isEqualToString:@"688"]){
                [self.instrumentIDDatas addObject:[NSString stringWithFormat:@"sh%@",daima]];
            }  else if ([head3 isEqualToString:@"300"] || [head3 isEqualToString:@"301"] || [head2 isEqualToString:@"00"]){
                [self.instrumentIDDatas addObject:[NSString stringWithFormat:@"sz%@",daima]];
            } else {
                NSLog(@"怪胎: %@",daima);
            }
            if(daima.length != 6){
                NSLog(@"怪胎: %@",daima);
            }
            
            NSLog(@"以下股票ID没有行情 = %@",daima);
            return;
        }
        
        
        if([head3 isEqualToString:@"600"] ||
           [head3 isEqualToString:@"601"] ||
           [head3 isEqualToString:@"603"] ||
           [head3 isEqualToString:@"605"] ){
            [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"sh%@",daima]];
        } else if ([head2 isEqualToString:@"00"]){
            [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"sz%@",daima]];
        } else if ([head3 isEqualToString:@"300"]){
            [self.instrumentIDDatas2 addObject:[NSString stringWithFormat:@"sz%@",daima]];
        }
        
    }
    
    
    
    
    
    
    
    
    //    0-10: 9
    //    10-20: 28
    //    20-35: 60
    //    35-50: 20
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    if(currentTime.length < 1){
        NSDate *today = [NSDate date];
        currentTime = [formatter stringFromDate:today];
    }
    if(lowestTime.length < 1){
        NSDate *today = [NSDate date];
        lowestTime = [formatter stringFromDate:today];
    }

//        三力士002224 100+100=200  100/100    167天 上涨 5% 年化 13%  继续持有

    NSString *title1 = [self current:[self buyTime:lowestTime buyPrice:lowestPrice sellTime:currentTime sellPrice:currentPrice] gupiaoName:name isPrint:YES];


    //    currentDecline= -80.14% , name = 134.92%😄14.58%  中钢国际000928  -51.23%   2020.3.23.............🍺🍺🍺⭐️⭐️⭐️

    NSString *title2 =  [self printName:name maxPrice:maxPrice firstMinPrice:firstMinPrice maxPrice2:maxPrice2 currentPrice:currentPrice buyTime:currentTime tiaojiandan:tiaojiandan isPrint:YES];
    if(YES){
        NSLog(@"\n.");
    }
    InfoModel *model = [[InfoModel alloc] initwithTitle1:title1 title2:title2];
//        if(!isPrint){
//            [self.infoDatas addObject:model];
//        }
}






-(void)printName:(NSString *)name
        maxPrice:(double)maxPrice
   firstMinPrice:(double)firstMinPrice
       maxPrice2:(double)maxPrice2
     lowestPrice:(double)lowestPrice
      lowestTime:(NSString *)lowestTime
    currentPrice:(double)currentPrice
     currentTime:(NSString *)currentTime
     tiaojiandan:(double)tiaojiandan
         isPrint:(BOOL)isPrint
{
    
    double result5 =  ((currentPrice - tiaojiandan) / currentPrice * 100);
    if(result5 > -30 && result5 < 10){
        
    } else {
        return;
        
    }
    //    0-10: 9
    //    10-20: 28
    //    20-35: 60
    //    35-50: 20
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    if(currentTime.length < 1){
        NSDate *today = [NSDate date];
        currentTime = [formatter stringFromDate:today];
    }
    if(lowestTime.length < 1){
        NSDate *today = [NSDate date];
        lowestTime = [formatter stringFromDate:today];
    }
    
    //    三力士002224 100+100=200  100/100    167天 上涨 5% 年化 13%  继续持有
    
    NSString *title1 = [self current:[self buyTime:lowestTime buyPrice:lowestPrice sellTime:currentTime sellPrice:currentPrice] gupiaoName:name isPrint:isPrint];
    
    
    //    currentDecline= -80.14% , name = 134.92%😄14.58%  中钢国际000928  -51.23%   2020.3.23.............🍺🍺🍺⭐️⭐️⭐️
    
    NSString *title2 =  [self printName:name maxPrice:maxPrice firstMinPrice:firstMinPrice maxPrice2:maxPrice2 currentPrice:currentPrice buyTime:currentTime tiaojiandan:tiaojiandan isPrint:isPrint];
    if(isPrint){
        NSLog(@"\n.");
    }
    InfoModel *model = [[InfoModel alloc] initwithTitle1:title1 title2:title2];
    if(!isPrint){
        [self.infoDatas addObject:model];
    }
}



-(void)printName:(NSString *)name
        maxPrice:(double)maxPrice
   firstMinPrice:(double)firstMinPrice
       maxPrice2:(double)maxPrice2
     lowestPrice:(double)lowestPrice
      lowestTime:(NSString *)lowestTime
    currentPrice:(double)currentPrice
     currentTime:(NSString *)currentTime
         isPrint:(BOOL)isPrint
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    if(currentTime.length < 1){
        NSDate *today = [NSDate date];
        currentTime = [formatter stringFromDate:today];
    }
    if(lowestTime.length < 1){
        NSDate *today = [NSDate date];
        lowestTime = [formatter stringFromDate:today];
    }
    
    //    三力士002224 100+100=200  100/100    167天 上涨 5% 年化 13%  继续持有
    
    NSString *title1 = [self current:[self buyTime:lowestTime buyPrice:lowestPrice sellTime:currentTime sellPrice:currentPrice] gupiaoName:name isPrint:isPrint];
    
    
    //    currentDecline= -80.14% , name = 134.92%😄14.58%  中钢国际000928  -51.23%   2020.3.23.............🍺🍺🍺⭐️⭐️⭐️
    
    NSString *title2 =  [self printName:name maxPrice:maxPrice firstMinPrice:firstMinPrice maxPrice2:maxPrice2 currentPrice:currentPrice buyTime:currentTime isPrint:isPrint];
    if(isPrint){
        NSLog(@"\n.");
    }
    InfoModel *model = [[InfoModel alloc] initwithTitle1:title1 title2:title2];
    if(!isPrint){
        [self.infoDatas addObject:model];
    }
}

// currentDecline= -83.61% , name = 82.82%😄-14.31%  三力士002224  -53.13%   2022.10.11
-(NSString *)printName:(NSString *)name
              maxPrice:(double)maxPrice
         firstMinPrice:(double)firstMinPrice
             maxPrice2:(double)maxPrice2
          currentPrice:(double)currentPrice
               buyTime:(NSString *)buyTime {
    
    return [self printName:name maxPrice:maxPrice firstMinPrice:firstMinPrice maxPrice2:maxPrice2 currentPrice:currentPrice buyTime:buyTime isPrint:YES];
    
}


// currentDecline= -80.14% , name = 134.92%😄14.58%  中钢国际000928  -51.23%   2020.3.23.............🍺🍺🍺⭐️⭐️⭐️

-(NSString *)printName:(NSString *)name
              maxPrice:(double)maxPrice
         firstMinPrice:(double)firstMinPrice
             maxPrice2:(double)maxPrice2
          currentPrice:(double)currentPrice
               buyTime:(NSString *)buyTime
           tiaojiandan:(double)tiaojiandan
               isPrint:(BOOL)isPrint {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    if(buyTime.length < 1){
        NSDate *today = [NSDate date];
        buyTime = [formatter stringFromDate:today];
    }
    
    if([name containsString:@"%"]){
        name = [name componentsSeparatedByString:@" "].firstObject;
    }
    double result1 =  ((currentPrice - maxPrice) / maxPrice * 100);
    double result2 =  ((maxPrice2 - firstMinPrice) / firstMinPrice * 100);
    double result3 =  ((currentPrice - firstMinPrice) / firstMinPrice * 100);
    double result4 =  ((currentPrice - maxPrice2) / maxPrice2 * 100);
    double result5 =  ((currentPrice - tiaojiandan) / currentPrice * 100);
    
    NSString *content = [NSString stringWithFormat:@"currentDecline= %.2f%% , name = %.2f%%😄%.2f%%  %@  %.2f%%   %@ 下跌多少提醒:%.2f%% ",result1,result2,result3,name,result4,buyTime,result5];
    if(isPrint){
        NSString *daiyinContent = content;
        if(result4 < -45){
            daiyinContent = [content stringByAppendingFormat:@"..下跌大于45🍺"];
        }
        if(result4 < -50){
            daiyinContent = [content stringByAppendingFormat:@"下跌大于50⭐️"];
        }
        NSLog(@"%@",daiyinContent);
        [self gupiaoName:name savecCrrentInfo:daiyinContent xiadieValue:[NSString stringWithFormat:@"%f",result4]];
        return @"";
    } else {
        return content;
    }
}


// currentDecline= -80.14% , name = 134.92%😄14.58%  中钢国际000928  -51.23%   2020.3.23.............🍺🍺🍺⭐️⭐️⭐️

-(NSString *)printName:(NSString *)name
              maxPrice:(double)maxPrice
         firstMinPrice:(double)firstMinPrice
             maxPrice2:(double)maxPrice2
          currentPrice:(double)currentPrice
               buyTime:(NSString *)buyTime
               isPrint:(BOOL)isPrint {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    if(buyTime.length < 1){
        NSDate *today = [NSDate date];
        buyTime = [formatter stringFromDate:today];
    }
    
    if([name containsString:@"%"]){
        name = [name componentsSeparatedByString:@" "].firstObject;
    }
    double result1 =  ((currentPrice - maxPrice) / maxPrice * 100);
    double result2 =  ((maxPrice2 - firstMinPrice) / firstMinPrice * 100);
    double result3 =  ((currentPrice - firstMinPrice) / firstMinPrice * 100);
    double result4 =  ((currentPrice - maxPrice2) / maxPrice2 * 100);
    
    NSString *content = [NSString stringWithFormat:@"currentDecline= %.2f%% , name = %.2f%%😄%.2f%%  %@  %.2f%%   %@",result1,result2,result3,name,result4,buyTime];
    
    if(isPrint){
        if(result4 < -45){
            content = [content stringByAppendingFormat:@".............🍺🍺🍺"];
        }
        if(result4 < -50){
            content = [content stringByAppendingFormat:@"⭐️⭐️⭐️"];
        }
        NSLog(@"%@",content);
        return @"";
    } else {
        return content;
    }
}


// 43天 上涨 18% 年化 333%
-(NSString *)buyTime:(NSString*)buyTime
            buyPrice:(double)buyPrice
            sellTime:(NSString*)sellTime
           sellPrice:(double)sellPrice {
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if ([buyTime containsString:@"."]){
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    
    NSDate *beginDate = [formatter dateFromString:buyTime];
    NSDate *endDate = [NSDate date];
    if(sellTime.length > 5){
        endDate = [formatter dateFromString:sellTime];
    }
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long day = (int) ((endDay - beginDay) / (3600 * 24));
    
    double result = (sellPrice/buyPrice -1)*100;
    double end = (pow(result/100+1, 1/(day/365.0))-1)*100;
    NSString *title = [NSString stringWithFormat:@"%ld天 上涨 %d%% 年化 %d%%",day,(int)result,(int)end];
    return title;
    
}





-(void)origin:(NSString *)origin{
    long long value = [origin longLongValue];
    NSLog(@"origin = %@,new = %lld",origin,value);
}



// 17+7=24  1/100    158天 上涨 296% 年化 2316%
-(void)current:(NSString *)current{
    int score = [self rank:self.originList current:current];
    int score2 = [self rank2:self.originList current:current];
    int score3 = [self rank3:self.originList current:current];
    
    for(int i=0;i<self.paimingList.count;i++){
        int pmint = [self.paimingList[i] intValue];
        if(pmint >= score){
            i++;
            NSLog(@"%d+%d=%d  %d/%ld    %@",score2,score3,score,i,self.paimingList.count,current);
            break;
        }
    }
}

// 三力士002224 100+100=200  100/100    167天 上涨 5% 年化 13%  继续持有
-(NSString *)current:(NSString *)current gupiaoName:(NSString *)gupiaoName{
    return [self current:current gupiaoName:gupiaoName isPrint:YES];
}


// 三力士002224 100+100=200  100/100    167天 上涨 5% 年化 13%  继续持有
-(NSString *)current:(NSString *)current gupiaoName:(NSString *)gupiaoName isPrint:(BOOL)isPrint{
    int score = [self rank:self.originList current:current];
    int score2 = [self rank2:self.originList current:current];
    int score3 = [self rank3:self.originList current:current];
    
    int _ = [[current componentsSeparatedByString:@"天"].firstObject intValue];
    int upper = [[[current componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
    int speed = [[current componentsSeparatedByString:@"年化"].lastObject intValue];
    
    BOOL b1 = upper > 146 && speed > 150; // 上涨146个点以上 并且 年华收益150点以上 24/100
    BOOL b2 = upper > 100 && speed > 100; // 上涨100个点以上 并且 年华收益100点以上 46/100
    BOOL b3 = upper > 90 && speed > 150; // 上涨90个点以上 并且 年华收益150点以上 40/100
    BOOL b4 = upper > 80 && speed > 100; // 上涨80个点以上 并且 年华收益100点以上 54/100
    BOOL b5 = upper > 70 && speed > 200; // 上涨70个点以上 并且 年华收益200点以上 38/100
    BOOL b6 = upper > 100 ;              // 上涨100个点以上  80/100
    
    // b1 || b2 ||b3 || b4 || b5  57/100
    // b1 || b2 ||b3 || b4 || b5 || b6  100/100
    
    
    NSString *advice = @"继续持有";
    if(b1 || b2 ||b3 || b4 || b5 ||  b6){
        advice = @"建议卖出";
    }
    
    for(int i=0;i<self.paimingList.count;i++){
        int pmint = [self.paimingList[i] intValue];
        if(pmint >= score){
            i++;
            NSString *content = [NSString stringWithFormat:@"%@ %d+%d=%d  %d/%ld    %@  %@",gupiaoName,score2,score3,score,i,self.paimingList.count,current,advice];
            if(isPrint){
                if(self.open){
                    InfoModel *model = [[InfoModel alloc] initwithTitle1:content title2:@""];
                    int score = [self infoModelSort:nil currentmodel:model];
                    int score2 = [self infoModelSort1:nil currentmodel:model];
                    int score3 = [self infoModelSort2:nil currentmodel:model];
                    
                    
                    for(int i=0;i<self.infoModelPaiming.count;i++){
                        int pmint = [self.infoModelPaiming[i] intValue];
                        if(pmint >= score || i == self.infoModelPaiming.count - 1){
                            i++;
                            NSString *content1 = [NSString stringWithFormat:@"买点数据排名:   %d+%d=%d  %d/%ld    ",score2,score3,score,i,self.infoModelPaiming.count];
                            if(score2 * 2 < self.infoModelPaiming.count || score3 * 2 < self.infoModelPaiming.count || i * 2 < self.infoModelPaiming.count){
                                content1 = [content1 stringByAppendingString:@"...可以买了🍺🍺"];
                            }
                            NSLog(@"%@",content1);
                            [self gupiaoName:gupiaoName saveBuy:content1];
                            break;
                        }
                    }
                }
                content = [NSString stringWithFormat:@"卖点数据排名: %@",content];
                [self gupiaoName:gupiaoName saveSell:content];
                NSLog(@"%@",content);
                return @"";
            } else {
                return content;
            }
        }
        if(i == self.paimingList.count - 1){
            i++;
            NSString *content = [NSString stringWithFormat:@"%@ %d+%d=%d  %d/%ld    %@  %@",gupiaoName,score2,score3,score,i,self.paimingList.count,current,advice];
            if(isPrint){
                if(self.open){
                    InfoModel *model = [[InfoModel alloc] initwithTitle1:content title2:@""];
                    int score = [self infoModelSort:nil currentmodel:model];
                    int score2 = [self infoModelSort1:nil currentmodel:model];
                    int score3 = [self infoModelSort2:nil currentmodel:model];
                    for(int i=0;i<self.infoModelPaiming.count;i++){
                        int pmint = [self.infoModelPaiming[i] intValue];
                        if(pmint >= score || i == self.infoModelPaiming.count - 1){
                            i++;
                            NSString *content1 = [NSString stringWithFormat:@"买点数据排名:   %d+%d=%d  %d/%ld    ",score2,score3,score,i,self.infoModelPaiming.count];
                            if(score2 * 2 < self.infoModelPaiming.count || score3 * 2 < self.infoModelPaiming.count || i * 2 < self.infoModelPaiming.count){
                                content1 = [content1 stringByAppendingString:@"...可以买了🍺🍺"];
                            }
                            NSLog(@"%@",content1);
                            [self gupiaoName:gupiaoName saveBuy:content1];
                            break;
                        }
                    }
                }
                content = [NSString stringWithFormat:@"卖点数据排名: %@",content];
                [self gupiaoName:gupiaoName saveSell:content];
                
                NSLog(@"%@",content);
                return @"";
            } else {
                return content;
            }
        }
    }
    return @"";
}


-(void)gupiaoName:(NSString *)gupiaoName saveBuy:(NSString *)buy{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString * name = [gupiaoName componentsSeparatedByString:@" "].firstObject;
        NSString *daima = [name substringFromIndex:name.length-6];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(model == nil){
            model = [AllInfoModel new];
        }
        model.buy = buy;
        data = [NSKeyedArchiver archivedDataWithRootObject:model];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:daima];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
    
}

-(void)gupiaoName:(NSString *)gupiaoName saveSell:(NSString *)sell{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString * name = [gupiaoName componentsSeparatedByString:@" "].firstObject;
        NSString *daima = [name substringFromIndex:name.length-6];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(model == nil){
            model = [AllInfoModel new];
        }
        model.sell = sell;
        data = [NSKeyedArchiver archivedDataWithRootObject:model];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:daima];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}


-(void)gupiaoName:(NSString *)gupiaoName savecCrrentInfo:(NSString *)currentInfo xiadieValue:(NSString *)xiadieValue{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString * name = [gupiaoName componentsSeparatedByString:@" "].firstObject;
        NSString *daima = [name substringFromIndex:name.length-6];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(model == nil){
            model = [AllInfoModel new];
        }
        model.currentInfo = currentInfo;
        model.xiadieValue = xiadieValue;
        data = [NSKeyedArchiver archivedDataWithRootObject:model];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:daima];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}



-(int)rank:(NSMutableArray *)mutArr
   current:(NSString *)current{
    NSUInteger a1 = mutArr.count;
    NSUInteger a2 = mutArr.count;
    
    if(self.mutArr1.count == 0){
        //按照上涨点数
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[[obj1 componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
            int value2 =[[[obj2 componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.mutArr1 addObjectsFromArray:mutArr];
    }
    for (int i = 0; i < self.mutArr1.count; i++) {
        NSString *data = self.mutArr1[i];
        int value1 =[[[data componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
        int value2 =[[[current componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
        if (value1 <= value2) {
            a1 = i + 1;
            break;
        }
    }
    
    //按照年华收益率
    if(self.mutArr2.count == 0){
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[obj1 componentsSeparatedByString:@"年化"].lastObject intValue];
            int value2 =[[obj2 componentsSeparatedByString:@"年化"].lastObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.mutArr2 addObjectsFromArray:mutArr];
    }
    
    for (int i = 0; i < self.mutArr2.count; i++) {
        NSString *data = self.mutArr2[i];
        int value1 =[[data componentsSeparatedByString:@"年化"].lastObject intValue];
        int value2 =[[current componentsSeparatedByString:@"年化"].lastObject intValue];
        if (value1 <= value2) {
            a2 = i + 1;
            break;
        }
    }
    return (int)(a1+a2);
}


-(int)rank2:(NSMutableArray *)mutArr
    current:(NSString *)current{
    NSUInteger a1 = mutArr.count;
    
    if(self.mutArr1.count == 0){
        //按照上涨点数
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[[obj1 componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
            int value2 =[[[obj2 componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.mutArr1 addObjectsFromArray:mutArr];
    }
    for (int i = 0; i < self.mutArr1.count; i++) {
        NSString *data = self.mutArr1[i];
        int value1 =[[[data componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
        int value2 =[[[current componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
        if (value1 <= value2) {
            a1 = i + 1;
            break;
        }
    }
    return (int)a1;
}


-(int)rank3:(NSMutableArray *)mutArr
    current:(NSString *)current{
    NSUInteger a2 = mutArr.count;
    //按照年华收益率
    if(self.mutArr2.count == 0){
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[obj1 componentsSeparatedByString:@"年化"].lastObject intValue];
            int value2 =[[obj2 componentsSeparatedByString:@"年化"].lastObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.mutArr2 addObjectsFromArray:mutArr];
    }
    
    for (int i = 0; i < self.mutArr2.count; i++) {
        NSString *data = self.mutArr2[i];
        int value1 =[[data componentsSeparatedByString:@"年化"].lastObject intValue];
        int value2 =[[current componentsSeparatedByString:@"年化"].lastObject intValue];
        if (value1 <= value2) {
            a2 = i + 1;
            break;
        }
    }
    return (int)a2;
}




-(void)sortType:(NSString *)type data:(NSMutableArray *)mutArr{
    if([type containsString:@"按照上涨点数"]){
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[[obj1 componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
            int value2 =[[[obj2 componentsSeparatedByString:@"年化"].firstObject componentsSeparatedByString:@"上涨"].lastObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        NSLog(@"");
        NSLog(@"%@",type);
        NSLog(@"");
        for(int i=0;i<mutArr.count;i++){
            NSString *str = mutArr[i];
            NSLog(@"序号: %d %@",i+1,str);
        }
        NSLog(@"");
    } else if([type containsString:@"按照年华收益率"]){
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[obj1 componentsSeparatedByString:@"年化"].lastObject intValue];
            int value2 =[[obj2 componentsSeparatedByString:@"年化"].lastObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        NSLog(@"%@",type);
        NSLog(@"");
        for(int i=0;i<mutArr.count;i++){
            NSString *str = mutArr[i];
            NSLog(@"序号: %d %@",i+1,str);
        }
        NSLog(@"");
        
    } else if([type containsString:@"按照上涨天数"]){
        [mutArr sortUsingComparator:^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
            int value1 =[[obj1 componentsSeparatedByString:@"天"].firstObject intValue];
            int value2 =[[obj2 componentsSeparatedByString:@"天"].firstObject intValue];
            if(value1 > value2){
                return NSOrderedAscending;
            } else if (value1 < value2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        NSLog(@"%@",type);
        NSLog(@"");
        for(int i=0;i<mutArr.count;i++){
            NSString *str = mutArr[i];
            NSLog(@"序号: %d %@",i+1,str);
        }
        NSLog(@"");
        
    }
}

@end


// 38+12=50  14/100    197天 上涨 182% 年化 583%

