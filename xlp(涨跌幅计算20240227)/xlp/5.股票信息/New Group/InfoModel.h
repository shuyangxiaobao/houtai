//
//  InfoModel.h
//  gupiao
//
//  Created by xiaobao on 2023/2/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoModel : NSObject
//东兴证券601198 100+82=182  99/100    127天 上涨 18% 年化 62%  继续持有
@property(nonatomic,copy) NSString * title1;

//currentDecline= -79.75% , name = 103.53%😄16.98%  东兴证券601198  -42.52%   2023.02.15
@property(nonatomic,copy) NSString * title2;

@property(nonatomic,assign)NSUInteger upper;   // 上涨点的数量
@property(nonatomic,assign)NSUInteger speed;  // 上涨的速度(年华收益)


-(instancetype)initwithTitle1:(NSString *)title1 title2:(NSString *)title2;





@end

NS_ASSUME_NONNULL_END
