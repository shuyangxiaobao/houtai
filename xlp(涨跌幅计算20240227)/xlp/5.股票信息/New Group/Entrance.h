//
//  Entrance.h
//  gupiao
//
//  Created by xiaobao on 2022/11/15.
//

#import <Foundation/Foundation.h>

//
//#ifdef DEBUG
//#define NSLog(FORMAT, ...) fprintf(stderr,"%s \n",[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
//#else
//#define NSLog(...)
//#endif


NS_ASSUME_NONNULL_BEGIN

@interface Entrance : NSObject

- (void)enter:(NSInteger) type;
@end

NS_ASSUME_NONNULL_END
