//
//  Enter2+method1.m
//  gupiao
//
//  Created by xiaobao on 2023/2/15.
//

#import "Enter2+method1.h"
#import "InfoModel.h"

@implementation Enter2 (method1)

//按照上涨点数 + 按照上涨速度
-(int)infoModelSort:(NSMutableArray *)mutArr currentmodel:(InfoModel *)currentModel{
    
    if(self.infoDatas1.count == 0){
        //按照上涨点数
        [mutArr sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
            int result = obj1.upper - obj2.upper;
            if(result > 0){
                return NSOrderedDescending;
            } else if (result < 0){
                return NSOrderedAscending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.infoDatas1 addObjectsFromArray:mutArr];
    }
    
    NSUInteger a1 = self.infoDatas1.count;
    NSUInteger a2 = self.infoDatas1.count;

    for (int i = 0; i < self.infoDatas1.count; i++) {
        InfoModel *model = self.infoDatas1[i];
        int value1 = model.upper;
        int value2 = currentModel.upper;
        if (value1 >= value2) {
            a1 = i + 1;
            break;
        }
    }
    
    
    if(self.infoDatas2.count == 0){
        //按照上涨速度
        [mutArr sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
            int result = obj1.speed - obj2.speed;
            if(result > 0){
                return NSOrderedDescending;
            } else if (result < 0){
                return NSOrderedAscending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.infoDatas2 addObjectsFromArray:mutArr];
    }
    for (int i = 0; i < self.infoDatas2.count; i++) {
        InfoModel *model = self.infoDatas2[i];
        int value1 = model.speed;
        int value2 = currentModel.speed;
        if (value1 >= value2) {
            a2 = i + 1;
            break;
        }
    }
    return (int)(a1+a2);
}

//按照上涨点数
-(int)infoModelSort1:(NSMutableArray *)mutArr currentmodel:(InfoModel *)currentModel{
    if(self.infoDatas1.count == 0){
        //按照上涨点数
        [mutArr sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
            int result = obj1.upper - obj2.upper;
            if(result > 0){
                return NSOrderedDescending;
            } else if (result < 0){
                return NSOrderedAscending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.infoDatas1 addObjectsFromArray:mutArr];
    }
    NSUInteger a1 = self.infoDatas1.count;
    for (int i = 0; i < self.infoDatas1.count; i++) {
        InfoModel *model = self.infoDatas1[i];
        int value1 = model.upper;
        int value2 = currentModel.upper;
        if (value1 >= value2) {
            a1 = i + 1;
            break;
        }
    }
    
 
    return (int) a1;
}

//按照上涨速度
-(int)infoModelSort2:(NSMutableArray *)mutArr currentmodel:(InfoModel *)currentModel{
    
  
    if(self.infoDatas2.count == 0){
        //按照上涨速度
        [mutArr sortUsingComparator:^NSComparisonResult(InfoModel*  _Nonnull obj1, InfoModel*  _Nonnull obj2) {
            int result = obj1.speed - obj2.speed;
            if(result > 0){
                return NSOrderedDescending;
            } else if (result < 0){
                return NSOrderedAscending;
            } else {
                return NSOrderedSame;
            }
        }];
        [self.infoDatas2 addObjectsFromArray:mutArr];
    }
    NSUInteger a2 = self.infoDatas2.count;
    for (int i = 0; i < self.infoDatas2.count; i++) {
        InfoModel *model = self.infoDatas2[i];
        int value1 = model.speed;
        int value2 = currentModel.speed;
        if (value1 >= value2) {
            a2 = i + 1;
            break;
        }
    }
    return (int) a2;
}


// 17+7=24  1/100    158天 上涨 296% 年化 2316%
-(void)printInfoModelSort:(InfoModel *)currentmodel index:(NSInteger)index{
    int score = [self infoModelSort:nil currentmodel:currentmodel];
    int score2 = [self infoModelSort1:nil currentmodel:currentmodel];
    int score3 = [self infoModelSort2:nil currentmodel:currentmodel];

    for(int i=0;i<self.infoModelPaiming.count;i++){
        int pmint = [self.infoModelPaiming[i] intValue];
        if(pmint >= score){
            i++;
            NSString *content1 = [NSString stringWithFormat:@"序号:%2d %d+%d=%d  %d/%ld    ",index,score2,score3,score,i,self.infoModelPaiming.count];
            NSString *content2 = [NSString stringWithFormat:@" %@ \n %@",currentmodel.title1,currentmodel.title2];
            NSLog(@"%@",content1);
            NSLog(@"%@",content2);
            NSLog(@"\n.");
            break;
        }
    }
    

}


@end
