//
//  AllInfoModel.h
//  xlp
//
//  Created by xiaobao on 2023/4/3.
//

//#import <JSONModel/JSONModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface AllInfoModel : NSObject
//南极电商,5.060,5.070,5.080,5.090,5.010,5.080,5.090,29874109,151308356.080,256500,5.080,198200,5.070,158200,5.060,123400,5.050,138200,5.040,765600,5.090,723383,5.100,313900,5.110,409600,5.120,233900,5.130,2023-04-03,14:26:21,00
@property (nonatomic,copy)  NSString* hanqing;//!<  接口查询回来的行情信息
@property (nonatomic,copy)  NSString* buy;//!<  买点数据排名:   14+49=63  29/72    .............
@property (nonatomic,copy)  NSString* sell;//!<  卖点数据排名: 东南网架002135 -50% 100+100=200  100/100    101天 上涨 5% 年化 21%  继续持有
@property (nonatomic,copy)  NSString* currentInfo;//!<  currentDecline= -67.98% , name = 202.85%😄42.54%  东南网架002135  -52.93%   2023.04.03
@property (nonatomic,copy)  NSString* xiadieValue;//!<  下跌多少


@end

NS_ASSUME_NONNULL_END
