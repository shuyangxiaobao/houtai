//
//  DetailViewController.h
//  xlp
//
//  Created by xiaobao on 2023/4/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController

-(instancetype)initWithModel:(AllInfoModel *)model list:(NSMutableArray *)instrumentIDDatas index:(int)index;

@end

NS_ASSUME_NONNULL_END
