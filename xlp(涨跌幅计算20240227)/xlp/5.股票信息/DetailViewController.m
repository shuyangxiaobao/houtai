//
//  DetailViewController.m
//  xlp
//
//  Created by xiaobao on 2023/4/3.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (nonatomic,strong) AllInfoModel *currentModel;
@property(nonatomic,strong)NSMutableArray *instrumentIDDatas;
@property(nonatomic,assign) int index;

@property (nonatomic,strong) UILabel *oneLab;
@property (nonatomic,strong) UILabel *twoLab;
@property (nonatomic,strong) UILabel *threeLab;
@property (nonatomic,strong) UILabel *fourLab;

@property (nonatomic,strong) UIButton *button1; //!<  下一条
@property (nonatomic,strong) UIButton *button2; //!<    上一条


@end



@implementation DetailViewController

-(instancetype)initWithModel:(AllInfoModel *)model list:(NSMutableArray *)instrumentIDDatas index:(int)index{
    self = [super init];
    if(self){
        _currentModel = model;
        _instrumentIDDatas = instrumentIDDatas;
        _index = index;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
    self.oneLab = [UILabel createLabelWith:@"" font:[UIFont systemFontOfSize:16] textColor:@"000000"];
    self.oneLab.numberOfLines = 0;
    [self.view addSubview:self.oneLab];
    [self.oneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(NavAndStatusBarHeight +10);
    }];
    
    
    self.twoLab = [UILabel createLabelWith:@"" font:[UIFont systemFontOfSize:16] textColor:@"000000"];
    self.twoLab.numberOfLines = 0;
    [self.view addSubview:self.twoLab];
    [self.twoLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.oneLab.mas_bottom).offset(50);
    }];
    
    self.threeLab = [UILabel createLabelWith:@"" font:[UIFont systemFontOfSize:16] textColor:@"000000"];
    self.threeLab.numberOfLines = 0;
    [self.view addSubview:self.threeLab];
    [self.threeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.twoLab.mas_bottom).offset(50);
    }];
    
    self.fourLab = [UILabel createLabelWith:@"" font:[UIFont systemFontOfSize:16] textColor:@"000000"];
    self.fourLab.numberOfLines = 0;
    [self.view addSubview:self.fourLab];
    [self.fourLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.threeLab.mas_bottom).offset(50);
    }];
    
    UIButton *button1 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [self.view addSubview:button1];
    [button1 setTitle:@"上一条" forState:(UIControlStateNormal)];
    [button1 setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    button1.backgroundColor = [UIColor colorWithHexString:@"4a79fb"];
    button1.layer.cornerRadius = 5;
    [button1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo((kScreen_Width-30)/2);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(-100);
    }];
    [button1 addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIButton *button2 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [self.view addSubview:button2];
    [button2 setTitle:@"下一条" forState:(UIControlStateNormal)];
    [button2 setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    button2.backgroundColor = [UIColor colorWithHexString:@"4a79fb"];
    button2.layer.cornerRadius = 5;
    [button2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.width.mas_equalTo((kScreen_Width-30)/2);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(-100);
    }];
    [button2 addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    self.button1 = button1;
    self.button2 = button2;
 
}


-(void)buttonClick:(UIButton *)button{
    if(button == self.button1){
        self.index--;
    }
    if(button == self.button2){
        self.index++;
    }
    if(self.index < 0){
        self.index = self.instrumentIDDatas.count-1;
    }
    if(self.index >= self.instrumentIDDatas.count){
        self.index = 0;
    }
    
    
    NSString *daima = self.instrumentIDDatas[self.index];
    daima = [daima substringFromIndex:2];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
    AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    self.currentModel = model;
    [self refreshUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
  
    [self refreshUI];
}
-(void)refreshUI{
    self.oneLab.text = self.currentModel.buy;
    self.twoLab.text = self.currentModel.sell;
    self.threeLab.text = self.currentModel.currentInfo;
    self.fourLab.text = self.currentModel.xiadieValue;
    self.title = [NSString stringWithFormat:@"%d/%d",self.index+1,self.instrumentIDDatas.count];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
