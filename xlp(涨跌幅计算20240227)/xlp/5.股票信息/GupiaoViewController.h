//
//  GupiaoViewController.h
//  xlp
//
//  Created by xiaobao on 2023/4/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GupiaoType) {
    GupiaoTypeCommon  = 0,
    GupiaoType_55_best     = 1
};

@interface GupiaoViewController : UIViewController


-(instancetype)initWithType:(GupiaoType)type;

@end

NS_ASSUME_NONNULL_END
