//
//  GupiaoViewController.m
//  xlp
//
//  Created by xiaobao on 2023/4/2.
//

#import "GupiaoViewController.h"
#import "StockModel.h""
#import "Entrance.h"
#import "Enter2.h"
#import "GQBOneCell.h"
#import "DetailViewController.h"


@interface GupiaoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *instrumentIDDatas;

@property (nonatomic,strong) UIButton *button1; //!<  45-50
@property (nonatomic,strong) UIButton *button2; //!<    >50
@property (nonatomic,assign) GupiaoType type; //!<    >50

@end

@implementation GupiaoViewController

-(instancetype)initWithType:(GupiaoType)type{
    self = [super init];
    if(self){
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.instrumentIDDatas = [NSMutableArray array];
    [self createSearchView];
    [self initTableview];
    
    [[GQBSettingmanager shareInstance].satisArr removeAllObjects];
    
    [HUDUtil showLoadingHUD:@""];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        Entrance *ent = [Entrance new];
        [ent enter:self.type];
        Enter2 *ent2 = [Enter2 new];
        
        MJWeakSelf
        ent2.showTableviewBlock = ^{
            [weakSelf.instrumentIDDatas removeAllObjects];
            [weakSelf.instrumentIDDatas addObjectsFromArray:[GQBSettingmanager shareInstance].instrumentIDDatas];
            [self sortdata];
            [weakSelf.tableView reloadData];
            [HUDUtil hideHUDAnimated:YES];
        };
        [ent2 enter:self.type];
        // xlpxlptest
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0*NSEC_PER_SEC)), dispatch_get_global_queue(0, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUDUtil hideHUDAnimated:YES];
            });
        });
    });
    
  
}

-(void)viewWillAppear:(BOOL)animated{

}

-(void)createSearchView{

    UITextField *searchField = [[UITextField alloc]initWithFrame:CGRectZero];
    searchField.textAlignment = NSTextAlignmentCenter;
    searchField.textColor = [UIColor colorWithHexString:@"000000"];
    [self.view addSubview:searchField];
    searchField.backgroundColor = [UIColor clearColor];
    searchField.borderStyle = UITextBorderStyleRoundedRect;
    searchField.clearButtonMode=UITextFieldViewModeWhileEditing;
    searchField.delegate = self;
    [searchField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavAndStatusBarHeight);
        make.left.mas_equalTo(2);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(kScreen_Width/2);
    }];

    [searchField addTarget:self
              action:@selector(textFieldDidChangeValue:)
    forControlEvents:UIControlEventEditingChanged];
    
    UIButton *button1 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.button1 = button1;
    [self.view addSubview:button1];
    [button1 setTitle:@">45" forState:(UIControlStateNormal)];
    [button1 setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    button1.backgroundColor = [UIColor colorWithHexString:@"4a79fb"];
    button1.layer.cornerRadius = 5;
    [button1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchField.mas_right).offset(2);
        make.centerY.mas_equalTo(searchField.mas_centerY);
        make.width.mas_equalTo(getAutoSizeWidth(90));
        make.height.mas_equalTo(40);
    }];
    [button1 addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIButton *button2 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.button2 = button2;
    button2.layer.cornerRadius = 5;
    [self.view addSubview:button2];
    [button2 setTitle:@"> 50" forState:(UIControlStateNormal)];
    [button2 setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    button2.backgroundColor = [UIColor colorWithHexString:@"4a79fb"];
    [button2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(button1.mas_right).offset(2);
        make.centerY.mas_equalTo(searchField.mas_centerY);
        make.width.mas_equalTo(getAutoSizeWidth(90));
        make.height.mas_equalTo(40);
    }];
    [button2 addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
}

-(void)buttonClick:(UIButton *)button{
    for (NSString *name in [GQBSettingmanager shareInstance].satisArr) {
        NSLog(@"%@",name);
    }
    double value = 0;
    if(button == self.button1){
        // 45-50
        value = -45.0;
        self.title = @">45";
    }
    if(button == self.button2){
        // 45-50
        value = -50.0;
        self.title = @">50";
    }
    
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSString *daima in [GQBSettingmanager shareInstance].instrumentIDDatas) {
        NSString *daima6 = [daima substringFromIndex:2];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima6];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if([model.xiadieValue doubleValue] <= value){
            [tempArr addObject:daima];
        }
    }
    
    tempArr =  [tempArr sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
        NSString *daima1 = [obj1 substringFromIndex:2];
        NSData *data1 = [[NSUserDefaults standardUserDefaults] objectForKey:daima1];
        AllInfoModel *model_1 = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
        
        NSString *daima2 = [obj2 substringFromIndex:2];
        NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:daima2];
        AllInfoModel *model_2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
        double xiadie1 = [model_1.xiadieValue doubleValue];
        double xiadie2 = [model_2.xiadieValue doubleValue];
        double result = xiadie1 - xiadie2;
        if(result < 0){
            return NSOrderedAscending;
        } else if(result > 0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];

    [self.instrumentIDDatas removeAllObjects];
    [self.instrumentIDDatas addObjectsFromArray:tempArr];
    [self.tableView reloadData];
    
    
    NSMutableArray *resultArr = [NSMutableArray array];
    for (NSString *daimaTemp in tempArr) {
        NSString *daima = [daimaTemp substringFromIndex:2];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale systemLocale];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateStr = [formatter stringFromDate:date];
        NSString *timeStr = [dateStr componentsSeparatedByString:@" "].lastObject;

        NSString *hangqing = model.hanqing;
        NSString *price = [hangqing componentsSeparatedByString:@","][5];
        
        if([GQBSettingmanager shareInstance].minestPrice){
            price = [hangqing componentsSeparatedByString:@","][5];//huxu625  最低价
        } else {
            price = [hangqing componentsSeparatedByString:@","][3];//huxu625  最新价
        }

        
        NSString *chineseName = [hangqing componentsSeparatedByString:@","][0];
        NSString *updateTime = [hangqing componentsSeparatedByString:@","][31];
        if([chineseName containsString:@" "]){
            chineseName = [[chineseName componentsSeparatedByString:@" "] componentsJoinedByString:@""];
        }
        NSString *title = [NSString stringWithFormat:@"%@  %@",chineseName,daima];
        
        if([self.title isEqualToString:@">50"] || [self.title containsString:@"-"]){
            price = model.xiadieValue;
        }
        
        NSString *result = [NSString stringWithFormat:@"%@%.2f",chineseName,fabs([price doubleValue])];
        [resultArr addObject:result];
    }
    NSMutableArray *arr1 = [NSMutableArray array];
    NSMutableArray *arr2 = [NSMutableArray array];

    for (NSString *result in resultArr) {
        if(arr1.count < 5){
            [arr1 addObject:result];
        } else {
            NSString *temp =  [arr1 componentsJoinedByString:@"  "];
            [arr2 addObject:temp];
            [arr1 removeAllObjects];
            [arr1 addObject:result];
        }
    }
    
    NSString *temp =  [arr1 componentsJoinedByString:@"  "];
    [arr2 addObject:temp];
    
    NSString *end = [arr2 componentsJoinedByString:@"\n"];
    NSLog(@"%@",end);
    
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    [pab setString:end];
    [self showData];
        
}

-(void)showData{
    for (NSString *daima in self.instrumentIDDatas) {
        NSString *daima6 = [daima substringFromIndex:2];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima6];
        AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSLog(@"%@",model.buy);
        NSLog(@"%@",model.sell);
        NSLog(@"%@",model.currentInfo);
        NSLog(@"");
    }
}

- (void)textFieldDidChangeValue:(UITextField *)field{
    NSString *searchText = field.text;
    
    
    // "40-50" 跌幅在 40-50之间
    if([searchText containsString:@"-"] && searchText.length >= 5){
        NSArray *arr = [searchText componentsSeparatedByString:@"-"];
        double value1 = 0 - [arr.firstObject doubleValue];
        double value2 = 0 - [arr.lastObject doubleValue];
        self.title = searchText;

        NSMutableArray *tempArr = [NSMutableArray array];
        for (NSString *daima in [GQBSettingmanager shareInstance].instrumentIDDatas) {
            NSString *daima6 = [daima substringFromIndex:2];
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima6];
            AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if([model.xiadieValue doubleValue] <= value1 && [model.xiadieValue doubleValue] >= value2){
                [tempArr addObject:daima];
            }
        }
        
        tempArr =  [tempArr sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
            NSString *daima1 = [obj1 substringFromIndex:2];
            NSData *data1 = [[NSUserDefaults standardUserDefaults] objectForKey:daima1];
            AllInfoModel *model_1 = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
            
            NSString *daima2 = [obj2 substringFromIndex:2];
            NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:daima2];
            AllInfoModel *model_2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
            double xiadie1 = [model_1.xiadieValue doubleValue];
            double xiadie2 = [model_2.xiadieValue doubleValue];
            double result = xiadie1 - xiadie2;
            if(result < 0){
                return NSOrderedAscending;
            } else if(result > 0){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        
        
        [self.instrumentIDDatas removeAllObjects];
        [self.instrumentIDDatas addObjectsFromArray:tempArr];
        [self.tableView reloadData];
        [self showData];

        return;
    }
    
    



    
    searchText = [searchText lowercaseString];
    // 清空行情数据
    if([searchText containsString:@"清空"] || [searchText containsString:@"删除"] || [searchText containsString:@"clear"] || [searchText containsString:@"shanchu"] ){
        [self.view endEditing:YES];
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"确定需要清空行情数据吗?" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertVC dismissModalViewControllerAnimated:YES];
            for (NSString *daima in [GQBSettingmanager shareInstance].instrumentIDDatas) {
                NSString *daima6 = [daima substringFromIndex:2];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:daima6];
            }
            [[GQBSettingmanager shareInstance].instrumentIDDatas removeAllObjects];
            [HUDUtil showTipHUD:@"完成" duration:1.5];
        }];
        [alertVC addAction:action1];
        
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertVC dismissModalViewControllerAnimated:YES];
        }];
        [alertVC addAction:action2];
        
        [self presentViewController:alertVC animated:YES completion:^{
            
        }];
        return;
    }
    
    // 搜索合约代码
    if(searchText.length == 6){
        NSMutableArray *tempArr = [NSMutableArray array];
        for (NSString *daima in [GQBSettingmanager shareInstance].instrumentIDDatas) {
            NSString *daima6 = [daima substringFromIndex:2];
            if([searchText isEqualToString:daima6]){
                [tempArr addObject:daima];
                break;
            }
        }
        if(tempArr.count > 0){
            [self.instrumentIDDatas removeAllObjects];
            [self.instrumentIDDatas addObjectsFromArray:tempArr];
            [self.tableView reloadData];
        }
    } else if (searchText.length == 0){
        self.title = @"All";
        [self.instrumentIDDatas removeAllObjects];
        [self.instrumentIDDatas addObjectsFromArray:[GQBSettingmanager shareInstance].instrumentIDDatas];
        [self.tableView reloadData];
        
        [self sortdata];
        
    }
    
    if([self isChineseWithStr:searchText]){
        self.title = searchText;
        NSMutableArray *tempArr = [NSMutableArray array];
        for (NSString *daima in [GQBSettingmanager shareInstance].instrumentIDDatas) {
            NSString *daima6 = [daima substringFromIndex:2];
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima6];
            AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            NSString *gupiaoName = [model.hanqing componentsSeparatedByString:@","].firstObject;
            if([gupiaoName containsString:@" "]){
                gupiaoName = [[gupiaoName componentsSeparatedByString:@" "] componentsJoinedByString:@""];
            }
            if([gupiaoName containsString:searchText]){
                [tempArr addObject:daima];
            }
        }
        
        tempArr =  [tempArr sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
            NSString *daima1 = [obj1 substringFromIndex:2];
            NSData *data1 = [[NSUserDefaults standardUserDefaults] objectForKey:daima1];
            AllInfoModel *model_1 = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
            
            NSString *daima2 = [obj2 substringFromIndex:2];
            NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:daima2];
            AllInfoModel *model_2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
            double xiadie1 = [model_1.xiadieValue doubleValue];
            double xiadie2 = [model_2.xiadieValue doubleValue];
            double result = xiadie1 - xiadie2;
            if(result < 0){
                return NSOrderedAscending;
            } else if(result > 0){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        
        
        [self.instrumentIDDatas removeAllObjects];
        [self.instrumentIDDatas addObjectsFromArray:tempArr];
        [self.tableView reloadData];
        [self showData];

        return;
    }
    
}

-(void)sortdata{
    NSMutableArray *tempArr = [NSMutableArray array];
    [tempArr addObjectsFromArray:self.instrumentIDDatas];
    tempArr =  [tempArr sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
        NSString *daima1 = [obj1 substringFromIndex:2];
        NSData *data1 = [[NSUserDefaults standardUserDefaults] objectForKey:daima1];
        AllInfoModel *model_1 = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
        
        NSString *daima2 = [obj2 substringFromIndex:2];
        NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:daima2];
        AllInfoModel *model_2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
        double xiadie1 = [model_1.xiadieValue doubleValue];
        double xiadie2 = [model_2.xiadieValue doubleValue];
        double result = xiadie1 - xiadie2;
        if(result < 0){
            return NSOrderedAscending;
        } else if(result > 0){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    [self.instrumentIDDatas removeAllObjects];
    [self.instrumentIDDatas addObjectsFromArray:tempArr];
    [self.tableView reloadData];
}

-(void)initTableview{
    self.view.backgroundColor = [UIColor whiteColor];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor colorWithHexString:@"979797" alpha:0.4];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource =self;
    _tableView.scrollEnabled = YES;
    _tableView.separatorStyle = UITableViewCellSelectionStyleGray;
    _tableView.userInteractionEnabled = YES;
    _tableView.layer.masksToBounds = YES;
    _tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[GQBOneCell class] forCellReuseIdentifier:@"lasdjlfa"];


    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavAndStatusBarHeight+52);
        make.left.equalTo(@(0));
        make.right.equalTo(@(0));
        make.bottom.equalTo(@(0));
    }];
    
    
  
    [self.tableView reloadData];
 
}



#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    return 10;
    return self.instrumentIDDatas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GQBOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lasdjlfa"];
    if (cell == nil) {
        cell = [[GQBOneCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"lasdjlfa"];
    }
    NSString *daima = self.instrumentIDDatas[indexPath.row];
    daima = [daima substringFromIndex:2];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
    AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale systemLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:date];
    NSString *timeStr = [dateStr componentsSeparatedByString:@" "].lastObject;
    
  
    
    
    
    NSString *hangqing = model.hanqing;
    NSString *price = [hangqing componentsSeparatedByString:@","][5];
    
    
    if([timeStr compare:@"15:00:00"] == NSOrderedAscending){
        price = [hangqing componentsSeparatedByString:@","][3];//huxu625  15点前 最新价
    } else {
        price = [hangqing componentsSeparatedByString:@","][5];//huxu625  15点后 最低价
    }
    if([hangqing containsString:@"荣泰健康"]){
        NSLog(@"234");
    }
    
    
    if([GQBSettingmanager shareInstance].minestPrice){
        price = [hangqing componentsSeparatedByString:@","][5];//huxu625  最低价
    } else {
        price = [hangqing componentsSeparatedByString:@","][3];//huxu625  最新价
    }

    
    NSString *chineseName = [hangqing componentsSeparatedByString:@","][0];
    NSString *updateTime = [hangqing componentsSeparatedByString:@","][31];    
    if([chineseName containsString:@" "]){
        chineseName = [[chineseName componentsSeparatedByString:@" "] componentsJoinedByString:@""];
    }
    NSString *title = [NSString stringWithFormat:@"%@  %@",chineseName,daima];
    
    if([self.title isEqualToString:@">50"] || [self.title containsString:@"-"]){
        price = model.xiadieValue;
    }
    NSString *rightTitle = [NSString stringWithFormat:@"%@  %@",price,updateTime];
    [cell refreshUI:title index:indexPath.row+1 rightTitle:rightTitle];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor colorWithHexString:@"888888"].CGColor;
//    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *daima = self.instrumentIDDatas[indexPath.row];
    daima = [daima substringFromIndex:2];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:daima];
    AllInfoModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    DetailViewController *vc = [[DetailViewController alloc] initWithModel:model list:self.instrumentIDDatas index:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

#pragma mark - 判断是否为中文
- (BOOL)isChineseWithStr:(NSString *)str
{
    for(int i=0; i< [str length];i ++)
    {
        int a = [str characterAtIndex:i];
        
        if( a > 0x4e00 && a < 0x9fff)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return NO;
}

-(void)dealloc{
    NSLog(@"%s",__func__);
}

@end



