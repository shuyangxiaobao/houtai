//
//  HTTPEngineGuide.m
//  SECollection
//
//  Created by HuangSam on 16/8/29.
//  Copyright © 2016年 suneee. All rights reserved.
//

#import "HTTPEngine.h"
#import "AFNetworking.h"
#import "XMLDictionary.h"


@interface HTTPEngine ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;

@property (nonatomic, strong) AFHTTPSessionManager *manager2;

@end

@implementation HTTPEngine

+ (HTTPEngine *)sharedEngine{
    static HTTPEngine *_sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedEngine = [[self alloc] init];
    });
    return _sharedEngine;
}

- (AFHTTPSessionManager *)manager{
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.requestSerializer.timeoutInterval = 5;
//         [_manager.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [_manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        [_manager.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];


        
//        [_manager.requestSerializer setValue:@"FlexiTrader 1.5.6" forHTTPHeaderField:@"User-Agent"];

    }
    return _manager;
}

- (AFHTTPSessionManager *)manager2{
    if (!_manager2) {
        _manager2 = [AFHTTPSessionManager manager];
        _manager2.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager2.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager2.requestSerializer.timeoutInterval = 5;
         [_manager2.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        

    }
    return _manager;
}


- (NSURLSessionDataTask *)getRequestWithURL:(NSString *)url
                                 parameters:(id)parameters
                                    success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *dataTask, NSError *error))failure{

    return [self.manager GET:url parameters:parameters headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (success) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                success(task, responseObject);
            }else if ([responseObject isKindOfClass:[NSData class]]) {
                NSError *error;
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    NSDictionary *jsonObj = [NSDictionary dictionaryWithXMLData:responseObject];
                    if (jsonObj) {
                        success(task, jsonObj);
                    }else{
                        success(task,@{@"error":error.localizedDescription});
                    }
                }else{
                    success(task, dic);
                }
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (failure) {
            failure(task, error);
        }
    }];
}

- (NSURLSessionDataTask *)post2RequestWithURL:(NSString *)url
                 parameters:(id)parameters
                   success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
////    把token取出来
//    NSString *token = [NSString stringWithFormat:@"%@", [QDUserDefaults getQDToken]];
//    if (token && ![token isEqualToString:@"(null)"] && ![token isEqualToString:@""]) {
//        [self.manager2.requestSerializer setValue:token forHTTPHeaderField:@"token"];
//    }else{
//        [self.manager2.requestSerializer setValue:nil forHTTPHeaderField:@"token"];
//    }
    return [self.manager2 POST:url parameters:parameters  headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (success) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                success(task, responseObject);
            }else if ([responseObject isKindOfClass:[NSData class]]) {
                NSError *error;
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                if (error) {
                    NSDictionary *jsonObj = [NSDictionary dictionaryWithXMLData:responseObject];
                    if (jsonObj) {
                        success(task, jsonObj);
                    }else{
                        success(task,@{@"error":error.localizedDescription});
                    }
                }else{
                    
                    success(task, dic);
                }
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (failure) {
            failure(task, error);
        }
    }];


}



- (NSURLSessionDataTask *)postRequestWithURL:(NSString *)url
                 parameters:(id)parameters
                   success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    return [self.manager POST:url parameters:parameters  headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (success) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                success(task, responseObject);
            }else if ([responseObject isKindOfClass:[NSData class]]) {
                NSError *error;
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                if (error) {
                    NSDictionary *jsonObj = [NSDictionary dictionaryWithXMLData:responseObject];
                    if (jsonObj) {
                        success(task, jsonObj);
                    }else{
                        success(task,@{@"error":error.localizedDescription});
                    }
                }else{
                    
                    success(task, dic);
                }
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (failure) {
            failure(task, error);
        }
    }];


}

/**
 *  上传
 *
 *  @param url       请求url
 *  @param parameter post参数
 *  @param block     上传数据
 *  @param success   成功回调
 *  @param failure   失败回调
 */
- (NSURLSessionDataTask *)uploadRequestWithURL:(NSString *)url
                    parameter:(id)parameter
            constructingBody:(void(^)(id <AFMultipartFormData> formData))block
                     success:(void (^)(NSURLSessionDataTask *operation, NSDictionary *responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    return [self.manager POST:url parameters:parameter  headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (block) {
            block(formData);
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (success) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                success(task, responseObject);
            }else if ([responseObject isKindOfClass:[NSData class]]) {
                NSError *error;
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    NSDictionary *jsonObj = [NSDictionary dictionaryWithXMLData:responseObject];
                    if (jsonObj) {
                        success(task, jsonObj);
                    }else{
                        success(task,@{@"error":error.localizedDescription});
                    }
                }else{
                    success(task, dic);
                }
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (failure) {
            failure(task,error);
        }
    }];
}

@end

