//
//  UILabel+Create.m
//  QDINFI
//
//  Created by xiaobao on 2020/4/2.
//  Copyright © 2020 quantdo. All rights reserved.
//

#import "UILabel+Create.h"

@implementation UILabel (Create)

+(UILabel *)createLabelWith:(NSString *)title
                   font:(UIFont *)font
              textColor:(id)textColor{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
    label.text = title;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumFontSize = 6;
    label.textAlignment = NSTextAlignmentLeft;
    if ([textColor isKindOfClass:[UIColor class]]) {
        label.textColor = textColor;
    } else {
        label.textColor = [UIColor colorWithHexString:textColor];
    }
    return label;
    
}


+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id )textColor
               cornerRadius:(CGFloat)cornerRadius
            backgroundColor:(UIColor*)backgroundColor
                borderWidth:(CGFloat)borderWidth
                borderColor:(UIColor*)borderColor

{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
    label.text = title;
    if ([textColor isKindOfClass:[UIColor class]]) {
        label.textColor = textColor;
    } else {
        label.textColor = [UIColor colorWithHexString:textColor];
    }
    label.font = font;
    label.layer.cornerRadius =cornerRadius;
    label.backgroundColor = backgroundColor;
    label.layer.borderWidth = borderWidth;
    label.layer.borderColor = borderColor.CGColor;
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.masksToBounds = YES;
    return label;
}

+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id)textColor
                       left:(CGFloat)left
                        top:(CGFloat)top
                     height:(CGFloat)height
                  superView:(UIView *)superView{
    UILabel *label = [UILabel createLabelWith:title font:font textColor:textColor];
    [superView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(left);
        make.top.mas_equalTo(top);
        make.height.mas_equalTo(height);
    }];
    return label;
}

+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id)textColor
                       left:(CGFloat)left
                        top:(CGFloat)top
                     height:(CGFloat)height
                     width:(CGFloat)width
                  superView:(UIView *)superView{
    UILabel *label = [UILabel createLabelWith:title font:font textColor:textColor];
    [superView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(left);
        make.top.mas_equalTo(top);
        if (height > 0) {
            make.height.mas_equalTo(height);
        }
        if (width > 0) {
            make.width.mas_equalTo(width);
        }        
    }];
    return label;
}


+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1000, 0)];
    label.text = title;
    label.font = font;
    [label sizeToFit];
    CGFloat width = label.frame.size.width;
    return ceil(width);
}

@end
