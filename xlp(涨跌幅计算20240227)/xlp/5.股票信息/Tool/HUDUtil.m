//
//  HUDUtil.m
//  SECollection
//
//  Created by Harvey on 16/9/12.
//  Copyright © 2016年 suneee. All rights reserved.
//

#import "HUDUtil.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@implementation HUDUtil

+ (void)showTipHUD:(NSString *)message duration:(NSTimeInterval)duration
{
    if (message.length == 0 || duration <= 0) {
        return;
    }
    
    [self hideHUDAnimated:NO];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.removeFromSuperViewOnHide = YES;
    hud.mode = MBProgressHUDModeText;
    hud.label.text = message;
    [hud hideAnimated:YES afterDelay:duration];
//    hud.labelText = message;
//    [hud hide:YES afterDelay:duration];
}

/**
 *  显示完成的HUD
 *
 *  @param message    消息
 *  @param duration   显示时长，单位秒
 */
+ (void)showDoneHUD:(NSString *)message duration:(NSTimeInterval)duration
{
    if (message.length == 0 || duration <= 0) {
        return;
    }
    
    [self hideHUDAnimated:NO];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.removeFromSuperViewOnHide = YES;
    hud.mode = MBProgressHUDModeCustomView;
    // Set an image view with a checkmark.
    UIImage *image = [[UIImage imageNamed:@"Checkmark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    hud.customView = [[UIImageView alloc] initWithImage:image];
    
    hud.square = YES;
    // Optional label text.
    hud.label.text = message;
    [hud hideAnimated:YES afterDelay:duration];
//    hud.labelText = message;
//    [hud hide:YES afterDelay:duration];
}

/**
 *  显示加载HUD
 *
 *  @param message 消息
 */
+ (void)showLoadingHUD:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideHUDAnimated:NO];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
        hud.removeFromSuperViewOnHide = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
    //    hud.labelText = message;
        hud.label.text = message;
    });
}

/**
 *  隐藏HUD
 *
 *  @param animated 是否显示动画
 */
+ (void)hideHUDAnimated:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *oldHUD = [MBProgressHUD HUDForView:window];
    if (oldHUD) {
        [oldHUD hideAnimated:animated];
    }
}

@end
