//
//  HUDUtil.h
//  SECollection
//
//  Created by Harvey on 16/9/12.
//  Copyright © 2016年 suneee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HUDUtil : NSObject

/**
 *  显示提示信息
 *
 *  @param message  提示信息
 *  @param duration 显示时长，单位秒
 */
+ (void)showTipHUD:(NSString *)message duration:(NSTimeInterval)duration;

/**
 *  显示完成的HUD
 *
 *  @param message    消息
 *  @param duration   显示时长，单位秒
 */
+ (void)showDoneHUD:(NSString *)message duration:(NSTimeInterval)duration;

/**
 *  显示加载HUD
 *
 *  @param message 消息
 */
+ (void)showLoadingHUD:(NSString *)message;

/**
 *  隐藏HUD
 *
 *  @param animated 是否显示动画
 */
+ (void)hideHUDAnimated:(BOOL)animated;

@end
