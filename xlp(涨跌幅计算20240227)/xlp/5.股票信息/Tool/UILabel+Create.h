//
//  UILabel+Create.h
//  QDINFI
//
//  Created by xiaobao on 2020/4/2.
//  Copyright © 2020 quantdo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Create)

+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id)textColor;

+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id)textColor
               cornerRadius:(CGFloat)cornerRadius
            backgroundColor:(UIColor*)backgroundColor
                borderWidth:(CGFloat)borderWidth
                borderColor:(UIColor*)borderColor;

+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id)textColor
                       left:(CGFloat)left
                        top:(CGFloat)top
                     height:(CGFloat)height
                  superView:(UIView *)superView;

+(UILabel *)createLabelWith:(NSString *)title
                       font:(UIFont *)font
                  textColor:(id)textColor
                       left:(CGFloat)left
                        top:(CGFloat)top
                     height:(CGFloat)height
                     width:(CGFloat)width
                  superView:(UIView *)superView;

+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font;
@end

NS_ASSUME_NONNULL_END
