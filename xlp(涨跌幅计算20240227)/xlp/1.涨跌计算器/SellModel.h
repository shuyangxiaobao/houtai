//
//  SellModel.h
//  xlp
//
//  Created by xiaobao on 2023/6/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


/// 卖出点数据
@interface SellModel : NSObject
@property (nonatomic,copy) NSString *name;//!< 东港股份002117
@property (nonatomic,copy) NSString *sellDate;//!<  2023.2.15(卖出日)
@property (nonatomic,copy) NSString *timelength;//!< 291天
@property (nonatomic,copy) NSString *rise;//!< 上涨92%
@property (nonatomic,copy) NSString *rate;//!< 年化127%
@property (nonatomic,copy) NSString *overDay;//!< 53天
@property (nonatomic,copy) NSString *ninetyRise;//!< 获利比例90%后继续上涨的点数


// 大波段(有回调)
-(instancetype)initWithName:(NSString *)name sellDate:(NSString *)sellDate timelength:(NSString *)timelength rise:(NSString *)rise rate:(NSString *)rate overDay:(NSString *)overDay;

// 没有回调
-(instancetype)initWithName:(NSString *)name sellDate:(NSString *)sellDate timelength:(NSString *)timelength rise:(NSString *)rise rate:(NSString *)rate overDay:(NSString *)overDay ninetyRise:(NSString *)ninetyRise;



@end

NS_ASSUME_NONNULL_END
