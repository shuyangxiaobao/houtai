//
//  JSViewController.m
//  xlp
//
//  Created by xiaobao on 2021/10/29.
//

#import "GQBJSViewController.h"
#import "SellModel.h"

@interface GQBJSViewController ()
@property (weak, nonatomic) IBOutlet UITextField *originField;
@property (weak, nonatomic) IBOutlet UITextField *currentField;
@property (weak, nonatomic) IBOutlet UILabel *resultLab;

@end

@implementation GQBJSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    if (@available(iOS 16.0, *)) {
//        // setNeedsUpdateOfSupportedInterfaceOrientations 方法是 UIViewController 的方法
//        [self setNeedsUpdateOfSupportedInterfaceOrientations];
//        NSArray *array = [[[UIApplication sharedApplication] connectedScenes] allObjects];
//        UIWindowScene *scene = [array firstObject];
//        // 屏幕方向
//        UIInterfaceOrientationMask orientation = YES ? UIInterfaceOrientationMaskLandscape: UIInterfaceOrientationMaskPortrait;
//        UIWindowSceneGeometryPreferencesIOS *geometryPreferencesIOS = [[UIWindowSceneGeometryPreferencesIOS alloc] initWithInterfaceOrientations:orientation];
//        // 开始切换
//        [scene requestGeometryUpdateWithPreferences:geometryPreferencesIOS errorHandler:^(NSError * _Nonnull error) {
//            NSLog(@"错误:%@", error);
//        }];
//    }

}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self jisuan];
    [self.view endEditing:YES];
    
    [self demo2];
//    [self demo1];
//    [self demo3];

    
}

#pragma mark - 计算批量数据跌幅
//003 计算单只股票批量每天涨跌幅
//003 计算单只股票批量每天涨跌幅

-(void)demo1{
    // 宏辉果蔬
    float highPrice = 5.324;
    NSMutableArray *mutArr = [NSMutableArray array];

    // 2.39
    [mutArr addObjectsFromArray:@[
        @"",@"",@"",@"",@"",
        @"",@"",@"",@"",@"",
        @"",@"",@"",@"",@"",
        @"",@"",@"",@"",@"",
    ]];
    
    // example
//    [mutArr addObjectsFromArray:@[
//        @"5.10-4.01",@"5.17-4.04",@"5.24-4.04",@"",@"",
//        @"",@"",@"",@"",@"",
//        @"",@"",@"",@"",@"",
//        @"",@"",@"",@"",@"",
//        @"",@"",@"",@"",@"",
//        @"",@"",@"",@"",@"",
//        @"",@"",@"",@"",@"",
//    ]];
    float result = 0;
    for (NSString *currentStr in mutArr) {
        float origin = highPrice;
        NSString *separtstr = @"-";
        float current = [[currentStr componentsSeparatedByString:separtstr].lastObject floatValue];
        NSString *time =  [currentStr componentsSeparatedByString:separtstr].firstObject;
        if(![currentStr containsString:separtstr]){
            time = @"";
        }
        result = (1-current/origin)*100;
        NSString *str = [NSString stringWithFormat:@"%@:  跌 %.2f%%",time,result];
        NSLog(@"%@",str);
    }
}

#pragma mark - 计算卖出点数据(没有回调)
-(void)demo3{
    
    //004 计算卖出点数据(没有回调)
//  example:  荣泰健康603579 2023.2.23  121天上涨 43% 年化 195% 68天    90%以上上涨15.88%
    NSLog(@"%s",__func__);
    NSMutableArray *mutArr = [NSMutableArray array];
 
    [mutArr addObject:[[SellModel alloc] initWithName:@"荣泰健康603579" sellDate:@"2023.2.23(卖出日)" timelength:@"121天" rise:@"43%" rate:@"年化195%" overDay:@"68天" ninetyRise:@"15.88%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"歌华有线600037" sellDate:@"2023.5.10(卖出日)" timelength:@"197天" rise:@"59%" rate:@"年化136%" overDay:@"47天" ninetyRise:@"31.46%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"正裕工业603089" sellDate:@"2022.9.8(卖出日)" timelength:@"134天" rise:@"71%" rate:@"年化331%" overDay:@"148天" ninetyRise:@"21.80%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"上海机电600835" sellDate:@"2022.8.1(卖出日)" timelength:@"96天" rise:@"62%" rate:@"年化552%" overDay:@"157天" ninetyRise:@"15.43%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"上海机电600835" sellDate:@"2023.7.3(卖出日)" timelength:@"256天" rise:@"74%" rate:@"年化115%" overDay:@"27天" ninetyRise:@"47.72%"]];

    [mutArr addObject:[[SellModel alloc] initWithName:@"人民同泰600829" sellDate:@"2022.12.6(卖出日)" timelength:@"223天" rise:@"64%" rate:@"年化125%" overDay:@"38天" ninetyRise:@"30.93%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"三力士002224" sellDate:@"2022.9.6(卖出日)" timelength:@"132天" rise:@"44%" rate:@"年化175%" overDay:@"61天" ninetyRise:@"12.13%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"冠豪高新600433" sellDate:@"2022.7.14(卖出日)" timelength:@"78天" rise:@"60%" rate:@"年化791%" overDay:@"168天" ninetyRise:@"0%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"冠豪高新600433" sellDate:@"2023.2.24(卖出日)" timelength:@"136天" rise:@"50%" rate:@"年化197%" overDay:@"78天" ninetyRise:@"18.28%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"金海高科603311" sellDate:@"2022.7.22(卖出日)" timelength:@"86天" rise:@"98%" rate:@"年化1721%" overDay:@"274天" ninetyRise:@"20.14%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"先锋电子002767" sellDate:@"2022.7.12(卖出日)" timelength:@"76天" rise:@"62%" rate:@"年化904%" overDay:@"177天" ninetyRise:@"0%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"先锋电子002767" sellDate:@"2023.2.3(卖出日)" timelength:@"116天" rise:@"95%" rate:@"年化712%" overDay:@"235天" ninetyRise:@"41.93%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"重庆燃气600917" sellDate:@"2022.8.12(卖出日)" timelength:@"107天" rise:@"71%" rate:@"年化522%" overDay:@"175天" ninetyRise:@"12.79%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"上海凤凰600679" sellDate:@"2023.6.14(卖出日)" timelength:@"50天" rise:@"56%" rate:@"年化2446%" overDay:@"184天" ninetyRise:@"29.49%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"联明股份603006" sellDate:@"2022.8.19(卖出日)" timelength:@"112天" rise:@"74%" rate:@"年化508%" overDay:@"180天" ninetyRise:@"7.86%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"联明股份603006" sellDate:@"2023.6.14(卖出日)" timelength:@"245天" rise:@"81%" rate:@"年化142%" overDay:@"68天" ninetyRise:@"42.26%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"东港股份002117" sellDate:@"2022.11.11(卖出日)" timelength:@"198天" rise:@"61%" rate:@"年化140%" overDay:@"52天" ninetyRise:@"24.97%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"东港股份002117" sellDate:@"2023.2.15(卖出日)" timelength:@"49天" rise:@"64%" rate:@"年化3861%" overDay:@"211天" ninetyRise:@"31.06%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"新亚制程002388" sellDate:@"2022.8.15(卖出日)" timelength:@"110天" rise:@"92%" rate:@"年化774%" overDay:@"234天" ninetyRise:@"27.90%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"新亚制程002388" sellDate:@"2022.12.9(卖出日)" timelength:@"59天" rise:@"92%" rate:@"年化5614%" overDay:@"285天" ninetyRise:@"46.24%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"神宇股份300563" sellDate:@"2022.8.15(卖出日)" timelength:@"131天" rise:@"86%" rate:@"年化461%" overDay:@"195天" ninetyRise:@"32.18%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"神宇股份300563" sellDate:@"2023.3.10(卖出日)" timelength:@"78天" rise:@"61%" rate:@"年化835%" overDay:@"174天" ninetyRise:@"33.52%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"神宇股份300563" sellDate:@"2023.6.19(卖出日)" timelength:@"35天" rise:@"56%" rate:@"年化10383%" overDay:@"200天" ninetyRise:@"28.80%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"美盈森002303" sellDate:@"2023.2.10(卖出日)" timelength:@"289天" rise:@"47%" rate:@"年化62%" overDay:@"-88天" ninetyRise:@"13.84%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"美盈森002303" sellDate:@"2022.9.5(卖出日)" timelength:@"131天" rise:@"35%" rate:@"年化132%" overDay:@"28天" ninetyRise:@"5.08%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"中电兴发002298" sellDate:@"2022.8.29(卖出日)" timelength:@"124天" rise:@"56%" rate:@"年化274%" overDay:@"112天" ninetyRise:@"0.63%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"中电兴发002298" sellDate:@"2023.3.1(卖出日)" timelength:@"142天" rise:@"38%" rate:@"年化127%" overDay:@"26天" ninetyRise:@"8.24%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"锋龙股份002931" sellDate:@"2022.8.10(卖出日)" timelength:@"105天" rise:@"87%" rate:@"年化779%" overDay:@"224天" ninetyRise:@"21%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"锋龙股份002931" sellDate:@"2023.7.3(卖出日)" timelength:@"69天" rise:@"47%" rate:@"年化666%" overDay:@"134天" ninetyRise:@"3.37%"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"北特科技603009" sellDate:@"2022.7.22(卖出日)" timelength:@"86天" rise:@"94%" rate:@"年化1577%" overDay:@"264天" ninetyRise:@"36%"]];
//    [mutArr addObject:[[SellModel alloc] initWithName:@"" sellDate:@"(卖出日)" timelength:@"天" rise:@"%" rate:@"年化%" overDay:@"天" ninetyRise:@"%"]];


    [self sort:mutArr key:@"rise" message:@"根据上涨点数排序"];
    [self sort:mutArr key:@"ninetyRise" message:@"根据90%后上涨点数排序"];
    [self sort:mutArr key:@"rate" message:@"根据年华收益率排序"];
    [self sort:mutArr key:@"overDay" message:@"根据多出的天数计算"];
    [self sort:mutArr key:@"timelength" message:@"timelength时间"];

  
}


#pragma mark - 计算卖出点数据(大波段,有回调)
-(void)demo2{
    
    //002 计算卖出点数据(大波段,有回调)
//  example:  先锋电子002767 2022.7.12(卖出日) 76天 62% 年化904% 177天
    
    NSMutableArray *mutArr = [NSMutableArray array];
 
    // 1-10
    [mutArr addObject:[[SellModel alloc] initWithName:@"航天工程603698" sellDate:@"2023.2.24(卖出日)" timelength:@"303天" rise:@"73%" rate:@"年化94%" overDay:@"-14天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"冠豪高新600433" sellDate:@"2023.2.24(卖出日)" timelength:@"303天" rise:@"74%" rate:@"年化95%" overDay:@"-12天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"人民同泰600829" sellDate:@"2022.12.6(卖出日)" timelength:@"223天" rise:@"64%" rate:@"年化125%" overDay:@"38天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"海鸥股份603269" sellDate:@"2023.3.3(卖出日)" timelength:@"310天" rise:@"98%" rate:@"年化124%" overDay:@"50天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"东港股份002117" sellDate:@"2023.2.15(卖出日)" timelength:@"291天" rise:@"92%" rate:@"年化127%" overDay:@"53天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"三力士002224" sellDate:@"2022.9.6(卖出日)" timelength:@"132天" rise:@"44%" rate:@"年化174%" overDay:@"60天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"荣晟环保603165" sellDate:@"2023.4.7(卖出日)" timelength:@"178天" rise:@"58%" rate:@"年化155%" overDay:@"63天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"深圳华强000062" sellDate:@"2022.9.19(卖出日)" timelength:@"145天" rise:@"50%" rate:@"年化175%" overDay:@"67天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"中核科技000777" sellDate:@"2022.9.15(卖出日)" timelength:@"141天" rise:@"50%" rate:@"年化186%" overDay:@"73天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"安迪苏600299" sellDate:@"2022.9.6(卖出日)" timelength:@"132天" rise:@"52%" rate:@"年化219%" overDay:@"89天"]];
    // 11-20
    [mutArr addObject:[[SellModel alloc] initWithName:@"东华能源002221" sellDate:@"2022.9.7(卖出日)" timelength:@"133天" rise:@"56%" rate:@"年化238%" overDay:@"101天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"木林森002745" sellDate:@"2022.8.24(卖出日)" timelength:@"119天" rise:@"57%" rate:@"年化298%" overDay:@"118天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"上海机电600835" sellDate:@"2022.8.1(卖出日)" timelength:@"119天" rise:@"57%" rate:@"年化298%" overDay:@"118天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"浙江众成002522" sellDate:@"2022.8.3(卖出日)" timelength:@"98天" rise:@"51%" rate:@"年化369%" overDay:@"120天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"新联电子002546" sellDate:@"2022.8.22(卖出日)" timelength:@"117天" rise:@"57%" rate:@"年化308%" overDay:@"120天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"浙数文化600633" sellDate:@"2022.12.5(卖出日)" timelength:@"222天" rise:@"96%" rate:@"年化203%" overDay:@"133天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"重庆燃气600917" sellDate:@"2022.8.12(卖出日)" timelength:@"107天" rise:@"60%" rate:@"年化398%" overDay:@"141天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"博深股份002282" sellDate:@"2022.9.9(卖出日)" timelength:@"135天" rise:@"70%" rate:@"年化317%" overDay:@"143天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"冠豪高新600433" sellDate:@"2022.7.14(卖出日)" timelength:@"82天" rise:@"54%" rate:@"年化583%" overDay:@"145天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"正裕工业603089" sellDate:@"2022.9.8(卖出日)" timelength:@"134天" rise:@"70%" rate:@"年化324%" overDay:@"145天"]];
    // 21-30
    [mutArr addObject:[[SellModel alloc] initWithName:@"睿能科技603933" sellDate:@"2022.8.10(卖出日)" timelength:@"105天" rise:@"61%" rate:@"年化425%" overDay:@"146天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"中电兴发002298" sellDate:@"2022.7.21(卖出日)" timelength:@"85天" rise:@"56%" rate:@"年化566%" overDay:@"148天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"方大集团000055" sellDate:@"2022.7.18(卖出日)" timelength:@"82天" rise:@"58%" rate:@"年化658%" overDay:@"158天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"杭电股份603618" sellDate:@"2022.8.30(卖出日)" timelength:@"125天" rise:@"71%" rate:@"年化380%" overDay:@"158天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"国元证券000728" sellDate:@"2022.8.25(卖出日)" timelength:@"119天" rise:@"70%" rate:@"年化412%" overDay:@"161天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"海立股份600619" sellDate:@"2022.4.27(卖出日)" timelength:@"106天" rise:@"67%" rate:@"年化481%" overDay:@"163天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"京能热力002893" sellDate:@"2022.8.12(卖出日)" timelength:@"107天" rise:@"69%" rate:@"年化499%" overDay:@"169天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"浙江东日600113" sellDate:@"2022.12.30(卖出日)" timelength:@"80天" rise:@"60%" rate:@"年化764%" overDay:@"169天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"先锋电子002767" sellDate:@"2022.7.12(卖出日)" timelength:@"76天" rise:@"62%" rate:@"年化904%" overDay:@"177天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"凯众股份603037" sellDate:@"2022.6.27(卖出日)" timelength:@"61天" rise:@"58%" rate:@"年化1429%" overDay:@"179天"]];
    // 31-40
    [mutArr addObject:[[SellModel alloc] initWithName:@"洛凯股份603829" sellDate:@"2022.8.30(卖出日)" timelength:@"125天" rise:@"81%" rate:@"年化465%" overDay:@"187天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"第一医药600833" sellDate:@"2022.6.24(卖出日)" timelength:@"59天" rise:@"60%" rate:@"年化1767%" overDay:@"190天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"亚普股份603013" sellDate:@"2022.8.9(卖出日)" timelength:@"104天" rise:@"82%" rate:@"年化722%" overDay:@"212天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"光大嘉宝600622" sellDate:@"2022.12.1(卖出日)" timelength:@"31天" rise:@"59%" rate:@"年化23664%" overDay:@"214天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"华锋股份002806" sellDate:@"2022.8.23(卖出日)" timelength:@"118天" rise:@"90%" rate:@"年化622%" overDay:@"219天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"锋龙股份002931" sellDate:@"2022.8.10(卖出日)" timelength:@"105天" rise:@"87%" rate:@"年化779%" overDay:@"224天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"远东传动002406" sellDate:@"2022.5.26(卖出日)" timelength:@"28天" rise:@"65%" rate:@"年化67502%" overDay:@"235天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"北特科技603009" sellDate:@"2022.7.22(卖出日)" timelength:@"86天" rise:@"94%" rate:@"年化1549%" overDay:@"262天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"金海高科603311" sellDate:@"2022.7.22(卖出日)" timelength:@"86天" rise:@"97%" rate:@"年化1677%" overDay:@"271天"]];
    [mutArr addObject:[[SellModel alloc] initWithName:@"新亚制程002388" sellDate:@"2022.12.9(卖出日)" timelength:@"225天" rise:@"161%" rate:@"年化375%" overDay:@"281天"]];

    [mutArr addObject:[[SellModel alloc] initWithName:@"丽岛新材603937" sellDate:@"2022.5.26(卖出日)" timelength:@"29天" rise:@"112%" rate:@"年化1282617%" overDay:@"367天"]];

//    @property (nonatomic,copy) NSString *rise;//!< 上涨92%
//    @property (nonatomic,copy) NSString *rate;//!< 年化127%
//    @property (nonatomic,copy) NSString *overDay;//!< 53天

    [self sort:mutArr key:@"rise" message:@"根据上涨点数排序"];
    [self sort:mutArr key:@"rate" message:@"根据年华收益率排序"];
    [self sort:mutArr key:@"overDay" message:@"根据多出的天数计算"];
    [self sort:mutArr key:@"timelength" message:@"timelength时间"];

  
}

-(void)sort:(NSMutableArray *)mutArr key:(NSString *)key message:(NSString *)message{
    NSArray *arr1 = [mutArr sortedArrayUsingComparator:^NSComparisonResult(SellModel * _Nonnull obj1, SellModel *  _Nonnull obj2) {
        int a1 = [self getValue: [obj1 valueForKey:key]];
        int a2 = [self getValue: [obj2 valueForKey:key]];
        if(a1 > a2){
            return NSOrderedAscending;
        } else if(a1 < a2){
            return NSOrderedDescending;
        } else{
            return NSOrderedSame;
        }
    }];
    NSLog(@"");
    NSLog(@"---------------%@---------------",message);
    for (int i = 0; i < arr1.count; i++) {
        SellModel *model = arr1[i];
        NSLog(@"序号:%d  %@",i+1,model);
    }
    NSLog(@"");
}



-(int)getValue:(NSString *)title{
    if([title containsString:@"年化"]){
        title = [title componentsSeparatedByString:@"年化"].lastObject;
    }
    if([title containsString:@"天"]){
        title = [title componentsSeparatedByString:@"天"].firstObject;
    }
    int value = [title intValue];
    return value;
}

-(void)jisuan{
    float origin = [self.originField.text floatValue];
    float current = [self.currentField.text floatValue];
    float result = 0;
    if (origin < current) {
         result = (current/origin -1)*100;
        _resultLab.text = [NSString stringWithFormat:@"涨%.2f%%",result];
    } else{
        result = (1-current/origin)*100;
        _resultLab.text = [NSString stringWithFormat:@"跌 %.2f%%",result];
    }
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    int result1 = result;
    NSString *title = [NSString stringWithFormat:@" end(%d%%)",(int)result1];
    [pab setString:title];
}

- (IBAction)exchange:(UIButton *)sender {
    NSString* origin = self.originField.text;
    NSString* current = self.currentField.text;
    self.originField.text = current;
    self.currentField.text = origin;
    
    
    UITextField *textField = _currentField;
    
    //点击UITextField，全选文字
       UITextPosition *endDocument = textField.endOfDocument;//获取 text的 尾部的 TextPositext
       
       UITextPosition *end = [textField positionFromPosition:endDocument offset:0];
       UITextPosition *start = [textField positionFromPosition:end offset:-textField.text.length];//左－右＋
       textField.selectedTextRange = [textField textRangeFromPosition:start toPosition:end];
      
    
}


- (BOOL)shouldAutorotate{
    
    return NO;
}

//显示状态栏
- (BOOL)prefersStatusBarHidden{
    
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationLandscapeRight;
}


@end
