//
//  SellModel.m
//  xlp
//
//  Created by xiaobao on 2023/6/2.
//

#import "SellModel.h"

@implementation SellModel


-(instancetype)initWithName:(NSString *)name sellDate:(NSString *)sellDate timelength:(NSString *)timelength rise:(NSString *)rise rate:(NSString *)rate overDay:(NSString *)overDay{
    self = [super init];
    if(self){
        _name = name;
        _sellDate = sellDate;
        _timelength = timelength;
        _rise = rise;
        _rate = rate;
        _overDay = overDay;
    }
    return self;
}

-(instancetype)initWithName:(NSString *)name sellDate:(NSString *)sellDate timelength:(NSString *)timelength rise:(NSString *)rise rate:(NSString *)rate overDay:(NSString *)overDay ninetyRise:(NSString *)ninetyRise{
    self = [super init];
    if(self){
        _name = name;
        _sellDate = sellDate;
        _timelength = timelength;
        _rise = rise;
        _rate = rate;
        _overDay = overDay;
        _ninetyRise = ninetyRise;
    }
    return self;
}


- (NSString *)description {
    return [NSString stringWithFormat: @"%@ %@ %@ %@ %@ %@ %@",_name,_sellDate,_timelength,_rise,_rate,_overDay,_ninetyRise.length > 0 ? _ninetyRise : @""];
}



@end
