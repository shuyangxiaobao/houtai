//
//  SceneDelegate.h
//  xlp
//
//  Created by xiaobao on 2021/10/29.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

