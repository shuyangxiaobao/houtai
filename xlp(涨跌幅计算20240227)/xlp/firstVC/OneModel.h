//
//  OneModel.h
//  74.OC-demo
//
//  Created by xiaobao on 2019/6/4.
//  Copyright © 2019 122. All rights reserved.
//


NS_ASSUME_NONNULL_BEGIN

@interface OneModel : NSObject

typedef void(^DetailBlock)(void);


@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * instrumentID;
@property(nonatomic,copy)NSString * maxPrice;
@property(nonatomic,copy)NSString * minPrice;
@property(nonatomic,copy)NSString * day;
@property(nonatomic,copy)NSString * lastPrice;


@property(nonatomic,copy)DetailBlock block;


@property(nonatomic,assign) BOOL select;
-(OneModel *)initWithName:(NSString *)name instrumentID:(NSString *)instrumentID maxPrice:(NSString *)maxPrice minPrice:(NSString *)minPrice day:(NSString *)day lastPrice:(NSString *)lastPrice block:(DetailBlock)block;
@end

NS_ASSUME_NONNULL_END
