//
//  OneModel.m
//  74.OC-demo
//
//  Created by xiaobao on 2019/6/4.
//  Copyright © 2019 122. All rights reserved.
//

#import "OneModel.h"

@implementation OneModel

-(OneModel *)initWithName:(NSString *)name instrumentID:(NSString *)instrumentID maxPrice:(NSString *)maxPrice minPrice:(NSString *)minPrice day:(NSString *)day lastPrice:(NSString *)lastPrice block:(DetailBlock)block{
    OneModel *model = [OneModel new];
    model.name = name;
    model.instrumentID = instrumentID;
    model.maxPrice = maxPrice;
    model.minPrice = minPrice;
    model.day = day;
    model.lastPrice = lastPrice;
    model.block = block;

    return model;
}

@end
