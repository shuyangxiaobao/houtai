//
//  OneCell.h
//  74.OC-demo
//
//  Created by xiaobao on 2019/6/4.
//  Copyright © 2019 122. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GQBOneCell : UITableViewCell

-(void)refreshUI:(NSString *)name;

-(void)refreshUI:(NSString *)name index:(int)index;

-(void)refreshUI:(NSString *)name index:(int)index rightTitle:(NSString *)rightTitle;

@end

NS_ASSUME_NONNULL_END
