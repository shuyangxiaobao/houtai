//
//  OneCell.m
//  74.OC-demo
//
//  Created by xiaobao on 2019/6/4.
//  Copyright © 2019 122. All rights reserved.
//

#import "GQBOneCell.h"

@interface GQBOneCell()
@property (nonatomic,strong) UILabel *indexLab;
@property (nonatomic,strong) UILabel *nameLab;
@property (nonatomic,strong) UILabel *rightLab;

@end

@implementation GQBOneCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}


-(void)createUI{
    _nameLab = [[UILabel alloc]init];
    _nameLab.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_nameLab];
    
    _rightLab = [[UILabel alloc]init];
    _rightLab.textAlignment = NSTextAlignmentRight;
    [self addSubview:_rightLab];
    
    _indexLab = [[UILabel alloc]init];
    _indexLab.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_indexLab];
    
    
    
    
//    NSLog(@"%@",NSStringFromCGRect(CGRectMake(0, 0, 0, 0 )));
}

-(void)layoutSubviews{
    [_indexLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(40);
    }];
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_indexLab.mas_right).offset(10);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    [_rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerX.centerY.mas_equalTo(0);
    }];
    
    
}


-(void)refreshUI:(NSString *)name{
    _nameLab.text = name;
}

-(void)refreshUI:(NSString *)name index:(int)index{
    _nameLab.text = name;
    _indexLab.text = [NSString stringWithFormat:@"%d",index];

}

-(void)refreshUI:(NSString *)name index:(int)index rightTitle:(NSString *)rightTitle{
    _nameLab.text = name;
    _rightLab.text = rightTitle;
    _indexLab.text = [NSString stringWithFormat:@"%d",index];
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc{
    NSLog(@"%s",__func__);
}

@end
