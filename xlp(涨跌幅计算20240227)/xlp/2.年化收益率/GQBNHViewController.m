#import "GQBNHViewController.h"
#import <MessageUI/MessageUI.h>
@interface GQBNHViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *originField;
@property (weak, nonatomic) IBOutlet UITextField *currentField;
@property (weak, nonatomic) IBOutlet UITextField *timeField;
@property (weak, nonatomic) IBOutlet UILabel *rateLab;
@property (weak, nonatomic) IBOutlet UITextField *buyTimefield;
@property (weak, nonatomic) IBOutlet UITextField *sellTimeField;
@property (weak, nonatomic) IBOutlet UIButton *addTimeButt;
@property (weak, nonatomic) IBOutlet UIButton *clearButt;

@property (nonatomic,strong)MFMailComposeViewController *mailComposer;

@end

@implementation GQBNHViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated{
    self.addTimeButt.backgroundColor = [UIColor blueColor];
    [self.addTimeButt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.addTimeButt.titleLabel.textColor = [UIColor whiteColor];
    self.addTimeButt.layer.cornerRadius = 5;

    self.clearButt.backgroundColor = [UIColor blueColor];
    [self.clearButt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.clearButt.titleLabel.textColor = [UIColor whiteColor];
    self.clearButt.layer.cornerRadius = 5;

}

- (IBAction)click:(UIButton *)sender {
    [self jisuan];
}
- (IBAction)clearClick:(UIButton *)sender {
    self.originField.text = @"";
    self.currentField.text = @"";
    self.timeField.text = @"";
    self.buyTimefield.text = @"";
    self.sellTimeField.text = @"";
    self.rateLab.text = @"";
    [self.originField becomeFirstResponder];
}


- (IBAction)addTimeClick:(id)sender {
    
    int day = [self.timeField.text intValue] +365;
    self.timeField.text = [NSString stringWithFormat:@"%d",day];
    
//    self.buyTimefield.text = @"";
    self.sellTimeField.text = @"";
    [self jisuan];

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self jisuan];
    [self.view endEditing:YES];

//    [self showMessageView:[NSArray arrayWithObjects:@"15626533741", nil] title:@"" body:@"这是测试用短信，勿回复！"];
//    [self sendemail];
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"sms://15338826129"]];

}

-(void)jisuan{

    float min = [self.originField.text doubleValue];
    float current = [self.currentField.text doubleValue];
    float result = 0;
    if (min < current) {
         result = (current/min -1)*100;
    } else{
        result = (1-current/min)*100;
    }
    NSString *buyTime = self.buyTimefield.text;
    NSString *sellTime = self.sellTimeField.text;
    
    
//    if(sellTime.length < 1){
//        NSDate *date = [NSDate date];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.locale = [NSLocale systemLocale];
//        [formatter setDateFormat:@"yyyy.MM.dd"];
//        NSString *dateStr = [formatter stringFromDate:date];
//        NSArray *arr = [dateStr componentsSeparatedByString:@"."];
//        sellTime = [NSString stringWithFormat:@"%@.%@",arr[1],arr[2]];
//        self.sellTimeField.text = sellTime;
//        [self jisuan];
//    }
    
   
    
    
    
    
    if(buyTime.length == 0 || sellTime.length == 0){
        buyTime = @"";
        sellTime = @"";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if ([buyTime containsString:@"."]){
        [formatter setDateFormat:@"yyyy.MM.dd"];
    }
    
    if([buyTime componentsSeparatedByString:@"."].count == 2){
        buyTime = [NSString stringWithFormat:@"2022.%@",buyTime];
        sellTime = [NSString stringWithFormat:@"2022.%@",sellTime];
    }
    NSDate *beginDate = [formatter dateFromString:buyTime];
    NSDate *endDate = [formatter dateFromString:sellTime];
    long beginDay = beginDate.timeIntervalSinceReferenceDate;
    long endDay = endDate.timeIntervalSinceReferenceDate;
    long n = (int) ((endDay - beginDay) / (3600 * 24));
    
    if (buyTime.length > 0 && sellTime.length > 0){
        self.timeField.text = [NSString stringWithFormat:@"%d",n];
    }
    int day = [self.timeField.text intValue];


    if (day < 1) {
        float result = 0;
        if (min < current) {
             result = (current/min -1)*100;
            _rateLab.text = [NSString stringWithFormat:@"涨%.2f%%",result];
        } else{
            result = (1-current/min)*100;
            _rateLab.text = [NSString stringWithFormat:@"跌 %.2f%%",result];
        }
        return;
    }
    
    double t1 = log2(result/100+1)*365;
    t1 = t1 - day;
    
    double end = (pow(result/100+1, 1/(day/365.0))-1)*100; // 年华收益
    NSString *title = [NSString stringWithFormat:@"%d天 上涨 %.0f%% 年化 %.0f%% %.0f天",day,result,end,t1];
    _rateLab.text = title;
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    [pab setString:title];
    
    float ttt1 = pow(2, 1.0/365.0);
    float ttt2 = pow(ttt1, 148);
    NSLog(@"%d",ttt2);
    
    
    
    
    
}
-(void)sendemail{
    if([MFMailComposeViewController canSendMail]){
        _mailComposer = [[MFMailComposeViewController alloc]init];
        _mailComposer.mailComposeDelegate = self;
        [_mailComposer setSubject:@"Test mail"];
//        [_mailComposer setCcRecipients:@[@"1379343323@qq.com"]];
        [_mailComposer setToRecipients:@[@"1379343323@qq.com"]];

        [_mailComposer setMessageBody:@"Testing message for the test mail" isHTML:NO];
        [self presentModalViewController:_mailComposer animated:YES];
    } else {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"提示信息" message:@"该设备不支持邮件功能" preferredStyle:UIAlertControllerStyleAlert];
      
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [actionSheet addAction:okAction];
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
   
}

#pragma mark - 发送短信方法
-(void)showMessageView:(NSArray *)phones title:(NSString *)title body:(NSString *)body
{
  if([MFMessageComposeViewController canSendText]){
    MFMessageComposeViewController * controller = [[MFMessageComposeViewController alloc] init];
    controller.recipients = phones;
    controller.navigationBar.tintColor = [UIColor redColor];
    controller.body = body;
    controller.messageComposeDelegate = self;
    [self presentViewController:controller animated:YES completion:nil];
    [[[[controller viewControllers] lastObject] navigationItem] setTitle:title];//修改短信界面标题
  } else {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息"
                            message:@"该设备不支持短信功能"
                            delegate:nil
                       cancelButtonTitle:@"确定"
                       otherButtonTitles:nil, nil];
    [alert show];
  }
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [self dismissViewControllerAnimated:YES completion:nil];
      switch (result) {
        case MessageComposeResultSent:
          //信息传送成功
              NSLog(@"信息传送成功");
          break;
        case MessageComposeResultFailed:
          //信息传送失败
              NSLog(@"信息传送失败");
          break;
        case MessageComposeResultCancelled:
          //信息被用户取消传送
              NSLog(@"信息被用户取消传送");
          break;
        default:
          break;
      }
}



-(void)mailComposeController:(MFMailComposeViewController *)controller
 didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
   if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissModalViewControllerAnimated:YES];

}


@end
