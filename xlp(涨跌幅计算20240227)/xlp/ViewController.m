//
//  ViewController.m
//  xlp
//
//  Created by xiaobao on 2021/10/29.
//

#import "ViewController.h"
#import "UIColor+QDColor.h"
#import "GQBOneCell.h"
#import "OneModel.h"
#import "GQBJSViewController.h"
#import "GQBNHViewController.h"
#import "GQBRootViewController.h"
#import "EncryViewController.h"
#import "GupiaoViewController.h"
#import "PositionViewController.h"
#import "JieTuViewController.h"
#import "NiuShuiViewController.h"





@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *data;

@property (nonatomic,strong) UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableview];
    
    
    NSDate* now = [NSDate date];
    NSTimeZone* beijingTimeZone = [NSTimeZone timeZoneWithName:@"beijingTimeZone"];
    NSTimeZone* shanghaiZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    NSInteger beijin = [beijingTimeZone secondsFromGMT];
    NSInteger shanhai = [shanghaiZone secondsFromGMT];
    
   
    
    NSTimeZone* localZone = [NSTimeZone defaultTimeZone];
    NSInteger local = [localZone secondsFromGMT];
   
    NSInteger hour = local/3600;
    NSInteger minute = (local-(hour*3600))/60;
    
    NSString *result = [NSString stringWithFormat:@"%02d:%02d",hour,minute];
    
    NSLog(@"23432");

    // Do any additional setup after loading the view.
}

-(void)switchClick:(UISwitch *)switchButt{
    if(switchButt.on){
        self.label.text = @"使用最低价";
        [GQBSettingmanager shareInstance].minestPrice = YES;
    } else {
        self.label.text = @"使用最新价";
        [GQBSettingmanager shareInstance].minestPrice = NO;

    }
}


-(void)initTableview{
    self.view.backgroundColor = [UIColor whiteColor];
    
    

    
    UISwitch *switchButt = [[UISwitch alloc] initWithFrame:CGRectZero];
    [self.view addSubview:switchButt];
    [switchButt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kScreen_Width/2-10);
        make.top.equalTo(@(91));
    }];
    [switchButt addTarget:self action:@selector(switchClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    
    UILabel *label = [UILabel createLabelWith:@"" font: QDFont(16) textColor:@"000000"];
    self.label = label;
    if([GQBSettingmanager shareInstance].minestPrice){
        self.label.text = @"使用最低价";
        switchButt.on = YES;
    } else {
        self.label.text = @"使用最新价";
        switchButt.on = NO;
    }
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kScreen_Width/2+10);
        make.centerY.mas_equalTo(switchButt.mas_centerY);
    }];
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor colorWithHexString:@"979797" alpha:0.4];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource =self;
    _tableView.scrollEnabled = YES;
    _tableView.separatorStyle = UITableViewCellSelectionStyleGray;
    _tableView.userInteractionEnabled = YES;
    _tableView.layer.masksToBounds = YES;
    _tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[GQBOneCell class] forCellReuseIdentifier:@"lasdjlfa"];
    
    


    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(switchButt.mas_bottom).offset(10);
        make.left.equalTo(@(0));
        make.right.equalTo(@(0));
        make.bottom.equalTo(@(0));
    }];
    
    self.data = [NSMutableArray array];
    
    MJWeakSelf
    if (1) {
        [self.data addObject:[[OneModel alloc]initWithName:@"涨跌计算" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            GQBJSViewController *vc = [[GQBJSViewController alloc]init];
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];
        [self.data addObject:[[OneModel alloc]initWithName:@"年化收益" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            GQBNHViewController *vc = [[GQBNHViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];
        [self.data addObject:[[OneModel alloc]initWithName:@"计算排名" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            GQBRootViewController *vc = [[GQBRootViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];

        [self.data addObject:[[OneModel alloc]initWithName:@"加密" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            EncryViewController *vc = [[EncryViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];
        [self.data addObject:[[OneModel alloc]initWithName:@"股票信息" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            GupiaoViewController *vc = [[GupiaoViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];

        [self.data addObject:[[OneModel alloc]initWithName:@"股票信息(清空数据)" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            [HUDUtil showDoneHUD:@"完成" duration:1.0];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0*NSEC_PER_SEC)), dispatch_get_global_queue(0, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    GupiaoViewController *vc = [[GupiaoViewController alloc]init];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                });
            });
        }]];
        
        [self.data addObject:[[OneModel alloc]initWithName:@"股票信息(清空数据) 55最佳版" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
            [HUDUtil showDoneHUD:@"完成" duration:1.0];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0*NSEC_PER_SEC)), dispatch_get_global_queue(0, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    GupiaoViewController *vc = [[GupiaoViewController alloc]initWithType:GupiaoType_55_best];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                });
            });
        }]];
        
        
        [self.data addObject:[[OneModel alloc]initWithName:@"持仓数据" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            PositionViewController *vc = [[PositionViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];
        [self.data addObject:[[OneModel alloc]initWithName:@"截图" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            JieTuViewController *vc = [[JieTuViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];
        
        [self.data addObject:[[OneModel alloc]initWithName:@"流水列表" instrumentID:@"      " maxPrice:@"xx.xx" minPrice:@"" day:@"" lastPrice:@"" block:^{
            NiuShuiViewController *vc = [[NiuShuiViewController alloc]init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }]];

     

    }

    [[GQBSettingmanager shareInstance].datas addObjectsFromArray:self.data];
    [[GQBSettingmanager shareInstance].datas removeObjectAtIndex:0];
    [[GQBSettingmanager shareInstance].datas removeObjectAtIndex:0];

    
    [self.tableView reloadData];
 
}



//MARK:-  UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    return 10;
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GQBOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lasdjlfa" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[GQBOneCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"lasdjlfa"];
    }
    NSLog(@"%p",cell);
    OneModel *model = self.data[indexPath.row];
    [cell refreshUI:model.name];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor colorWithHexString:@"888888"].CGColor;
    cell.backgroundColor = [UIColor whiteColor];
    if (model.select) {
        cell.backgroundColor = [UIColor grayColor];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OneModel *model = self.data[indexPath.row];
    if(model.block){
        model.block();
        return;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}




- (BOOL)tableView:(UITableView*)tableViewcanEditRowAtIndexPath:(NSIndexPath*)indexPath {
    
        return YES;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        return UITableViewCellEditingStyleDelete;
    
}

//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return @"删除";
//
//}



// 自定义左滑显示编辑按钮

- (NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除1" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        
                
    }];
    deleteAction.backgroundColor= [UIColor colorWithHexString:@"#fa514a"];
    return @[deleteAction];
}



-(void)dealloc{
    NSLog(@"%s",__func__);
}


@end

