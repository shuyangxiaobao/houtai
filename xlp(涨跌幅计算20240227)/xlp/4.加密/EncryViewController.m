//
//  EncryViewController.m
//  xlp
//
//  Created by xiaobao on 2022/9/15.
//

#import "EncryViewController.h"
#import <CommonCrypto/CommonDigest.h>

@interface EncryViewController ()
@property (weak, nonatomic) IBOutlet UITextField *field;
@property (weak, nonatomic) IBOutlet UILabel *resultLab;

@end

@implementation EncryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self jisuan];
}

-(void)jisuan{
    NSString *inputStr = self.field.text;
    NSString *start = @"哈萨克斯坦首都街头飘扬中国红";
    NSString *end = @"雷军卸任小米电子软件董事长";

    _resultLab.text = @"";
    if (inputStr.length > 0) {
        inputStr = [NSString stringWithFormat:@"%@%@%@",start,[inputStr lowercaseString],end];
        _resultLab.text = [self md5:inputStr];
    }
}


// 2022.9.15 版本
//-(void)jisuan{
//    NSString *inputStr = self.field.text;
//    NSString *start = @"哈萨克斯坦首都街头飘扬中国红";
//    NSString *end = @"雷军卸任小米电子软件董事长";
//
//    _resultLab.text = @"";
//    if (inputStr.length > 0) {
//        inputStr = [NSString stringWithFormat:@"%@%@%@",start,[inputStr lowercaseString],end];
//        _resultLab.text = [self md5:inputStr];
//    }
//}

// 2022.9.15 版本
//-(void)jisuan{
//    NSString *inputStr = self.field.text;
//    NSString *start = @"哈萨克斯坦首都街头飘扬中国红";
//    NSString *end = @"雷军卸任小米电子软件董事长";
//
//    _resultLab.text = @"";
//    if (inputStr.length > 0) {
//        inputStr = [NSString stringWithFormat:@"%@%@%@",start,[inputStr lowercaseString],end];
//        _resultLab.text = [self md5:inputStr];
//    }
//}

// 2022.9.15 版本
//-(void)jisuan{
//    NSString *inputStr = self.field.text;
//    NSString *start = @"哈萨克斯坦首都街头飘扬中国红";
//    NSString *end = @"雷军卸任小米电子软件董事长";
//
//    _resultLab.text = @"";
//    if (inputStr.length > 0) {
//        inputStr = [NSString stringWithFormat:@"%@%@%@",start,[inputStr lowercaseString],end];
//        _resultLab.text = [self md5:inputStr];
//    }
//}


// 2022.9.15 版本
//-(void)jisuan{
//    NSString *inputStr = self.field.text;
//    NSString *start = @"哈萨克斯坦首都街头飘扬中国红";
//    NSString *end = @"雷军卸任小米电子软件董事长";
//
//    _resultLab.text = @"";
//    if (inputStr.length > 0) {
//        inputStr = [NSString stringWithFormat:@"%@%@%@",start,[inputStr lowercaseString],end];
//        _resultLab.text = [self md5:inputStr];
//    }
//}

-(NSString *)md5:(NSString *)inputText
{
    if (inputText==nil || [inputText length]==0) {
        return nil;
    }
    const char *value = [inputText UTF8String];
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger count=0; count<CC_MD5_DIGEST_LENGTH; count++) {
        [outputString appendFormat:@"%02x", outputBuffer[count]];
    }
    return [outputString lowercaseString];
}



@end
