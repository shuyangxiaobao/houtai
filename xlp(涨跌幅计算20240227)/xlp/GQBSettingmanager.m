//
//  Settingmanager.m
//  xlp
//
//  Created by xiaobao on 2021/10/29.
//

#import "GQBSettingmanager.h"

@implementation GQBSettingmanager
static GQBSettingmanager *instance = nil;

//懒汉式
+(GQBSettingmanager *)shareInstance{
    if (instance == nil) {// 防止频繁加锁
        @synchronized(self) {
            if (instance == nil) { // 防止创建多次
                instance = [[self alloc] init];
            }
        }
    }
    return instance;
}


- (id)init {
    self = [super init];
    if(self) {
        //初始化配制功能
        [self initDefaultData];
    }
    return self;
    
}

- (void)initDefaultData {
    self.datas = [NSMutableArray arrayWithCapacity:10];
    _satisArr = [NSMutableArray array];
    _instrumentIDDatas = [NSMutableArray array];
    _positionInfoDic = [NSMutableDictionary dictionary];
}


#pragma mark - 根据股票id 返回带市场标识的id
-(NSString *)returnMarketID:(NSString *)instrumentID{
    NSString *head3 = [instrumentID substringToIndex:3];
    NSString *head2 = [instrumentID substringToIndex:2];
    NSString *result = @"";
    
    
    if([head3 isEqualToString:@"600"] ||
       [head3 isEqualToString:@"601"] ||
       [head3 isEqualToString:@"603"] ||
       [head3 isEqualToString:@"605"] ||
       [head3 isEqualToString:@"688"]
       ){
        result = [NSString stringWithFormat:@"sh%@",instrumentID];
    } else if([head2 isEqualToString:@"00"] || [head3 isEqualToString:@"300"]){
        result = [NSString stringWithFormat:@"sz%@",instrumentID];
    } else {
        NSLog(@"怪胎: %@",instrumentID);
    }
    
//
//
//
//    if([head3 isEqualToString:@"600"] ||
//       [head3 isEqualToString:@"601"] ||
//       [head3 isEqualToString:@"603"] ||
//       [head3 isEqualToString:@"605"] ||
//       [head3 isEqualToString:@"688"]
//       ){
//        result = [NSString stringWithFormat:@"sh%@",instrumentID];
//    } else if ([head2 isEqualToString:@"00"]){
//        result = [NSString stringWithFormat:@"sz%@",instrumentID];
//    } else if ([head3 isEqualToString:@"300"]){
//        result = [NSString stringWithFormat:@"sz%@",instrumentID];
//    } else {
//        NSLog(@"股票id异常");
//        NSMutableArray *arr = [NSMutableArray array];
//        [arr addObject:nil];
//    }
    return result;
    
}



@end
